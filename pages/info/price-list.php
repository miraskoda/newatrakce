<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 12.02.2018
 * Time: 19:48
 */
use backend\controllers\CustomerController;
use backend\models\Url;

$pageId = 27;

require_once __DIR__ . "/../../backend/modules/app/prepare.php";

$customer = CustomerController::isLoggedCustomer();

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="public">
<?php

//public header - categories, pages, cart (search)
require_once Url::getBackendPathTo("/modules/page-parts/header.php");

echo '<div class="container-fluid">';

require_once Url::getBackendPathTo("/modules/page-parts/price-list/price-list-price.php");

require_once Url::getBackendPathTo("/modules/page-parts/footer.php");

echo '</div>';
echo '<div id="overlay"></div>';

echo '<script>';
require_once Url::getPathTo("/js/cart/add-to-cart.js");
require_once Url::getPathTo("/js/price-list/price-list-init.min.js");
echo '</script>';

require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

// modals
//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
?>
</body>
</html>