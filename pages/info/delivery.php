<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 10.02.2018
 * Time: 16:39
 */
use backend\controllers\CustomerController;
use backend\models\Url;

$pageId = 33;

require_once __DIR__ . "/../../backend/modules/app/prepare.php";

$customer = CustomerController::isLoggedCustomer();

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="public">
<?php

//public header - categories, pages, cart (search)
require_once Url::getBackendPathTo("/modules/page-parts/header.php");

echo '<div class="container-fluid">';
require_once Url::getBackendPathTo("/modules/page-parts/editable-content.php");

require_once Url::getBackendPathTo("/modules/page-parts/price-list/price-list-delivery.php");

require_once Url::getBackendPathTo("/modules/page-parts/footer.php");

echo '</div>';
echo '<div id="overlay"></div>';

echo '<script>';
require_once Url::getPathTo("/js/save-editable-content.min.js");
echo '</script>';

require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

// modals
//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
?>
</body>
</html>