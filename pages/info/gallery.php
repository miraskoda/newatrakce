<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 20.04.2018
 * Time: 16:08
 */

use backend\controllers\CustomerController;
use backend\models\Url;

$pageId = 44;

require_once __DIR__ . "/../../backend/modules/app/prepare.php";

$customer = CustomerController::isLoggedCustomer();

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="public" class="photo-gallery">
<?php

//public header - categories, pages, cart (search)
require_once Url::getBackendPathTo("/modules/page-parts/header.php");

echo '<div class="container-fluid">';

require_once Url::getBackendPathTo("/modules/page-parts/editable-content.php");


require_once Url::getBackendPathTo("/modules/page-parts/gallery-content.php");

require_once Url::getBackendPathTo("/modules/page-parts/footer.php");

echo '</div>';
echo '<div id="overlay"></div>';

echo '<script>';
require_once Url::getPathTo("/js/gallery.js");
require_once Url::getPathTo("/js/save-editable-content.min.js");


echo '</script>';

require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

// modals
//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
?>
</body>
</html>