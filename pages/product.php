<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 23.07.2017
 * Time: 21:46
 */

use backend\controllers\CustomerController;
use backend\models\Url;

$pageId = 11;

require_once __DIR__ . "/../backend/modules/app/prepare.php";

$customer = CustomerController::isLoggedCustomer();

// loads category data to $category variable
require_once Url::getBackendPathTo("/modules/page-parts/product/product-loader.php");

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="public">
<?php

//public header - categories, pages, cart (search)
require_once Url::getBackendPathTo("/modules/page-parts/header.php");

echo '<div class="container-fluid">';

require_once Url::getBackendPathTo("/modules/page-parts/product/product-content.php");
require_once Url::getBackendPathTo("/modules/page-parts/footer.php");

echo '</div>';
echo '<div id="overlay"></div>';

echo '<!-- Lightbox2 by Lokesh Dhakar -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/css/lightbox.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/js/lightbox.min.js"></script>';

echo '<script>';
echo 'lightbox.option({
      \'albumLabel\': "Fotografie %1 z %2",
      \'wrapAround\': true,
      \'alwaysShowNavOnTouchDevices\': true
    });';
require_once Url::getPathTo("/js/product/image-swap.min.js");
require_once Url::getPathTo("/js/cart/add-to-cart.js");
require_once Url::getPathTo("/js/cart/administration/cart-dates-save.js");
require_once Url::getPathTo("/js/product/check-availability.js");
echo '</script>';

require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

// modals
require_once Url::getBackendPathTo("/modules/modals/cart-action.php");
require_once Url::getBackendPathTo("/modules/modals/reservation.php");
require_once Url::getBackendPathTo("/modules/modals/login-modal.php");
require_once Url::getBackendPathTo("/modules/modals/reservation-success.php");
require_once Url::getBackendPathTo("/modules/modals/user-not-logged.php");
//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
?>
</body>
</html>
