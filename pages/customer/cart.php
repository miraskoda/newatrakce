<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 17.08.2017
 * Time: 10:52
 */

use backend\controllers\CustomerController;
use backend\models\Url;

$pageId = 17;

require_once __DIR__ . "/../../backend/modules/app/prepare.php";

//before any action check user and redirect to administration if needed
$customer = CustomerController::isLoggedCustomer();
/*if(!$customer){
    header('Location: /prihlaseni/', true);
    die();
}*/

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="public">
<?php

//public header - categories, pages, cart (search)
require_once Url::getBackendPathTo("/modules/page-parts/header.php");
//require_once Url::getBackendPathTo("/modules/page-parts/customer/navigation.php");

echo '<div class="container-fluid">
            <div class="cart">';
require_once Url::getBackendPathTo("/modules/page-parts/customer/cart/cart-content.php");
echo '</div>';
require_once Url::getBackendPathTo("/modules/page-parts/footer.php");
echo '</div>';
echo '<div id="overlay"></div>';

echo '<script>';
require_once Url::getPathTo("/js/cart/administration/cart-list-loader.js");
require_once Url::getPathTo("/js/cart/administration/cart-dates-loader.js");
require_once Url::getPathTo("/js/cart/administration/cart-dates-counter.js");
require_once Url::getPathTo("/js/cart/administration/cart-price-loader.min.js");
require_once Url::getPathTo("/js/cart/administration/cart-quantity-save.min.js");
require_once Url::getPathTo("/js/cart/administration/cart-dates-save.js");
require_once Url::getPathTo("/js/cart/price-info-modal.min.js");
require_once Url::getPathTo("/js/cart/validate-cart.min.js");
require_once Url::getPathTo("/js/admin/navigation-drawer.js");
echo '</script>';
require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

// modals
require_once Url::getBackendPathTo("/modules/modals/price-info.php");
require_once Url::getBackendPathTo("/modules/modals/cart-additional.php");

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
?>
</body>
</html>
