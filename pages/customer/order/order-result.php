<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 14.09.2017
 * Time: 18:39
 */

use backend\controllers\CustomerController;
use backend\controllers\OrderController;
use backend\models\Order;
use backend\models\Url;

$pageId = 21;

require_once __DIR__ . "/../../../backend/modules/app/prepare.php";

//before any action check user and redirect to administration if needed
$customer = CustomerController::isLoggedCustomer();
if(!$customer) {
    header('Location: /prihlaseni/?ret=objednavka', true);
    die();
}

// get draft
$order = OrderController::getDraftOrder();

// then validate a finish order
$orderResult = OrderController::finishOrder();

// if everything successful -> load finished order
if($orderResult === true && is_a($order, Order::class)){
    $order->setType(Order::TYPE_FINISHED);
    $order = $order->load();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="public">
<?php

//public header - categories, pages, cart (search)
require_once Url::getBackendPathTo("/modules/page-parts/header.php");
//require_once Url::getBackendPathTo("/modules/page-parts/customer/order/detail/detail-loader.php");

echo '<div class="container-fluid">
            <div class="cart order summary">';
require_once Url::getBackendPathTo("/modules/page-parts/customer/order/finish/finish-content.php");
echo '</div>';
//require_once Url::getBackendPathTo("/modules/page-parts/footer.php");
echo '</div>';
echo '<div id="overlay"></div>';

echo '<script>';
require_once Url::getPathTo("/js/customer/order/finish-order.min.js");
echo '</script>';
require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
?>
</body>
</html>