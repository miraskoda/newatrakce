<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 21.09.2017
 * Time: 20:05
 */

use backend\controllers\CustomerController;
use backend\controllers\OrderController;
use backend\controllers\UserController;
use backend\models\Address;
use backend\models\CustomCurrency;
use backend\models\Customer;
use backend\models\Order;
use backend\models\OrderProduct;
use backend\models\OrderTerm;
use backend\models\Payment;
use backend\models\PaymentType;
use backend\models\Url;

require_once __DIR__ . "/../../../../backend/modules/app/prepare.php";

require_once '../../../../backend/plugins/mpdf/vendor/autoload.php';

$orderId = 0;
if(isset($_GET['orderId']) && is_numeric($_GET['orderId']) && $_GET['orderId'] > 0)
    $orderId = $_GET['orderId'];

$order = new Order($orderId);
$order = $order->load();
if(!is_a($order, Order::class)) {
    die('Objednávka s daným ID neexistuje. Prosím kontaktujte zákaznickou podporu.');
}



if(!UserController::isLoggedUser() && $order->getCustomerId() != $customer->getCustomerId()) {
    die('Přístup odepřen.');
}

$orderNo = DateTime::createFromFormat('Y-m-d H:i:s', $order->getChangedRaw())->format('y') . $order->getFormattedOrderId();

$depositInvoiceNo = DateTime::createFromFormat('Y-m-d H:i:s', $order->getChangedRaw())->format('y') . $order->getOrderId() . '03';


$css = file_get_contents(__DIR__ . "/../../../../styles/file-style.css");

$stringifiedRentHours = OrderTerm::getRentHoursAsStrings($order);
$products = OrderProduct::getOrderProducts($order);


$arr = json_decode(\backend\controllers\OrderController::getGoogleDistanceApiData($order->getVenueStreet(),$order->getVenueCity(), $order->getVenueZip()),true);

$distance = $arr["rows"]["0"]["elements"]["0"]["distance"]["text"];
$duration = $arr["rows"]["0"]["elements"]["0"]["duration"]["text"];
$durationValue = $arr["rows"]["0"]["elements"]["0"]["duration"]["value"];

$invoiceAddress = new Address();
$invoiceAddress->setOrderId($order->getOrderId());
$invoiceAddress->setAddressType(Address::ADDRESS_INVOICE);
$invoiceAddress = $invoiceAddress->load();

$payment = new Payment($order->getPaymentId());
$payment = $payment->load();
$paymentType = new PaymentType($order->getPaymentTypeId());
$paymentType = $paymentType->load();

$terms = OrderTerm::getOrderTermsByOrder($order);
$numOfTerms = count(array_filter($stringifiedRentHours, function ($x) {
    return ($x > 0);
}));
if ($numOfTerms < 1)
    $numOfTerms = 1;
else if ($numOfTerms > 5)
    $numOfTerms = 5;


// typ faktury: propozice
$invoiceType = 'propozice';


$printHtml = false;

try {
    set_error_handler(function() {});

    $venueData = OrderController::getVenueCountedDataByOrder($order);

    restore_error_handler();
} catch (Exception $e) {
    if($printHtml) {
        echo 'Venue load error: ' . $e->getMessage();
    }
}

// html start
$html = '<h2 class="col-6 left">Propozice č. P' . $orderNo . '</h2>';

$html .= '<h2 class="col-6 right">OnlineAtrakce.cz</h2>';

$html .= '<table class="border-around smaller-font">';



// names
$html .= '<tr class="padding-around">
            <td class="first bold">Dodavatel:</td>
            <td class="last">Josef Rybár</td>
            <td class="first bold vert-line">Odběratel:</td>
            <td class="last">' . $invoiceAddress->getName() . '</td>
          </tr>';

// addresses
$html .= '<tr class="padding-around">
            <td class="first bold">Adresa:</td>
            <td class="last">Syrovátka 11<br>Lhota pod Libčany 503 27</td>
            <td class="first bold vert-line">Adresa:</td>
            <td class="last">' . $invoiceAddress->getStreet() . '<br>' . $invoiceAddress->getCity() . ' ' . substr($invoiceAddress->getZip(), 0, 3) . ' ' . substr($invoiceAddress->getZip(), 3) . '</td>
          </tr>';

// ičo
$html .= '<tr class="padding-around">
            <td class="first bold">IČO:</td>
            <td class="last">627 23 499</td>' .
    (($order->getInvoiceIco() !== null && is_numeric($order->getInvoiceIco()) && $order->getInvoiceIco() > 0) ? '<td class="first bold vert-line">IČO:</td><td class="last">' . $order->getInvoiceIco() . '</td>' : '<td colspan="2" class="first vert-line"></td>')
    . '</tr>';

// dič
$html .= '<tr class="padding-around">
            <td class="first bold padding-bottom">DIČ:</td>
            <td class="padding-bottom last">CZ7605093111</td>' .
    (($order->getInvoiceDic() !== null && strlen($order->getInvoiceDic()) > 0) ? '<td class="first bold vert-line padding-bottom">DIČ:</td><td class="last padding-bottom">' . $order->getInvoiceDic() . '</td>' : '<td colspan="2" class="first vert-line"></td>')
    . '</tr>';

$html .= '<tr class="line"><td colspan="4"></td></tr>';



// payment
$html .= '<tr class="padding-around">
            <td class="first bold">Forma úhrady:</td>
            <td class="last">' . $payment->getName() . '</td>
            
            <td class="first bold vert-line">Kontaktní osoba:</td>
            <td class="last">' . $order->getContactName() . '</td>
          </tr>';

$html .= '<tr class="padding-around">
            ' . (($payment->getPaymentId() == 2) ? '<td class="first bold">Číslo účtu:</td><td class="last">184218759 / 0300</td>' : '<td colspan="2"></td>') . '
            <td class="first bold vert-line">Telefon:</td>
            <td class="last">' . $order->getContactPhone() . '</td>
          </tr>';

$html .= '<tr class="padding-around">
            ' . (($payment->getPaymentId() == 2) ? '<td class="first bold">Konstantní symbol:</td><td class="last">308</td>' : '<td colspan="2" class="padding-bottom"></td>') . '
            <td class="first bold vert-line padding-bottom">Místo konání:</td>
            <td class="last padding-bottom">' . $order->getVenueStreet() . '<br>' . $order->getVenueCity() . ' ' . substr($order->getVenueZip(), 0, 3) . ' ' . substr($order->getVenueZip(), 3) . '</td>
          </tr>';

if ($payment->getPaymentId() == 2) {
    $html .= '<tr class="padding-around">
            <td class="first bold padding-bottom">Variabilní symbol:</td><td class="last padding-bottom">' . $orderNo . '</td><td colspan="2" class="first vert-line last padding-bottom"></td>
          </tr>';
}

$html .= '<tr class="line"><td colspan="4"></td></tr>';


// terms
$html .= '</table>';

$html .= '<table class="border-around">';

$html .= '<thead>
            <tr class="padding-around">
                <td></td>
                <td class="center bold">Začátek</td>
                <td class="center bold">Konec</td>
                <td class="center bold last">Počet hodin</td>
            </tr>
          </thead>';


foreach ($terms as $index => $term) {
    $html .= '<tr class="padding-around">
                <td class="first bold ' . (($index + 1 == count($terms)) ? 'padding-bottom' : '') . '">' . ($index + 1) . '. termín</td>
                <td class="center ' . (($index + 1 == count($terms)) ? 'padding-bottom' : '') . '">' . $term->getStart() . '</td>
                <td class="center ' . (($index + 1 == count($terms)) ? 'padding-bottom' : '') . '">' . $term->getEnd() . '</td>
                <td class="center last">' . $term->getHours() . '</td>
              </tr>';
}

$html .= '</table>';

// products
$html .= '<table class="border-around">';

$html .= '<thead>
            <tr class="padding-around">
                <td class="center bold first">Název</td>
                <td class="center bold first last">Množství</td>
                <td class="center bold first last">Jednotková cena</td>
                <td class="center bold first last">Sleva</td>
                <td class="center bold first last">Celkem</td>
            </tr>
            <tr class="line-with-spaces"><td colspan="5"></td></tr>
          </thead>';

$discount = $order->getDiscount();

    foreach ($products as $index => $product) {
        // only for long output testing
        /*for ($i = 0; $i < 30; $i++) {
            $product = $products[0];*/

        $sumPrice = 0;
        foreach ($stringifiedRentHours as $rentHour) {
            if ($rentHour > 0) {
                $sumPrice += (($rentHour <= 5) ?
                    (($product['price']) * $product['quantity'])
                    :
                    ((($product['price']) + (($product['price'] / 100) * (5 * ($rentHour - (($rentHour > 5) ? 5 : $rentHour))))) * $product['quantity'])
                );
            }
        }

        $html .= '<tr class="padding-around">
                <td class="left bold first">' . $product['name'] . '</td>
                <td class="right first last">' . $product['quantity'] . ' ks</td>
                <td class="right first last">' . CustomCurrency::setDecimals(round($product['price'])) . '</td>
                <td class="right first last">' . $discount . '%</td>
                <td class="right first last">' . CustomCurrency::setDecimals(round($sumPrice)) . '</td>
              </tr>';

        if ($numOfTerms > 1) {
            $counter = 1;
            foreach ($stringifiedRentHours as $rentHour) {
                if ($rentHour > 0) {
                    $html .= '<tr class="padding-around"><td class="right bold">' . $counter . '. termín</td><td colspan="3"></td><td class="right first last">' .
                        (($rentHour <= 5) ?
                            CustomCurrency::setDecimals(round(($product['price']) * $product['quantity']))
                            :
                            CustomCurrency::setDecimals(round((($product['price']) + (($product['price'] / 100) * (5 * ($rentHour - (($rentHour > 5) ? 5 : $rentHour))))) * $product['quantity']))
                        )
                        . '</td></tr>';
                    $counter++;
                }
            }
            $html .= '<tr class="line-with-spaces"><td colspan="5"></td></tr>';
        }
    }


$html .= '<tr class="padding-around">
            <td class="left bold first padding-bottom">Doprava</td>
            <td class="right first last padding-bottom">' . (($order->getDeliveryPaymentPrice() > 0) ? CustomCurrency::setDecimals($order->getDeliveryPaymentPrice() / 6) : 0) . ' km</td>
            <td class="right first last padding-bottom">' . CustomCurrency::setDecimals(6) . '</td>
            <td class="right first last padding-bottom">' . (($order->getDeliveryPaymentPrice() > 0) ? '0%' : '100%') . '</td>
            <td class="right first last padding-bottom">' . CustomCurrency::setDecimals($order->getDeliveryPaymentPrice()) . '</td>
          </tr>';

$html .= '</table>';




$html .= '
<table class="border-around">
<tbody>
<tr class="padding-around">
<td class="first" colspan="3" style="width: 68px;"> <b>Místo akce, přesná adresa:</b> ' . $order->getVenueStreet() . ', ' . $order->getVenueCity() . ', ' . substr($order->getVenueZip(), 0, 3) . ' ' . substr($order->getVenueZip(), 3) . '</td>
</tr>
<tr class="line"><td colspan="3"></td></tr>
<tr class="padding-around">
<td class="first" style="width: 68px;">Odjezd na akci: </td>
<td style="width: 70px;"> '.$term->TimeArrived($durationValue,60,15).' </td>
<td style="width: 195px;"></td>
</tr>
<tr class="padding-around">
<td class="first" style="width: 68px;">Příjezd na akci: </td>
<td style="width: 70px;"> '. $term->getStartWithoutDayMinusOne(60) .' </td>
<td rowspan="13"  style="width: 195px;">

<img style="margin-bottom: 10px" width="350px"  src="

https://maps.googleapis.com/maps/api/staticmap?center='.$order->getVenueStreet().', '.$order->getVenueCity().'&zoom=17&size=600x300&maptype=roadmap&key=AIzaSyCSt9cx0xT32owIpz1xiaDaw8kLvk1jdw8&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C'.$order->getVenueStreet().','.$order->getVenueCity().'
"/>


<img width="350px"  src="

https://maps.googleapis.com/maps/api/staticmap?center='.$order->getVenueStreet().', '.$order->getVenueCity().'&zoom=14&size=600x300&maptype=roadmap&key=AIzaSyCSt9cx0xT32owIpz1xiaDaw8kLvk1jdw8&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C'.$order->getVenueStreet().','.$order->getVenueCity().'
"/> 



</td>
</tr>
<tr class="padding-around">
<td class="first" style="width: 68px;">Atrakce připravená k provozu: </td>
<td style="width: 70px;"> ' . $term->getStartWithoutDay() . ' </td>
</tr>
<tr class="padding-around">
<td class="first" style="width: 68px;">Délka produkce: </td>
<td style="width: 70px;"> ' . $term->getHours() . ' h </td>
</tr>
<tr class="padding-around">
<td class="first" style="width: 68px;">Mobil: </td>
<td style="width: 70px;">'.$order->getContactPhone().'</td>
</tr>
<tr class="padding-around">
<td class="first" style="width: 68px;">Kontaktní osoba: </td>
<td style="width: 70px;"> '.$order->getContactName().' </td>
</tr>
<tr class="padding-around">
<td class="first" style="width: 68px;">Vzdálenost ze Syrovátky: </td>
<td style="width: 70px;"> '.$distance.' </td>
</tr>
<tr class="padding-around">
<td class="first" style="width: 68px;">Platba hotově: </td>
<td style="width: 70px;">'. ($payment -> getPaymentId() == 3 ? "ANO" : "NE") .'</td>
</tr>
<tr class="padding-around">
<td class="first" style="width: 68px;">Kontakt sklad pro nakládku: </td>
<td style="width: 70px;">Tomáš Vrba</td>
</tr>
<tr class="padding-around">
<td class="first" style="width: 68px;">Kontakt sklad mobil: </td>
<td style="width: 70px;">774965276</td>
</tr>
<tr class="padding-around">
<td class="first" style="width: 68px;">Auto: </td>
<td style="width: 70px;">Ano / Ne</td>
</tr>
<tr class="padding-around">
<td class="first" style="width: 68px;">Poznámka: </td>
<td style="width: 70px;">'.$order -> getAdminNote().'</td>
</tr>
<tr class="padding-around">
<td class="first" style="width: 68px;">&nbsp;</td>
<td style="width: 70px;">&nbsp;</td>
</tr>
<tr class="padding-around">
<td class="first" style="width: 68px;">Vozík: </td>
<td style="width: 70px;">Ano / Ne</td>
</tr>
<tr class="line"><td colspan="3"></td></tr>

</tbody>
</table>




';



$html .= '</table>';

$html .= '<p class="registration-note">Registrace: Ž.l. vydal MM v HK Č.j.: HK-008579.7-FL</p>';
//$html .= '<p class="eet-codes">FIK:</p>';
//$html .= '<p class="eet-codes">BKP:</p>';
$html .= '<h2 class="center thanks">Děkujeme za Váš nákup a těšíme se na další spolupráci!</h2>';
$html .= '<h2 class="center thanks-smaller">OnlineAtrakce.cz</h2>';




$pdf = new mPDF('', '', 0, '', 8, 8, 5, 12, 5, 5);

if($printHtml == true) {
    echo '<style>';
    echo $css;
    echo '</style>';

    echo $html;
} else {
	    $pdf->SetTitle('OnlineAtrakce.cz - propozice ' . $orderNo);
    $pdf->SetHTMLFooter('<p style="font-family: \'Comfortaa\', cursive; font-size: 0.9rem">Strana {PAGENO} z {nbpg}</p>');
    $pdf->WriteHTML($css, 1);
    $pdf->WriteHTML($html, 2);
    $pdf->Output('OnlineAtrakce.cz-pr' . $orderNo . '.pdf', 'I');

}