<?php
/**
 * Created by PhpStorm.
 * User: miraaaa
 * Date: 10.12.2019
 * Time: 20:05
 */

use backend\controllers\CustomerController;
use backend\controllers\InvoiceCreditController;
use backend\controllers\OrderController;
use backend\controllers\UserController;
use backend\models\Address;
use backend\models\CustomCurrency;
use backend\models\Customer;
use backend\models\Order;
use backend\models\Payment;
use backend\models\PaymentType;

require_once __DIR__ . "/../../../../backend/modules/app/prepare.php";

require_once '../../../../backend/plugins/mpdf/vendor/autoload.php';


const INVOICE_FINAL = 'faktura';
const INVOICE_DOBROPIS = 'dobropis';



$Id = $_GET['orderId'];
if($_GET['invoiceType'] == INVOICE_FINAL){
   $orderId = InvoiceCreditController::getOrderIDfromId($Id);
    $inv = InvoiceCreditController::getInvoiceFromId($Id)[0];

}else{
    $orderId = InvoiceCreditController::getOrderIDfromIdInvoice($Id);
    $inv = InvoiceCreditController::getInvoiceFromIdInvoice($Id)[0];
}

$order = new Order($orderId);
$order = $order->load();


$customer = CustomerController::isLoggedCustomer();
if(!is_a($customer, Customer::class)) {
    if(!isset($_GET['customerId'])) {
        $customer = new Customer();
    } else {
        $customer = new Customer($_GET['customerId']);
        $customer = $customer->load();
    }
}

if(!UserController::isLoggedUser() && $order->getCustomerId() != $customer->getCustomerId()) {
    die('Přístup odepřen.');
}



    $invoiceType = $_GET['invoiceType'];


    $css = file_get_contents(__DIR__ . "/../../../../styles/file-style.min.css");

    $invoiceAddress = new Address();
    $invoiceAddress->setOrderId($order->getOrderId());
    $invoiceAddress->setAddressType(Address::ADDRESS_INVOICE);
    $invoiceAddress = $invoiceAddress->load();

    $payment = new Payment($order->getPaymentId());
    $payment = $payment->load();
    $paymentType = new PaymentType($order->getPaymentTypeId());
    $paymentType = $paymentType->load();

$dph = intval($inv["wVAT"])-intval($inv["woVAT"]);
    $printHtml = false;

    try {
        set_error_handler(function () {
        });

        $venueData = OrderController::getVenueCountedDataByOrder($order);

        restore_error_handler();
    } catch (Exception $e) {
        if ($printHtml) {
            echo 'Venue load error: ' . $e->getMessage();
        }
    }

// html start
 if ($invoiceType == INVOICE_DOBROPIS) {
        // maybe some new counter for new dobropis
        $html = '<h2 class="col-6 left">Dobropis č. ' . $Id . '</h2>';
    } else {
        $html = '<h2 class="col-6 left">Faktura č. ' . $Id . ' - daňový doklad</h2>';
    }
    $html .= '<h2 class="col-6 right">OnlineAtrakce.cz</h2>';



    $html .= '<table class="border-around smaller-font">';

// names
    $html .= '<tr class="padding-around">
            <td class="first bold">Dodavatel:</td>
            <td class="last">Josef Rybár</td>
            <td class="first bold vert-line">Odběratel:</td>
            <td class="last">' . $invoiceAddress->getName() . '</td>
          </tr>';

// addresses
    $html .= '<tr class="padding-around">
            <td class="first bold">Adresa:</td>
            <td class="last">Syrovátka 11<br>Lhota pod Libčany 503 27</td>
            <td class="first bold vert-line">Adresa:</td>
            <td class="last">' . $invoiceAddress->getStreet() . '<br>' . $invoiceAddress->getCity() . ' ' . substr($invoiceAddress->getZip(), 0, 3) . ' ' . substr($invoiceAddress->getZip(), 3) . '</td>
          </tr>';

// ičo
    $html .= '<tr class="padding-around">
            <td class="first bold">IČO:</td>
            <td class="last">627 23 499</td>' .
        (($order->getInvoiceIco() !== null && is_numeric($order->getInvoiceIco()) && $order->getInvoiceIco() > 0) ? '<td class="first bold vert-line">IČO:</td><td class="last">' . $order->getInvoiceIco() . '</td>' : '<td colspan="2" class="first vert-line"></td>')
        . '</tr>';

// dič
    $html .= '<tr class="padding-around">
            <td class="first bold padding-bottom">DIČ:</td>
            <td class="padding-bottom last">CZ7605093111</td>' .
        (($order->getInvoiceDic() !== null && strlen($order->getInvoiceDic()) > 0) ? '<td class="first bold vert-line padding-bottom">DIČ:</td><td class="last padding-bottom">' . $order->getInvoiceDic() . '</td>' : '<td colspan="2" class="first vert-line"></td>')
        . '</tr>';

    $html .= '<tr class="line"><td colspan="4"></td></tr>';

// payment
    $html .= '<tr class="padding-around">
            <td class="first bold">Forma úhrady:</td>
            <td class="last"> Hotově/Účet </td>
            
            <td class="first bold vert-line">Kontaktní osoba:</td>
            <td class="last">' . $order->getContactName() . '</td>
          </tr>';

    $html .= '<tr class="padding-around">
            ' . (($payment->getPaymentId() == 2) ? '<td class="first bold">Číslo účtu:</td><td class="last">184218759 / 0300</td>' : '<td colspan="2"></td>') . '
            <td class="first bold vert-line">Telefon:</td>
            <td class="last">' . $order->getContactPhone() . '</td>
          </tr>';

    $html .= '<tr class="padding-around">
            ' . (($payment->getPaymentId() == 2) ? '<td class="first bold">Konstantní symbol:</td><td class="last">308</td>' : '<td colspan="2" class="padding-bottom"></td>') . '
            <td class="first bold vert-line padding-bottom">Místo konání:</td>
            <td class="last padding-bottom">' . $order->getVenueStreet() . '<br>' . $order->getVenueCity() . ' ' . substr($order->getVenueZip(), 0, 3) . ' ' . substr($order->getVenueZip(), 3) . '</td>
          </tr>';

    if ($payment->getPaymentId() == 2) {
        $html .= '<tr class="padding-around">
            <td class="first bold padding-bottom">Variabilní symbol:</td><td class="last padding-bottom">' . (($invoiceType == INVOICE_FINAL) ? 'final' : $orderId) . '</td><td colspan="2" class="first vert-line last padding-bottom"></td>
          </tr>';
    }

    $html .= '<tr class="line"><td colspan="4"></td></tr>';

// pay date
    $html .= '<tr class="padding-around">
            <td class="first bold">Datum vystavení:</td>
            <td class="last">' . $order->getChangedDate() . '</td>
            <td class="first bold vert-line">Datum usk. zdan. plnění:</td>
            <td class="last">' . $order->getChangedDate() . '</td>
          </tr>';
    $html .= '<tr class="padding-around">
            <td class="first bold padding-bottom">Datum splatnosti:</td>
            <td class="last padding-bottom">' . $order->getPayDate() . '</td>
            <td colspan="2" class="first vert-line last padding-bottom"></td>
          </tr>';

// terms
    $html .= '</table>';

    $html .= '<table class="border-around">';



    $html .= '<tr class="line-with-spaces"><td colspan="4"></td></tr>';



    $html .= '</table>';

// products
    $html .= '<table class="border-around">';

    $html .= '<thead>
            <tr class="padding-around">
                <td class="center bold first">Název</td>
                <td class="center bold first last">Množství</td>
                <td class="center bold first last">Jednotková cena</td>
                <td class="center bold first last">Sleva</td>
                <td class="center bold first last">Celkem</td>
            </tr>
            <tr class="line-with-spaces"><td colspan="5"></td></tr>
          </thead>';



            $html .= '<tr class="padding-around">
                <td class="left bold first">' . $inv["content"] . '</td>
                <td class="right first last">' . $inv["amount"] . ' ks</td>
                <td class="right first last">' . $inv["jednotkova"] . 'kč</td>
                <td class="right first last">' . $inv["sleva"] . '%</td>
                <td class="right first last">' . $inv["wVAT"] . ' kč</td>
              </tr>';





                $html .= '<tr class="line-with-spaces"><td colspan="5"></td></tr>';



    $html .= '</table>';


$html .= '<table class="border-around">';
$html .= '<tr class="padding-around">
            <td  colspan="6" class="left bold first"> ' . $inv["descr"] . ' </td>
          </tr>';
$html .= '</table>';

// sum price
    $html .= '<table class="border-around">';

    if ($discount > 0) {

            $discountAmount = round((floatval($order->getProductPrice()) / floatval(100 - $discount)) * floatval($discount));


        if ($invoiceType == INVOICE_DOBROPIS)
            $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">Sleva ' . $discount . '%:</td>
            <td class="right fist last">' . CustomCurrency::setDecimals($discountAmount) . '</td>
          </tr>';
        else
            $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">Sleva ' . $discount . '%:</td>
            <td class="right fist last">-' . CustomCurrency::setDecimals($discountAmount) . '</td>
          </tr>';
    }





 if($invoiceType == INVOICE_DOBROPIS){
    //maybe it will correct.



    $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">Celkem bez DPH:</td>
            <td class="right fist last">' . $inv["woVAT"] . ' Kč</td>
          </tr>';

    $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">DPH 21%:</td>
            <td class="right fist last">' . $dph . ' Kč</td>
          </tr>';

    $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last padding-bottom bigger-font">Suma celkem s DPH:</td>
            <td class="right bold fist last padding-bottom bigger-font">' . $inv["wVAT"] . ' Kč</td>
          </tr>';


}else{
	$html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">Celkem bez DPH:</td>
            <td class="right fist last">' . $inv["woVAT"] . ' Kč</td>
          </tr>';

        $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">DPH 21%:</td>
            <td class="right fist last">' . $dph . ' Kč</td>
          </tr>';

        $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last padding-bottom bigger-font">Suma celkem s DPH:</td>
            <td class="right bold fist last padding-bottom bigger-font">' . $inv["wVAT"] . ' Kč</td>
          </tr>';
    }

    $html .= '</table>';

// stamps
    $html .= '<table class="border-full" page-break-inside="avoid">';

    $html .= '<tr class="padding-around">
            <td class="first bold col-3">Za odběratele:</td>
            <td class="last col-3"></td>
            <td class="first bold vert-line col-3">Fakturoval:</td>
            <td class="last col-3"></td>
          </tr>';

    $html .= '<tr class="padding-around">
            <td class="first bold col-3">Razítko:</td>
            <td class="last col-3"></td>
            <td class="first bold vert-line col-3">Razítko:</td>
            <td class="last col-3"></td>
          </tr>';

    $html .= '<tr class="padding-around double-row">
            <td colspan="2" class="col-6"></td>
            <td colspan="2" class="vert-line col-6 double-row"></td>
          </tr>';

    $html .= '<tr class="padding-around">
            <td class="first bold col-3">Podpis:</td>
            <td class="last col-3"></td>
            <td class="first bold vert-line col-3">Podpis:</td>
            <td class="last col-3"></td>
          </tr>';

    $html .= '</table>';

    $html .= '<p class="registration-note">Registrace: Ž.l. vydal MM v HK Č.j.: HK-008579.7-FL</p>';
//$html .= '<p class="eet-codes">FIK:</p>';
//$html .= '<p class="eet-codes">BKP:</p>';
    $html .= '<h2 class="center thanks">Děkujeme za Váš nákup a těšíme se na další spolupráci!</h2>';
    $html .= '<h2 class="center thanks-smaller">OnlineAtrakce.cz</h2>';

    $pdf = new mPDF('', '', 0, '', 8, 8, 5, 12, 5, 5);





if($printHtml == true) {
    echo '<style>';
    echo $css;
    echo '</style>';

    echo $html;
} else {
    if ($invoiceType == INVOICE_DOBROPIS) {
        $pdf->SetTitle('OnlineAtrakce.cz - dobropis ' . $Id);

    } else {
        $pdf->SetTitle('OnlineAtrakce.cz - faktura ' . $Id);
    }
    $pdf->SetHTMLFooter('<p style="font-family: \'Comfortaa\', cursive; font-size: 0.9rem">Strana {PAGENO} z {nbpg}</p>');
    $pdf->WriteHTML($css, 1);
    $pdf->WriteHTML($html, 2);
}

    if ($invoiceType == INVOICE_DOBROPIS) {
        $pdf->Output('OnlineAtrakce.cz-d' . $Id . '.pdf', 'I');

    } else {
        $pdf->Output('OnlineAtrakce.cz-f' . $Id . '.pdf', 'I');
    }

