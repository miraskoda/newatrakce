<?php
/**
 * Created by PhpStorm.
 * User: miroslavskoda
 * Date: 26/01/2019
 * Time: 14:01
 */


use backend\controllers\CustomerController;
use backend\controllers\InvoiceCreditController;
use backend\controllers\OrderController;
use backend\controllers\UserController;
use backend\models\Address;
use backend\models\CustomCurrency;
use backend\models\Customer;
use backend\models\Order;
use backend\models\Payment;
use backend\models\PaymentType;
use backend\models\OrderTerm;
use backend\models\OrderProduct;



require_once __DIR__ . "/../../../../backend/modules/app/prepare.php";

require_once '../../../../backend/plugins/mpdf/vendor/autoload.php';




$orderId = 0;
if(isset($_GET['orderId']) && is_numeric($_GET['orderId']) && $_GET['orderId'] > 0)
    $orderId = $_GET['orderId'];

$order = new Order($orderId);
$order = $order->load();
if(!is_a($order, Order::class)) {
    die('Objednávka s daným ID neexistuje. Prosím kontaktujte zákaznickou podporu.');
}
$Id = InvoiceCreditController::getIDfromConfirmation($orderId);


$order = new Order($orderId);
$order = $order->load();



$customer = CustomerController::isLoggedCustomer();
if(!is_a($customer, Customer::class)) {
    if(!isset($_GET['customerId'])) {
        $customer = new Customer();
    } else {
        $customer = new Customer($_GET['customerId']);
        $customer = $customer->load();
    }
}




$css = file_get_contents(__DIR__ . "/../../../../styles/file-style.min.css");

$stringifiedRentHours = OrderTerm::getRentHoursAsStrings($order);
$products = OrderProduct::getOrderProducts($order);
$terms = OrderTerm::getOrderTermsByOrder($order);


$invoiceAddress = new Address();
$invoiceAddress->setOrderId($order->getOrderId());
$invoiceAddress->setAddressType(Address::ADDRESS_INVOICE);
$invoiceAddress = $invoiceAddress->load();

$payment = new Payment($order->getPaymentId());
$payment = $payment->load();
$paymentType = new PaymentType($order->getPaymentTypeId());
$paymentType = $paymentType->load();

$printHtml = false;

try {
    set_error_handler(function () {
    });

    $venueData = OrderController::getVenueCountedDataByOrder($order);

    restore_error_handler();
} catch (Exception $e) {
    if ($printHtml) {
        echo 'Venue load error: ' . $e->getMessage();
    }
}



// write to database now
\backend\controllers\InvoiceCreditController::ManageConfirmation($order->getOrderId());


// html start
    // maybe some new counter for new conf


$html = '<h2 class="col-6 left">Potvrzení o platbě č. '. $Id .' - daňový doklad</h2>';

$html .= '<h2 class="col-6 right">OnlineAtrakce.cz</h2>';

$html .= '<table class="border-around smaller-font">';

// names
$html .= '<tr class="padding-around">
            <td class="first bold">Dodavatel:</td>
            <td class="last">Josef Rybár</td>
            <td class="first bold vert-line">Odběratel:</td>
            <td class="last">' . $invoiceAddress->getName() . '</td>
          </tr>';

// addresses
$html .= '<tr class="padding-around">
            <td class="first bold">Adresa:</td>
            <td class="last">Syrovátka 11<br>Lhota pod Libčany 503 27</td>
            <td class="first bold vert-line">Adresa:</td>
            <td class="last">' . $invoiceAddress->getStreet() . '<br>' . $invoiceAddress->getCity() . ' ' . substr($invoiceAddress->getZip(), 0, 3) . ' ' . substr($invoiceAddress->getZip(), 3) . '</td>
          </tr>';

// ičo
$html .= '<tr class="padding-around">
            <td class="first bold">IČO:</td>
            <td class="last">627 23 499</td>' .
    (($order->getInvoiceIco() !== null && is_numeric($order->getInvoiceIco()) && $order->getInvoiceIco() > 0) ? '<td class="first bold vert-line">IČO:</td><td class="last">' . $order->getInvoiceIco() . '</td>' : '<td colspan="2" class="first vert-line"></td>')
    . '</tr>';

// dič
$html .= '<tr class="padding-around">
            <td class="first bold padding-bottom">DIČ:</td>
            <td class="padding-bottom last">CZ7605093111</td>' .
    (($order->getInvoiceDic() !== null && strlen($order->getInvoiceDic()) > 0) ? '<td class="first bold vert-line padding-bottom">DIČ:</td><td class="last padding-bottom">' . $order->getInvoiceDic() . '</td>' : '<td colspan="2" class="first vert-line"></td>')
    . '</tr>';

$html .= '<tr class="line"><td colspan="4"></td></tr>';

// payment
$html .= '<tr class="padding-around">
            <td class="first bold">Uhrazeno:</td>
            <td class="last">' . $payment->getName() . '</td>
            
            <td class="first bold vert-line">Kontaktní osoba:</td>
            <td class="last">' . $order->getContactName() . '</td>
          </tr>';

$html .= '<tr class="padding-around">
            ' . (($payment->getPaymentId() == 2) ? '<td class="first bold">Číslo účtu:</td><td class="last">184218759 / 0300</td>' : '<td colspan="2"></td>') . '
            <td class="first bold vert-line">Telefon:</td>
            <td class="last">' . $order->getContactPhone() . '</td>
          </tr>';

$html .= '<tr class="padding-around">
            ' . (($payment->getPaymentId() == 2) ? '<td class="first bold">Konstantní symbol:</td><td class="last">308</td>' : '<td colspan="2" class="padding-bottom"></td>') . '
            <td class="first bold vert-line padding-bottom">Místo konání:</td>
            <td class="last padding-bottom">' . $order->getVenueStreet() . '<br>' . $order->getVenueCity() . ' ' . substr($order->getVenueZip(), 0, 3) . ' ' . substr($order->getVenueZip(), 3) . '</td>
          </tr>';

if ($payment->getPaymentId() == 2) {
    $html .= '<tr class="padding-around">
            <td class="first bold padding-bottom">Variabilní symbol:</td><td class="last padding-bottom">' . (($invoiceType == INVOICE_FINAL) ? 'final' : $orderNo) . '</td><td colspan="2" class="first vert-line last padding-bottom"></td>
          </tr>';
}

$html .= '<tr class="line"><td colspan="4"></td></tr>';

// terms
$html .= '</table>';

$html .= '<table class="border-around">';

$html .= '<thead>
            <tr class="padding-around">
                <td></td>
                <td class="center bold">Začátek</td>
                <td class="center bold">Konec</td>
                <td class="center bold last">Počet hodin</td>
            </tr>
          </thead>';

$html .= '<tr class="line-with-spaces"><td colspan="4"></td></tr>';

foreach ($terms as $index => $term) {
    $html .= '<tr class="padding-around">
                <td class="first bold ' . (($index + 1 == count($terms)) ? 'padding-bottom' : '') . '">' . ($index + 1) . '. termín</td>
                <td class="center ' . (($index + 1 == count($terms)) ? 'padding-bottom' : '') . '">' . $term->getStart() . '</td>
                <td class="center ' . (($index + 1 == count($terms)) ? 'padding-bottom' : '') . '">' . $term->getEnd() . '</td>
                <td class="center last">' . $term->getHours() . '</td>
              </tr>';
}

$html .= '</table>';

// products
$html .= '<table class="border-around">';

$html .= '<thead>
            <tr class="padding-around">
                <td class="center bold first">Název</td>
                <td class="center bold first last">Množství</td>
                <td class="center bold first last">Jednotková cena</td>
                <td class="center bold first last">Sleva</td>
                <td class="center bold first last">Celkem</td>
            </tr>
            <tr class="line-with-spaces"><td colspan="5"></td></tr>
          </thead>';

$discount = $order->getDiscount();


foreach ($products as $index => $product) {
    // only for long output testing
    /*for ($i = 0; $i < 30; $i++) {
        $product = $products[0];*/

    $sumPrice = 0;
    foreach ($stringifiedRentHours as $rentHour) {
        if ($rentHour > 0) {
            $sumPrice += (($rentHour <= 5) ?
                (($product['price']) * $product['quantity'])
                :
                ((($product['price']) + (($product['price'] / 100) * (5 * ($rentHour - (($rentHour > 5) ? 5 : $rentHour))))) * $product['quantity'])
            );
        }
    }

    $html .= '<tr class="padding-around">
                <td class="left bold first">' . $product['name'] . '</td>
                <td class="right first last">' . $product['quantity'] . ' ks</td>
                <td class="right first last">' . CustomCurrency::setDecimals(round($product['price'])) . '</td>
                <td class="right first last">' . $discount . '%</td>
                <td class="right first last">' . CustomCurrency::setDecimals(round($sumPrice)) . '</td>
              </tr>';

    if ($numOfTerms > 1) {
        $counter = 1;
        foreach ($stringifiedRentHours as $rentHour) {
            if ($rentHour > 0) {
                $html .= '<tr class="padding-around"><td class="right bold">' . $counter . '. termín</td><td colspan="3"></td><td class="right first last">' .
                    (($rentHour <= 5) ?
                        CustomCurrency::setDecimals(round(($product['price']) * $product['quantity']))
                        :
                        CustomCurrency::setDecimals(round((($product['price']) + (($product['price'] / 100) * (5 * ($rentHour - (($rentHour > 5) ? 5 : $rentHour))))) * $product['quantity']))
                    )
                    . '</td></tr>';
                $counter++;
            }
        }
        $html .= '<tr class="line-with-spaces"><td colspan="5"></td></tr>';
    }
}

$html .= '<tr class="line-with-spaces"><td colspan="5"></td></tr>';
$html .= '<tr class="padding-around">
                <td class="left bold first">Záloha ' . $paymentType->getDeposit() . '% za objednávku ' . $orderNo . '</td>
                <td class="right first last">1 ks</td>
                <td class="right first last">' . CustomCurrency::setDecimals(round((floatval($order->getSumPrice()) / 100) * floatval($paymentType->getDeposit()))) . '</td>
                <td class="right first last">0%</td>
                <td class="right first last">' . CustomCurrency::setDecimals(round((floatval($order->getSumPrice()) / 100) * floatval($paymentType->getDeposit()))) . '</td>
              </tr>';


$html .= '<tr class="padding-around">
            <td class="left bold first padding-bottom">Doprava</td>
            <td class="right first last padding-bottom">' . (($order->getDeliveryPaymentPrice() > 0) ? CustomCurrency::setDecimals($order->getDeliveryPaymentPrice() / 6) : 0) . ' km</td>
            <td class="right first last padding-bottom">' . CustomCurrency::setDecimals(6) . '</td>
            <td class="right first last padding-bottom">' . (($order->getDeliveryPaymentPrice() > 0) ? '0%' : '100%') . '</td>
            <td class="right first last padding-bottom">' . CustomCurrency::setDecimals($order->getDeliveryPaymentPrice()) . '</td>
          </tr>';

$html .= '</table>';

// sum price
$html .= '<table class="border-around">';

if ($discount > 0) {
        $discountAmount = round(((floatval($order->getProductPrice()) / floatval(100 - $discount)) * floatval($discount) / 100) * $paymentType->getDeposit());


        $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">Sleva ' . $discount . '%:</td>
            <td class="right fist last">' . CustomCurrency::setDecimals($discountAmount) . '</td>
          </tr>';

}

    $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">Celkem bez DPH:</td>
            <td class="right fist last">' . CustomCurrency::setDecimals(((($order->getProductPrice() + $order->getDeliveryPaymentPrice()) / 100) * $paymentType->getDeposit())) . '</td>
          </tr>';

    $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">DPH 21%:</td>
            <td class="right fist last">' . CustomCurrency::setDecimals((($order->getVat() / 100) * $paymentType->getDeposit())) . '</td>
          </tr>';

    $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">Celkem včetně DPH:</td>
            <td class="right fist last">' . CustomCurrency::setDecimals(round(($order->getSumPrice() / 100) * $paymentType->getDeposit())) . '</td>
          </tr>';

    $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last padding-bottom bigger-font">Zaplaceno včetně DPH:</td>
            <td class="right bold fist last padding-bottom bigger-font">' . CustomCurrency::setCustomCurrencyWithoutDecimals(round(($order->getSumPrice() / 100) * $paymentType->getDeposit())) . '</td>
          </tr>';


$html .= '</table>';

// stamps
$html .= '<table class="border-full" page-break-inside="avoid">';

$html .= '<tr class="padding-around">
            <td class="first bold col-3">Za odběratele:</td>
            <td class="last col-3"></td>
            <td class="first bold vert-line col-3">Fakturoval:</td>
            <td class="last col-3"></td>
          </tr>';

$html .= '<tr class="padding-around">
            <td class="first bold col-3">Razítko:</td>
            <td class="last col-3"></td>
            <td class="first bold vert-line col-3">Razítko:</td>
            <td class="last col-3"></td>
          </tr>';

$html .= '<tr class="padding-around double-row">
            <td colspan="2" class="col-6"></td>
            <td colspan="2" class="vert-line col-6 double-row"></td>
          </tr>';

$html .= '<tr class="padding-around">
            <td class="first bold col-3">Podpis:</td>
            <td class="last col-3"></td>
            <td class="first bold vert-line col-3">Podpis:</td>
            <td class="last col-3"></td>
          </tr>';

$html .= '</table>';

$html .= '<p class="registration-note">Registrace: Ž.l. vydal MM v HK Č.j.: HK-008579.7-FL</p>';
//$html .= '<p class="eet-codes">FIK:</p>';
//$html .= '<p class="eet-codes">BKP:</p>';
$html .= '<h2 class="center thanks">Děkujeme za Váši platbu a těšíme se na další spolupráci!</h2>';
$html .= '<h2 class="center thanks-smaller">OnlineAtrakce.cz</h2>';

$pdf = new mPDF('', '', 0, '', 8, 8, 5, 12, 5, 5);




if($printHtml == true) {
    echo '<style>';
    echo $css;
    echo '</style>';

    echo $html;
} else {

    $pdf->SetTitle('OnlineAtrakce.cz - doklad ' . $Id);
    $pdf->SetHTMLFooter('<p style="font-family: \'Comfortaa\', cursive; font-size: 0.9rem">Strana {PAGENO} z {nbpg}</p>');
    $pdf->WriteHTML($css, 1);
    $pdf->WriteHTML($html, 2);
}

    $pdf->Output('OnlineAtrakce.cz-dokl' . $Id . '.pdf', 'I');

