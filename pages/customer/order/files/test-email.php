<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 17.04.2018
 * Time: 14:31
 */

use backend\models\MailHelper;
use backend\models\Url;

require_once __DIR__ . "/../../../../backend/modules/app/prepare.php";

require_once '../../../../backend/plugins/mpdf/vendor/autoload.php';
require_once Url::getBackendPathTo("/modules/page-parts/customer/order/detail/detail-loader.php");


$mailer = new MailHelper();
echo $mailer->getOrderBody($orderNo, $order);