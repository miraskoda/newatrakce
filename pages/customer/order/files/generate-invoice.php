<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 21.09.2017
 * Time: 20:05
 */

use backend\controllers\CustomerController;
use backend\controllers\OrderController;
use backend\controllers\UserController;
use backend\models\Address;
use backend\models\CustomCurrency;
use backend\models\Customer;
use backend\models\Order;
use backend\models\OrderProduct;
use backend\models\OrderTerm;
use backend\models\Payment;
use backend\models\PaymentType;
use backend\models\Url;

require_once __DIR__ . "/../../../../backend/modules/app/prepare.php";

require_once '../../../../backend/plugins/mpdf/vendor/autoload.php';





$orderId = 0;
if(isset($_GET['orderId']) && is_numeric($_GET['orderId']) && $_GET['orderId'] > 0)
    $orderId = $_GET['orderId'];

$order = new Order($orderId);
$order = $order->load();
if(!is_a($order, Order::class)) {
    die('Objednávka s daným ID neexistuje. Prosím kontaktujte zákaznickou podporu.');
}

$customer = CustomerController::isLoggedCustomer();
if(!is_a($customer, Customer::class)) {
    if(!isset($_GET['customerId'])) {
        $customer = new Customer();
    } else {
        $customer = new Customer($_GET['customerId']);
        $customer = $customer->load();
    }
}

if(!UserController::isLoggedUser() && $order->getCustomerId() != $customer->getCustomerId()) {
    die('Přístup odepřen.');
}

$orderNo = $order->getFormattedOrderId();


const INVOICE_DEPOSIT = 'deposit';
const INVOICE_FINAL = 'final';
const INVOICE_DOBROPIS = 'dobropis';



// tady bylo ''deposit
$invoiceType = 'deposit';
if(isset($_GET['invoiceType']) && strlen($_GET['invoiceType']) > 0)
    $invoiceType = $_GET['invoiceType'];
else {

    // maybe todo
    echo 'Není definován typ faktury';
    return;
}







//database writting
if($invoiceType == INVOICE_DOBROPIS) {
    \backend\controllers\InvoiceCreditController::ManageDobropis($order->getOrderId());
}


if($invoiceType == INVOICE_FINAL){
    \backend\controllers\InvoiceCreditController::ManageInvoice($order->getOrderId());

}

if($invoiceType == INVOICE_DEPOSIT){
    \backend\controllers\InvoiceCreditController::ManageDeposit($order->getOrderId());

}



        if($invoiceType == INVOICE_DOBROPIS) {
    //02 number in the end for dobropis only
    $depositInvoiceNo = DateTime::createFromFormat('Y-m-d H:i:s', $order->getChangedRaw())->format('y') . $order->getOrderId() . '02';

}else {
    $depositInvoiceNo = DateTime::createFromFormat('Y-m-d H:i:s', $order->getChangedRaw())->format('y') . $order->getOrderId() . '01';
}


    $css = file_get_contents(__DIR__ . "/../../../../styles/file-style.min.css");

    $stringifiedRentHours = OrderTerm::getRentHoursAsStrings($order);
    $products = OrderProduct::getOrderProducts($order);

    $invoiceAddress = new Address();
    $invoiceAddress->setOrderId($order->getOrderId());
    $invoiceAddress->setAddressType(Address::ADDRESS_INVOICE);
    $invoiceAddress = $invoiceAddress->load();

    $payment = new Payment($order->getPaymentId());
    $payment = $payment->load();
    $paymentType = new PaymentType($order->getPaymentTypeId());
    $paymentType = $paymentType->load();

    $terms = OrderTerm::getOrderTermsByOrder($order);
    $numOfTerms = count(array_filter($stringifiedRentHours, function ($x) {
        return ($x > 0);
    }));
    if ($numOfTerms < 1)
        $numOfTerms = 1;
    else if ($numOfTerms > 5)
        $numOfTerms = 5;

    $printHtml = false;

    try {
        set_error_handler(function () {
        });

        $venueData = OrderController::getVenueCountedDataByOrder($order);

        restore_error_handler();
    } catch (Exception $e) {
        if ($printHtml) {
            echo 'Venue load error: ' . $e->getMessage();
        }
    }

// html start
    if ($invoiceType == INVOICE_DEPOSIT) {
        if ($paymentType->getDeposit() < 100)
            $html = '<h2 class="col-6 left">Zálohová faktura č. ' . \backend\controllers\InvoiceCreditController::getInvoiceFromOrderIdDeposit($order->getOrderId()) . '</h2>';
        else
            $html = '<h2 class="col-6 left">Proforma faktura č. ' . \backend\controllers\InvoiceCreditController::getInvoiceFromOrderIdDeposit($order->getOrderId()) . '</h2>';
    } elseif ($invoiceType == INVOICE_DOBROPIS) {
        // maybe some new counter for new dobropis
        $html = '<h2 class="col-6 left">Dobropis č. ' . \backend\controllers\InvoiceCreditController::getInvoiceFromOrderIdDobropis($order->getOrderId()) . '</h2>';
    } else {
        $html = '<h2 class="col-6 left">Faktura č. ' . \backend\controllers\InvoiceCreditController::getInvoiceFromOrderId($order->getOrderId()) . ' - daňový doklad</h2>';
    }
    $html .= '<h2 class="col-6 right">OnlineAtrakce.cz</h2>';

    if ($invoiceType == INVOICE_DEPOSIT)
        $html .= '<p class="col-12 left smaller-font no-spaces">Není daňový doklad</p>';

    $html .= '<table class="border-around smaller-font">';

// names
    $html .= '<tr class="padding-around">
            <td class="first bold">Dodavatel:</td>
            <td class="last">Josef Rybár</td>
            <td class="first bold vert-line">Odběratel:</td>
            <td class="last">' . $invoiceAddress->getName() . '</td>
          </tr>';

// addresses
    $html .= '<tr class="padding-around">
            <td class="first bold">Adresa:</td>
            <td class="last">Syrovátka 11<br>Lhota pod Libčany 503 27</td>
            <td class="first bold vert-line">Adresa:</td>
            <td class="last">' . $invoiceAddress->getStreet() . '<br>' . $invoiceAddress->getCity() . ' ' . substr($invoiceAddress->getZip(), 0, 3) . ' ' . substr($invoiceAddress->getZip(), 3) . '</td>
          </tr>';

// ičo
    $html .= '<tr class="padding-around">
            <td class="first bold">IČO:</td>
            <td class="last">627 23 499</td>' .
        (($order->getInvoiceIco() !== null && is_numeric($order->getInvoiceIco()) && $order->getInvoiceIco() > 0) ? '<td class="first bold vert-line">IČO:</td><td class="last">' . $order->getInvoiceIco() . '</td>' : '<td colspan="2" class="first vert-line"></td>')
        . '</tr>';

// dič
    $html .= '<tr class="padding-around">
            <td class="first bold padding-bottom">DIČ:</td>
            <td class="padding-bottom last">CZ7605093111</td>' .
        (($order->getInvoiceDic() !== null && strlen($order->getInvoiceDic()) > 0) ? '<td class="first bold vert-line padding-bottom">DIČ:</td><td class="last padding-bottom">' . $order->getInvoiceDic() . '</td>' : '<td colspan="2" class="first vert-line"></td>')
        . '</tr>';

    $html .= '<tr class="line"><td colspan="4"></td></tr>';

// payment
    $html .= '<tr class="padding-around">
            <td class="first bold">Forma úhrady:</td>
            <td class="last">' . $payment->getName() . '</td>
            
            <td class="first bold vert-line">Kontaktní osoba:</td>
            <td class="last">' . $order->getContactName() . '</td>
          </tr>';

    $html .= '<tr class="padding-around">
            ' . (($payment->getPaymentId() == 2) ? '<td class="first bold">Číslo účtu:</td><td class="last bold">184218759 / 0300</td>' : '<td colspan="2"></td>') . '
            <td class="first bold vert-line">Telefon:</td>
            <td class="last">' . $order->getContactPhone() . '</td>
          </tr>';

    $html .= '<tr class="padding-around">
            ' . (($payment->getPaymentId() == 2) ? '<td class="first bold">Konstantní symbol:</td><td class="last">308</td>' : '<td colspan="2" class="padding-bottom"></td>') . '
            <td class="first bold vert-line padding-bottom">Místo konání:</td>
            <td class="last padding-bottom">' . $order->getVenueStreet() . '<br>' . $order->getVenueCity() . ' ' . substr($order->getVenueZip(), 0, 3) . ' ' . substr($order->getVenueZip(), 3) . '</td>
          </tr>';

    if ($payment->getPaymentId() == 2) {
        $html .= '<tr class="padding-around">
            <td class="first bold padding-bottom">Variabilní symbol:</td><td class="last padding-bottom">' . (($invoiceType == INVOICE_FINAL) ? 'final' : $orderNo) . '</td><td colspan="2" class="first vert-line last padding-bottom"></td>
          </tr>';
    }

    $html .= '<tr class="line"><td colspan="4"></td></tr>';

// pay date

if($invoiceType == INVOICE_DEPOSIT){

    $html .= '<tr class="padding-around">
            <td class="first bold">Datum vystavení:</td>
            <td class="last">' . $order->getChangedDate() . '</td>
            <td colspan="2" class="first bold vert-line">Při neuhrazení se objednávka automaticky stornuje.</td>
          </tr>';

    $html .= '<tr class="padding-around">
            <td class="first bold padding-bottom">Datum splatnosti:</td>
            <td class="last padding-bottom">' . $order->getPayDate() . '</td>
            <td colspan="2" class="first vert-line last padding-bottom"></td>
          </tr>';
}else{

    $html .= '<tr class="padding-around">
            <td class="first bold">Datum vystavení:</td>
            <td class="last">' . $order->getChangedDate() . '</td>
            <td class="first bold vert-line">Datum usk. zdan. plnění:</td>
            <td class="last">' . $order->getChangedDate() . '</td>
          </tr>';

    $html .= '<tr class="padding-around">
            <td class="first bold padding-bottom">Datum splatnosti:</td>
            <td class="last padding-bottom">' . $order->getPayDate() . '</td>
            <td colspan="2" class="first vert-line last padding-bottom"></td>
          </tr>';
}

// terms
    $html .= '</table>';

    $html .= '<table class="border-around">';

    $html .= '<thead>
            <tr class="padding-around">
                <td></td>
                <td class="center bold">Začátek</td>
                <td class="center bold">Konec</td>
                <td class="center bold last">Počet hodin</td>
            </tr>
          </thead>';

    $html .= '<tr class="line-with-spaces"><td colspan="4"></td></tr>';

    foreach ($terms as $index => $term) {
        $html .= '<tr class="padding-around">
                <td class="first bold ' . (($index + 1 == count($terms)) ? 'padding-bottom' : '') . '">' . ($index + 1) . '. termín</td>
                <td class="center ' . (($index + 1 == count($terms)) ? 'padding-bottom' : '') . '">' . $term->getStart() . '</td>
                <td class="center ' . (($index + 1 == count($terms)) ? 'padding-bottom' : '') . '">' . $term->getEnd() . '</td>
                <td class="center last">' . $term->getHours() . '</td>
              </tr>';
    }

    $html .= '</table>';

// products
    $html .= '<table class="border-around">';

    $html .= '<thead>
            <tr class="padding-around">
                <td class="center bold first">Název</td>
                <td class="center bold first last">Množství</td>
                <td class="center bold first last">Jednotková cena</td>
                <td class="center bold first last">Sleva</td>
                <td class="center bold first last">Celkem</td>
            </tr>
            <tr class="line-with-spaces"><td colspan="5"></td></tr>
          </thead>';

    $discount = $order->getDiscount();

    if ($invoiceType == INVOICE_DEPOSIT) {
        foreach ($products as $index => $product) {
            $html .= '<tr class="padding-around">
                <td class="left bold first">' . $product['name'] . '</td>
                <td class="right first last">' . $product['quantity'] . ' ks</td>
                <td class="right first last"></td>
                <td class="right first last"></td>
                <td class="right first last"></td>
              </tr>';
        }

        $html .= '<tr class="line-with-spaces"><td colspan="5"></td></tr>';
        $html .= '<tr class="padding-around">
                <td class="left bold first">Záloha ' . $paymentType->getDeposit() . '% za objednávku ' . $orderNo . '</td>
                <td class="right first last">1 ks</td>
                <td class="right first last">' . CustomCurrency::setDecimals(round((floatval($order->getSumPrice()) / 100) * floatval($paymentType->getDeposit()))) . '</td>
                <td class="right first last">0%</td>
                <td class="right first last">' . CustomCurrency::setDecimals(round((floatval($order->getSumPrice()) / 100) * floatval($paymentType->getDeposit()))) . '</td>
              </tr>';
    } else {
        foreach ($products as $index => $product) {
            // only for long output testing
            /*for ($i = 0; $i < 30; $i++) {
                $product = $products[0];*/

            $sumPrice = 0;
            foreach ($stringifiedRentHours as $rentHour) {
                if ($rentHour > 0) {
                    $sumPrice += (($rentHour <= 5) ?
                        (($product['price']) * $product['quantity'])
                        :
                        ((($product['price']) + (($product['price'] / 100) * (5 * ($rentHour - (($rentHour > 5) ? 5 : $rentHour))))) * $product['quantity'])
                    );
                }
            }

            $html .= '<tr class="padding-around">
                <td class="left bold first">' . $product['name'] . '</td>
                <td class="right first last">' . $product['quantity'] . ' ks</td>
                <td class="right first last">' . CustomCurrency::setDecimals(round($product['price'])) . '</td>
                <td class="right first last">' . $discount . '%</td>
                <td class="right first last">' . CustomCurrency::setDecimals(round($sumPrice)) . '</td>
              </tr>';

            if ($numOfTerms > 1) {
                $counter = 1;
                foreach ($stringifiedRentHours as $rentHour) {
                    if ($rentHour > 0) {
                        $html .= '<tr class="padding-around"><td class="right bold">' . $counter . '. termín</td><td colspan="3"></td><td class="right first last">' .
                            (($rentHour <= 5) ?
                                CustomCurrency::setDecimals(round(($product['price']) * $product['quantity']))
                                :
                                CustomCurrency::setDecimals(round((($product['price']) + (($product['price'] / 100) * (5 * ($rentHour - (($rentHour > 5) ? 5 : $rentHour))))) * $product['quantity']))
                            )
                            . '</td></tr>';
                        $counter++;
                    }
                }
                $html .= '<tr class="line-with-spaces"><td colspan="5"></td></tr>';
            }
        }
    }

    $html .= '<tr class="padding-around">
            <td class="left bold first padding-bottom">Doprava</td>
            <td class="right first last padding-bottom">' . (($order->getDeliveryPaymentPrice() > 0) ? CustomCurrency::setDecimals($order->getDeliveryPaymentPrice() / 6) : 0) . ' km</td>
            <td class="right first last padding-bottom">' . CustomCurrency::setDecimals(6) . '</td>
            <td class="right first last padding-bottom">' . (($order->getDeliveryPaymentPrice() > 0) ? '0%' : '100%') . '</td>
            <td class="right first last padding-bottom">' . CustomCurrency::setDecimals($order->getDeliveryPaymentPrice()) . '</td>
          </tr>';

    $html .= '</table>';

// sum price
    $html .= '<table class="border-around">';

    if ($discount > 0) {
        if ($invoiceType == INVOICE_DEPOSIT)
            $discountAmount = round(((floatval($order->getProductPrice()) / floatval(100 - $discount)) * floatval($discount) / 100) * $paymentType->getDeposit());
        else
            $discountAmount = round((floatval($order->getProductPrice()) / floatval(100 - $discount)) * floatval($discount));


        if ($invoiceType == INVOICE_DOBROPIS)
            $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">Sleva ' . $discount . '%:</td>
            <td class="right fist last">' . CustomCurrency::setDecimals($discountAmount) . '</td>
          </tr>';
        else
            $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">Sleva ' . $discount . '%:</td>
            <td class="right fist last">-' . CustomCurrency::setDecimals($discountAmount) . '</td>
          </tr>';
    }

    if ($invoiceType == INVOICE_DEPOSIT) {
        $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">Celkem bez DPH:</td>
            <td class="right fist last">' . CustomCurrency::setDecimals(((($order->getProductPrice() + $order->getDeliveryPaymentPrice()) / 100) * $paymentType->getDeposit())) . '</td>
          </tr>';

        $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">DPH 21%:</td>
            <td class="right fist last">' . CustomCurrency::setDecimals((($order->getVat() / 100) * $paymentType->getDeposit())) . '</td>
          </tr>';

        $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">Celkem včetně DPH:</td>
            <td class="right fist last">' . CustomCurrency::setDecimals(round(($order->getSumPrice() / 100) * $paymentType->getDeposit())) . '</td>
          </tr>';

        $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last padding-bottom bigger-font">Celkem k platbě včetně DPH:</td>
            <td class="right bold fist last padding-bottom bigger-font">' . CustomCurrency::setCustomCurrencyWithoutDecimals(round(($order->getSumPrice() / 100) * $paymentType->getDeposit())) . '</td>
          </tr>';
} elseif($invoiceType == INVOICE_DOBROPIS){
    //maybe it will correct.

    $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">Celkem bez DPH:</td>
            <td class="right fist last">-' . CustomCurrency::setDecimals( $order->getProductPrice() + $order->getDeliveryPaymentPrice() ) . '</td>
          </tr>';

    $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">DPH 21%:</td>
            <td class="right fist last">-' . CustomCurrency::setDecimals( $order->getVat() ) . '</td>
          </tr>';

    $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">Celkem včetně DPH:</td>
            <td class="right fist last">-' . CustomCurrency::setDecimals( round($order->getSumPrice()) ) . '</td>
          </tr>';

    if($paymentType->getDeposit() > 0) {
        $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">Zálohová platba:</td>
            <td class="right fist last">' . CustomCurrency::setCustomCurrencyWithoutDecimals( round( ($order->getSumPrice() / 100) * $paymentType->getDeposit()) ) . '</td>
          </tr>';
    }

    $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last padding-bottom bigger-font">Celkem s DPH:</td>
            <td class="right bold fist last padding-bottom bigger-font">-' . CustomCurrency::setCustomCurrencyWithoutDecimals( round( $order->getSumPrice() ) - round( ($order->getSumPrice() / 100) * $paymentType->getDeposit()) ) . '</td>
          </tr>';


}else{
	$html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">Celkem bez DPH:</td>
            <td class="right fist last">' . CustomCurrency::setDecimals($order->getProductPrice() + $order->getDeliveryPaymentPrice()) . '</td>
          </tr>';

        $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">DPH 21%:</td>
            <td class="right fist last">' . CustomCurrency::setDecimals($order->getVat()) . '</td>
          </tr>';

        $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">Celkem včetně DPH:</td>
            <td class="right fist last">' . CustomCurrency::setDecimals(round($order->getSumPrice())) . '</td>
          </tr>';

        if ($paymentType->getDeposit() > 0) {
            $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last">Zálohová platba:</td>
            <td class="right fist last">-' . CustomCurrency::setCustomCurrencyWithoutDecimals(round(($order->getSumPrice() / 100) * $paymentType->getDeposit())) . '</td>
          </tr>';
        }

        $html .= '<tr class="padding-around">
            <td colspan="3" class="right bold first last padding-bottom bigger-font">Celkem k platbě včetně DPH:</td>
            <td class="right bold fist last padding-bottom bigger-font">' . CustomCurrency::setCustomCurrencyWithoutDecimals(round($order->getSumPrice()) - round(($order->getSumPrice() / 100) * $paymentType->getDeposit())) . '</td>
          </tr>';
    }

    $html .= '</table>';

// stamps
    $html .= '<table class="border-full" page-break-inside="avoid">';

    $html .= '<tr class="padding-around">
            <td class="first bold col-3">Za odběratele:</td>
            <td class="last col-3"></td>
            <td class="first bold vert-line col-3">Fakturoval:</td>
            <td class="last col-3"></td>
          </tr>';

    $html .= '<tr class="padding-around">
            <td class="first bold col-3">Razítko:</td>
            <td class="last col-3"></td>
            <td class="first bold vert-line col-3">Razítko:</td>
            <td class="last col-3"></td>
          </tr>';

    $html .= '<tr class="padding-around double-row">
            <td colspan="2" class="col-6"></td>
            <td colspan="2" class="vert-line col-6 double-row"></td>
          </tr>';

    $html .= '<tr class="padding-around">
            <td class="first bold col-3">Podpis:</td>
            <td class="last col-3"></td>
            <td class="first bold vert-line col-3">Podpis:</td>
            <td class="last col-3"></td>
          </tr>';

    $html .= '</table>';

    $html .= '<p class="registration-note">Registrace: Ž.l. vydal MM v HK Č.j.: HK-008579.7-FL</p>';
//$html .= '<p class="eet-codes">FIK:</p>';
//$html .= '<p class="eet-codes">BKP:</p>';
$html .= '<h2 class="center thanks">V případě nejasností kontaktujte prosím administraci, nebo na tel. +420 777 777 328</h2>';

if ($invoiceType == INVOICE_DEPOSIT) {
    $html .= '<h2 class="center thanks">Při neuhrazení se objednávka automaticky stornuje.</h2>';

}else{
    $html .= '<h2 class="center thanks">Děkujeme za Váš nákup a těšíme se na další spolupráci!</h2>';

}
    $html .= '<h2 class="center thanks-smaller">OnlineAtrakce.cz</h2>';

    $pdf = new mPDF('', '', 0, '', 8, 8, 5, 12, 5, 5);





if($printHtml == true) {
    echo '<style>';
    echo $css;
    echo '</style>';

    echo $html;
} else {
    if ($invoiceType == INVOICE_DEPOSIT) {
        $pdf->SetTitle('OnlineAtrakce.cz - zalohova ' . \backend\controllers\InvoiceCreditController::getInvoiceFromOrderIdDeposit($order->getOrderId()));
    } elseif ($invoiceType == INVOICE_DOBROPIS) {
        $pdf->SetTitle('OnlineAtrakce.cz - dobropis ' . \backend\controllers\InvoiceCreditController::getInvoiceFromOrderIddobropis($order->getOrderId()) );

    } else {
        $pdf->SetTitle('OnlineAtrakce.cz - faktura ' . \backend\controllers\InvoiceCreditController::getInvoiceFromOrderId($order->getOrderId()));
    }
        $pdf->SetHTMLFooter('<p style="font-family: \'Comfortaa\', cursive; font-size: 0.9rem">Strana {PAGENO} z {nbpg}</p>');
        $pdf->WriteHTML($css, 1);
        $pdf->WriteHTML($html, 2);

}
    if ($invoiceType == INVOICE_DEPOSIT)
        $pdf->Output('OnlineAtrakce.cz-z' . \backend\controllers\InvoiceCreditController::getInvoiceFromOrderIdDeposit($order->getOrderId()) . '.pdf', 'I');
    elseif($invoiceType == INVOICE_FINAL)
        $pdf->Output('OnlineAtrakce.cz-f' . $orderNo . '.pdf', 'I');
    elseif($invoiceType == INVOICE_DOBROPIS)
        $pdf->Output('OnlineAtrakce.cz-d' . $orderNo . '.pdf', 'I');
