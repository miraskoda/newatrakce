<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 10.09.2017
 * Time: 23:57
 */

use backend\controllers\CustomerController;
use backend\controllers\OrderController;
use backend\models\Url;

$pageId = 18;

require_once __DIR__ . "/../../../backend/modules/app/prepare.php";

//before any action check user and redirect to administration if needed
$customer = CustomerController::isLoggedCustomer();
if(!$customer) {
    header('Location: /prihlaseni/?ret=objednavka', true);
    die();
}

$cartValidation = OrderController::validateCart();
$venueValidation = OrderController::validateVenue();
$paymentValidation = OrderController::validatePayment();
if($cartValidation !== true) {
    header('Location: /kosik/', true);
    die();
} else if($venueValidation !== true) {
    header('Location: /objednavka/misto-konani/', true);
    die();
} else if($paymentValidation !== true) {
    header('Location: /objednavka/udaje/', true);
    die();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="public">
<?php

//public header - categories, pages, cart (search)
require_once Url::getBackendPathTo("/modules/page-parts/header.php");

// loads order to $order variable
require_once Url::getBackendPathTo("/modules/page-parts/customer/order/order-loader.php");

echo '<div class="container-fluid">
            <div class="cart order summary">';
require_once Url::getBackendPathTo("/modules/page-parts/customer/order/summary/summary-content.php");






echo '</div>';
//require_once Url::getBackendPathTo("/modules/page-parts/footer.php");
echo '</div>';
echo '<div id="overlay"></div>';

echo '<script>';
require_once Url::getPathTo("/js/customer/order/finish-order.min.js");
require_once Url::getPathTo("/js/cart/conditions.js");
require_once Url::getPathTo("/js/cart/person.js");


echo '</script>';
require_once Url::getBackendPathTo("/modules/modals/person.php");

require_once Url::getBackendPathTo("/modules/modals/conditions.php");

require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
?>
</body>
</html>