<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 24.08.2017
 * Time: 10:12
 */

use backend\controllers\CustomerController;
use backend\controllers\OrderController;
use backend\models\Url;

$pageId = 19;

require_once __DIR__ . "/../../../backend/modules/app/prepare.php";

//before any action check user and redirect to administration if needed
$customer = CustomerController::isLoggedCustomer();
if(!$customer) {
    header('Location: /prihlaseni/?ret=objednavka', true);
    die();
}

$cartValidation = OrderController::validateCart();
if($cartValidation !== true) {
    header('Location: /kosik/', true);
    die();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="public">
<?php

//public header - categories, pages, cart (search)
require_once Url::getBackendPathTo("/modules/page-parts/header.php");

// loads order to $order variable
require_once Url::getBackendPathTo("/modules/page-parts/customer/order/order-loader.php");

echo '<div class="container-fluid">
            <div class="cart order">';
require_once Url::getBackendPathTo("/modules/page-parts/customer/order/venue/venue-content.php");
echo '</div>';
//require_once Url::getBackendPathTo("/modules/page-parts/footer.php");
echo '</div>';
echo '<div id="overlay"></div>';

echo '<script>';
require_once Url::getPathTo("/js/admin/navigation-drawer.js");
require_once Url::getPathTo("/js/customer/order/venue-contact-save.js");
require_once Url::getPathTo("/js/customer/order/venue-save.js");
require_once Url::getPathTo("/js/customer/order/count-venue-data.js");
require_once Url::getPathTo("/js/customer/order/validate-venue.js");
echo '</script>';
require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
?>
</body>