<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 16.09.2017
 * Time: 17:32
 */

use backend\controllers\CustomerController;
use backend\models\Url;

$pageId = 22;

require_once __DIR__ . "/../../backend/modules/app/prepare.php";

//before any action check user and redirect to administration if needed
$customer = CustomerController::isLoggedCustomer();
if(!$customer){
    header('Location: /prihlaseni/', true);
    die();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="public">
<?php

//public header - categories, pages, cart (search)
require_once Url::getBackendPathTo("/modules/page-parts/header.php");
require_once Url::getBackendPathTo("/modules/page-parts/customer/navigation.php");

echo '<div class="container-fluid">
            <div class="row customer panel order-list">';
require_once Url::getBackendPathTo("/modules/page-parts/customer/order-list/order-list-content.php");
echo '</div>';
//require_once Url::getBackendPathTo("/modules/page-parts/footer.php");
echo '</div>';
echo '<div id="overlay"></div>';

echo '<script>';
require_once Url::getPathTo("/js/admin/navigation-drawer.js");
require_once Url::getPathTo("/js/order/cancel-order.js");
echo '</script>';
require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

require_once Url::getBackendPathTo("/modules/modals/order-delete.php");

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
?>
</body>
</html>