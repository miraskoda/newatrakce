<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 05.12.2017
 * Time: 19:05
 */

use backend\controllers\CustomerController;
use backend\models\Url;

$pageId = 29;

require_once __DIR__ . "/../../backend/modules/app/prepare.php";

//before any action check user and redirect to administration if needed
$customer = CustomerController::isLoggedCustomer();
if(!$customer){
	header('Location: /prihlaseni/', true);
	die();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="public">
<?php

//public header - categories, pages, cart (search)
require_once Url::getBackendPathTo("/modules/page-parts/header.php");
require_once Url::getBackendPathTo("/modules/page-parts/customer/navigation.php");

echo '<div class="container-fluid">
            <div class="row customer panel order-list">';
require_once Url::getBackendPathTo("/modules/page-parts/customer/reservation-list/reservation-list-content.php");
echo '</div>';
//require_once Url::getBackendPathTo("/modules/page-parts/footer.php");
echo '</div>';
echo '<div id="overlay"></div>';

echo '<script>';
require_once Url::getPathTo("/js/admin/navigation-drawer.js");
require_once Url::getPathTo("/js/customer/reservation/reservation-delete.min.js");
echo '</script>';
require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);

// modals
require Url::getBackendPathTo("/modules/modals/reservation-cart.php");
require_once Url::getBackendPathTo("/admin/modules/modals/delete-confirm.php");
?>
</body>
</html>