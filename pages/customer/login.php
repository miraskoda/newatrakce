<?php

use backend\controllers\CustomerController;
use backend\models\Url;

$pageId = 12;

require_once __DIR__ . "/../../backend/modules/app/prepare.php";

//before any action check user and redirect to administration if needed
$customer = CustomerController::isLoggedCustomer();
if($customer) {
    header('Location: ../../muj-ucet/', true);
    die();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="public">
<?php

//public header - categories, pages, cart (search)
require_once Url::getBackendPathTo("/modules/page-parts/header.php");

    echo '<div class="container-fluid">
            <div class="row customer">';
            require_once Url::getBackendPathTo("/modules/page-parts/customer/login/alert.php");
            require_once Url::getBackendPathTo("/modules/page-parts/customer/login/login-content.php");
            require_once Url::getBackendPathTo("/modules/page-parts/customer/login/registration-content.php");
            echo '</div>';
        require_once Url::getBackendPathTo("/modules/page-parts/footer.php");
    echo '</div>';
echo '<div id="overlay"></div>';

echo '<script>';
require_once Url::getPathTo("/js/register-switch.min.js");
require_once Url::getPathTo("/js/customer/login.js");
require_once Url::getPathTo("/js/customer/register.js");
require_once Url::getPathTo("/js/customer/forgotten-password.min.js");
echo '</script>';
require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
?>
</body>
</html>