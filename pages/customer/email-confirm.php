<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 08.08.2017
 * Time: 20:05
 */

use backend\models\Url;

$pageId = 15;

require_once __DIR__ . "/../../backend/modules/app/prepare.php";

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="public">
<?php

//public header - categories, pages, cart (search)
require_once Url::getBackendPathTo("/modules/page-parts/header.php");

echo '<div class="container-fluid">
            <div class="row customer panel">';
require_once Url::getBackendPathTo("/modules/page-parts/customer/email-confirm/email-confirm-content.php");
echo '</div>';
//require_once Url::getBackendPathTo("/modules/page-parts/footer.php");
echo '</div>';
echo '<div id="overlay"></div>';

echo '<script>';
echo '</script>';
require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
?>
</body>
</html>
