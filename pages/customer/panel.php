<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 01.08.2017
 * Time: 15:20
 */

use backend\controllers\CustomerController;
use backend\models\Url;

$pageId = 13;

require_once __DIR__ . "/../../backend/modules/app/prepare.php";

//before any action check user and redirect to administration if needed
$customer = CustomerController::isLoggedCustomer();
if(!$customer){
    header('Location: /prihlaseni/', true);
    die();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="public">
<?php

//public header - categories, pages, cart (search)
require_once Url::getBackendPathTo("/modules/page-parts/header.php");
require_once Url::getBackendPathTo("/modules/page-parts/customer/navigation.php");

echo '<div class="container-fluid">
            <div class="row customer panel">';
    require_once Url::getBackendPathTo("/modules/page-parts/customer/panel/panel-content.php");
    echo '</div>';
//require_once Url::getBackendPathTo("/modules/page-parts/footer.php");
echo '</div>';
echo '<div id="overlay"></div>';

echo '<script>';
require_once Url::getPathTo("/js/customer/email-confirm-resend.min.js");
require_once Url::getPathTo("/js/admin/navigation-drawer.js");
echo '</script>';
require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
?>
</body>
</html>