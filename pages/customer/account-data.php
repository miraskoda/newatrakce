<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 02.08.2017
 * Time: 18:50
 */

use backend\controllers\CustomerController;
use backend\models\Url;

$pageId = 14;

require_once __DIR__ . "/../../backend/modules/app/prepare.php";

//before any action check user and redirect to administration if needed
$customer = CustomerController::isLoggedCustomer();
if(!$customer){
    header('Location: /prihlaseni/', true);
    die();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="public">
<?php

//public header - categories, pages, cart (search)
require_once Url::getBackendPathTo("/modules/page-parts/header.php");
require_once Url::getBackendPathTo("/modules/page-parts/customer/navigation.php");

echo '<div class="container-fluid">
            <div class="row customer panel">';
        require_once Url::getBackendPathTo("/modules/page-parts/customer/account-data/account-data-content.php");
        echo '</div>';
//require_once Url::getBackendPathTo("/modules/page-parts/footer.php");
echo '</div>';
echo '<div id="overlay"></div>';

echo '<script>';
require_once Url::getPathTo("/js/register-switch.min.js");
require_once Url::getPathTo("/js/customer/account-data/company-switch.min.js");
require_once Url::getPathTo("/js/customer/account-data/load-ares-data.min.js");
require_once Url::getPathTo("/js/customer/account-data/basic-data-save.min.js");
require_once Url::getPathTo("/js/customer/account-data/other-data-save.min.js");
require_once Url::getPathTo("/js/customer/account-data/account-data-save.min.js");
require_once Url::getPathTo("/js/customer/account-data/invoice-data-save.min.js");
require_once Url::getPathTo("/js/customer/account-data/delivery-data-save.min.js");
require_once Url::getPathTo("/js/customer/account-data/password-change.min.js");
require_once Url::getPathTo("/js/admin/navigation-drawer.js");
echo 'function refreshMasonry() {
        setTimeout(function () {
            $(\'.grid\').masonry(\'layout\');
        }, 500);
    }';
echo '</script>';
require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
?>
</body>
</html>