<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 14.07.2017
 * Time: 1:44
 */

use backend\controllers\CustomerController;
use backend\models\Url;

$pageId = 10;

require_once __DIR__ . "/../backend/modules/app/prepare.php";

$customer = CustomerController::isLoggedCustomer();

// loads category data to $category variable
require_once Url::getBackendPathTo("/modules/page-parts/products/products-pager.php");
require_once Url::getBackendPathTo("/modules/page-parts/product-filter.php");
require_once Url::getBackendPathTo("/modules/page-parts/products/products-loader.php");

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="public">
<?php

//public header - categories, pages, cart (search)
require_once Url::getBackendPathTo("/modules/page-parts/header.php");

echo '<div class="container-fluid">';

require_once Url::getBackendPathTo("/modules/page-parts/products/products-content.php");
require_once Url::getBackendPathTo("/modules/page-parts/footer.php");

echo '</div>';
echo '<div id="overlay"></div>';

echo '<script>';
require_once Url::getPathTo("/js/cart/add-to-cart.js");
echo '</script>';
require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

// modals
require_once Url::getBackendPathTo("/modules/modals/cart-action.php");
require_once Url::getBackendPathTo("/modules/modals/reservation.php");
require_once Url::getBackendPathTo("/modules/modals/login-modal.php");
require_once Url::getBackendPathTo("/modules/modals/reservation-success.php");
require_once Url::getBackendPathTo("/modules/modals/user-not-logged.php");
//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
?>
</body>
</html>
