function changeValueByNumber(val) {
    var validValue = parseInt(val, 10);
    if (validValue <= 12 && validValue >= 0) {
        document.getElementById("numberOfHours").value = validValue;
    } else {
        if (validValue > 12)
            document.getElementById("numberOfHours").value = 12;
        else if (validValue < 0)
            document.getElementById("numberOfHours").value = 0;
    }
}
