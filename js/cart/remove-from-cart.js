function initRemoving() {
    $('.cart-list span.cart-remove').click(function (ev) {
        ev.preventDefault();
        setTimeout(function () {
            $('.cart-list').parent().addClass('open');
        }, 1);

        var productId = $(this).closest('li').attr('product-id');
        $(this).closest('li').slideUp();

        var cartCounters = document.querySelectorAll('.cart-list-counter');
        for (var inter in cartCounters) {
            cartCounters[inter].innerHTML = parseInt(cartCounters[inter].innerHTML, 10) - 1 || 0;
        }

        removeProductFromCart(productId)
    });

    $('.admin-cart-list .cart-product span.ti-close').click(function (ev) {
        ev.preventDefault();

        var productId = $(this).closest('.cart-product').attr('product-id');
        $(this).closest('.cart-product').slideUp();

        setTimeout(function () {
            loadCartAdminList();
            loadCartAdminPrice();
        }, 500);

        removeProductFromCart(productId);
    });

    $('.admin-cart-list .additional-cart-product span.ti-close').click(function (ev) {
        ev.preventDefault();

        var productId = $(this).closest('.additional-cart-product').attr('product-id');
        var additionToProductId = $(this).closest('.additional-cart-product').attr('addition-to-product-id');
        $(this).closest('.additional-cart-product').slideUp();

        setTimeout(function () {
            loadCartAdminList();
            loadCartAdminPrice();
        }, 500);

        removeAdditionalProductFromCart(productId, additionToProductId);
    });
}

function removeProductFromCart(productId) {
    if(productId) {
        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/cart/remove-from-cart.php",
            data: {productId: productId}
        }).done(function (data) {
            //alert(JSON.stringify(data));
            if (data === "1") {
                //loadCart();
            } else {
                //alert(JSON.stringify(data));

                var errorModal = $('#cart-alert');
                errorModal.find('.alert-content').text(data);

                errorModal.modal({
                    backdrop: 'static',
                    keyboard: false
                })
                    .one('click', '.go-shopping', function () {
                        errorModal.modal('hide');
                        errorModal.unbind('click');
                    })
                    .one('click', '.go-to-cart', function () {
                        window.location.href = '/kosik/';
                    });
            }
        }).fail(function (data) {
            //alert(JSON.stringify(data));

            var errorModal = $('#cart-alert');
            errorModal.find('.alert-content').text(data);

            errorModal.modal({
                backdrop: 'static',
                keyboard: false
            })
                .one('click', '.go-shopping', function () {
                    errorModal.modal('hide');
                    errorModal.unbind('click');
                })
                .one('click', '.go-to-cart', function () {
                    window.location.href = '/kosik/';
                });
        });
    }
}

function removeAdditionalProductFromCart(productId, additionToProductId) {
    if(productId) {
        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/cart/remove-addition-from-cart.php",
            data: {
                productId: productId,
                additionToProductId: additionToProductId
            }
        }).done(function (data) {
            //alert(JSON.stringify(data));
            if (data === "1") {
                //loadCart();
            } else {
                //alert(JSON.stringify(data));

                var errorModal = $('#cart-alert');
                errorModal.find('.alert-content').text(data);

                errorModal.modal({
                    backdrop: 'static',
                    keyboard: false
                })
                    .one('click', '.go-shopping', function () {
                        errorModal.modal('hide');
                        errorModal.unbind('click');
                    })
                    .one('click', '.go-to-cart', function () {
                        window.location.href = '/kosik/';
                    });
            }
        }).fail(function (data) {
            //alert(JSON.stringify(data));

            var errorModal = $('#cart-alert');
            errorModal.find('.alert-content').text(data);

            errorModal.modal({
                backdrop: 'static',
                keyboard: false
            })
                .one('click', '.go-shopping', function () {
                    errorModal.modal('hide');
                    errorModal.unbind('click');
                })
                .one('click', '.go-to-cart', function () {
                    window.location.href = '/kosik/';
                });
        });
    }
}