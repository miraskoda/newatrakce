$(function () {
    var conditions = $('a.conditions-info');

    conditions.click(function () {
        var condtModal = $('#conditions-info');

        condtModal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .one('click', '.cancel', function () {
                condtModal.modal('hide');
                condtModal.unbind('click');
            });
    });
});