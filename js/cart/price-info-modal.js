$(function () {
    var priceInfoModalCaller = $('a.price-info');

    priceInfoModalCaller.click(function () {
        var priceModal = $('#price-info');

        priceModal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .one('click', '.cancel', function () {
                priceModal.modal('hide');
                priceModal.unbind('click');
            });
    });
});