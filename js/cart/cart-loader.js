$(function () {
    loadCart();
});

function loadCart() {
    var cartList = $('ul.cart-list');

    if(cartList) {
        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/cart/cart-loader.php",
            data: ''
        }).done(function (data) {
            //alert(JSON.stringify(data));

            var products = JSON.parse(data);

            var cartCounters = document.querySelectorAll('.cart-list-counter');
            for (var inter in cartCounters) {
                cartCounters[inter].innerHTML = products.length || 0;
            }

            if(products.length > 0) {
                cartList.empty();

                for (var i in products) {
                    if (products.hasOwnProperty(i)) {
                        //alert(JSON.stringify(productCategories[i]));
                        cartList.append('<li product-id="' + products[i].productId + '">' +
                                            '<div>' +
                                                '<a href="/produkty/' + products[i].nameSlug + '/">' +
                                                    '<img src="/assets/images/products/' + products[i].image + '">' +
                                                    '<span>' + products[i].name + '</span> ' +
                                                '</a>' +
                                                '<span class="ti-close cart-remove"></span>' +
                                            '</div>' +
                                        '</li>');
                    }
                }
            }

            initRemoving();
        }).fail(function (data) {
            alert(JSON.stringify(data));
        });
    }
}