$(function () {

    $(".btn-additional").on('click touchend', function (e) {
        e.preventDefault();

        var modalId = $(this).attr('data-target');
        var successModal = $(modalId);

        successModal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .one('click', '.additional-close', function () {
                successModal.modal('hide');
                successModal.unbind('click');
            })


    });

});