//reservation-customer-agree

$(function () {
    $('.customer-add-reservation').click(function (ev) {
        ev.preventDefault();

        var reservationId = $(this).attr('data-target');
        var productName = $(this).attr('data-name');
        var productQuantity = $(this).attr('data-quantity');
        var productTermStart = $(this).attr('data-term-start');
        var productTermHours = $(this).attr('data-term-hours');

        var successModal = $('#cart-customer-reservation');
        var modalTitle = successModal.find('.modal-title');
        modalTitle.html('Vložení produktu ' + productName + ' do košíku');

        successModal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .one('click', '.reservation-customer-agree', function () {
                addToCart(productName, productQuantity, function (err, message) {
                    if (!err) {
                        return saveCartDateDataWithHoursReservation(productTermStart, productTermStart, productTermHours, function (err, message) {
                            if (!err) {
                                return deleteByReservationId(reservationId, function (err, message) {
                                    if (!err) {
                                        $('#reservation-' + reservationId).remove();
                                        successModal.modal('hide');
                                        successModal.unbind('click');
                                        window.location.href = '/kosik/';
                                    } else {
                                        setInfoStatus('reservation', message, 'warning');
                                    }
                                });
                            } else {
                                setInfoStatus('reservation', message, 'warning');
                            }
                        })
                    } else {
                        setInfoStatus('reservation', message, 'warning');
                    }
                });
            });
    });
});

function addToCart(productId, quantity, cb) {
    return $.ajax({
        type: 'post',
        url: "/../../backend/admin/ajax/cart/add-reservation-to-cart.php",
        data: {productId: productId, quantity: quantity}
    }).done(function (data) {
        if (data === "1") {
            cb(null)
        } else {
            cb(true, data);
        }
    }).fail(function (data) {
        cb(true, data);
    });
}

function deleteByReservationId(reservationId, cb) {
    return $.ajax({
        type: "POST",
        url: "/../../backend/admin/ajax/customer/reservation/reservation-delete.php",
        data: {reservationId: reservationId}
    }).done(function (data) {
        if (data === "1") {
            cb(null);
        } else {
            cb(false, JSON.stringify(data));
        }
    }).fail(function (data) {
        cb(false, JSON.stringify(data));
    });
}

function saveCartDateDataWithHoursReservation(date, termNo, rentHours, cb) {
    return $.ajax({
        type: 'post',
        url: "/../../backend/admin/ajax/cart/admin/cart-dates-save.php",
        data: {start: date, termNo: 1, rentHours: rentHours, fromReservation: true}
    }).done(function () {
        cb(null);
    }).fail(function (data) {
        cb(true, data);
    });
}
