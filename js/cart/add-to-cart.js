$(function () {
    $('.buy a').click(function (ev) {
        ev.preventDefault();

        var rentTime = $('#availableTermStart').val();
        var rentHours = $('#availableTermHours').val();

        var productId = $(this).closest('.item-box').attr('product-id');
        if(!productId)
            productId = $(this).closest('.actions').attr('product-id');

        if(productId) {
            $.ajax({
                type: 'post',
                url: "/../../backend/admin/ajax/cart/add-to-cart.php",
                data: {productId: productId}
            }).done(function (data) {
                //alert(JSON.stringify(data));
                if (data === "1") {
                    loadCart();
                    saveCartDateDataWithHours(rentTime, 1, [rentHours]);
                    var successModal = $('#cart-action');

                    successModal.modal({
                        backdrop: 'static',
                        keyboard: false
                    })
                    .one('click', '.go-shopping', function () {
                        successModal.modal('hide');
                        successModal.unbind('click');
                    })
                    .one('click', '.go-to-cart', function () {
                        window.location.href = '/kosik/';
                    });
                } else {
                    //alert(JSON.stringify(data));
                    saveCartDateDataWithHours(rentTime, 1, [rentHours]);

                    var errorModal = $('#cart-alert');
                    errorModal.find('.alert-content').text(data);

                    errorModal.modal({
                        backdrop: 'static',
                        keyboard: false
                    })
                    .one('click', '.go-shopping', function () {
                        errorModal.modal('hide');
                        errorModal.unbind('click');
                    })
                    .one('click', '.go-to-cart', function () {
                        window.location.href = '/kosik/';
                    });
                }
            }).fail(function (data) {
                //alert(JSON.stringify(data));

                var errorModal = $('#cart-alert');
                errorModal.find('.alert-content').text(data);

                errorModal.modal({
                    backdrop: 'static',
                    keyboard: false
                })
                .one('click', '.go-shopping', function () {
                    errorModal.modal('hide');
                    errorModal.unbind('click');
                })
                .one('click', '.go-to-cart', function () {
                    window.location.href = '/kosik/';
                });
            });
        }
    });

    $('#addons input[type=checkbox]').change(function () {
        var li = $(this).closest('li');
        var productId = li.attr('product-id');
        var additionTo = li.attr('addition-to');

        if(productId && additionTo) {
            var checkbox = $(this);
            if($(this).is(':checked')) {
                $.ajax({
                    type: 'post',
                    url: "/../../backend/admin/ajax/cart/add-addition-to-cart.php",
                    data: {
                        productId: productId,
                        additionToProductId: additionTo
                    }
                }).done(function (data) {
                    //alert(JSON.stringify(data));
                    if (data === "1") {
                        loadCart();
                        setInfoStatus('additional-product', 'Produkt byl úspěšně přidán jako doplněk do košíku.', 'success');
                    } else {
                        setInfoStatus('additional-product', JSON.stringify(data), 'warning');
                        checkbox.prop('checked', false);
                    }
                }).fail(function (data) {
                    setInfoStatus('additional-product', JSON.stringify(data), 'danger');
                    checkbox.prop('checked', false);
                });
            } else {
                $.ajax({
                    type: 'post',
                    url: "/../../backend/admin/ajax/cart/remove-addition-from-cart.php",
                    data: {
                        productId: productId,
                        additionToProductId: additionTo
                    }
                }).done(function (data) {
                    //alert(JSON.stringify(data));
                    if (data === "1") {
                        loadCart();
                        setInfoStatus('additional-product', 'Doplněk byl úspěšně odebrán z košíku.', 'success');
                    } else {
                        setInfoStatus('additional-product', JSON.stringify(data), 'warning');
                        checkbox.prop('checked', true);
                    }
                }).fail(function (data) {
                    setInfoStatus('additional-product', JSON.stringify(data), 'danger');
                    checkbox.prop('checked', true);
                });
            }
        }
    });
});
