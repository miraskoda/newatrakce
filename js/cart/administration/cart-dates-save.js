$(function () {
    setTimeout(function () {
        saveCartDates();
    }, 500);
});

function saveCartDates() {
    var datesInput = $('.date-container').find('input');
    //datesInput.unbind('change focusout');

    datesInput.on('change focusout', function () {
        var date = $(this).val();
        var termNo = $(this).closest('.date-content').attr('date-no');

        saveCartDateData(date, termNo);
    });
}

function saveCartDateData(date, termNo) {
    if(date && termNo) {
        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/cart/admin/cart-dates-save.php",
            data: {start: date, termNo: termNo, rentHours: rentHours}
        }).done(function (data) {
            // no action needed
        }).fail(function (data) {
            alert(JSON.stringify(data));
        });
    }
}

function saveCartDateDataWithHours(date, termNo, rentHours) {
    if(date && termNo && rentHours) {
        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/cart/admin/cart-dates-save.php",
            data: {start: date, termNo: termNo, rentHours: rentHours}
        }).done(function (data) {
            console.log(data);
            // no action needed
        }).fail(function (data) {
            alert(JSON.stringify(data));
        });
    }
}