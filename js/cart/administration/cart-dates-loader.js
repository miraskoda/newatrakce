$(function () {
    var dateNumber = 1;
    var cartDelete = document.querySelector('.remove-date');
    loadCartAdminDates(dateNumber);

    $.ajax({
        type: 'post',
        url: "/../../backend/admin/ajax/cart/admin/get-dates-count.php",
        data: ''
    }).done(function (data) {
        //alert(JSON.stringify(data));
        if(jQuery.isNumeric(data) && data > 1) {
            for (var i = 2; i <= data; i++) {
                loadCartAdminDates(i);
                dateNumber++;
            }
        }
    }).fail(function (data) {
        alert(JSON.stringify(data));
    });

    $('.add-date').click(function () {
        if(dateNumber < 5) {
            dateNumber++;
            loadCartAdminDates(dateNumber);
            if (dateNumber > 1) {
                cartDelete.setAttribute("style", "display: block;");
            }
        }
    });

    $('.remove-date').click(function () {
        if(dateNumber > 1) {
            rentHours[dateNumber-1] = 0;
            removeCartAdminDates(dateNumber);
            dateNumber--;
            if (dateNumber === 1) {
                cartDelete.setAttribute("style", "display: none;");
            }
        }
    });
});

function loadCartAdminDates(number) {
    var cartDates = $('.date-blocks');
    var cartDelete = document.querySelector('.remove-date');
    cartDelete.setAttribute("style", "display: none;");

    if(cartDates) {
        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/cart/admin/cart-dates-loader.php",
            data: {number: number}
        }).done(function (data) {
            //alert(JSON.stringify(data));
            if(number === 1) {
                cartDates.html(data);
            }
            else {
                cartDates.append(data);
                cartDelete.setAttribute("style", "display: block;");
            }
            setDateCounters();
            initDateTimePickers();
            saveCartDates();
        }).fail(function (data) {
            alert(JSON.stringify(data));
        });
    }
}

function removeCartAdminDates(number) {
    var dateToRemove = $('.date-blocks div.date-content[date-no='+number+']');
    var dateTitle = dateToRemove.prev('.date-title');

    if(dateToRemove && dateTitle) {
        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/cart/admin/cart-dates-remove.php",
            data: ''
        }).done(function (data) {
            // alert(JSON.stringify(data));
            if(data === '1') {
                dateToRemove.slideUp();
                dateTitle.slideUp();

                setTimeout(function () {
                    dateToRemove.remove();
                    dateTitle.remove();
                    setDateCounters();
                    saveCartDates();
                }, 500);
            } else {
                alert(data);
            }
        }).fail(function (data) {
            alert(JSON.stringify(data));
        });
    }
}