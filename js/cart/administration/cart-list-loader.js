$(function () {
    loadCartAdminList();
});

function loadCartAdminList() {
    var cartList = $('.admin-cart-list');

    if (cartList) {
        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/cart/admin/cart-loader.php",
            data: {rentHours: rentHours}
        }).done(function (data) {
            //alert(JSON.stringify(data));
            cartList.html(data);

            $(".btn-additional").on('click touchend', function (e) {
                e.preventDefault();

                var successModal = $('.modal-additional');
                var contentBody = successModal.find('.modal-body');
                var contentTitle = successModal.find('.modal-title');

                var productId = $(this).attr('data-target');
                var productName = $(this).attr('data-name');

                if (productId) {
                    $.ajax({
                        type: 'post',
                        url: "/../../backend/admin/ajax/cart/admin/additional-cart-loader.php",
                        data: {productId: productId}
                    }).done(function (data) {
                        // alert(JSON.stringify(data));
                        contentBody.html(data);
                        contentTitle.html('Vyberte doplňující položky pro ' + productName);

                        successModal.modal({
                            backdrop: 'static',
                            keyboard: false
                        }).one('click', '.additional-close', function () {
                            successModal.modal('hide');
                            successModal.unbind('click');
                        });

                        handleAddAdditional();
                    }).fail(function (data) {
                        alert(JSON.stringify(data));
                    });
                }
            });

            saveCartQuantity();
            initRemoving();
        }).fail(function (data) {
            alert(JSON.stringify(data));
        });
    }
}

function handleAddAdditional() {
    $('#addons-modal input[type=checkbox]').change(function () {
        var li = $(this).closest('li');
        var productId = li.attr('product-id');
        var additionTo = li.attr('addition-to');

        if (productId && additionTo) {
            var checkbox = $(this);
            if ($(this).is(':checked')) {
                $.ajax({
                    type: 'post',
                    url: "/../../backend/admin/ajax/cart/add-addition-to-cart.php",
                    data: {
                        productId: productId,
                        additionToProductId: additionTo
                    }
                }).done(function (data) {
                    //alert(JSON.stringify(data));
                    if (data === "1") {
                        loadCart();
                        setInfoStatus('additional-product-cart', 'Produkt byl úspěšně přidán jako doplněk do košíku.', 'success');
                        loadCartAdminList();
                    } else {
                        setInfoStatus('additional-product-cart', JSON.stringify(data), 'warning');
                        checkbox.prop('checked', false);
                    }
                }).fail(function (data) {
                    setInfoStatus('additional-product-cart', JSON.stringify(data), 'danger');
                    checkbox.prop('checked', false);
                });
            } else {
                $.ajax({
                    type: 'post',
                    url: "/../../backend/admin/ajax/cart/remove-addition-from-cart.php",
                    data: {
                        productId: productId,
                        additionToProductId: additionTo
                    }
                }).done(function (data) {
                    //alert(JSON.stringify(data));
                    if (data === "1") {
                        loadCart();
                        setInfoStatus('additional-product-cart', 'Doplněk byl úspěšně odebrán z košíku.', 'success');
                        loadCartAdminList();
                    } else {
                        setInfoStatus('additional-product-cart', JSON.stringify(data), 'warning');
                        checkbox.prop('checked', true);
                    }
                }).fail(function (data) {
                    setInfoStatus('additional-product-cart', JSON.stringify(data), 'danger');
                    checkbox.prop('checked', true);
                });
            }
        }
    });
}
