$(function () {
    var conditionsP = $('a.person-info');

    conditionsP.click(function () {
        var condtPModal = $('#person-info');

        condtPModal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .one('click', '.cancel', function () {
                condtPModal.modal('hide');
                condtPModal.unbind('click');
            });
    });
});