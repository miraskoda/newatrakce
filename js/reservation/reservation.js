$(function () {
    var availableTermHours = document.querySelector('#availableTermHours');
    var availableTermStart = document.querySelector('#availableTermStart');
    var reservationHours = document.querySelector('#reservationHours');
    if(reservationHours) {
        reservationHours.addEventListener('input', function (e) {
            var validValue = parseInt(e.target.value, 10);
            if (validValue > 0 && validValue < 48) {
                reservationHours.value = validValue;
            } else {
                if (e.target.value === "") {
                    reservationHours.value = 0;
                } else {
                    reservationHours.value = 48;
                }
            }
        });
    }
    if(availableTermHours) {
        availableTermHours.addEventListener('input', function (e) {
            var validValue = parseInt(e.target.value, 10);
            if (validValue > 0 && validValue < 48) {
                availableTermHours.value = validValue;
            } else {
                if (e.target.value === "") {
                    availableTermHours.value = 0;
                } else {
                    availableTermHours.value = 48;
                }
            }
        });
    }
    $('.reserve').click(function (ev) {
        ev.preventDefault();

        var productId = $(this).closest('.item-box').attr('product-id');
        if(!productId)
            productId = $(this).closest('.actions').attr('product-id');

        // check if user is logged in
        $.ajax({
            type: "POST",
            url: "/../../backend/admin/ajax/customer/logged.php",
            data: {}
        }).done(function(data) {
            //alert(JSON.stringify(data));
            if(data === "1") {
                var reservationModal = $('#reservation');
                var termStart = document.querySelector('#termStart');
                var termHours = document.querySelector('#termHours');
                termStart.value = availableTermStart.value;
                termHours.value = availableTermHours.value;
                reservationModal.modal({
                    backdrop: 'static',
                    keyboard: false
                })
                    .on('click', '.save', function () {
                        $.ajax({
                            type: "POST",
                            url: "/../../backend/admin/ajax/reservation/reservation-create.php",
                            data: {
                                productId: productId,
                                quantity: reservationModal.find('input[name=quantity]').val(),
                                termStart: reservationModal.find('input[name=termStart]').val(),
                                termHours: reservationModal.find('input[name=termHours]').val(),
                                reservationHours: reservationModal.find('input[name=reservationHours]').val()
                            }
                        }).done(function(data) {
                            if(data === "1") {
                                // close reservation form
                                reservationModal.modal('hide');
                                reservationModal.unbind('click');

                                // show success modal
                                var reservationSuccessModal = $('#reservation-success');
                                reservationSuccessModal.modal({
                                    backdrop: 'static',
                                    keyboard: false
                                })
                                .one('click', '.reservations', function () {
                                    window.location.href = '/prehled-rezervaci/';
                                })
                                .one('click', '.cancel', function () {
                                    reservationSuccessModal.modal('hide');
                                    reservationSuccessModal.unbind('click');
                                });
                            } else {
                                setInfoStatus('reservation', data, 'warning');
                            }
                        }).fail(function(data) {
                            //alert(JSON.stringify(data));
                        });
                    })
                    .one('click', '.cancel', function () {
                        reservationModal.modal('hide');
                        reservationModal.unbind('click');
                    });
            } else {
                var userNotLoggedModal = $('#login-modal');

                userNotLoggedModal.modal({
                    backdrop: 'static',
                    keyboard: false
                })
                    .one('click', '.cancel', function () {
                        userNotLoggedModal.modal('hide');
                        userNotLoggedModal.unbind('click');
                    });
            }
        }).fail(function(data) {
            //alert(JSON.stringify(data));
        });
    });
});