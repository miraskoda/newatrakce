$(function () {
    var loginForm = $('.login-form form');

    loginForm.submit(function (ev) {
        ev.preventDefault();
        var alertDiv = loginForm.children('.action-alert');

        // password validation
        var password = loginForm.find('input[name=password]');
        if(password.val().length < 6){
            showWarning(alertDiv, '<p>Heslo je příliš krátké</p>');
            return;
        }
        requestLogin(loginForm.serialize(), alertDiv);
    });
});

function showWarning(alertDiv, data) {
    alertDiv.html(data).removeClass('hidden').addClass('bounceIn').addClass('infinite').animate();
    setTimeout(function () {
        alertDiv.removeClass('infinite');
    }, 750);
}

function requestLogin(formData, alertDiv) {
    $.ajax({
        type: 'post',
        url: "/../../backend/admin/ajax/customer/login.php",
        data: replaceRegisterWithLogin(formData)
    }).done(function(data) {
        // alert(JSON.stringify(data));
        if(data === "1") {
            console.log(data);
            if (window.location.href.toLowerCase().indexOf("?ret=objednavka") >= 0)
                window.location.replace('/objednavka/misto-konani/');
            else
                window.location.replace('/muj-ucet/');
        } else {
            showWarning(alertDiv, data);
        }
    }).fail(function(data) {
        //alert(JSON.stringify(data));
        if(alertDiv) {
            alertDiv.text(JSON.stringify(data)).removeClass('hidden').animate();
        }
    });
}

function replaceRegisterWithLogin(string) {
    return string.split('register-').join('');
}
