$(function () {
    var resendButton = $('a.confirm-email-resend');
   resendButton.click(function () {
       $.ajax({
           type: 'post',
           url: "/../../backend/admin/ajax/customer/email-confirm-resend.php",
           data: ''
       }).done(function(data) {
           //alert(JSON.stringify(data));
           if(data === "1") {
                resendButton.parent().after('<p>Email úspěšně odeslán</p>');
                resendButton.remove();
           } else {
                alert(JSON.stringify(data));
           }
       }).fail(function(data) {
           alert(JSON.stringify(data));
       });
   });
});