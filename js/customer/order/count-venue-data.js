$(function () {
    countVenueData();
});




function countVenueData() {




    var validate = document.querySelector('.validate-venue');
    var validateError = document.querySelector('.alert-venue');
    $.ajax({
        type: 'post',
        url: "/../../backend/admin/ajax/customer/order/venue-get.php",
        data: ''
    }).done(function (data) {
        // alert(JSON.stringify(data));
        var venue = JSON.parse(data);
        if (venue) {
            if (venue.status === 'OK') {
                var elementStatus = venue.rows[0].elements[0].status;
                if(elementStatus === 'OK') {
                    validate.setAttribute('data-valid', 'valid');
                    validateError.classList.remove('display-venue-error');
                    validate.classList.remove('disabled-btn');
                    $('.travel-length').text(venue.rows[0].elements[0].distance.text);

                    var durationValue = (venue.rows[0].elements[0].duration.value * 1.15);
                    var quarters = Math.floor(durationValue / 900);
                    var remainder = durationValue % 900;
                    if(remainder > 0.1)
                        quarters++;

                    var durationText = "";
                    var hours = Math.floor(quarters / 4);
                    if(hours > 0)
                        durationText += hours + ' ' + getCzechHours(hours) + ' ';

                    var minutes = Math.round((quarters % 4) * 15);
                    if(minutes > 0)
                        durationText += minutes + ' ' + getCzechMinutes(minutes);

                    $('.travel-time').text(durationText);
                    $('.travel-price').text(Math.round((venue.rows[0].elements[0].distance.value / 1000) * 2 * 6) + ' Kč');
                } else {
                    validate.setAttribute('data-valid', 'invalid');
                    validateError.classList.add('display-venue-error');
                    validate.classList.add('disabled-btn');
                    setInfoStatus('venue-price', 'Nepodařilo se vypočítat cestu. Prosím zkontolujte zadanou adresu.', 'danger');
                }
            } else if(venue.status === 'OVER_QUERY_LIMIT') {
                setInfoStatus('venue-price', 'Služba přesáhla počet požadavků. Prosím kontaktujte zákaznickou podporu. Děkujeme.', 'danger');
            } else {
                setInfoStatus('venue-price', JSON.stringify(data), 'danger');
            }
        }
    }).fail(function (data) {
        setInfoStatus('venue-price', JSON.stringify(data), 'danger');
    });
}

function getCzechHours(hours) {
    switch(hours) {
        case 1:
            return 'hodina';
            break;
        case 2:
        case 3:
        case 4:
            return 'hodiny';
            break;
        default:
            return 'hodin';
            break;

    }
}

function getCzechMinutes(minutes) {
    switch(minutes) {
        case 1:
            return 'minuta';
            break;
        case 2:
        case 3:
        case 4:
            return 'minuty';
            break;
        default:
            return 'minut';
            break;

    }
}