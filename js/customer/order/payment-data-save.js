$(function () {
    var radioInputs = $('input[type=radio]');

    radioInputs.change(function (ev) {
        ev.preventDefault();

        var name = $(this).attr('name');
        var value = $(this).attr('value');

        var data = {};
        data[name] = value;

        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/customer/order/payment-data-save.php",
            data: data
        }).done(function (data) {
            //alert(JSON.stringify(data));
            if (data === "1") {
                setInfoStatus('payment-data', 'Data uložena', 'success');
            } else {
                setInfoStatus('payment-data', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('payment-data', JSON.stringify(data), 'danger');
        });
    });

    // customer note
    $('textarea[name=customer-note]').change(function () {
        var customerNote = $(this).val();

        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/customer/order/customer-note-save.php",
            data: {
                customerNote : customerNote
            }
        }).done(function (data) {
            //alert(JSON.stringify(data));
            if (data === "1") {
                setInfoStatus('customer-note', 'Data uložena', 'success');
            } else {
                setInfoStatus('customer-note', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('customer-note', JSON.stringify(data), 'danger');
        });
    });
});