$(function () {
    var venueDataForm = $('form#venue-data');
    var venueDataInputs = venueDataForm.find('input, select');

    venueDataInputs.each(function () {
        $(this).change(function () {
            // autosave
            venueDataForm.submit();
        });
    });

    venueDataForm.submit(function (ev) {
        ev.preventDefault();

        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/customer/order/venue-save.php",
            data: venueDataForm.serialize()
        }).done(function (data) {
            //alert(JSON.stringify(data));
            if (data === "1") {
                // setInfoStatus('venue-data', 'Data uložena', 'success');
                countVenueData();
            } else {
                setInfoStatus('venue-data', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('venue-data', JSON.stringify(data), 'danger');
        });
    });

    venueDataForm.submit();
});