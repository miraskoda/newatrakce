$(function () {
    var validateVenueButton = $('a.validate-venue');
    var validate = document.querySelector('.validate-venue');
    var validateError = document.querySelector('.alert-venue');
    validateVenueButton.click(function () {
        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/customer/order/validate-venue.php",
            data: ''
        }).done(function (data) {
            //alert(JSON.stringify(data));
            if (data === "1") {
                if (validate.getAttribute('data-valid') === 'valid') {
                    validateError.classList.remove('display-venue-error');
                    window.location.href = '/objednavka/udaje/';
                } else {
                    validateError.classList.add('display-venue-error');
                }
            } else {
                setInfoStatus('validate-venue', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('validate-venue', JSON.stringify(data), 'danger');
        });
    });
});