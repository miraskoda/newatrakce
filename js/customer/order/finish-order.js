$(function () {
    var finishOrderButton = $('.finish-order');
    /*var personalDataCheckbox = $('input[name=personal-data]');
    var vopCheckbox = $('input[name=vop]');*/

    finishOrderButton.click(function () {
        // agreements
        /*if(personalDataCheckbox.is(':checked')){
            if(vopCheckbox.is(':checked')){*/
                // validate cart
                $.ajax({
                    type: 'post',
                    url: "/../../backend/admin/ajax/cart/validate-cart.php",
                    data: ''
                }).done(function (data) {
                    //alert(JSON.stringify(data));
                    if (data === "1") {
                        // validate venue
                        $.ajax({
                            type: 'post',
                            url: "/../../backend/admin/ajax/customer/order/validate-venue.php",
                            data: ''
                        }).done(function (data) {
                            //alert(JSON.stringify(data));
                            if (data === "1") {
                                // validate payment
                                $.ajax({
                                    type: 'post',
                                    url: "/../../backend/admin/ajax/customer/order/validate-payment.php",
                                    data: ''
                                }).done(function (data) {
                                    //alert(JSON.stringify(data));
                                    if (data === "1") {
                                        // everything validated
                                        window.location.href = '/objednavka/dokonceni/';
                                    } else {
                                        setInfoStatus('validate-payment', JSON.stringify(data), 'warning');
                                    }
                                }).fail(function (data) {
                                    setInfoStatus('validate-payment', JSON.stringify(data), 'danger');
                                });
                            } else {
                                setInfoStatus('finish-order', JSON.stringify(data), 'warning');
                            }
                        }).fail(function (data) {
                            setInfoStatus('finish-order', JSON.stringify(data), 'danger');
                        });
                    } else {
                        setInfoStatus('finish-order', JSON.stringify(data), 'warning');
                    }
                }).fail(function (data) {
                    setInfoStatus('finish-order', JSON.stringify(data), 'danger');
                });
            /*} else {
                setInfoStatus('finish-order', 'Pro dokončení objednávky musíte souhlasit s obchodními podmínkami.', 'warning');
            }
        } else {
            setInfoStatus('finish-order', 'Pro dokončení objednávky musíte souhlasit se zpracováním osobních údajů.', 'warning');
        }*/
    });
});