$(function () {
    var venueContactDataForm = $('form#contact-data');
    var venueContactDataInputs = venueContactDataForm.find('input, select');

    venueContactDataInputs.each(function () {
        $(this).change(function () {
            // autosave
            venueContactDataForm.submit();
        });
    });

    venueContactDataForm.submit(function (ev) {
        ev.preventDefault();

        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/customer/order/venue-contact-save.php",
            data: venueContactDataForm.serialize()
        }).done(function (data) {
            //alert(JSON.stringify(data));
            if (data === "1") {
                // setInfoStatus('contact-data', 'Data uložena', 'success');
            } else {
                setInfoStatus('contact-data', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('contact-data', JSON.stringify(data), 'danger');
        });
    });

    venueContactDataForm.submit();
});