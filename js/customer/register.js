window.onScriptLoad = function () {
    // this callback will be called by recaptcah/api.js once its loaded. If we used
    // render=explicit as param in script src, then we can explicitly render reCaptcha at this point

    // element to "render" invisible captcha in
    var htmlEl = document.querySelector('.g-recaptcha');

    // option to captcha
    var captchaOptions = {
        sitekey: '6LevNCsUAAAAAIW1I0GOBn1WPsYFeEnsQM9NzhND',
        size: 'invisible',
        // tell reCaptcha which callback to notify when user is successfully verified.
        // if this value is string, then it must be name of function accessible via window['nameOfFunc'],
        // and passing string is equivalent to specifying data-callback='nameOfFunc', but it can be
        // reference to an actual function
        callback: window.onUserVerified
    };

    // Only for "invisible" type. if true, will read value from html-element's data-* attribute if its not passed via captchaOptions
    var inheritFromDataAttr = true;

    // now render
    recaptchaId = window.grecaptcha.render(htmlEl, captchaOptions, inheritFromDataAttr);
};

// this is assigned from "data-callback" or render()'s "options.callback"
window.onUserVerified = function (token) {
    //alert('User Is verified');
    console.log('token=', token);
};

var registerForm = $('.register-form form');
var registerFormSubmitButton = registerForm.find('button.g-recaptcha');

// click handler for form's submit button
registerFormSubmitButton.click(function (ev) {
    registerForm.submit(function (ev) {
        ev.preventDefault();
    });

    var token = window.grecaptcha.getResponse(recaptchaId);

    // if no token, mean user is not validated yet
    if (!token) {
        // trigger validation
        window.grecaptcha.execute(recaptchaId);
        //return;
    }

    var alertDiv = registerForm.children('.action-alert');
    alertDiv.removeClass('alert-success').addClass('alert-danger');

    $.ajax({
        type: registerForm.attr('method'),
        url: "/../../backend/admin/ajax/customer/register.php",
        data: registerForm.serialize()
    }).done(function(data) {
        if(data === "1") {
            alertDiv.removeClass('alert-danger').addClass('alert-success');
            showWarning(alertDiv, '<p>Registrace úspěšná. Nyní se můžete přihlásit.</p>');

            requestLogin(registerForm.serialize(), alertDiv);
        } else {
            // refresh token
            window.grecaptcha.reset();
            window.grecaptcha.execute(recaptchaId);

            showWarning(alertDiv, data);
        }
    }).fail(function(data) {
        //alert(JSON.stringify(data));
        alertDiv.text(JSON.stringify(data)).removeClass('hidden').animate();
    });
});

function showWarning(alertDiv, data) {
    alertDiv.html(data).removeClass('hidden').addClass('bounceIn').addClass('infinite').animate();
    setTimeout(function () {
        alertDiv.removeClass('infinite');
    }, 750);
}