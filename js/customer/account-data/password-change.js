$(function () {
    var passwordDataForm = $('form#password-data');

    passwordDataForm.submit(function (ev) {
        ev.preventDefault();

        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/customer/account-data/password-change.php",
            data: passwordDataForm.serialize()
        }).done(function (data) {
            //alert(JSON.stringify(data));
            refreshMasonry();
            if (data === "1") {
                setInfoStatus('password-data', 'Data uložena', 'success');
            } else {
                setInfoStatus('password-data', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('password-data', JSON.stringify(data), 'danger');
        });
    });
});