$(function () {
    var accountDataForm = $('form#account-data');
    var accountDataInputs = accountDataForm.find('input');

    accountDataInputs.each(function () {
        $(this).change(function () {
            // autosave
            accountDataForm.submit();
        });
    });

    accountDataForm.submit(function (ev) {
        ev.preventDefault();

        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/customer/account-data/account-data-save.php",
            data: accountDataForm.serialize()
        }).done(function (data) {
            //alert(JSON.stringify(data));
            refreshMasonry();
            if (data === "1") {
                setInfoStatus('account-data', 'Data uložena', 'success');
            } else {
                setInfoStatus('account-data', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('account-data', JSON.stringify(data), 'danger');
        });
    });
});