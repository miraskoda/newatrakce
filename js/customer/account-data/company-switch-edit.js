$(function () {
    var companySwitch = $('#company-switch');

    if(companySwitch.is(':checked')){
        var form = companySwitch.closest('form');
        form.find('.company').show();
        form.find('.person').hide();
    }

    companySwitch.change(function () {
        var form = $(this).closest('form');
        form.find('.company').slideToggle();
        form.find('.person').slideToggle();
    });

    var daliverySwitch = $('#delivery-address-switch');

    if(!daliverySwitch.is(':checked')){
        var deliveryForm = daliverySwitch.closest('form');
        deliveryForm.find('.delivery-address').show();
    }

    daliverySwitch.change(function () {
        var form = $(this).closest('form');
        form.find('.delivery-address').slideToggle();
    });
});