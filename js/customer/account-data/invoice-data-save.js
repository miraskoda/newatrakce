$(function () {
    var invoiceDataForm = $('form#invoice-data');
    var invoiceDataInputs = invoiceDataForm.find('input');

    invoiceDataInputs.each(function () {
        $(this).change(function () {
            // autosave
            invoiceDataForm.submit();
        });
    });

    invoiceDataForm.submit(function (ev) {
        ev.preventDefault();

        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/customer/account-data/invoice-data-save.php",
            data: invoiceDataForm.serialize()
        }).done(function (data) {
            //alert(JSON.stringify(data));
            refreshMasonry();
            if (data === "1") {
                setInfoStatus('invoice-data', 'Data uložena', 'success');
            } else {
                setInfoStatus('invoice-data', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('invoice-data', JSON.stringify(data), 'danger');
        });
    });
});