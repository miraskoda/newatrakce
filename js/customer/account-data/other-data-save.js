$(function () {
    var otherDataForm = $('form#other-data');
    var otherDataInputs = otherDataForm.find('input');

    otherDataInputs.each(function () {
        $(this).change(function () {
            // autosave
            otherDataForm.submit();
        });
    });

    otherDataForm.submit(function (ev) {
        ev.preventDefault();

        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/customer/account-data/other-data-save.php",
            data: otherDataForm.serialize()
        }).done(function (data) {
            //alert(JSON.stringify(data));
            refreshMasonry();
            if (data === "1") {
                setInfoStatus('other-data', 'Data uložena', 'success');
            } else {
                setInfoStatus('other-data', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('other-data', JSON.stringify(data), 'danger');
        });
    });
});