$(function () {
    $('button#ares-load').click(function (ev) {
        ev.preventDefault();

        var icoInput = $('input[name=ico]');
        var icoVal = icoInput.val();
        // validate input
        icoVal = icoVal.replace(/\s+|\D+/g, '');
        icoInput.val(icoVal);

        if(icoInput.val()) {
            $.ajax({
                type: 'get',
                url: "/../../backend/admin/ajax/customer/ares-load.php",
                data: {ico: icoInput.val()}
            }).done(function (data) {
                //var parsedData = JSON.parse(data);
                if (data.status === 200) {
                    var invoiceForm = $('form#invoice-data');
                    if (data.company)
                        invoiceForm.find('input[name=invoice-name]').val(data.company);
                    if (data.street)
                        invoiceForm.find('input[name=invoice-street]').val(data.street);
                    if (data.city)
                        invoiceForm.find('input[name=invoice-city]').val(data.city);
                    if (data.zip)
                        invoiceForm.find('input[name=invoice-zip]').val(data.zip);
                    var dicInput = $('input[name=dic]');
                    if (data.dic && !dicInput.val())
                        dicInput.val(data.dic);

                    invoiceForm.find('input[name=invoice-name]').trigger('change');
                } else if (data.status === 400) {
                    alert('Ares není dostupný. Zkuste to prosím znovu za chvíli.');
                } else if (data.status === 404) {
                    alert('IČO nebylo nalezeno.');
                } else {
                    alert('Nastala chyba. Zkuste to prosím znovu za chvíli.');
                }
            }).fail(function (data) {
                alert(JSON.stringify(data));
            });
        } else {
            alert('Nejprve zadejte IČO');
        }
    });
});