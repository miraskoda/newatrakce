$(function () {
    var deliveryDataForm = $('form#delivery-data');
    var deliveryDataInputs = deliveryDataForm.find('input');

    deliveryDataInputs.each(function () {
        $(this).change(function () {
            // autosave
            deliveryDataForm.submit();
        });
    });

    deliveryDataForm.submit(function (ev) {
        ev.preventDefault();

        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/customer/account-data/delivery-data-save.php",
            data: deliveryDataForm.serialize()
        }).done(function (data) {
            //alert(JSON.stringify(data));
            refreshMasonry();
            if (data === "1") {
                setInfoStatus('delivery-data', 'Data uložena', 'success');
            } else {
                setInfoStatus('delivery-data', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('delivery-data', JSON.stringify(data), 'danger');
        });
    });
});