$(function () {
    var loginForm = $('#login-modal .login-form form');

    loginForm.submit(function (ev) {
        ev.preventDefault();

        var loginModal = $('#login-modal');

        var alertDiv = loginForm.children('.action-alert');

        var password = loginForm.find('input[name=password]');
        if(password.val().length < 6){
            showWarning(alertDiv, '<p>Heslo je příliš krátké</p>');
            return;
        }

        $.ajax({
            type: loginForm.attr('method'),
            url: "/../../backend/admin/ajax/customer/login.php",
            data: loginForm.serialize()
        }).done(function(data) {
            if(data === "1") {
                loginModal.modal('hide');
                loginModal.unbind('click');
                var reservationButton = $('.reserve');
                reservationButton.click();
            } else {
                showWarning(alertDiv, data);
            }
        }).fail(function(data) {
            alertDiv.text(JSON.stringify(data)).removeClass('hidden').animate();
        });
    });

    function showWarning(alertDiv, data) {
        alertDiv.html(data).removeClass('hidden').addClass('bounceIn').addClass('infinite').animate();
        setTimeout(function () {
            alertDiv.removeClass('infinite');
        }, 750);
    }
});
