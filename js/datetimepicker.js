/**
 * Created by Ondra on 24.05.2017.
 */

$(function () {
    initDateTimePickers();
});

function initDateTimePickers() {

    $('.datetimepicker').datetimepicker({
        format: 'DD. MM. YYYY',
        locale: 'cs'
    });



    $('.datetimepickeredit').datetimepicker({
        format: 'DD. MM. YYYY HH:mm',
        locale: 'cs',
        stepping: 15,
        sideBySide: true,
    });



    var searchDateTimePicker = $('.search-datetimepicker');
    searchDateTimePicker.datetimepicker({
        format: 'DD. MM. YYYY',
        locale: 'cs',
        minDate: moment().add(2, 'd').toDate(),
        collapse: true,
        widgetPositioning: {
            vertical: 'bottom',
            horizontal: 'right'
        }
    });

    var searchDate = Cookies.get('search-date');
    if(searchDate) {
        searchDateTimePicker.data("DateTimePicker").date(moment(searchDate));
    }



    searchDateTimePicker.on('dp.hide', function (e) {
        Cookies.set('search-date', e.date);
        document.getElementById('date-form').submit();
    });


    $('.cart-datetimepicker').datetimepicker({
        format: 'DD. MM. YYYY HH:mm',
        locale: 'cs',
        sideBySide: true,
        minDate: moment().add(2, 'd').toDate(),
        stepping: 15,
        widgetPositioning: {vertical: 'top', horizontal: 'auto'}
    });

    var reservationDateTimePicker = $('.reservation-datetimepicker');
    reservationDateTimePicker.datetimepicker({
        format: 'DD. MM. YYYY HH:mm',
        locale: 'cs',
        sideBySide: true,
        minDate: moment().add(2, 'd').toDate(),
        stepping: 15
    });

    if(reservationDateTimePicker && searchDate && reservationDateTimePicker.data("DateTimePicker")) {
        reservationDateTimePicker.data("DateTimePicker").date(moment(searchDate));
        $('input[name=availableTermHours]').val('5');

        // send it
        $('a.check-availability').click();
    }

}