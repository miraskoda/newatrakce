$(function () {
    fillCustomerGroupGrid();
});

function fillCustomerGroupGrid() {
    var customerGroupGrid = $('.customer-group-grid');

    $.ajax({
        type: "POST",
        url: "/../../backend/admin/ajax/customer-group/customer-group-get-grid.php",
        data: {}
    }).done(function (data) {
        customerGroupGrid.html(data);
        initAdd();
        initEdit();
        initDelete();
    }).fail(function (data) {
        setInfoStatus('page', JSON.stringify(data), 'danger');
    });
}