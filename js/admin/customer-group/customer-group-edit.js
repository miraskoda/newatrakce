$(function () {
    initEdit();
});

function initEdit() {
    $('a.edit-customer-group').click(function (ev) {
        ev.preventDefault();

        var customerGroupRow = $(this).closest('.customer-group-row');
        var customerGroupId = customerGroupRow.attr('customer-group-id');

        if (customerGroupId) {
            $.ajax({
                type: "POST",
                url: "/../../backend/admin/ajax/customer-group/customer-group-get.php",
                data: {
                    customerGroupId: customerGroupId
                }
            }).done(function (data) {
                var parsedData = JSON.parse(data);

                if ($.isPlainObject(parsedData)) {
                    var modal = $('#customer-group-edit-modal');
                    var nameInput = modal.find('input[name=name]');
                    var normalPaymentsInput = modal.find('input[name=normal-payments]');
                    var vipPaymentsInput = modal.find('input[name=vip-payments]');
                    var discountInput = modal.find('input[name=discount]');
                    var maxReservationHours = modal.find('input[name=max-reservation-hours]');

                    nameInput.val(parsedData.name);
                    if(parsedData.normalPayments === "1")
                        normalPaymentsInput.prop('checked', true).change();
                    else
                        normalPaymentsInput.prop('checked', false).change();
                    if(parsedData.vipPayments === "1")
                        vipPaymentsInput.prop('checked', true).change();
                    else
                        vipPaymentsInput.prop('checked', false).change();
                    discountInput.val(parsedData.discount);
                    maxReservationHours.val(parsedData.maxReservationHours);

                    modal.modal({
                        backdrop: 'static',
                        keyboard: false
                    })
                        .on('click', '.save', function () {
                            $.ajax({
                                type: "POST",
                                url: "/../../backend/admin/ajax/customer-group/customer-group-edit.php",
                                data: {
                                    name: nameInput.val(),
                                    normalPayments: ((normalPaymentsInput.prop('checked') ? 1 : 0)),
                                    vipPayments: ((vipPaymentsInput.prop('checked') ? 1 : 0)),
                                    discount: discountInput.val(),
                                    maxReservationHours: maxReservationHours.val(),
                                    customerGroupId: customerGroupId
                                }
                            }).done(function (data) {
                                if (data === "1") {
                                    setInfoStatus('page', 'Skupina úspěšně upravena.', 'success');
                                    fillCustomerGroupGrid();
                                } else {
                                    setInfoStatus('page', JSON.stringify(data), 'warning');
                                }
                            }).fail(function (data) {
                                setInfoStatus('page', JSON.stringify(data), 'danger');
                            });
                            modal.unbind('click');
                        })
                        .on('click', '.cancel', function () {
                            modal.modal('hide');
                            modal.unbind('click');
                        });
                } else {
                    setInfoStatus('page', "Nepodařilo se načíst data skupiny. Data: " + data, 'warning');
                }
            }).fail(function (data) {
                setInfoStatus('page', JSON.stringify(data), 'danger');
            });
        } else {
            setInfoStatus('page', "ID skupiny nenalezeno.", 'danger');
        }
    });
}