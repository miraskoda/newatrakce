$(function () {
    initAdd();
});

function initAdd() {
    $('a.add-customer-group').click(function (ev) {
        ev.preventDefault();

        var modal = $('#customer-group-add-modal');

        var nameInput = modal.find('input[name=name]');
        var normalPaymentsInput = modal.find('input[name=normal-payments]');
        var vipPaymentsInput = modal.find('input[name=vip-payments]');
        var discountInput = modal.find('input[name=discount]');
        var maxReservationHours = modal.find('input[name=max-reservation-hours]');

        modal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .on('click', '.save', function () {
                $.ajax({
                    type: "POST",
                    url: "/../../backend/admin/ajax/customer-group/customer-group-add.php",
                    data: {
                        name: nameInput.val(),
                        normalPayments: ((normalPaymentsInput.prop('checked') ? 1 : 0)),
                        vipPayments: ((vipPaymentsInput.prop('checked') ? 1 : 0)),
                        discount: discountInput.val(),
                        maxReservationHours: maxReservationHours.val()
                    }
                }).done(function (data) {
                    if (data === "1") {
                        setInfoStatus('page', 'Skupina úspěšně přidána.', 'success');
                        fillCustomerGroupGrid();
                    } else {
                        setInfoStatus('page', JSON.stringify(data), 'warning');
                    }
                }).fail(function (data) {
                    setInfoStatus('page', JSON.stringify(data), 'danger');
                });
                modal.unbind('click');
            })
            .on('click', '.cancel', function () {
                modal.modal('hide');
                modal.unbind('click');
            });
    });
}