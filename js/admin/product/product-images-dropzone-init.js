/**
 * Created by ondra on 13.06.2017.
 */

$(function () {
    imagesModalInit();
});

function imagesModalInit() {
    var imagesModal = $('input.product-images');

    imagesModal.click(function (ev) {
        ev.preventDefault();

        //fillProductImagesFormValues($(this));
        var productId = $(this).closest('form').find('input[name=productId]').val();
        var productMainImageContainer = $('#product-image-main');
        var productImagesContainer = $('#product-images');

        fillProductImages(productId, 'main', 1, productMainImageContainer, "/../../backend/admin/modules/file-processors/product-image-main.php");
        fillProductImages(productId, 'other', 20, productImagesContainer, "/../../backend/admin/modules/file-processors/product-images.php");

        var modal = $('#product-images-modal');

        modal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .on('click', '#cancel', function () {
                modal.modal('hide');
                modal.unbind('click');
            });
        //replaceWithForm($(this));
    });
}

function fillProductImages(productId, type, maxImageCount, container, uploadUrl) {
    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/product/product-images-get.php",
        data: {productId: productId, type: type}
    }).done(function (data) {
        //alert(JSON.stringify(data));
        var productImages = JSON.parse(data);

        container.empty();

        var count = 0;
        for (var i in productImages) {
            if(productImages.hasOwnProperty(i)){
                var name = productImages[i].name;
                var imageId = productImages[i].productImageId;
                var description = productImages[i].description;

                var html = '<div class="single-image filled" image-id="' + imageId + '" image-name="' + name + '" image-type="' + type + '">' +
                                '<img src="/assets/images/products/' + name + '">' +
                                '<input type="text" class="form-control image-description" value="' + description + '" placeholder="Popisek ...">' +
                                '<a class="btn btn-danger image-delete"><span class="ti-trash"></span> Smazat</a>' +
                            '</div>';
                container.append(html);

                count++;
            }
        }

        if(count < maxImageCount) {
            container.append('<div class="single-image">\n' +
                                '<label for="file-upload-main" class="file-upload">\n' +
                                    '<i class="ti-upload"></i> Vyber soubor pro nahrání\n' +
                                '</label>\n' +
                                '<form class="image-upload"><input id="file-upload-main" type="file" name="image"/>\n' +
                                '<input class="btn btn-primary" type="submit" value="Nahrát"></form>\n' +
                            '</div>');
        }

        initUploadButton();
        initDescriptionEdit();
        initImageDelete(productId, type, maxImageCount, container, uploadUrl);
        initImageUpload(productId, type, maxImageCount, container, uploadUrl);
    }).fail(function (data) {
        setInfoStatus('image', JSON.stringify(data), 'danger');
    });
}

function initUploadButton() {
    var uploadFileInputs = $('#product-images-modal').find('input[type=file]');
    uploadFileInputs.unbind('change');

    uploadFileInputs.change(function () {
        var uploadButton = $(this).closest('.single-image').find('.file-upload');

        if($(this).val()) {
            uploadButton.html('<i class="ti-upload"></i> Vybrán soubor ' + $(this)[0].files[0].name);
        }
    });
}

function initDescriptionEdit() {
    var descriptions = $('.image-description');
    descriptions.unbind('change');

    descriptions.change(function () {
        var value = $(this).val();
        var imageId = $(this).closest('.single-image').attr('image-id');

        $.ajax({
            type: "post",
            url: "/../../backend/admin/ajax/product/product-image-update.php",
            data: {
                imageId: imageId,
                description: value
            }
        }).done(function (data) {
            //alert(JSON.stringify(data));
            if(data === "1") {
                setInfoStatus('image', "Popisek úspěšně upraven.", 'success');
            } else {
                setInfoStatus('image', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('image', JSON.stringify(data), 'danger');
        });
    });
}

function initImageDelete(productId, type, maxImageCount, container, uploadUrl) {
    var deleteButtons = container.find('.image-delete');
    deleteButtons.unbind('click');

    deleteButtons.click(function (ev) {
        ev.preventDefault();

        var singleImage = $(this).closest('.single-image');
        var imageName = singleImage.attr('image-name');
        var type = singleImage.attr('image-type');

        $.ajax({
            type: "post",
            url: "/../../backend/admin/ajax/product/product-image-delete-by-name.php",
            data: {
                imageName: imageName,
                type: type
            }
        }).done(function (data) {
            //alert(JSON.stringify(data));
            if(data === "1") {
                fillProductImages(productId, type, maxImageCount, container, uploadUrl);
                setInfoStatus('image', "Obrázek úspěšně smazán.", 'success');
            } else {
                setInfoStatus('image', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('image', JSON.stringify(data), 'danger');
        });
    });
}

function initImageUpload(productId, type, maxImageCount, container, uploadUrl) {
    var imageUploads = container.find('.image-upload');

    imageUploads.submit(function (ev) {
        ev.preventDefault();

        var singleImageContainer = $(this).closest('.single-image');

        var imageSelect = $(this).closest('.single-image').find('input[type=file]');
        if(imageSelect && imageSelect.val()) {
            var formData = new FormData();
            formData.append('image', imageSelect[0].files[0]);

            $.ajax({
                type:'POST',
                url: uploadUrl,
                data: formData,
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress',progress, false);
                        myXhr.upload.container = singleImageContainer;
                    }
                    return myXhr;
                },
                cache:false,
                contentType: false,
                processData: false
            }).done(function (data) {
                //alert(JSON.stringify(data));

                //var parsedData = JSON.parse(data);

                if(data.code === 200) {
                    insertImage(productId, data.newName + "." + data.extension, type, maxImageCount, container, uploadUrl);
                } else {
                    setInfoStatus('image', JSON.stringify(data.message), 'warning');
                }

                /*if(data === "1") {
                    singleImage.remove();
                    setInfoStatus('image', "Obrázek úspěšně smazán.", 'success');
                } else {
                    setInfoStatus('image', JSON.stringify(data), 'warning');
                }*/
            }).fail(function (data) {
                setInfoStatus('image', JSON.stringify(data), 'danger');
            });
        }
    });
}

function insertImage(productId, name, type, maxImageCount, container, uploadUrl) {
    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/product/product-image.php",
        data: {
            productId: productId,
            imageName: name,
            imageType: type
        }
    }).done(function (data) {
        //alert(JSON.stringify(data));
        if (data === "1") {
            setInfoStatus('image', 'Obrázek úspěšně nahrán', 'success');
            fillProductImages(productId, type, maxImageCount, container, uploadUrl);
        } else {
            setInfoStatus('image', JSON.stringify(data), 'warning');
        }
    }).fail(function (data) {
        setInfoStatus('image', JSON.stringify(data), 'danger');
    });
}

function progress(ev){

    if(ev.lengthComputable){
        var max = ev.total;
        var current = ev.loaded;

        var percentage = Math.round((current * 100)/max);
        console.log(percentage);

        ev.target.container.empty();
        ev.target.container.html('<p>Nahráno ' + percentage + '%</p>');
    }
}

/*
function fillProductImagesFormValues(imagesModalOpenButton) {
    var productId = imagesModalOpenButton.closest('form').find('input[name=productId]').val();
    //alert(JSON.stringify(productId));
    var imagesModal = $('#product-images-modal');

    // insert productId to form
    var modalId = imagesModal.find('input[name=productId]').val();
    if(modalId !== productId) {
        imagesModal.find('input[name=productId]').val(productId);

        //get data and insert them into form
        $.ajax({
            type: "post",
            url: "/../../backend/admin/ajax/product/product-get.php",
            data: {productId: productId}
        }).done(function (data) {
            //alert(JSON.stringify(data));
            var product = JSON.parse(data);

            imagesModal.find('h4').text("Fotografie k produktu " + product.name);

            //change status
            removingProductFiles = true;

            //first remove all files
            if (productMainImageFiles.length > 0) {
                $.each(productMainImageFiles, function (index, value) {
                    imageMainDropzone.removeFile(value);
                    imageMainDropzone.options.maxFiles = imageMainDropzone.options.maxFiles + 1;
                });
            }
            if (productImagesFiles.length > 0) {
                $.each(productImagesFiles, function (index, value) {
                    imagesDropzone.removeFile(value);
                });
            }

            productMainImageFiles = [];
            productImagesFiles = [];

            removingProductFiles = false;

            //load image files
            loadAndFillDropzone(imageMainDropzone, productMainImageFiles, productId, 'main');
            loadAndFillDropzone(imagesDropzone, productImagesFiles, productId, 'other');
        }).fail(function (data) {
            setInfoStatus('image', JSON.stringify(data), 'danger');
        });
    }
}

function loadAndFillDropzone(dropzone, fileArray, productId, type) {
    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/product/product-images-get.php",
        data: {productId: productId, type: type}
    }).done(function (data) {
        //alert(JSON.stringify(data));
        var productImages = JSON.parse(data);

        for (var i in productImages) {
            if(productImages.hasOwnProperty(i)){
                var name = productImages[i].name;
                var mockFile = { name: name, size: 0 };
                dropzone.emit("addedfile", mockFile);
                dropzone.emit("thumbnail", mockFile, "/assets/images/products/" + name);
                dropzone.createThumbnailFromUrl(mockFile, "/assets/images/products/" + name);
                dropzone.emit("complete", mockFile);

                fileArray.push(mockFile);

                if(dropzone.options.maxFiles !== null)
                    dropzone.options.maxFiles = dropzone.options.maxFiles - 1;
            }
        }
    }).fail(function (data) {
        setInfoStatus('image', JSON.stringify(data), 'danger');
    });
}

var imageMainDropzone;
var imagesDropzone;

$(function () {
    imagesModalInit();

    var productImageMain = $("div#product-image-main");

    imageMainDropzone = new Dropzone("div#product-image-main", {
        url: "/backend/admin/modules/file-processors/product-image-main.php",
        maxFileSize: 4,
        maxFiles: 1,
        addRemoveLinks: true,
        acceptedFiles: "image/jpeg,image/pjpeg,image/png,image/,image/svg+xml",
        init: function() {
            this.on("success", function(file, response) {
                //productMainImageFiles.push(file);
                var jsonResponse = JSON.parse(response);
                if(jsonResponse.code === 200) {
                    var productId = productImageMain.closest('form').find('input[name=productId]').val();
                    var fileName = jsonResponse.newName + "." + file.name.split('.').pop();
                    insertImage(productId, fileName, 'main');

                    // remove file and immediately add it (because of name change)
                    removingProductFiles = true;

                    this.removeFile(file);

                    var mockFile = { name: fileName, size: 0 };

                    this.emit("addedfile", mockFile);
                    this.emit("thumbnail", mockFile, "/assets/images/products/" + fileName);
                    this.createThumbnailFromUrl(mockFile, "/assets/images/products/" + fileName);
                    this.emit("complete", mockFile);

                    productMainImageFiles.push(mockFile);

                    if(this.options.maxFiles !== null)
                        this.options.maxFiles = this.options.maxFiles - 1;

                    removingProductFiles = false;
                } else
                    setInfoStatus('image', 'Chyba. '+ jsonResponse.message, 'danger');
            });
            this.on("error", function(file, errorMessage) {
                setInfoStatus('image', 'Chyba. ' + errorMessage, 'danger');
            });
            this.on("maxfilesexceeded", function(file) {
                removingProductFiles = true;
                this.removeFile(file);
                removingProductFiles = false;
            });
            this.on("removedfile", function(file) {
                if(!removingProductFiles) {
                    imageMainDropzone.options.maxFiles = imageMainDropzone.options.maxFiles + 1;

                    $.ajax({
                        type: "post",
                        url: "/../../backend/admin/ajax/product/product-image-delete-by-name.php",
                        data: {
                            imageName: file.name,
                            type: 'main'
                        }
                    }).done(function (data) {
                        //alert(JSON.stringify(data));
                        if (data === "1") {
                            setInfoStatus('image', 'Obrázek úspěšně smazán.', 'success');
                        } else {
                            setInfoStatus('image', JSON.stringify(data), 'warning');
                        }
                    }).fail(function (data) {
                        setInfoStatus('image', JSON.stringify(data), 'danger');
                    });
                }
            });
        }
    });

    productImageMain.addClass('dropzone');

    var productImages = $("div#product-images");

    imagesDropzone = new Dropzone("div#product-images", {
        url: "/backend/admin/modules/file-processors/product-images.php",
        maxFileSize: 4,
        addRemoveLinks: true,
        acceptedFiles: "image/jpeg,image/pjpeg,image/png,image/,image/svg+xml",
        init: function() {
            this.on("addedfile", function () {
               this.options.maxFiles = null;
            });
            this.on("success", function(file, response) {
                //productImagesFiles.push(file);
                var jsonResponse = JSON.parse(response);
                if(jsonResponse.code === 200) {
                    var productId = productImages.closest('form').find('input[name=productId]').val();
                    var fileName = jsonResponse.newName + "." + file.name.split('.').pop();
                    insertImage(productId, fileName, 'other');

                    // replacing file with file with new name
                    removingProductFiles = true;

                    this.removeFile(file);

                    var mockFile = { name: fileName, size: 0 };

                    this.emit("addedfile", mockFile);
                    this.emit("thumbnail", mockFile, "/assets/images/products/" + fileName);
                    this.createThumbnailFromUrl(mockFile, "/assets/images/products/" + fileName);
                    this.emit("complete", mockFile);

                    productImagesFiles.push(mockFile);

                    removingProductFiles = false;
                } else
                    setInfoStatus('image', 'Chyba. '+ jsonResponse.message, 'danger');
            });
            this.on("error", function(file, errorMessage) {
                setInfoStatus('image', 'Chyba. ' + errorMessage, 'danger');
            });
            this.on("maxfilesexceeded", function(file) {
                removingProductFiles = true;
                this.removeFile(file);
                removingProductFiles = false;
            });
            this.on("removedfile", function(file) {
                if(!removingProductFiles) {
                    $.ajax({
                        type: "post",
                        url: "/../../backend/admin/ajax/product/product-image-delete-by-name.php",
                        data: {
                            imageName: file.name,
                            type: 'other'
                        }
                    }).done(function (data) {
                        //alert(JSON.stringify(data));
                        if (data === "1") {
                            setInfoStatus('image', 'Obrázek úspěšně smazán.', 'success');
                        } else {
                            setInfoStatus('image', JSON.stringify(data), 'warning');
                        }
                    }).fail(function (data) {
                        setInfoStatus('image', JSON.stringify(data), 'danger');
                    });
                }
            });
        }
    });

    productImages.addClass('dropzone');
});

function insertImage(productId, name, type) {
    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/product/product-image.php",
        data: {
            productId: productId,
            imageName: name,
            imageType: type
        }
    }).done(function (data) {
        //alert(JSON.stringify(data));
        if (data === "1") {
            setInfoStatus('image', 'Obrázek úspěšně nahrán', 'success');
        } else {
            setInfoStatus('image', JSON.stringify(data), 'warning');
        }
    }).fail(function (data) {
        setInfoStatus('image', JSON.stringify(data), 'danger');
    });
}
*/