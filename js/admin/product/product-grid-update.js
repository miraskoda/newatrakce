/**
 * Created by ondra on 28.03.2017.
 */

function updateGridProduct(productId) {
    $.ajax({
        type: "POST",
        url: "/../../backend/admin/ajax/product/product-grid-update.php",
        data: { productId: productId }
    }).done(function (data) {
        /*alert(JSON.stringify(data));
         alert(".admin-grid div#" + userId);*/
        //alert(JSON.stringify(data) + " draw " + productId);
        $(".admin-grid div#" + productId).replaceWith(data);
        $(".admin-table div#" + productId).replaceWith(data);

        refreshDetail();
        refreshTriggers();
        refreshDelete();
    }).fail(function (data) {
        setInfoStatus('page', JSON.stringify(data), 'danger');
    });
}