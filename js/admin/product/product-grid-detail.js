/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 28.03.2017
 * Time: 13:14
 */

var detailButton = $('button.detail-product');
var detailCancelButton = $('button.cancel-detail-product');
refreshDetail();

//because ajax was called multiple times
//counter was added
var callCounter = 0;

function refreshDetail() {
    //reset counter
    callCounter = 0;
    detailButton = $('button.detail-product');
    detailCancelButton = $('button.cancel-detail-product');

    detailCancelButton.off();
    detailCancelButton.click(function (ev) {
        cancelDetailGridProduct($(this).closest('.product-detail').attr('id'));
        detailCancelButton.unbind('click');
        ev.preventDefault();
    });

    detailButton.off();
    detailButton.click(function (ev) {
        detailGridProduct($(this).closest('.data-cell').attr('id'));
        detailButton.unbind('click');
        ev.preventDefault();
    });
}

function detailGridProduct(productId) {
    //if this is the first call time
    if(callCounter < 1) {
        $.ajax({
            type: "POST",
            url: "/../../backend/admin/ajax/product/product-grid-detail.php",
            data: {productId: productId}
        }).done(function (data) {
            $(".admin-grid div#" + productId).replaceWith(data);
            $(".admin-table div#" + productId).replaceWith(data);

            refreshDetail();
            refreshTriggers();
            refreshDelete();
        }).fail(function (data) {
            setInfoStatus('page', JSON.stringify(data), 'danger');
        });
        callCounter++;
    }
}

function cancelDetailGridProduct(productId) {
    if(callCounter < 1) {
        $.ajax({
            type: "POST",
            url: "/../../backend/admin/ajax/product/product-grid-detail-cancel.php",
            data: {productId: productId}
        }).done(function (data) {
            //alert(JSON.stringify(data));
            $(".admin-grid div#" + productId).replaceWith(data);
            $(".admin-table div#" + productId).replaceWith(data);

            refreshDetail();
            refreshTriggers();
            refreshDelete();
        }).fail(function (data) {
            alert(JSON.stringify(data));
        });
        callCounter++;
    }
}