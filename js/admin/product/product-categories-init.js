/**
 * Created by ondra on 20.06.2017.
 */

var categories = [];
var categorySelects;
var categoryProductId;

function categoriesModalInit() {
    var categoryModal = $('input.product-categories');

    categoryModal.click(function (ev) {
        ev.preventDefault();

        loadCategories();

        var modal = $('#product-categories-modal');
        //var addProductCategoryButton = modal.find("#add-product-category");
        categorySelects = modal.find('#category-selects');

        fillCategoryModal($(this));

        modal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .one('click', '#cancel', function () {
                modal.modal('hide');
                modal.unbind('click');
            })
            .on('click', '#add-product-category', function () {
                categorySelects.append(insertSelect());
            });
    });
}

function fillCategoryModal(categoryModalOpenButton) {
    categoryProductId = categoryModalOpenButton.closest('form').find('input[name=productId]').val();
    //alert(JSON.stringify(productId));
    var categoriesModal = $('#product-categories-modal');

    // insert productId to form
    var modalId = categoriesModal.find('input[name=productId]').val();
    if(modalId !== categoryProductId) {
        categorySelects.empty();
        categoriesModal.find('input[name=productId]').val(categoryProductId);

        //get data and insert them into form
        $.ajax({
            type: "post",
            url: "/../../backend/admin/ajax/product/product-get.php",
            data: {productId: categoryProductId}
        }).done(function (data) {
            //alert(JSON.stringify(data));
            var product = JSON.parse(data);

            categoriesModal.find('h4').text("Kategorie k produktu " + product.name);

            $.ajax({
                type: "post",
                url: "/../../backend/admin/ajax/product/product-categories-get-by-product.php",
                data: {productId: categoryProductId}
            }).done(function (data) {
                //alert(JSON.stringify(data));
                var productCategories = JSON.parse(data);

                for (var i in productCategories) {
                    if (productCategories.hasOwnProperty(i)) {
                        //alert(JSON.stringify(productCategories[i]));
                        categorySelects.append(insertSelect(productCategories[i].categoryId, productCategories[i].productCategoryId));
                    }
                }
            }).fail(function (data) {
                setInfoStatus('category', JSON.stringify(data), 'danger');
            });
        }).fail(function (data) {
            setInfoStatus('category', JSON.stringify(data), 'danger');
        });
    }
}

function loadCategories() {
    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/product/product-categories-get.php",
        data: {}
    }).done(function (data) {
        //alert(JSON.stringify(data));
        categories = JSON.parse(data);
        //categorySelects.append(insertSelect());
    }).fail(function (data) {
        setInfoStatus('category', JSON.stringify(data), 'danger');
    });
}

function insertSelect(selectedValue, productCategoryId) {
    var html = "<li>" +
        "<div class='row'>" +
        "<div class='col-md-10 form-group'>" +
        "<select class='form-control' name='category' onchange='categorySelected($(this))' id='" + productCategoryId + "'>";
    html += "<option " + ((selectedValue === null) ? "selected" : "") + ">Vyberte kategorii ...</option>";

    for (var i in categories) {
        if(categories.hasOwnProperty(i)){
            html += "<option value='" + categories[i].categoryId + "'" + ((selectedValue === categories[i].categoryId) ? "selected" : "") + ">" + categories[i].name + "</option>"
        }
    }

    html += "</select>" +
        "</div>" +
        "<div class='col-md-2'><button type='button' class='btn btn-danger remove-product-category' onclick='removeCategorySelect($(this))'><span class='ti-trash'></span></button></div>" +
        "</div>" +
        "</li>";

    return html;
}

function categorySelected(select) {
    var categoryId = select.find('option:selected').val();
    var productCategoryId = select.attr('id');

    if($.isNumeric(categoryId) && categoryId > 0) {
        $.ajax({
            type: "post",
            url: "/../../backend/admin/ajax/product/product-category-create.php",
            data: {productId: categoryProductId, categoryId: categoryId}
        }).done(function (data) {
            try {
                var productCategory = JSON.parse(data);

                select.attr('id', productCategory.productCategoryId);

                setInfoStatus('category', "Kategorie úspěšně uložena.", 'success');
            }
            catch (error) {
                setInfoStatus('category', "Chyba. " + error.message + " " + JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('category', JSON.stringify(data), 'danger');
        });

        deleteProductCategory(productCategoryId);
    }
}

function removeCategorySelect(selectDeleteButton) {
    var selectId = selectDeleteButton.closest('.row').find('select[name=category]').attr('id');

    if($.isNumeric(selectId) && selectId > 0)
        deleteProductCategory(selectId);

    selectDeleteButton.closest('li').remove();
}

function deleteProductCategory(productCategoryId) {
    if($.isNumeric(productCategoryId) && productCategoryId > 0) {
        $.ajax({
            type: "post",
            url: "/../../backend/admin/ajax/product/product-category-delete.php",
            data: {productCategoryId: productCategoryId}
        }).done(function (data) {
            if(data !== "1")
                setInfoStatus('category', "Chyba. " + JSON.stringify(data), 'warning');
        }).fail(function (data) {
            setInfoStatus('category', JSON.stringify(data), 'danger');
        });
    }
}

$(function () {
    categoriesModalInit();
});