/**
 * Created by ondra on 27.03.2017.
 */

var deleteButton = $('button.delete-product');

deleteButton.click(function (ev) {
    ev.preventDefault();
    deleteButtonAction($(this));
});

//after working with elements you need to refresh selector
function deleteButtonAction(deleteButton) {
    var deleteDiv = deleteButton.closest('.data-cell');
    var productId = deleteDiv.attr('id');

    var modal = $('#delete-modal');
    modal.modal({
        backdrop: 'static',
        keyboard: false
    })
        .on('click', '#delete', function() {
            $.ajax({
                type: "POST",
                url: "/../../backend/admin/ajax/product/product-delete.php",
                data: { productId : productId }
            }).done(function(data) {
                if(data === "1") {
                    setInfoStatus('page', 'Produkt úspěšně smazán.', 'success');
                    deleteGridProduct(deleteDiv);
                } else {
                    setInfoStatus('page', JSON.stringify(data), 'warning');
                }
            }).fail(function(data) {
                setInfoStatus('page', JSON.stringify(data), 'danger');
            });
            modal.unbind('click');
        })
        .on('click', '#cancel', function () {
            modal.modal('hide');
            modal.unbind('click');
        });
}

function refreshDelete() {
    deleteButton = $('button.delete-product');

    deleteButton.click(function (ev) {
        ev.preventDefault();
        deleteButtonAction($(this));
    });
}