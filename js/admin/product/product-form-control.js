/**
 * Created by ondra on 24.03.2017.
 */

var productForm = $('#form-box');
var createForm = $('#create-form');
var updateForm = $('#update-form');
var createFormTrigger = $('.create-trigger');
var updateFormTrigger = $('.update-product');
var createFormCancel = $('.cancel');

var beforeScroll = null;



var createPriceInput = createForm.find('input[name=price]');
var createPriceWithVatInput = createForm.find('input[name=priceWithVat]');

var updatePriceInput = updateForm.find('input[name=price]');
var updatePriceWithVatInput = updateForm.find('input[name=priceWithVat]');

var allSet = true;

productGridInit();

function refreshInstances() {
    // after search is all content replaced
    productForm = $('#form-box');
    createForm = $('#create-form');
    updateForm = $('#update-form');
    createFormTrigger = $('.create-trigger');
    updateFormTrigger = $('.update-product');
    createFormCancel = $('.cancel');

    createPriceInput = createForm.find('input[name=price]');
    createPriceWithVatInput = createForm.find('input[name=priceWithVat]');

    updatePriceInput = updateForm.find('input[name=price]');
    updatePriceWithVatInput = updateForm.find('input[name=priceWithVat]');

    imagesModalInit();
    productVideoModalInit();
    categoriesModalInit();
    priceIncludedModalInit();
    technicalModalInit();
    additionsModalInit();

    if(!allSet) {
        productGridInit();
        allSet = true;
    }

    tinyMCE.init({
        selector: 'textarea',
        language_url: '/backend/plugins/tinymce_languages/langs/cs_CZ.js',
        plugins: ["lists", "textcolor", "code"],
        toolbar: "undo redo | formatselect | bold italic fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | forecolor backcolor | removeformat | code",
        height: 300,
        content_css: "https://fonts.googleapis.com/css?family=Comfortaa",
        content_style: "body{ font-family: 'Comfortaa', cursive; }"
    });
}

function refreshTriggers() {
    refreshInstances();

    updateFormTrigger = $('.update-product');

    updateFormTrigger.off();
    updateFormTrigger.click(function (ev) {
        initUpdate($(this), ev);
    });

    initDuplicate();
}



function initUpdate(updateFormTrigger, ev) {
    beforeScroll = null;



    window.onscroll = function() {myScroll()};



    productForm.children('.box').children('h2').text("Upravit produkt");
    var updateProductId = updateFormTrigger.closest('.data-cell').attr('id');

    //get data and insert them into form
    $.ajax({
        type: updateForm.attr('method'),
        url: "/../../backend/admin/ajax/product/product-get.php",
        data: {productId: updateProductId}
    }).done(function (data) {

        $('html, body').animate({
            scrollTop: productForm.offset().top
        }, 500);

        //alert(JSON.stringify(data));
        var product = JSON.parse(data);
        productForm.children('.box').children('h2').text("Upravit produkt č. " + product.productId);
        fillUpdateForm(product);
    }).fail(function (data) {
        alertDiv.text(JSON.stringify(data)).removeClass('hidden').animate();
    });

    setTimeout(function () {
        createForm.addClass('hidden');
        updateForm.removeClass('hidden');
        createFormTrigger.removeClass('');
        createFormTrigger.children('.box').removeClass('').addClass('');
        createFormTrigger.addClass('hidden');
        productForm.removeClass('hidden');
    }, 1000);

    if(ev)
        ev.preventDefault();
}

function myScroll() {
    if(!beforeScroll){
        beforeScroll = document.documentElement.scrollTop;
    }
}


function fillUpdateForm(product) {
    updateForm.find('input[name=productId]').val(product.productId);
    updateForm.find('input[name=name]').val(product.name);
    updateForm.find('textarea[name=description]').val(product.description);
    updateForm.find('textarea[name=target]').val(product.target);
    try {
        tinyMCE.get('description-update').setContent(product.description);
        tinyMCE.get('target-update').setContent(product.target);
    } catch (err) {}

    if(product.visible) {
        updateForm.find('input[name=visible]').bootstrapToggle('on');
    } else {
        updateForm.find('input[name=visible]').bootstrapToggle('off');
    }
    updateForm.find('input[name=price]').val(product.price);
    updateForm.find('input[name=priceWithVat]').val(product.priceWithVat);
    updateForm.find('input[name=quantity]').val(product.quantity);
    updateForm.find('input[name=keywords]').val(product.keywords);
}

function productGridInit() {
    createFormTrigger.off();
    createFormTrigger.click(function (ev) {
        if (productForm.css('display') === 'none') {
            productForm.children('.box').children('h2').text("Nový produkt");
            setTimeout(function () {
                updateForm.addClass('hidden');
                createForm.removeClass('hidden');
                createFormTrigger.removeClass('');
                createFormTrigger.children('.box').removeClass('').addClass('');
                createFormTrigger.addClass('hidden');
                productForm.removeClass('hidden');
                productForm.children('.box').animate();
            }, 1000);
        }

        ev.preventDefault();
    });

    updateFormTrigger.off();
    updateFormTrigger.click(function (ev) {
        initUpdate($(this), ev);
    });

    createFormCancel.off();
    createFormCancel.click(function (ev) {
        if (productForm.css('display') === 'block') {
            productForm.children('.box').addClass('').addClass('').animate();
            productForm.children('.box').children('h2').text("Nový produkt");
            setTimeout(function () {
                productForm.children('.box').removeClass('');
                productForm.addClass('hidden');
                createFormTrigger.removeClass('hidden');
                productForm.children('.box').animate();
            }, 1000);
        }

        //scroll to position
        $('html, body').animate({
            scrollTop: beforeScroll+700 }, 100);


        ev.preventDefault();
    });

    createPriceInput.change(function () {
        createPriceWithVatInput.val(((createPriceInput.val() / 100) * 121).toFixed(2));
    });
    updatePriceInput.change(function () {
        updatePriceWithVatInput.val(((updatePriceInput.val() / 100) * 121).toFixed(2));
    });

    createForm.submit(function (ev) {

        ev.preventDefault();

        var alertDiv = createForm.children('.action-alert');
        tinyMCE.triggerSave();

        $.ajax({
            type: createForm.attr('method'),
            url: "/../../backend/admin/ajax/product/product-create.php",
            data: createForm.serialize()
        }).done(function (data) {
            //alert(JSON.stringify(data));
            if (data === "1") {
                setInfoStatus('page', 'Produkt úspěšně vytvořen.', 'success');
                createGridProduct();
                createForm[0].reset();
            } else {
                alertDiv.html(data).removeClass('hidden').addClass('bounceIn').addClass('infinite').animate();
                setTimeout(function (data) {
                    alertDiv.removeClass('infinite');
                }, 750);
            }
        }).fail(function (data) {
            alertDiv.text(JSON.stringify(data)).removeClass('hidden').animate();
        });
    });

    updateForm.submit(function (ev) {


        ev.preventDefault();

        var alertDiv = updateForm.children('.action-alert');
        tinyMCE.triggerSave();




        $.ajax({
            type: updateForm.attr('method'),
            url: "/../../backend/admin/ajax/product/product-update.php",
            data: updateForm.serialize()
        }).done(function (data) {
            if (data === "1") {
                setInfoStatus('page', 'Produkt úspěšně aktualizován.', 'success');
                updateGridProduct(updateForm.find('input[name=productId]').val());

                //scroll to position
                $('html, body').animate({
                    scrollTop: beforeScroll+700 }, 100);

                    // hidden update form
                if (productForm.css('display') === 'block') {
                    productForm.children('.box').addClass('').addClass('').animate();
                    productForm.children('.box').children('h2').text("Nový produkt");
                    setTimeout(function () {
                        productForm.children('.box').removeClass('');
                        productForm.addClass('hidden');
                        createFormTrigger.removeClass('hidden');
                        productForm.children('.box').animate();
                    }, 1000);
                }

            } else {
                alertDiv.html(data).removeClass('hidden').addClass('bounceIn').addClass('infinite').animate();
                setTimeout(function () {
                    alertDiv.removeClass('infinite');
                }, 750);
            }
        }).fail(function (data) {
            alertDiv.text(JSON.stringify(data)).removeClass('hidden');
        });
    });
}