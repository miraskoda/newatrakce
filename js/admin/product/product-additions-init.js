var additions = [];
var additionsSelects;
var mainProductId;

function additionsModalInit() {
    var additionsModal = $('input.product-additions');

    additionsModal.click(function (ev) {
        ev.preventDefault();

        loadAdditions();

        var modal = $('#product-additions-modal');
        additionsSelects = modal.find('#additions-selects');

        fillAdditionsModal($(this));

        modal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .one('click', '#cancel', function () {
                modal.modal('hide');
                modal.unbind('click');
            })
            .on('click', '#add-product-addition', function () {
                additionsSelects.append(insertAdditionSelect());
            });
    });
}

function fillAdditionsModal(additionsModalOpenButton) {
    mainProductId = additionsModalOpenButton.closest('form').find('input[name=productId]').val();
    var additionsModal = $('#product-additions-modal');

    // insert productId to form
    var modalId = additionsModal.find('input[name=productId]').val();
    if(modalId !== mainProductId) {
        additionsSelects.empty();
        additionsModal.find('input[name=productId]').val(mainProductId);

        //get data and insert them into form
        $.ajax({
            type: "post",
            url: "/../../backend/admin/ajax/product/product-additions-get.php",
            data: {mainProductId: mainProductId}
        }).done(function (data) {
            //alert(JSON.stringify(data));
            var productAdditions = JSON.parse(data);

            for (var i in productAdditions) {
                if (productAdditions.hasOwnProperty(i)) {
                    additionsSelects.append(insertAdditionSelect(productAdditions[i].additionalProductId, productAdditions[i].productAdditionId));
                }
            }
        }).fail(function (data) {
            setInfoStatus('additions', JSON.stringify(data), 'danger');
        });
    }
}

function loadAdditions() {
    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/product/product-additions-all-get.php",
        data: {}
    }).done(function (data) {
        //alert(JSON.stringify(data));
        additions = JSON.parse(data);
    }).fail(function (data) {
        setInfoStatus('additions', JSON.stringify(data), 'danger');
    });
}

function insertAdditionSelect(selectedValue, productAdditionId) {
    var html = "<li>" +
        "<div class='row'>" +
        "<div class='col-md-10 form-group'>" +
        "<select class='form-control' name='additions' onchange='additionsSelected($(this))' id='" + productAdditionId + "'>";
    html += "<option " + ((selectedValue === null) ? "selected" : "") + ">Vyberte doplňek ...</option>";

    for (var i in additions) {
        if(additions.hasOwnProperty(i)){
            html += "<option value='" + additions[i].productId + "'" + ((selectedValue === additions[i].productId) ? "selected" : "") + ">" + additions[i].name + "</option>"
        }
    }

    html += "</select>" +
        "</div>" +
        "<div class='col-md-2'><button type='button' class='btn btn-danger remove-product-addition' onclick='removeAdditionsSelect($(this))'><span class='ti-trash'></span></button></div>" +
        "</div>" +
        "</li>";

    return html;
}

function additionsSelected(select) {
    var additionalProductId = select.find('option:selected').val();
    var productAdditionId = select.attr('id');


    if($.isNumeric(additionalProductId) && additionalProductId > 0) {
        $.ajax({
            type: "post",
            url: "/../../backend/admin/ajax/product/product-additions-create.php",
            data: {mainProductId: mainProductId, additionalProductId: additionalProductId}
        }).done(function (data) {
            try {
                var productAddition = JSON.parse(data);

                select.attr('id', productAddition.productAdditionId);

                setInfoStatus('additions', "Doplněk úspěšně uložen.", 'success');
            } catch (error) {
                setInfoStatus('additions', "Chyba. " + error.message + " " + JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('additions', JSON.stringify(data), 'danger');
        });

        deleteProductAddition(productAdditionId);
    }
}

function removeAdditionsSelect(selectDeleteButton) {
    var selectId = selectDeleteButton.closest('.row').find('select[name=additions]').attr('id');

    if($.isNumeric(selectId) && selectId > 0)
        deleteProductAddition(selectId);

    selectDeleteButton.closest('li').remove();
}

function deleteProductAddition(productAdditionId) {
    if($.isNumeric(productAdditionId) && productAdditionId > 0) {
        $.ajax({
            type: "post",
            url: "/../../backend/admin/ajax/product/product-additions-delete.php",
            data: {productAdditionId: productAdditionId}
        }).done(function (data) {
            if(data !== "1")
                setInfoStatus('additions', "Chyba. " + JSON.stringify(data), 'warning');
        }).fail(function (data) {
            setInfoStatus('additions', JSON.stringify(data), 'danger');
        });
    }
}

$(function () {
    additionsModalInit();
});