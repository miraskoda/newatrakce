/**
 * Created by Ondra on 27.03.2017.
 */

function deleteGridProduct(deleteDiv) {
    deleteDiv.addClass('animated').addClass('fadeOutLeft').animate();
    setTimeout(function (data) {
        deleteDiv.animate({width: '0', padding: '0'});
        setTimeout(function () {
            deleteDiv.remove();
        }, 1000);
    }, 750);
}
