/**
 * Created by Adam on 13.03.2017.
 */

function isMobile() {
    return window.matchMedia("only screen and (max-width: 760px)").matches;
}


$('.hamburger-trigger').click(function (ev) {

    var sidebar = $('.sidebar');
    var fullWidth = "250px";
    var width = "54px";

    var full = "260px";
    var min = "55px";

    if(sidebar.css("width") === fullWidth){
        if(!isMobile()) {
            $('#conte').animate({marginLeft: min});
            $('#conte').css({"display":"block"});
        }
        sidebar.animate({width: width});
    }

    else{
        if(!isMobile()) {
            $('#conte').animate({marginLeft: full});
            $('#conte').css({"display":"block"});
        }
        sidebar.animate({width: fullWidth});
    }

    ev.preventDefault();

});