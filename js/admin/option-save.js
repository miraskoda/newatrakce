$(function () {
    $('.save-options').click(function () {
        saveOptions();
    });
});

function saveOptions() {
    var inputs = $('.options-container').find('input');

    inputs.each(function () {
        var optionId = $(this).attr('name');
        var value = $(this).val();

        $.ajax({
            type: "POST",
            url: "/../../backend/admin/ajax/option/option-save.php",
            data: {
                optionId : optionId,
                value : value
            }
        }).done(function(data) {
            if(data !== "1") {
                setInfoStatus('page', JSON.stringify(data), 'warning');
                return false;
            }
        }).fail(function(data) {
            setInfoStatus('page', JSON.stringify(data), 'danger');
        });
    });

    setInfoStatus('page', 'Nastavení úspěšně uložena.', 'success');
}