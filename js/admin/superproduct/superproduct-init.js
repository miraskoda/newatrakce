$(function () {
    initSuperproducts();
});

function initSuperproducts() {
    var superproductContainer = $('.superproduct-container');

    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/superproduct/superproduct-get-all.php",
        data: {}
    }).done(function (data) {
        //alert(JSON.stringify(data));
        var superproducts = JSON.parse(data);

        superproductContainer.empty();
        if (superproducts) {
            $.each(superproducts, function (index, value) {
                superproductContainer.append('<div class="col-md-12 superproduct-row" superproduct-id="' + value.superproductId + '">' +
                    '<div class="box text-center">' +
                    '<div class="row">' +
                    '<div class="col-md-2">' + value.superproductId + '</div>' +
                    '<div class="col-md-4">' + value.name + '</div>' +
                    '<div class="col-md-2">' + value.quantity + '</div>' +
                    '<div class="col-md-2"><a class="btn btn-primary manage-products"><span class="ti-search"></span> Spravovat podprodukty</a></div>' +
                    '<div class="col-md-2"><a class="btn btn-primary edit-superproduct"><span class="ti-pencil"></span></a><a class="btn btn-danger delete-superproduct"><span class="ti-trash"></span></a></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>');
            });
        }

        superproductContainer.append('<div class="col-md-12">' +
            '<div class="box">' +
            '<div class="row form-inline">' +
            '<div class="col-md-2"></div>' +
            '<div class="col-md-4 form-group"><label>Název: <input type="text" name="name" class="form-control"></label></div>' +
            '<div class="col-md-4 form-group"><label>Počet kusů: <input type="number" name="quantity" min="1" step="1" class="form-control"></label></div>' +
            '<div class="col-md-2"><a class="btn btn-success add-superproduct"><span class="ti-plus"></span> Přidat</a></div>' +
            '</div>' +
            '</div>' +
            '</div>');

        addSuperproduct();
        editSuperproduct();
        deleteSuperproduct();

        initProducts();
    }).fail(function (data) {
        setInfoStatus('page', JSON.stringify(data), 'danger');
    });
}

function addSuperproduct() {
    $('.add-superproduct').click(function () {
        var name = $('input[name=name]');
        var quantity = $('input[name=quantity]');

        if (name && quantity && name.val() && quantity.val()) {
            $.ajax({
                type: "post",
                url: "/../../backend/admin/ajax/superproduct/superproduct-create.php",
                data: {
                    name: name.val(),
                    quantity: quantity.val()
                }
            }).done(function (data) {
                //alert(JSON.stringify(data));
                initSuperproducts();
                if (data === '1') {
                    setInfoStatus('page', 'Superprodukt úspěšně přidán', 'success');
                } else {
                    setInfoStatus('page', JSON.stringify(data), 'warning');
                }
            }).fail(function (data) {
                setInfoStatus('page', JSON.stringify(data), 'danger');
            });
        }
    });
}

function editSuperproduct() {
    $('.edit-superproduct').click(function () {
        var superproductId = $(this).closest('.superproduct-row').attr('superproduct-id');

        if (superproductId) {
            var modal = $('#superproduct-edit');

            // fill modal values
            $.ajax({
                type: "post",
                url: "/../../backend/admin/ajax/superproduct/superproduct-get.php",
                data: {
                    superproductId : superproductId
                }
            }).done(function (data) {
                //alert(JSON.stringify(data));
                var parsedData = JSON.parse(data);

                if(parsedData) {
                    modal.find('input#name').val(parsedData.name);
                    modal.find('input#quantity').val(parsedData.quantity);
                } else {
                    setInfoStatus('page', JSON.stringify(data), 'warning');
                }
            }).fail(function (data) {
                setInfoStatus('page', JSON.stringify(data), 'danger');
            });

            // show modal
            modal.modal({
                backdrop: 'static',
                keyboard: false
            })
                .on('click', '#save', function () {
                    var name = modal.find('input#name').val();
                    var quantity = modal.find('input#quantity').val();

                    $.ajax({
                        type: "post",
                        url: "/../../backend/admin/ajax/superproduct/superproduct-edit.php",
                        data: {
                            superproductId : superproductId,
                            name : name,
                            quantity : quantity
                        }
                    }).done(function (data) {
                        //alert(JSON.stringify(data));
                        initSuperproducts();
                        if (data === '1') {
                            setInfoStatus('page', 'Superprodukt úspěšně upraven', 'success');
                        } else {
                            setInfoStatus('page', JSON.stringify(data), 'warning');
                        }
                    }).fail(function (data) {
                        setInfoStatus('page', JSON.stringify(data), 'danger');
                    });
                    modal.unbind('click');
                })
                .on('click', '#cancel', function () {
                    modal.modal('hide');
                    modal.unbind('click');
                });
        }
    });
}

function deleteSuperproduct() {
    $('.delete-superproduct').click(function () {
        var superproductId = $(this).closest('.superproduct-row').attr('superproduct-id');

        if (superproductId) {
            var modal = $('#delete-modal');
            modal.modal({
                backdrop: 'static',
                keyboard: false
            })
                .on('click', '#delete', function () {
                    $.ajax({
                        type: "post",
                        url: "/../../backend/admin/ajax/superproduct/superproduct-delete.php",
                        data: {
                            superproductId: superproductId
                        }
                    }).done(function (data) {
                        //alert(JSON.stringify(data));
                        initSuperproducts();
                        if (data === '1') {
                            setInfoStatus('page', 'Superprodukt úspěšně smazán', 'success');
                        } else {
                            setInfoStatus('page', JSON.stringify(data), 'warning');
                        }
                    }).fail(function (data) {
                        setInfoStatus('page', JSON.stringify(data), 'danger');
                    });
                    modal.unbind('click');
                })
                .on('click', '#cancel', function () {
                    modal.modal('hide');
                    modal.unbind('click');
                });
        }
    });
}