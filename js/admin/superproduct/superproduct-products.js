function initProducts() {
    $('.manage-products').click(function () {
        var superproductId = $(this).closest('.superproduct-row').attr('superproduct-id');

        var modal = $('#superproduct-products');

        loadSuperproductProductContent(superproductId, modal);

        modal.modal({
            backdrop: 'static',
            keyboard: false
        })
        .on('click', '#cancel', function () {
            modal.modal('hide');
            modal.unbind('click');
        });
    });
}

function loadSuperproductProductContent(superproductId, modal) {
    var productContainer = modal.find('.products-container');
    productContainer.empty();

    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/superproduct/superproduct-get-products.php",
        data: {
            superproductId: superproductId
        }
    }).done(function (data) {
        //alert(JSON.stringify(data));
        var parsedData = JSON.parse(data);

        $.each(parsedData, function (index, value) {
            //console.log(parsedData);
            productContainer.append('<div class="row superproduct-product-row" product-id="' + value.productId + '" superproduct-id="' + superproductId + '">' +
                                        '<div class="col-md-8">' + value.name + '</div>' +
                                        '<div class="col-md-2 form-group"><input class="form-control" type="number" min="1" step="1" value="' + value.quantity + '"></div>' +
                                        '<div class="col-md-2"><a class="btn btn-danger delete-superproduct-product"><span class="ti-trash"></span></a></div>' +
                                    '</div>');
        });

        productContainer.append('<hr><h3>Přidání nového produktu do celku</h3><div class="form-group row new-superproduct-product-row" superproduct-id="' + superproductId + '">' +
            '<div class="col-md-7"><label>Podprodukt:<select class="form-control products-select"></select></label></div>' +
            '<div class="col-md-3"><label>Počet kusů: ' +
            '<span class="ti-help-alt" data-toggle="tooltip" title="Počet kusů superproduktu, které budou potřeba pro jeden kus podproduktu"></span>' +
            '<input class="form-control superproduct-product-quantity" type="number" min="1" step="1"></label></div>' +
            '<div class="col-md-2" style="bottom: -19px"><a class="btn btn-success add-superproduct-product">Přidat</a></div>' +
            '</div>');

        var productSelect = $('.products-select');

        $.ajax({
            type: "post",
            url: "/../../backend/admin/ajax/product/product-all-get.php",
            data: {
                'no-description' : true
            }
        }).done(function (data) {
            //alert(JSON.stringify(data));
            var parsedData = JSON.parse(data);

            if(parsedData) {
                $.each(parsedData, function (index, value) {
                    productSelect.append('<option value="' + value.productId + '">' + value.name + '</option>');
                });
            }
        }).fail(function (data) {
            setInfoStatus('page', JSON.stringify(data), 'danger');
        });

        addSuperproductProduct();
        editSuperproductProduct();
        deleteSuperproductProduct();
        $('[data-toggle="tooltip"]').tooltip();
    }).fail(function (data) {
        setInfoStatus('page', JSON.stringify(data), 'danger');
    });
}

function addSuperproductProduct() {
    $('.add-superproduct-product').click(function () {
        var superproductProductRow = $(this).closest('.new-superproduct-product-row');
        var modal = superproductProductRow.closest('#superproduct-products');
        var superproductId = superproductProductRow.attr('superproduct-id');
        var productId = superproductProductRow.find('.products-select').val();
        var quantity = superproductProductRow.find('input[type=number]').val();

        if(superproductId && productId && quantity) {
            $.ajax({
                type: "post",
                url: "/../../backend/admin/ajax/superproduct/superproduct-product-create.php",
                data: {
                    superproductId : superproductId,
                    productId : productId,
                    quantity : quantity
                }
            }).done(function (data) {
                //alert(JSON.stringify(data));
                if(data === '1') {
                    setInfoStatus('superproduct-product', "Podprodukt úspěšně přidán.", 'success');
                    loadSuperproductProductContent(superproductId, modal);
                } else {
                    setInfoStatus('superproduct-product', JSON.stringify(data), 'warning');
                }
            }).fail(function (data) {
                setInfoStatus('superproduct-product', JSON.stringify(data), 'danger');
            });
        }
    });
}

function editSuperproductProduct() {
    $('.superproduct-product-row').find('input[type=number]').change(function () {
        var superproductProductRow = $(this).closest('.superproduct-product-row');
        var modal = superproductProductRow.closest('#superproduct-products');
        var superproductId = superproductProductRow.attr('superproduct-id');
        var productId = superproductProductRow.attr('product-id');
        var quantity = superproductProductRow.find('input[type=number]').val();

        if(superproductId && productId && quantity && quantity > 0) {
            $.ajax({
                type: "post",
                url: "/../../backend/admin/ajax/superproduct/superproduct-product-edit.php",
                data: {
                    superproductId : superproductId,
                    productId : productId,
                    quantity : quantity
                }
            }).done(function (data) {
                //alert(JSON.stringify(data));
                if(data === '1') {
                    setInfoStatus('superproduct-product', "Podprodukt úspěšně upraven.", 'success');
                    loadSuperproductProductContent(superproductId, modal);
                } else {
                    setInfoStatus('superproduct-product', JSON.stringify(data), 'warning');
                }
            }).fail(function (data) {
                setInfoStatus('superproduct-product', JSON.stringify(data), 'danger');
            });
        }
    });
}

function deleteSuperproductProduct() {
    $('.delete-superproduct-product').click(function () {
        var superproductProductRow = $(this).closest('.superproduct-product-row');
        var modal = superproductProductRow.closest('#superproduct-products');
        var superproductId = superproductProductRow.attr('superproduct-id');
        var productId = superproductProductRow.attr('product-id');

        if(superproductId && productId) {
            $.ajax({
                type: "post",
                url: "/../../backend/admin/ajax/superproduct/superproduct-product-delete.php",
                data: {
                    superproductId : superproductId,
                    productId : productId
                }
            }).done(function (data) {
                //alert(JSON.stringify(data));
                if(data === '1') {
                    setInfoStatus('superproduct-product', "Podprodukt úspěšně smazán.", 'success');
                    loadSuperproductProductContent(superproductId, modal);
                } else {
                    setInfoStatus('superproduct-product', JSON.stringify(data), 'warning');
                }
            }).fail(function (data) {
                setInfoStatus('superproduct-product', JSON.stringify(data), 'danger');
            });
        }
    });
}