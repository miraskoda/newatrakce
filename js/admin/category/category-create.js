/**
 * Created by Ondra on 22.05.2017.
 */

function initializeCreateForm() {
    $('form').each(function () {
        setupCreateForms($(this));
    });
}

$(function() {
    initializeCreateForm();
});

function setupCreateForms(createForm) {
    createForm.submit(function (ev) {
        ev.preventDefault();
        $.ajax({
            type: createForm.attr('method'),
            url: "/../../backend/admin/ajax/category/category-create.php",
            data: createForm.serialize()
        }).done(function (data) {
            //alert(JSON.stringify(data));
            if (data === "1") {
                setInfoStatus('page', 'Kategorie přidána.', 'success');
                reloadCategoryList();
            } else {
                setInfoStatus('page', JSON.stringify(data), 'warning');
            }
        }).fail(function (data) {
            setInfoStatus('page', JSON.stringify(data), 'danger');
        });
    });
}
