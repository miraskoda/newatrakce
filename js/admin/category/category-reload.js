/**
 * Created by Ondra on 22.05.2017.
 */

function reloadCategoryList() {
    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/category/category-reload.php"
    }).done(function (data) {
        $("#category-list").find("> ul").replaceWith(data);

        $(function () {
            initializeCreateForm();
            initializeDelete();
            initializeUpdate();
            initializeSubCategory();
            initializeNewCategorySwitch();
            initializeSortable();
        })
    }).fail(function (data) {
        alert(JSON.stringify(data));
    });
}