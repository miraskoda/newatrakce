/**
 * Created by Ondra on 23.05.2017.
 */

function initializeSortable() {
    $('#category-list').find('ul').sortable({
        update: function () {
            updateCategoryPositions($(this));
        }
    });
}

$(function () {
    initializeSortable();
});

function updateCategoryPositions(list) {
    var categoryIds = [];
    var categoryPositions = [];
    list.find('li').each(function () {
        var id = $(this).attr('id');
        if(id){
            categoryIds.push(parseInt(id));
            categoryPositions.push($(this).index());
        }
    });

    /*alert(JSON.stringify(categoryIds));
    alert(JSON.stringify(categoryPositions));*/

    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/category/category-sort.php",
        data: {categoryIds : categoryIds, categoryPositions : categoryPositions}
    }).done(function (data) {
        //alert(JSON.stringify(data));
        if (data === "1") {
            setInfoStatus('page', 'Pořadí uloženo.', 'success');
            reloadCategoryList();
        } else {
            setInfoStatus('page', JSON.stringify(data), 'warning');
        }
    }).fail(function (data) {
        setInfoStatus('page', JSON.stringify(data), 'danger');
    });
}