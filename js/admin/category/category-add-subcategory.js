/**
 * Created by Ondra on 23.05.2017.
 */

function initializeSubCategory() {
    var addSubButton = $('button.add-sub-category');

    addSubButton.click(function (ev) {
        ev.preventDefault();
        replaceWithSubForm($(this));
    });
}

$(function () {
    initializeSubCategory();
});

function replaceWithSubForm(addSubButton) {
    var addSubDiv = addSubButton.closest('li');
    var parentCategoryId = addSubDiv.attr('id');
    $("<li><ul><li class='new-category'>" +
        "<div'>" +
            "<form class='new-category-form' method='post'>" +
                "<div class=\"form-group required\">" +
                "<label for=\"name\" class=\"control-label\">Název</label>" +
                "<input name=\"name\" class=\"form-control\" id=\"name\" placeholder=\"Název kategorie ...\"" +
                " required>" +
                "<input type='hidden' name='parentCategoryId' value='" + parentCategoryId + "'>" +
                "<input type=\"submit\" class=\"btn btn-primary\" value=\"Přidat\">" +
                "<input type=\"button\" class=\"btn btn-danger\" value=\"Zrušit\">" +
                "</div>" +
            "</form>" +
        "</div>" +
    "</li></ul></li>").insertAfter(addSubDiv);

    initializeCreateForm();
}