/**
 * Created by ondra on 17.03.2017.
 */

function createGridUser() {
    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/user/user-grid-create.php"
    }).done(function (data) {
        //alert(JSON.stringify(data));
        $(".admin-grid").append(data);

        refreshUpdate();
        refreshDelete();
    }).fail(function (data) {
        setInfoStatus('page', JSON.stringify(data), 'danger');
    });
}