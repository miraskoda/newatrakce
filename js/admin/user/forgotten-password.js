$(function () {
    var forgottenPasswordButton = $('a.forgotten-password');
    forgottenPasswordButton.click(function () {
        $(this).after('<form>\n' +
            '            <div class="form-group required">\n' +
            '                <label class="control-label" for="forgotten-password-email">Email, který byl použit pro registraci</label>\n' +
            '                <input type="email" name="forgotten-password-email" class="form-control" id="forgotten-password-email" placeholder="Email" required>\n' +
            '            </div>\n' +
            '            <button class="btn btn-info forgotten-password-send">Odeslat odkaz pro obnovení hesla</button>' +
            '</form>');
        $(this).remove();

        $('button.forgotten-password-send').click(function (ev) {
            ev.preventDefault();

            var forgottenPasswordEmail = $('input#forgotten-password-email');
            var forgottenPasswordEmailVal = forgottenPasswordEmail.val();
            var form = forgottenPasswordEmail.closest('form');
            if(forgottenPasswordEmailVal) {
                $.ajax({
                    type: 'post',
                    url: "/../../backend/admin/ajax/user/forgotten-password-send.php",
                    data: {'forgotten-password-email': forgottenPasswordEmailVal}
                }).done(function(data) {
                    //alert(JSON.stringify(data));
                    if(data === "1") {
                        form.parent().after('<p>Email úspěšně odeslán</p>');
                        form.remove();
                    } else {
                        alert(JSON.stringify(data));
                    }
                }).fail(function(data) {
                    alert(JSON.stringify(data));
                });
            }
        });

    });
});