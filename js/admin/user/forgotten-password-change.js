$(function () {
    var forgottenPasswordChangeForm = $('form#forgotten-password-change');
    forgottenPasswordChangeForm.submit(function (ev) {
        ev.preventDefault();

        var newPassword = $('input[name=forgotten-password]').val();
        var newPasswordConfirm = $('input[name=forgotten-password-confirm]').val();
        var email = $('input[name=email]').val();
        var hash = $('input[name=hash]').val();

        if(email && hash && newPassword && newPasswordConfirm) {
            $.ajax({
                type: 'post',
                url: "/../../backend/admin/ajax/user/forgotten-password-change.php",
                data: {email: email, hash: hash, 'new-password': newPassword, 'new-password-confirm': newPasswordConfirm}
            }).done(function(data) {
                //alert(JSON.stringify(data));
                if(data === "1") {
                    forgottenPasswordChangeForm.after('<p>Heslo úspěšně změněno. Nyní se můžete přihlásit.<br><a href="/admin/prihlaseni/" class="btn btn-info">Přihlásit se</a></p> ');
                    forgottenPasswordChangeForm.remove();
                } else {
                    setInfoStatus('forgotten-password', JSON.stringify(data), 'warning');
                }
            }).fail(function(data) {
                setInfoStatus('forgotten-password', JSON.stringify(data), 'danger');
            });
        }
    });
});