/**
 * Created by ondra on 18.03.2017.
 */

function deleteGridUser(deleteDiv) {
    deleteDiv.addClass('animated').addClass('fadeOutLeft').animate();
    setTimeout(function () {
        deleteDiv.animate({width: '0', padding: '0'});
        setTimeout(function () {
            deleteDiv.remove();
        }, 1000);
    }, 750);
}