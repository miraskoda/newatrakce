/**
 * Created by ondra on 29.05.2017.
 */
var isMouseOver = false;

function setInfoStatus(container, content, status) {

    var pageStatusContainer;

    pageStatusContainer = $('.' + container + '-status');
    if(!pageStatusContainer.length){
        // selector not exists
        pageStatusContainer = $('.page-status');
    }

    content = $("<div/>").html('<span>' + content + '</span>').text();
    content = content.replace(/"/g, '');
    content = content.replace(/'/g, "");

    var divClasses = 'alert alert-info';
    switch (status){
        case 'success':
            divClasses = 'alert alert-success';
            break;
        case 'warning':
            divClasses = 'alert alert-warning';
            break;
        case 'danger':
            divClasses = 'alert alert-danger';
            break;
    }

    content = '<div class="' + divClasses + '"><div class="inner">' + content + '</div><span class="ti-eye" onclick="alert(\'' + content + '\')"></span></div>';

    //set html of container
    pageStatusContainer.html(content);

    //remove previous animation classes
    var el = pageStatusContainer,
        newOne = el.clone(true);
    newOne.removeClass('zoomIn').removeClass('zoomOut');
    el.before(newOne);
    el.remove();
    pageStatusContainer = newOne;

    //add new animation classes
    pageStatusContainer.addClass('animated zoomIn');

    //display it
    pageStatusContainer.show();

    //set timer to 10 sec
    setAlertTimeout(pageStatusContainer);

    //do not hide if is hovered
    pageStatusContainer.hover(
        function () {
            isMouseOver = true;
        }, function () {
            isMouseOver = false;
        });
}

function setAlertTimeout(pageStatusContainer) {
    setTimeout(function () {
        if(!isMouseOver) {
            var el = pageStatusContainer,
                newOne = el.clone(true);
            newOne.removeClass('zoomIn').addClass('zoomOut');
            el.before(newOne);
            el.remove();
            setTimeout(function () {
                newOne.css('display', 'none');
                if($.isFunction($('.grid').masonry)) {
                    refreshMasonry();
                }
            }, 500);
        } else {
            setAlertTimeout(pageStatusContainer);
        }
    }, 10000);
}

function refreshMasonry() {
    setTimeout(function () {
        $('.grid').masonry('layout');
    }, 500);
}