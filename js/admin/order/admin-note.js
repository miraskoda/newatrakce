$(function () {
    $('textarea[name=admin-note]').change(function () {
        var orderId = $(this).attr('order-id');
        var adminNote = $(this).val();

        $.ajax({
            type: "POST",
            url: "/../../backend/admin/ajax/customer/order/admin-note-save.php",
            data: {
                orderId : orderId,
                adminNote : adminNote
            }
        }).done(function(data) {
            if(data === "1") {
                setInfoStatus('page', 'Poznámka úspěšně uložena.', 'success');
            } else {
                setInfoStatus('page', JSON.stringify(data), 'warning');
            }
        }).fail(function(data) {
            setInfoStatus('page', JSON.stringify(data), 'danger');
        });
    });
});