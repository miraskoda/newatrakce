$(function() {
    var orderStateSelects = $('select.order-state');

    var modal = $('#order-state-email-modal');

    orderStateSelects.each(function () {
        $(this).change(function () {
            var orderId = $(this).closest('.row').attr('order-id');
            var selectedState = $(this).val();

            $.ajax({
                type: 'POST',
                url: "/../../backend/admin/ajax/customer/order/order-state-save.php",
                data: {
                    'order-state' : selectedState,
                    'order-id' : orderId
                }
            }).done(function (data) {
                //alert(JSON.stringify(data));
                if (data === "1") {
                    setInfoStatus('page', 'Stav objednávky uložen.', 'success');

                    modal.modal({
                        backdrop: 'static',
                        keyboard: false
                    })
                        .one('click', '#yes', function(e) {
                            $.ajax({
                                type: "POST",
                                url: "/../../backend/admin/ajax/customer/order/order-state-email.php",
                                data: { 'order-id' : orderId }
                            }).done(function(data) {
                                if(data === "1") {
                                    setInfoStatus('page', 'Email úspěšně odeslán.', 'success');
                                } else {
                                    setInfoStatus('page', JSON.stringify(data), 'warning');
                                }
                            }).fail(function(data) {
                                setInfoStatus('page', JSON.stringify(data), 'danger');
                            });
                            modal.unbind('click');
                        })
                        .one('click', '#no', function (e) {
                            modal.modal('hide');
                            modal.unbind('click');
                        });
                } else {
                    setInfoStatus('page', JSON.stringify(data), 'warning');
                }
            }).fail(function (data) {
                setInfoStatus('page', JSON.stringify(data), 'danger');
            });
        });
    });
});