$(function() {

    $('#invoice-btn').click(function (ev) {
        ev.preventDefault();
        var editModals = $('#modalInvoice');
        editModals.modal({
            backdrop: 'static',
            keyboard: false
        })

            .on('click', '#invoice-edit', function () {
                var orderId = $('#orderId');
                var nazev = $('#names');
                var mnozstvi = $('#mnozstvi');
                var jednotkova = $('#jednotkova');
                var sleva = $('#sleva');
                var bezdph = $('#bezdph');
                var sdph = $('#sdph');
                var desc = $('#desc');


                var serializedData =
'id=' + orderId.val() + '&nazev=' + nazev.val() + '&mnozstvi=' + mnozstvi.val() + '&jednotkova=' + jednotkova.val() + '&sleva=' + sleva.val() + '&bezdph=' + bezdph.val() + '&sdph=' + sdph.val() + '&desc=' + desc.val();

                $.ajax({
                    type: 'POST',
                    url: "/../../backend/admin/ajax/order/order-request-document.php",
                    data: serializedData
                }).done(function(data) {
                    window.open("https://www.onlineatrakce.cz/prehled-objednavek/detail/"+data+"/doplnkova/faktura/faktura/");
                    editModals.modal('hide');
                    editModals.unbind('click');
                });


            })
            .one('click', '#cancel-edit', function () {
                editModals.modal('hide');
                editModals.unbind('click');
            });

        //inside
    });

    $('#credit-btn').click(function (ev) {
        ev.preventDefault();
        var editModalss = $('#modalCredit');
        editModalss.modal({
            backdrop: 'static',
            keyboard: false
        })



            .on('click', '#credit-edit', function () {


                var orderId = $('#orderId2');
                var nazev = $('#names2');
                var mnozstvi = $('#mnozstvi2');
                var jednotkova = $('#jednotkova2');
                var sleva = $('#sleva2');
                var bezdph = $('#bezdph2');
                var sdph = $('#sdph2');
                var desc = $('#desc2');


                var serializedData =
                    'id=' + orderId.val() + '&nazev=' + nazev.val() + '&mnozstvi=' + mnozstvi.val() + '&jednotkova=' + jednotkova.val() + '&sleva=' + sleva.val() + '&bezdph=' + bezdph.val() + '&sdph=' + sdph.val() + '&desc=' + desc.val();

                $.ajax({
                    type: 'POST',
                    url: "/../../backend/admin/ajax/order/order-request-document-credit.php",
                    data: serializedData
                }).done(function(data) {
                    window.open("https://www.onlineatrakce.cz/prehled-objednavek/detail/"+data+"/doplnkova/faktura/dobropis/");
                    editModalss.modal('hide');
                    editModalss.unbind('click');
                });


            })
            .one('click', '#cancel-edit', function () {
                editModalss.modal('hide');
                editModalss.unbind('click');
            });

        //inside
    });

});