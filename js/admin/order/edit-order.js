$(function () {

    $('#edit-order-btn').click(function (ev) {
        ev.preventDefault();
        var orderId = $(this).closest('button').attr('data-target');
        var editModal = $('#order-edit-modal');
        editModal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .on('click', '#accept-edit', function () {
                var contactForm = $('#contact-form');
                var venueForm = $('#venue-data');
                var startTerm = $('#start-term');
                var numberOfHours = $('#numberOfHours');
                var paymentType;
                var payment;
                var accountData = $('#edit-account-data');

                var radios = document.getElementsByName('payment-type');
                for (var i = 0, length = radios.length; i < length; i++)
                {
                    if (radios[i].checked)
                    {
                        paymentType = radios[i].value;
                        break;
                    }
                }

                var radiosPay = document.getElementsByName('payment');
                for (var j = 0, lengthP = radiosPay.length; j < lengthP; j++)
                {
                    if (radiosPay[j].checked)
                    {
                        payment = radiosPay[j].value;
                        break;
                    }
                }

                var serializedData = 'payment=' + payment + '&payment-type=' + paymentType + '&orderId=' + orderId + '&' + accountData.serialize() + '&' + '&' + contactForm.serialize() + '&' + venueForm.serialize() + '&start-term=' + startTerm.val() + '&number-of-hours=' + numberOfHours.val();

                $.ajax({
                    type: "POST",
                    url: "/../../backend/admin/ajax/order/order-request-edit.php",
                    data: serializedData
                }).done(function(data) {
                    //alert(JSON.stringify(data));
                    if(data && JSON.stringify(data).includes('true')) {
                        setInfoStatus('page', 'Objednávka byla editována, dále prosím pracujte pouze s nově vytvořenou objednávkou.', 'success');
                    } else {
                        setInfoStatus('page', JSON.stringify(data), 'warning');
                    }
                }).fail(function(data) {
                    setInfoStatus('page', JSON.stringify(data), 'danger');
                });
                editModal.unbind('click');


            })
            .one('click', '#cancel-edit', function () {
                editModal.modal('hide');
                editModal.unbind('click');
            });
    });


});
