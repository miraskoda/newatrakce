/**
 * Created by Ondra on 12.03.2017.
 */

$('.admin-logout').click(function (ev) {
    $.ajax({
        type: "post",
        url: "/../../backend/admin/ajax/logout.php"
    }).done(function(data) {
        //alert(JSON.stringify(data));
        location.reload();
    }).fail(function(data) {
        alert(JSON.stringify(data));
        //$('#action-alert').text(JSON.stringify(data)).removeClass('hidden').animate();
    });

    ev.preventDefault();
});