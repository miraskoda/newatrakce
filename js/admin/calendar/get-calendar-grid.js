$(function () {
    fillSmallCalendarGrid();

    fillData();

    var layoutSelect = $('select[name=calendar-grid-layout]');
    if(Cookies.get('calendar-layout')){
        layoutSelect.val(Cookies.get('calendar-layout'));
    }

    layoutSelect.change(function () {
        fillData(null, $(this).val());
        fillSmallCalendarGrid();
    });
});

function fillCalendarGrid(selectedDay, layout) {
    if(!selectedDay)
        layout = Cookies.get('calendar-layout');

    if(!layout)
        layout = Cookies.get('calendar-layout');

    $('.current-month').text(moment(selectedDay).format('MMMM') + " " + moment(selectedDay).format('YYYY'));

    $.ajax({
        type: "POST",
        url: "/../../backend/admin/ajax/calendar/get-calendar-grid.php",
        data: {
            'layout' : layout,
            'date' : selectedDay
        }
    }).done(function (data) {
        var parsedData = JSON.parse(data);

        var grid = $('.calendar-grid');
        grid.empty();

        if (layout === 'week') {
            fillWeekData(grid, parsedData);
        } else if (layout === 'three-day') {
            fillThreeDayData(grid, parsedData);
        } else if (layout === 'day') {
            fillDayData(grid, parsedData);
        }
    }).fail(function (data) {
        setInfoStatus('page', JSON.stringify(data), 'danger');
    });
}

function fillSmallCalendarGrid(month, year) {
    var smallGrid = $('.small-calendar-grid');

    if(!month || !year) {
        if (Cookies.get('small-calendar-month') && Cookies.get('small-calendar-year')) {
            month = Cookies.get('small-calendar-month');
            year = Cookies.get('small-calendar-year');
        }

        if(!month || !year) {
            month = moment().month() + 1;
            year = moment().year();

            Cookies.set('small-calendar-month', month);
            Cookies.set('small-calendar-year', year);
        }
    } else {
        Cookies.set('small-calendar-month', month);
        Cookies.set('small-calendar-year', year);
    }

    $('.small-current-month').text(moment().month(month-1).format('MMMM') + " " + year);

    $.ajax({
        type: "POST",
        url: "/../../backend/admin/ajax/calendar/get-small-calendar-grid.php",
        data: {
            'year' : year,
            'month' : month,
            'selected-date' : Cookies.get('calendar-selected-day'),
            'range' : Cookies.get('calendar-layout')
        }
    }).done(function (data) {
        var parsedData = JSON.parse(data);

        smallGrid.empty();
        fillSmallMonthData(smallGrid, parsedData);
    }).fail(function (data) {
        setInfoStatus('page', JSON.stringify(data), 'danger');
    });
}

function nextSmallSwitch(forward) {
    if(Cookies.get('small-calendar-month') && Cookies.get('small-calendar-year')) {
        var month = Cookies.get('small-calendar-month');
        var year = Cookies.get('small-calendar-year');

        if(forward) {
            if(month < 12)
                month++;
            else {
                month = 1;
                year++;
            }
        } else {
            if(month > 1)
                month--;
            else {
                month = 12;
                year--;
            }
        }

        fillSmallCalendarGrid(month, year);
    }
}

function fillSmallMonthData(grid, data) {
    var counter = 0;
    var weekCounter = 1;

    $.each(data.days, function (index, value) {
        counter++;

        if(value.dayOfTheWeek === "1")
            weekCounter++;

        var html = '<a class="month-day ' + ((value.dayOfTheWeek === "6" || value.dayOfTheWeek === "7") ? 'light' : '') + ' ' + ((value.isToday) ? 'today' : '') + ' ' + ((value.highlighted) ? 'highlighted' : '') + '" ' +
                'onclick="fillData(\'' + index + '\', null)" ' +
                'style="grid-column: ' + value.dayOfTheWeek + '; grid-row: ' + weekCounter + '">' +
             counter +
            '</a>';
        grid.append(html);
    });
}

function fillData(selectedDay, layout) {
    if(!selectedDay) {
        if (Cookies.get('calendar-selected-day')) {
            selectedDay = Cookies.get('calendar-selected-day');
        }

        if(!selectedDay) {
            selectedDay = moment().format('YYYY-MM-DD');

            Cookies.set('calendar-selected-day', selectedDay);
        }
    } else {
        Cookies.set('calendar-selected-day', selectedDay);
    }

    if(!layout) {
        if (Cookies.get('calendar-layout')) {
            layout = Cookies.get('calendar-layout');
        }

        if(!layout) {
            layout = 'week';

            Cookies.set('calendar-layout', layout);
        }
    } else {
        Cookies.set('calendar-layout', layout);
    }

    fillSmallCalendarGrid();
    fillCalendarGrid(selectedDay, layout);
}

function nextSwitch(forward) {
    var layout = Cookies.get('calendar-layout');
    var date = moment(Cookies.get('calendar-selected-day'));

    if(forward) {
        switch (layout) {
            case 'week':
                date = date.add(7, 'd');
                break;
            case 'three-day':
                date = date.add(3, 'd');
                break;
            case 'day':
                date = date.add(1, 'd');
                break;
        }
    } else {
        switch (layout) {
            case 'week':
                date = date.subtract(7, 'd');
                break;
            case 'three-day':
                date = date.subtract(3, 'd');
                break;
            case 'day':
                date = date.subtract(1, 'd');
                break;
        }
    }

    Cookies.set('calendar-selected-day', date.format('YYYY-MM-DD'));

    fillSmallCalendarGrid();
    fillCalendarGrid(date.format('YYYY-MM-DD'), null);
}

function getAvailableProducts(date, hours) {
    $.ajax({
        type: "POST",
        url: "/../../backend/admin/ajax/product/product-get-available.php",
        data: {
            'date' : date,
            'hours' : hours
        }
    }).done(function (data) {
        //alert(JSON.stringify(data));
        var parsedData = JSON.parse(data);

        var modal = $('#available-products-modal');

        var listAvailableProducts = modal.find('ul#available-products');
        listAvailableProducts.empty();

        $.each(parsedData, function (index, value) {
            listAvailableProducts.append('<li>' + value.name + '</li>');
        });

        modal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .one('click', '#cancel', function () {
                modal.modal('hide');
                modal.unbind('click');
            });

    }).fail(function (data) {
        setInfoStatus('page', JSON.stringify(data), 'danger');
    });
}

// WEEK

function fillWeekData(grid, data) {
    var counter = 0;

    var firstDate = '';
    var lastDate = '';
    var orderCounter = 0;
    var reservationCounter = 0;

    $.each(data.days, function (index, value) {
        if(counter === 0)
            firstDate = new Date(index);

        counter++;

        var date = new Date(index);

        var html = generateHTML(counter, date, value);

        grid.append(html);

        orderCounter += value.ordersCount;
        reservationCounter += value.reservationsCount;
        lastDate = new Date(index);
    });

    updateInfo(firstDate, lastDate, orderCounter, reservationCounter);
}

function outputOrders(order, terms, products) {
    var output = '';

    $.each(order, function (index, value) {
        var orderStateClass = 'unpaid';
        switch (value.orderStateId) {
            case '2':
            case '3':
            case '4':
                orderStateClass = 'confirmed';
                break;
            case '5':
                orderStateClass = 'sentInvoice';
                break;
            case '6':
                orderStateClass = 'cancelled';
                break;
            case '7':
                orderStateClass = 'finished';
                break;

            default:
                break;
        }

        output += '<a href="/backend/admin/pages/order-detail.php?orderId=' + value.orderId + '"><div class="order ' + orderStateClass + '">';
        var termStart = new Date(terms[index].start);
        var termEnd = new Date(termStart.getTime() + (terms[index].hours*60*60*1000));
        output += '<p class="time">' + twoDigit(termStart.getHours()) + ':' + twoDigit(termStart.getMinutes()) + ' - ' + twoDigit(termEnd.getHours()) + ':' + twoDigit(termEnd.getMinutes()) + '</p>';
        //output += ((parseInt(value.isPaid) === 0) ? '<p class="not-paid">Nezaplaceno</p>' : '');
        output += '<p class="place">Místo konání: ' + value.venueCity + '</p>';

        products[index] = products[index].toString().split(",");
        output += '<p class="product">';
            $.each(products[index], function (pIndex, pValue) {
                if(pValue.length > 40)
                    pValue = pValue.substring(0, 40) + '...';
                if(pIndex > 0)
                    output += ', ';
                output += pValue;
            });
        output += '</p>';
        if(value.adminNote)
            output += '<p class="place">Poznámka: ' + value.adminNote + '</p>';

        output += '</div></a>';
    });

    return output;
}

function outputReservations(reservations) {
    var output = '';

    $.each(reservations, function (index, value) {
        output += '<a href="/backend/admin/pages/reservation-management.php"><div class="reserv reservation">';
        var termStart = new Date(value.termStart);
        var termEnd = new Date(termStart.getTime() + (value.termHours*60*60*1000));
        output += '<p class="time">' + twoDigit(termStart.getHours()) + ':' + twoDigit(termStart.getMinutes()) + ' - ' + twoDigit(termEnd.getHours()) + ':' + twoDigit(termEnd.getMinutes()) + '</p>';
        var reservationEnd = new Date((new Date(value.reservationStart)).getTime() + (value.reservationHours*60*60*1000));
        var hourDifference = Math.round((reservationEnd.getTime() - Date.now()) / (60*60*1000));
        if(hourDifference > 0)
            output += '<p class="timeout">Rezervace vyprší za ' + hourDifference + ' hodin</p>';
        else
            output += '<p class="timeout">Rezervace vypršela. Brzy bude odstraněna z databáze.</p>';

        output += '<p class="product">' + value.quantity + 'x ' + value.productId + '</p>';

        output += '</div></a>';
    });

    return output;
}

// THREE DAY

function fillThreeDayData(grid, data) {
    var counter = 0;

    var firstDate = '';
    var lastDate = '';
    var orderCounter = 0;
    var reservationCounter = 0;

    $.each(data.days, function (index, value) {
        if(counter === 0) {
            firstDate = new Date(index);
        }

        counter++;

        var date = new Date(index);

        var html = generateHTML(counter, date, value);

        grid.append(html);

        orderCounter += value.ordersCount;
        reservationCounter += value.reservationsCount;
        lastDate = new Date(index);
    });

    updateInfo(firstDate, lastDate, orderCounter, reservationCounter);
}

// SINGLE DAY

function fillDayData(grid, data) {
    var counter = 0;

    var firstDate = '';
    var lastDate = '';
    var orderCounter = 0;
    var reservationCounter = 0;

    $.each(data.days, function (index, value) {
        if(counter === 0) {
            firstDate = new Date(index);
        }

        counter++;

        var date = new Date(index);

        var html = generateHTML(counter, date, value);

        grid.append(html);

        orderCounter += value.ordersCount;
        reservationCounter += value.reservationsCount;
        lastDate = new Date(index);
    });

    updateInfo(firstDate, lastDate, orderCounter, reservationCounter);
}

// HELPERS

var weekDaysCS = [
    'Pondělí',
    'Úterý',
    'Středa',
    'Čtvrtek',
    'Pátek',
    'Sobota',
    'Neděle'
];

function generateHTML(counter, date, value) {
    return '<div class="calendar-day" style="grid-column: ' + counter + '">\n' +
            '<div class="day-header" style="grid-row: 1">\n' +
                '<p>' + weekDaysCS[value.dayOfTheWeek - 1] + '</p>\n' +
                '<h3 class="">' + date.getDate() + '.' + ((value.isToday) ? ' (Dnes)' : '') + '</h3><span class="ti-search" onclick="getAvailableProducts(\'' + dateFormat(date) + ' 00:00\', 24)"></span>\n' +
            '</div>\n' +
            ((value.ordersCount > 0) ? outputOrders(value.ordersData.orders, value.ordersData.orderTerms, value.ordersData.orderProducts) : '') +
            ((value.reservationsCount > 0) ? outputReservations(value.reservationsData) : '') +
        '</div>';
}

function twoDigit(number) {
    return ((number > 9) ? number : "0" + number);
}

function updateInfo(firstDate, lastDate, orderCount, reservationCount) {
    if(firstDate.getTime() === lastDate.getTime())
        $('.dates').text(' ze dne ' + dateFormat(firstDate));
    else
        $('.dates').text('od ' + dateFormat(firstDate) + ' do ' + dateFormat(lastDate));
    $('.order-count').text(orderCount);
    $('.reservation-count').text(reservationCount);
}

function dateFormat(date) {
    return date.getDate() + '. ' + (date.getMonth()+1) + '. ' + date.getFullYear();
}