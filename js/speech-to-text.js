function toggleVoiceControl() {
  //beforeState = enableVoiceControl;
  alert("Nyní prosím znovunačtěte stránku. (F5)");
  var enableVoiceControl = document.getElementById("enableVoiceControl").checked;
  //alert(enableVoiceControl);
  localStorage.setItem("voice-control", enableVoiceControl);
  //startVoiceControl();
}

$(document).ready(function() {

  var ignore_onend = false;
  var final_transcript = '';
  var recognizing = false;
  var enableVoiceControl = null;
  //alert(localStorage.getItem("voice-control"));
  if(localStorage.getItem("voice-control") == null){
    localStorage.setItem("voice-control", true);
    enableVoiceControl = true;
  } else {
    enableVoiceControl = localStorage.getItem("voice-control");
  }

  if(enableVoiceControl == "true"){
    //alert("Enabl");
    document.getElementById("enableVoiceControl").checked = true;
  } else if(enableVoiceControl == "false") {
    //alert("Disabl");
    document.getElementById("enableVoiceControl").checked = false;
  }

  if(enableVoiceControl)
    startVoiceControl();

  //alert(enableVoiceControl);

  function startVoiceControl(){
    //je poslouchani povoleno?
    if(enableVoiceControl == "true"){
      //zjistim jestli prohlizec podporuje (nyni jen chrome)
      if (!('webkitSpeechRecognition' in window)) {
        //vse schovám
        upgrade();
      } else {
        //pokud ano
        var recognition = new webkitSpeechRecognition();
        recognition.continuous = true;
        recognition.interimResults = true;
        recognition.lang = 'cs-CZ';
        recognition.start();

        recognition.onstart = function() {
          recognizing = true;
        };

        recognition.onerror = function(event) {
          if (event.error == 'no-speech') {
            ignore_onend = true;
          }
          if (event.error == 'audio-capture') {
            ignore_onend = true;
          }
          if (event.error == 'not-allowed') {
            /*if (event.timeStamp - start_timestamp < 100) {
              showInfo('info_blocked');
            } else {
              showInfo('info_denied');
            }*/
            ignore_onend = true;
          }
        };

        recognition.onend = function() {
          recognizing = false;
          if (ignore_onend) {
            return;
          }
          if (!final_transcript) {
            return;
          }
        };

        recognition.onresult = function(event) {
          var interim_transcript = '';
          for (var i = event.resultIndex; i < event.results.length; ++i) {
            if (event.results[i].isFinal) {
              final_transcript += event.results[i][0].transcript;
            } else {
              interim_transcript += event.results[i][0].transcript;
            }
          }

          sendInterimTranscript(interim_transcript);
        };
      }

      function sendInterimTranscript(interim_transcript){
        if(enableVoiceControl == "true"){
          if(interim_transcript.indexOf("profil") > -1 || interim_transcript.indexOf("já") > -1){
            window.location.href = "/admin/pages/user.php";
          } else if (interim_transcript.indexOf("produkt") > -1){
            if (interim_transcript.indexOf("nový") > -1)
              document.getElementById("new-product").style.display = "table-row";
            else
              window.location.href = "/admin/pages/products.php";
          } else if (interim_transcript.indexOf("kategor") > -1){
            window.location.href = "/admin/pages/categories.php";
          } else if (interim_transcript.indexOf("obráz") > -1 || interim_transcript.indexOf("foto") > -1){
            window.location.href = "/admin/pages/images.php";
          } else if (interim_transcript.indexOf("objedn") > -1){
            window.location.href = "/admin/pages/orders.php";
          } else if (interim_transcript.indexOf("kupón") > -1 || interim_transcript.indexOf("kupon") > -1 || interim_transcript.indexOf("slev") > -1){
            window.location.href = "/admin/pages/coupons.php";
          }
        }
        //alert(interim_transcript);
      }

    }
  }

  function upgrade() {
    document.getElementById("voice-control").innerHTML = "<small><small>Váš prohlížeč nepodporuje ovládání hlasem. Pokud jej chcete, používejte Google Chrome.</small></small>";
    document.getElementById("voice-control").style.margin = "10px 0px";
  }

});