$(function () {



    $('.check-availability').click(function () {
        var date = $('#availableTermStart').val();
        var hours = $('#availableTermHours').val();
        var productId = $('.actions').attr('product-id');

        var availabilityResultDiv = $('.availability-result');

        if(date === '') {
            setAvaResult(availabilityResultDiv, 'danger', 'Vyberte datum');
            return;
        }
        if(hours === '' || hours < 1){
            setAvaResult(availabilityResultDiv, 'danger', 'Vyberte počet hodin');
            return;
        }
        if(hours > 48){
            setAvaResult(availabilityResultDiv, 'danger', 'Maximální počet hodin je 48');
            return;
        }
        if(!productId){
            setAvaResult(availabilityResultDiv, 'danger', 'Nepodařilo se načíst ID produktu');
            return;
        }

        // change term in cart
        saveCartDateDataWithHours(date, 1, [5]);
        var momentDate = moment(date, 'DD. MM. YYYY hh:mm');
        Cookies.set('search-date', momentDate);

        $.ajax({
            type: 'post',
            url: "/../../backend/admin/ajax/product/check-availability.php",
            data: {
                date : date,
                hours : hours,
                productId : productId
            }
        }).done(function(data) {
            // alert(JSON.stringify(data));
            var parsedData = JSON.parse(data);

            if(parsedData.result) {
                setAvaResult(availabilityResultDiv, parsedData.result, parsedData.text);
            }
        }).fail(function(data) {
            alert(JSON.stringify(data));
        });
    });
    
    function setAvaResult(div, state, text) {
        div.parent().show();
        if(state === "danger"){
            div.parent().removeClass('alert-success').removeClass('alert-warning').addClass('alert-danger');
        } else if(state === "warning") {
            div.parent().removeClass('alert-success').removeClass('alert-danger').addClass('alert-warning');
        } else if(state === "success") {
            div.parent().removeClass('alert-warning').removeClass('alert-danger').addClass('alert-success');
        }

        div.html(text);
    }
});