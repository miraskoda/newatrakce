$(function () {
    var ul = $('#price-list').find('ul');
    ul.find('li').each(function () {
        if($(this).has('ul').length){
            $(this).click(function (ev) {
                ev.stopPropagation();
                $(this).find('ul').first().slideToggle();
                var span = $(this).find('span').first();
                if(span.hasClass('ti-angle-up')){
                    span.removeClass('ti-angle-up').addClass('ti-angle-down')
                } else {
                    span.removeClass('ti-angle-down').addClass('ti-angle-up')
                }
            });
        }
    });
});