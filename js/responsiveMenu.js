var displayP = 'display';
var blockP = 'block';

function hasTouch() {
    return 'ontouchstart' in document.documentElement
        || navigator.maxTouchPoints > 0
        || navigator.msMaxTouchPoints > 0;
}

if (hasTouch()) {
    try {
        for (var si in document.styleSheets) {
            var styleSheet = document.styleSheets[si];
            if (!styleSheet.rules) continue;

            for (var ri = styleSheet.rules.length - 1; ri >= 0; ri--) {
                if (!styleSheet.rules[ri].selectorText) continue;

                if (styleSheet.rules[ri].selectorText.match(':hover')) {
                    styleSheet.deleteRule(ri);
                }
            }
        }
    } catch (ex) {
    }
}

function toggleVisibility(node) {
    var current = node.style[displayP];
    if (current === blockP) {
        node.style.setProperty('display', 'none', 'important');
    } else {
        node.style.setProperty('display', 'block', 'important');
    }
}

var toggleButton = document.querySelector('#toggleButton');
toggleButton.addEventListener('click', function (e) {
    e.stopPropagation();
    var navbarId = toggleButton.getAttribute('data-target');
    var navbar = document.querySelector(navbarId);
    toggleVisibility(navbar);
});

var timeClicked;
var openP = 'open';

$('.dropdown-toggle').on('touchstart', function () {
    timeClicked = new Date().getTime();
});

// MOBILE NATIVE DROPDOWNbox light-blue
$(".dropdown-toggle").on('click touchend', function (e) {
    e.preventDefault();
    var link = $(this).closest('a').attr('href');
    if (link === '/kosik/') {
        if(e.type === 'touchend' && (new Date().getTime() - timeClicked < 111)) {
            window.location.href = link;
        }
        else if (e.type === 'click') {
            window.location.href = link;
        }
    }
    var parent = $(this).attr('data-target');
    var parentNode = document.querySelector(parent);
    if (parentNode) {
        parentNode.classList.toggle(openP);
    }
});

$('.dropdown-menu li a').on('click', function () {
    var link = $(this).attr('href');
    window.location.href = link;
});

$('.dropdown-menu li a').on('touchstart', function () {
    timeClicked = new Date().getTime();
});

$('.dropdown-menu li a').on('touchend', function () {
    var newDate = new Date().getTime();
    if (newDate - timeClicked < 80) {
        var link = $(this).attr('href');
        window.location.href = link;
    }
});
