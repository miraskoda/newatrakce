$(function () {
    $('.reserve').click(function (ev) {
        ev.preventDefault();

        var productId = $(this).closest('.item-box').attr('product-id');
        if(!productId)
            productId = $(this).closest('.actions').attr('product-id');

        // check if user is logged in
        $.ajax({
            type: "POST",
            url: "/../../backend/admin/ajax/customer/logged.php",
            data: {}
        }).done(function(data) {
            //alert(JSON.stringify(data));
            if(data === "1") {
                var reservationModal = $('#reservation');

                reservationModal.modal({
                    backdrop: 'static',
                    keyboard: false
                })
                    .on('click', '.save', function (ev) {
                        $.ajax({
                            type: "POST",
                            url: "/../../backend/admin/ajax/reservation/reservation-create.php",
                            data: {
                                productId: productId,
                                quantity: reservationModal.find('input[name=quantity]').val(),
                                termStart: reservationModal.find('input[name=termStart]').val(),
                                termHours: reservationModal.find('input[name=termHours]').val(),
                                reservationHours: reservationModal.find('input[name=reservationHours]').val()
                            }
                        }).done(function(data) {
                            if(data === "1") {
                                // close reservation form
                                reservationModal.modal('hide');
                                reservationModal.unbind('click');

                                // show success modal
                                var reservationSuccessModal = $('#reservation-success');
                                reservationSuccessModal.modal({
                                    backdrop: 'static',
                                    keyboard: false
                                })
                                .one('click', '.cancel', function () {
                                    reservationSuccessModal.modal('hide');
                                    reservationSuccessModal.unbind('click');
                                });
                            } else {
                                setInfoStatus('reservation', data, 'warning');
                            }
                        }).fail(function(data) {
                            alert(JSON.stringify(data));
                        });
                    })
                    .one('click', '.cancel', function () {
                        reservationModal.modal('hide');
                        reservationModal.unbind('click');
                    });
            } else {
                var userNotLoggedModal = $('#user-not-logged');

                userNotLoggedModal.modal({
                    backdrop: 'static',
                    keyboard: false
                })
                    .one('click', '.login', function () {
                        window.location.href = '/prihlaseni/';
                    })
                    .one('click', '.cancel', function () {
                        userNotLoggedModal.modal('hide');
                        userNotLoggedModal.unbind('click');
                    });
            }
        }).fail(function(data) {
            alert(JSON.stringify(data));
        });
    });
});