$(function () {

    var requestModal = $('#request-order-change');
    var errorModal = $('#change-alert');
    var successModal = $('#success-order-request-change');

    $('.request-change-order').click(function (ev) {
        ev.preventDefault();
        var orderId = $(this).closest('a').attr('data-target');

        requestModal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .on('click', '.send-request', function () {
                var requestChange = $('#request-change-text-area').val();
                $.ajax({
                    type: "POST",
                    url: "/../../backend/admin/ajax/order/order-request-change.php",
                    data: {orderId: orderId, requestChange: requestChange}
                }).done(function (data) {
                    // alert(JSON.stringify(data));
                    if (data === "1") {
                        successModal.modal('hide');
                        successModal.unbind('click');

                        successModal.modal({
                            backdrop: 'static',
                            keyboard: false
                        })
                            .on('click', '.request-close', function () {
                                successModal.modal('hide');
                                successModal.unbind('click');
                            })

                    } else {
                        errorModal.modal({
                            backdrop: 'static',
                            keyboard: false
                        })
                            .on('click', '.request-close-error', function () {
                                successModal.modal('hide');
                                successModal.unbind('click');
                            })
                    }
                }).fail(function (data) {
                    errorModal.modal({
                        backdrop: 'static',
                        keyboard: false
                    })
                        .on('click', '.request-close-error', function () {
                            successModal.modal('hide');
                            successModal.unbind('click');
                        })
                });
            })
            .one('order-delete-close', '.decline-request', function () {
                requestModal.modal('hide');
                requestModal.unbind('click');
            });
    });
});