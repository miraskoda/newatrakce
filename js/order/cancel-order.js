$(function () {
    $('.cancel-order').click(function (ev) {
        ev.preventDefault();
        var orderId = $(this).closest('a').attr('data-target');
        var deleteModal = $('#order-delete');
        deleteModal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .on('click', '.order-delete-agree', function () {
                $.ajax({
                    type: "POST",
                    url: "/../../backend/admin/ajax/order/order-delete.php",
                    data: { orderId: orderId }
                }).done(function (data) {
                    alert(JSON.stringify(data));
                    if (data === "1") {
                        // close reservation form
                        deleteModal.modal('hide');
                        deleteModal.unbind('click');
                    } else {
                        setInfoStatus('order', data, 'warning');
                    }
                }).fail(function (data) {
                    // alert(JSON.stringify(data));
                });
            })
            .one('order-delete-close', '.cancel', function () {
                deleteModal.modal('hide');
                deleteModal.unbind('click');
            });
    });
});