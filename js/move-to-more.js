$(function () {
   $('.more a').click(function (ev) {
       ev.preventDefault();

       var position = $('.owl-carousel').height();

       $(this).closest('.container-fluid').animate({
           scrollTop: position
       }, 1000);
   });
});