var getConfig = function () {
  return {
      groupCells: true,
      freeScroll: true,
      prevNextButtons: true,
      pageDots: false,
      wrapAround: false,
  };
};

var elem = document.querySelector('.favourite-carousel');
new Flickity(elem, getConfig());

var priceElem = document.querySelector('.price-carousel');
new Flickity(priceElem, getConfig());




var eleme = document.querySelector('.carousel');
new Flickity( eleme, {
    groupCells: true,
    contain: true,
    wrapAround: true,
    imagesLoaded: true,
    percentPosition: false,
    bgLazyLoad: true
});



var ele = document.querySelector('.tips-carousel');
new Flickity( ele, {
    groupCells: true,
    freeScroll: true,
    prevNextButtons: true,
    pageDots: false,
    wrapAround: true
});


var ee = document.querySelector('.gallery-carousel');
new Flickity( ee, {
    groupCells: true,
    freeScroll: true,
    prevNextButtons: true,
    pageDots: true,
    wrapAround: true
});


var eee = document.querySelector('.product-carousel');
new Flickity( eee, {
    groupCells: true,
    freeScroll: true,
    prevNextButtons: false,
    pageDots: false,
    wrapAround: false
});