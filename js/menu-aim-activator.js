/**
 * Created by ondra on 06.05.2017.
 */

function isMobile() {
    return window.matchMedia("only screen and (max-width: 760px)").matches;
}

function isTablet() {
    return window.matchMedia("only screen and (max-width: 1270px)").matches;
}

var $menu = $(".dropdown-menu");

// jQuery-menu-aim: <meaningful part of the example>
// Hook up events to be fired on menu row activation.
$menu.menuAim({
    activate: activateSubmenu,
    deactivate: deactivateSubmenu
});
// jQuery-menu-aim: </meaningful part of the example>

// jQuery-menu-aim: the following JS is used to show and hide the submenu
// contents. Again, this can be done in any number of ways. jQuery-menu-aim
// doesn't care how you do this, it just fires the activate and deactivate
// events at the right times so you know when to show and hide your submenus.
function activateSubmenu(row) {
    var $row = $(row),
        submenuId = $row.data("submenuId"),
        $submenu = $("#" + submenuId),
        height = $menu.outerHeight(),
        width = $menu.outerWidth();

    // Show the submenu
    /*$submenu.css({
        display: "block",
        //top: -1,
        //left: width - 3,  // main should overlay submenu
        /!*height: height  // padding for main dropdown's arrow*!/
    });*/
    $submenu.css({display: 'block'});

    // Keep the currently activated row's highlighted look
    // $row.find("a").addClass("maintainHover");
}

function deactivateSubmenu(row) {
    var $row = $(row),
        submenuId = $row.data("submenuId"),
        $submenu = $("#" + submenuId);

    // Hide the submenu and remove the row's highlighted look
    $submenu.css({display: 'none'});
    // $row.find("a").removeClass("maintainHover");
}

$(document).click(function () {
    // Simply hide the submenu on any click. Again, this is just a hacked
    // together menu/submenu structure to show the use of jQuery-menu-aim.
    $(".popover").css("display", "none");
    $("a.maintainHover").removeClass("maintainHover");
});

var isMouseInMenu = false;

$(".dropdown-toggle").mouseenter(function () {
    if (!isTablet()) {
        $(this).parent().addClass("open");
        if (!overlayActivated)
            onOverlay();
        overlayActivated = true;
    }
});

$(".dropdown-toggle").mouseleave(function () {
    var toggleParent = $(this).parent();
    setTimeout(function () {
        if (!isMouseInMenu) {
            toggleParent.removeClass("open");
        }
    }, 200);
});
$('.navbar').mouseleave(function () {
    offOverlay();
    overlayActivated = false;
});
$(".dropdown-menu").mouseenter(function () {
    if (!isTablet()) {
        isMouseInMenu = true;
        onOverlay();
        overlayActivated = true;
    }
});
$(".dropdown-menu").mouseleave(function () {
    $(this).parent().removeClass("open");
    offOverlay();
    overlayActivated = false;
    isMouseInMenu = false;
});


// on click on menu text go to link
$("span.topmenu-text").click(function () {
    // get href
    var link = $(this).closest('a').attr('href');
    if (link) {
        if (isTablet()) {
            return false;
        } else {
            window.location.href = link;
        }
    }
});


