<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 03.08.2018
 * Time: 14:20
 */
use backend\models\Url;

?>

<?php
echo '<div class="container-fluid">';
?>

	<div class="row">
		<div class="col-sm-8 col-md-offset-2">

			<?php
			if ( have_posts() ) : while ( have_posts() ) : the_post();
				get_template_part( 'content-single', get_post_format() );
			endwhile; endif;
			?>

		</div> <!-- /.col -->
	</div> <!-- /.row -->

<?php
require_once Url::getBackendPathTo("/modules/page-parts/footer.php");

echo '</div>';
echo '<div id="overlay"></div>';

echo '<script>';
require_once Url::getPathTo("/js/cart/add-to-cart.js");
echo '</script>';
require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

// modals
require_once Url::getBackendPathTo("/modules/modals/cart-action.php");
require_once Url::getBackendPathTo("/modules/modals/reservation.php");
require_once Url::getBackendPathTo("/modules/modals/login-modal.php");
require_once Url::getBackendPathTo("/modules/modals/reservation-success.php");
require_once Url::getBackendPathTo("/modules/modals/user-not-logged.php");
?>