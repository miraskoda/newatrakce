<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 08.11.2017
 * Time: 11:26
 */


require_once __DIR__ . "/../../../../backend/modules/app/prepare.php";

use backend\controllers\CustomerController;
use backend\models\Url;

$customer = CustomerController::isLoggedCustomer();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo get_bloginfo( 'description' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "WebSite",
      "name": "OnlineAtrakce.cz",
      "alternateName": "OnlineAtrakce",
      "url": "https://www.onlineatrakce.cz/",
      "potentialAction": {
        "@type": "SearchAction",
        "target": "https://www.onlineatrakce.cz/vyhledavani/?search={search_term_string}",
        "query-input": "required name=search_term_string"
      }
    }
    </script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!--Bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="/js/menu-aim.js"></script>

    <!--Bootstrap datetimepicker-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/locale/cs.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

    <!--Custom styles-->
    <link rel="stylesheet" href="/styles/style.min.css">
    <link rel="stylesheet" href="/styles/themify-icons.min.css">
    <link rel="stylesheet" href="/styles/online-atrakce.min.css">

    <!--Animations-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

    <!--Fonts-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700&subset=latin,latin-ext" rel="stylesheet">

	<?php include_once($root . "/modules/app/analyticstracking.php") ?>

    <?php wp_head(); ?>
</head>

<body id="public">

<?php

require_once Url::getBackendPathTo("/modules/page-parts/header.php");

?>
<!--<div class="container-fluid">

    <div class="page-top">
        <div class="row">
            <div class="col-md-12 header-container">
                <div class="header animated zoomIn">
                    <div class="title">
                        <img src="<?/*= get_template_directory_uri() */?>/assets/img/logo-round.png">
                        <h1><a href="<?php /*echo get_bloginfo( 'wpurl' ); */?>">Broccoli Games</a></h1>
                    </div>
                    <p class="description">Tvorba mobilních, počítačových a webových her na zakázku.</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 nav-container">
                <nav class="nav animated fadeIn">
                    <li><a class="nav-item active" href="#">Domů</a></li>
					<?php /*wp_list_pages( '&title_li=' ); */?>
                </nav>
            </div>
        </div>

	    <?php /*get_template_part( 'main-page/carousel/carousel' ); */?>
    </div>

-->