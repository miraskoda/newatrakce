<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 08.11.2017
 * Time: 12:17
 */

// WordPress Titles
add_theme_support( 'title-tag' );
// Support Featured Images
add_theme_support( 'post-thumbnails' );
/*
// Add Google Fonts
function broccolitheme_google_fonts() {
	wp_register_style('Josefin Sans', 'http://fonts.googleapis.com/css?family=Josefin+Sans:300,400,600&subset=latin,latin-ext');
	wp_enqueue_style( 'Josefin Sans');
}

add_action('wp_print_styles', 'broccolitheme_google_fonts');*/

// Add scripts and stylesheets
/*function broccolitheme_scripts() {
	// JQuery
	wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js', [], '', true );
	wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js', [], '', true );

	wp_enqueue_style( 'styles', get_template_directory_uri() . '/style.min.css' );
	wp_enqueue_style( 'themify-icons', get_template_directory_uri() . '/css/themify-icons.min.css' );

	// Bootstrap
	wp_enqueue_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js', ['jquery'], '', true );

	// OWL carousel
	wp_enqueue_style( 'owl-carousel-style', get_template_directory_uri() . '/plugins/owlcarousel/dist/assets/owl.carousel.min.css' );
	wp_enqueue_style( 'owl-carousel-theme', get_template_directory_uri() . '/plugins/owlcarousel/dist/assets/owl.theme.default.min.css' );
	wp_enqueue_script( 'owl-carousel-js', get_template_directory_uri() . '/plugins/owlcarousel/dist/owl.carousel.min.js', ['jquery'], '1', true );
	wp_enqueue_script( 'owl-carousel-init', get_template_directory_uri() . '/js/owl-carousel-init.js', ['jquery'], '1', true );

	// Animate.css
	wp_enqueue_style( 'animate', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css' );
    wp_enqueue_script( 'more-scroll', get_template_directory_uri() . '/js/more-scroll.min.js', ['jquery'], '1', true );

	// Google maps
	wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDReSgCP9sITDeaBodHL83oSSM-iVNUAOI&callback=initMap', ['google-maps-init'], '1', true );
	wp_enqueue_script( 'google-maps-init', get_template_directory_uri() . '/js/google-maps-init.min.js', ['jquery'], '1', true );
}

add_action( 'wp_enqueue_scripts', 'broccolitheme_scripts' );*/

//CUSTOM POST TYPE - CAROUSEL
/*function broccolitheme_carousel_post_type() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Carousel', 'Post Type General Name', 'broccolitheme' ),
        //'singular_name'       => _x( 'Carousel', 'Post Type Singular Name', 'broccolitheme' ),
        'menu_name'           => __( 'Carousel', 'broccolitheme' ),
        'parent_item_colon'   => __( 'Parent Carousel', 'broccolitheme' ),
        'all_items'           => __( 'All Carousel Posts', 'broccolitheme' ),
        'view_item'           => __( 'View Carousel', 'broccolitheme' ),
        'add_new_item'        => __( 'Add New Carousel', 'broccolitheme' ),
        'add_new'             => __( 'Add Carousel', 'broccolitheme' ),
        'edit_item'           => __( 'Edit Carousel', 'broccolitheme' ),
        'update_item'         => __( 'Update Carousel', 'broccolitheme' ),
        'search_items'        => __( 'Search Carousel', 'broccolitheme' ),
        'not_found'           => __( 'Not Found', 'broccolitheme' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'broccolitheme' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'carousel', 'broccolitheme' ),
        'description'         => __( 'Carousel posts', 'broccolitheme' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.

        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'capability_type'     => 'post',
    );

    // Registering your Custom Post Type
    register_post_type( 'carousel', $args );

}*/

/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/

/*add_action( 'init', 'broccolitheme_carousel_post_type', 0 );


//PROJECTS POST TYPE
function broccolitheme_projects_post_type() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Projects', 'Post Type General Name', 'broccolitheme' ),
		'singular_name'       => _x( 'Project', 'Post Type Singular Name', 'broccolitheme' ),
		'menu_name'           => __( 'Projects', 'broccolitheme' ),
		'parent_item_colon'   => __( 'Parent Project', 'broccolitheme' ),
		'all_items'           => __( 'All Project Posts', 'broccolitheme' ),
		'view_item'           => __( 'View Project', 'broccolitheme' ),
		'add_new_item'        => __( 'Add New Project', 'broccolitheme' ),
		'add_new'             => __( 'Add Project', 'broccolitheme' ),
		'edit_item'           => __( 'Edit Project', 'broccolitheme' ),
		'update_item'         => __( 'Update Project', 'broccolitheme' ),
		'search_items'        => __( 'Search Project', 'broccolitheme' ),
		'not_found'           => __( 'Not Found', 'broccolitheme' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'broccolitheme' ),
	);

// Set other options for Custom Post Type

	$args = array(
		'label'               => __( 'project', 'broccolitheme' ),
		'description'         => __( 'Company projects', 'broccolitheme' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', ),
		// You can associate this CPT with a taxonomy or custom taxonomy.
		'taxonomies'          => array( 'genres' ),

		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'capability_type'     => 'post',
	);

	// Registering your Custom Post Type
	register_post_type( 'projects', $args );

}*/

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

//add_action( 'init', 'broccolitheme_projects_post_type', 0 );