<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 08.11.2017
 * Time: 11:28
 */

use backend\models\Url;

?>
<?php
echo '<div class="container-fluid">';

//require_once Url::getBackendPathTo("/modules/page-parts/category/category-content.php");
get_template_part("main-page/blog-list");

require_once Url::getBackendPathTo("/modules/page-parts/footer.php");

echo '</div>';
echo '<div id="overlay"></div>';

echo '<script>';
require_once Url::getPathTo("/js/cart/add-to-cart.js");
echo '</script>';
require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

// modals
require_once Url::getBackendPathTo("/modules/modals/cart-action.php");
require_once Url::getBackendPathTo("/modules/modals/reservation.php");
require_once Url::getBackendPathTo("/modules/modals/login-modal.php");
require_once Url::getBackendPathTo("/modules/modals/reservation-success.php");
require_once Url::getBackendPathTo("/modules/modals/user-not-logged.php");
?>
<!--<div class="col-md-3">
    <div class="blog-post">
        <h2 class="blog-post-title"><a href="<?php /*the_permalink(); */?>"><?php /*the_title(); */?></a></h2>

        <?php /*the_content('... číst dál', false); */?>
    </div>
</div><!-- /.blog-post -->