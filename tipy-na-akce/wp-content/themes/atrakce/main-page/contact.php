<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 20.11.2017
 * Time: 13:33
 */
?>

<div class="col-md-12 contact">
    <div id="map"></div>
    <div id="disabler"></div>

    <h3>Kontakt</h3>

    <div class="row">
		<div class="col-md-8 mx-auto contact-list">
			<div class="row">
				<div class="col-md-3">
					<span class="ti-user"></span>
					<p>Ondřej Šimeček</p>
				</div>
				<div class="col-md-3">
					<span class="ti-mobile"></span>
					<p><a href="tel: +420730632652">730 632 652</a></p>
				</div>
				<div class="col-md-3">
					<span class="ti-email"></span>
					<p><a href="mail:info@broccoli.games">info@broccoli.games</a></p>
				</div>
				<div class="col-md-3">
					<span class="ti-map"></span>
					<p>Butoves 79, 506 01 Jičín</p>
				</div>
			</div>
		</div>
		<div class="col-md-12 button">
			<a href="">Napište nám</a>
		</div>
	</div>

</div>