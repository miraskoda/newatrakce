<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 14.11.2017
 * Time: 1:10
 */
?>

<div class="company-features-container">
    <div class="col-md-8 mx-auto company-features">

        <h3>Co umíme</h3>

        <div class="row">
			<?php
			$args = array(
				'posts_per_page' => 6,
				'category_name'  => 'company-features'
			);

			$blogQuery = new WP_Query( $args );
			while ( $blogQuery->have_posts() ) : $blogQuery->the_post();
				get_template_part( 'main-page/company-features/company-features-content', get_post_format() );
			endwhile;
			?>
        </div>

        <div class="contact-button"><a href="<?php get_permalink( get_page_by_title( 'Kontakt' ) ); ?>">Kontaktujte nás</a>
        </div>

    </div>
</div>