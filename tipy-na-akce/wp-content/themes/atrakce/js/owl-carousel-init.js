jQuery(document).ready(function($) {
    //large carousel
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        loop: true,
        nav: true,
        video: true,
        autoplay: true,
        navText: ["<span class='ti-angle-left'></span>", "<span class='ti-angle-right'></span>"],
        smartSpeed: 1000,
        autoplayTimeout: 5000,
        //autoplaySpeed: 4000,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });

    //Events
    owl.on('changed.owl.carousel', function () {
        /*$('.item .animated').removeClass("fadeInDown").addClass("infinite").addClass("zoomIn").animate({}, {
            duration: 1000, start: function () {
                setTimeout(function () {
                    $('.item .animated').removeClass('infinite').removeClass("zoomIn");
                }, 1000);
            }
        });*/
    })
});