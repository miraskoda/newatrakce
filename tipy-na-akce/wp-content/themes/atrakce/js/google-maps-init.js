function initMap() {
    var broccoliGames = {lat: 50.435, lng: 15.361};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: broccoliGames,
        disableDefaultUI: true
    });
    var marker = new google.maps.Marker({
        position: broccoliGames,
        map: map
    });
}