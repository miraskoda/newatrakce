<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 21.08.2017
 * Time: 10:11
 */

use backend\controllers\CartController;
use backend\models\Cart;

require_once __DIR__ . "/../../../modules/app/prepare.php";

if(isset($_POST['additionToProductId']))
    $remove = CartController::removeAdditionFromCart();
else
    $remove = CartController::removeFromCart();

if($remove == true) {
    $add = CartController::addToCart();
    if(is_a($add, Cart::class))
        echo true;
    else
        echo $add;
} else {
    echo $remove;
}