<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 22.08.2017
 * Time: 11:23
 */

use backend\controllers\OrderTermController;

require_once __DIR__ . "/../../../../modules/app/prepare.php";

echo OrderTermController::removeLastOrderTerm();