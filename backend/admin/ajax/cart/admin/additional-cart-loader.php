<?php
/**
 * Created by PhpStorm.
 * User: Adam
 */

use backend\view\CartGrid;

require_once __DIR__ . "/../../../../modules/app/prepare.php";

if(isset($_POST['productId']))
    echo CartGrid::generateAdditionalList($_POST['productId']);