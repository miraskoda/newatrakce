<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 18.08.2017
 * Time: 13:26
 */

use backend\view\CartGrid;

require_once __DIR__ . "/../../../../modules/app/prepare.php";

if(isset($_POST['rentHours']) && is_array($_POST['rentHours']) && count($_POST['rentHours']) == 5)
    echo CartGrid::generatePrice($_POST['rentHours']);