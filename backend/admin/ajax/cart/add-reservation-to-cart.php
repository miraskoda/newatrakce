<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 13.11.2018
 * Time: 17:18
 */

use backend\controllers\CartController;
use backend\models\Cart;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$cart = CartController::saveCartDataFromReservation();
if(is_a($cart, Cart::class))
    echo true;
else
    echo $cart;