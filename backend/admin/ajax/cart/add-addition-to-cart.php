<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 27.03.2018
 * Time: 12:18
 */

use backend\controllers\CartController;
use backend\models\Cart;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$cart = CartController::saveAdditionalCartData();
if(is_a($cart, Cart::class))
	echo true;
else
	echo $cart;