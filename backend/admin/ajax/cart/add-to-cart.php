<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 14.08.2017
 * Time: 12:22
 */

use backend\controllers\CartController;
use backend\models\Cart;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$cart = CartController::addToCart();
if(is_a($cart, Cart::class))
    echo true;
else
    echo $cart;