<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 07.09.2017
 * Time: 11:20
 */

use backend\controllers\OrderController;

require_once __DIR__ . "/../../../modules/app/prepare.php";
$result = OrderController::validateCart();
if($result === true) {
    echo true;
} else {
    echo $result;
}