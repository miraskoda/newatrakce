<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 09.04.2018
 * Time: 14:30
 */

use backend\controllers\SuperproductProductController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo json_encode(SuperproductProductController::getProductsBySuperproduct());