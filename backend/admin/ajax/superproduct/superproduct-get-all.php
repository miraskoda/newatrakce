<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 08.04.2018
 * Time: 17:58
 */

use backend\models\Superproduct;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo json_encode(Superproduct::getSuperproducts(true));