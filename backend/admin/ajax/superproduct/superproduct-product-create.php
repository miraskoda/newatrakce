<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 10.04.2018
 * Time: 9:36
 */

use backend\controllers\SuperproductProductController;
use backend\models\SuperproductProduct;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$result = SuperproductProductController::createSuperproductProduct();
if(is_a($result, SuperproductProduct::class)) {
	echo true;
} else {
	echo $result;
}