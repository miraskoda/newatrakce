<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 31.05.2017
 * Time: 13:29
 */

use backend\controllers\CategoryController;
use backend\models\Category;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$imageUpdateResult = CategoryController::updateCategoryImage();
if(is_a($imageUpdateResult, Category::class))
    echo true;
else
    echo $imageUpdateResult;