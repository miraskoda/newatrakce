<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 23.05.2017
 * Time: 1:08
 */

use backend\controllers\CategoryController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo CategoryController::deleteCategory();