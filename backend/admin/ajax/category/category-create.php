<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 22.05.2017
 * Time: 21:02
 */

use backend\controllers\CategoryController;
use backend\models\Category;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$categoryCreateResult = CategoryController::createCategory();
if(is_a($categoryCreateResult, Category::class))
    echo true;
else
    echo $categoryCreateResult;