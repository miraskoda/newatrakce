<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 05.12.2017
 * Time: 9:07
 */

use backend\controllers\ReservationController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo ReservationController::deleteReservationFromAdmin();