<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 10.02.2018
 * Time: 17:42
 */

use backend\controllers\PageController;
use backend\models\Page;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$result = PageController::saveEditableContent();
if(is_a($result, Page::class))
	echo true;
else
	echo $result;