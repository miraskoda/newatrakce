<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 28.11.2017
 * Time: 12:12
 */

use backend\controllers\CustomerController;
use backend\models\Customer;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$customer = CustomerController::isLoggedCustomer();
if(is_a($customer, Customer::class))
	echo true;
else
	echo false;