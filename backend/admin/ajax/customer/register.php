<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 01.08.2017
 * Time: 10:22
 */

use backend\controllers\CustomerController;
use backend\models\Customer;
use backend\models\Url;

require_once __DIR__ . "/../../../modules/app/prepare.php";

require_once Url::getBackendPathTo('/plugins/recaptcha/reCaptchaHelper.php');

//echo var_dump($_POST);

$recaptcha = $_POST['g-recaptcha-response'];

$object = new reCaptchaHelper();
$response = $object->verifyResponse($recaptcha);


//isset($response['success']) and $response['success'] != true
//disabling recaptcha in localhost --- type code above into if clause to get work
if(false) {
    echo "Nepodařilo se ověřit totožnost uživatele. Prosím zkuste to znovu později. Chybový kód: " . $response['error-codes'][0];
}else {
    //echo "Correct Recaptcha";
    $customer = CustomerController::createCustomer();
    if(is_a($customer, Customer::class)){
        echo true;
    } else {
        echo $customer;
    }
}
