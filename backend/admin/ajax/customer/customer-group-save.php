<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 16.02.2018
 * Time: 10:07
 */

use backend\controllers\CustomerController;
use backend\models\Customer;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$result = CustomerController::setCustomerGroup();
if(is_a($result, Customer::class))
    echo true;
else
    echo $result;