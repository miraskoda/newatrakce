<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 10.08.2017
 * Time: 10:03
 */

header("Content-Type: application/json; charset=UTF-8");
define('ARES','http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_bas.cgi?ico=');
$ico = intval($_GET['ico']);
$file = @file_get_contents(ARES.$ico);

if ($file) {
    $xml = @simplexml_load_string($file);
}

$result = array();
if (isset($xml)) {
    $namespace = $xml->getDocNamespaces();
    $data = $xml->children($namespace['are']);
    $element = $data->children($namespace['D'])->VBAS;
    if (strval($element->ICO) == $ico) {
        $result['ico'] 	= strval($element->ICO);
        $result['dic'] 	= strval($element->DIC);
        $result['company'] = strval($element->OF);
        // load street (if street doesn't exist - Butoves) -> load part of city (if not exists) -> load city
        $result['street']	= ((isset($element->AA->NU) && $element->AA->NU != '') ? strval($element->AA->NU) : ((isset($element->AA->NCO) && $element->AA->NCO != '') ? strval($element->AA->NCO) : strval($element->AA->N)) )
            . ' '
            . strval($element->AA->CD)
            . ((isset($element->AA->CO) && $element->AA->CO != '') ? (' / ' . strval($element->AA->CO)) : '');
        $result['city']	= strval($element->AA->N);
        $result['zip']	= strval($element->AA->PSC);
        $result['status'] 	= 200;
    } else {
        $result['status'] = 404;
    }
} else {
    $result['status'] = 400;
}

echo json_encode($result);