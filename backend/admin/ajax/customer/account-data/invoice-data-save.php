<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 10.08.2017
 * Time: 23:14
 */

use backend\controllers\CustomerController;
use backend\models\Address;

require_once __DIR__ . "/../../../../modules/app/prepare.php";

$address = CustomerController::saveInvoiceData();
if(is_a($address, Address::class))
    echo true;
else
    echo $address;