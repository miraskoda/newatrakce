<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 13.10.2017
 * Time: 0:13
 */

use backend\controllers\OrderController;
use backend\models\Order;

require_once __DIR__ . "/../../../../modules/app/prepare.php";

$order = OrderController::saveOrderState();
if(is_a($order, Order::class))
    echo true;
else
    echo $order;