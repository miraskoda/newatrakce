<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 05.09.2017
 * Time: 10:25
 */

use backend\controllers\OrderController;
use backend\models\Order;

require_once __DIR__ . "/../../../../modules/app/prepare.php";

$order = OrderController::savePaymentData();
if(is_a($order, Order::class))
    echo true;
else
    echo $order;