<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 13.10.2017
 * Time: 0:48
 */

use backend\controllers\OrderController;

require_once __DIR__ . "/../../../../modules/app/prepare.php";

echo OrderController::sendOrderStatusUpdate();