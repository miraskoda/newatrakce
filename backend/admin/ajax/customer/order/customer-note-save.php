<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 16.04.2018
 * Time: 13:39
 */

use backend\controllers\OrderController;
use backend\models\Order;

require_once __DIR__ . "/../../../../modules/app/prepare.php";

$order = OrderController::saveCustomerNote();
if(is_a($order, Order::class))
	echo true;
else
	echo $order;