<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 09.08.2017
 * Time: 11:11
 */

use backend\controllers\CustomerController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo CustomerController::sendForgottenPassword();