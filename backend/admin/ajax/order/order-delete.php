<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 23.11.2018
 * Time: 9:07
 */

use backend\controllers\OrderController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo OrderController::cancelOrder();