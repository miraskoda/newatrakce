<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 17.03.2017
 * Time: 15:53
 */

use backend\controllers\UserController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo UserController::deleteUser();