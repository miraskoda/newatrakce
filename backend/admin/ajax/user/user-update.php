<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 20.03.2017
 * Time: 7:40
 */


use backend\controllers\UserController;
use backend\models\User;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$userUpdateResult = UserController::updateUser();
if(is_a($userUpdateResult, User::class))
    echo true;
else
    echo $userUpdateResult;