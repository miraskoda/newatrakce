<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 17.03.2017
 * Time: 0:25
 */
use backend\models\User;

/**
 * Returns formatted last created user
 */

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo User::getLastCreatedUser();