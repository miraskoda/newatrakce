<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 23.01.2018
 * Time: 13:06
 */

use backend\models\Calendar;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo Calendar::getGridData();