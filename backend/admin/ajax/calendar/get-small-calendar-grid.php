<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 04.04.2018
 * Time: 21:53
 */

use backend\models\Calendar;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo Calendar::getSmallGridData();