<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 08.12.2017
 * Time: 19:49
 */

use backend\view\CustomerGroupGrid;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo CustomerGroupGrid::getCustomerGroupGrid();