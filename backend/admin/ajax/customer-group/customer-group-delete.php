<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 08.12.2017
 * Time: 21:16
 */

use backend\controllers\CustomerGroupController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo CustomerGroupController::deleteCustomerGroup();
