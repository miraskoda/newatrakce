<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 29.07.2017
 * Time: 0:31
 */

use backend\controllers\ProductDataController;
use backend\models\ProductData;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$productData = ProductDataController::saveProductData();
if(is_a($productData, ProductData::class)) {
    echo json_encode(['result' => 1, 'data' => $productData->_toArray()]);
} else {
    echo json_encode(['result' => 0, 'data' => $productData->_toArray()]);
}