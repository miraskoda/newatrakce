<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 21.02.2018
 * Time: 15:36
 */

use backend\models\Product;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo json_encode(Product::getProductsAsArray());
