<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 20.06.2017
 * Time: 23:03
 */

use backend\controllers\ProductCategoryController;
use backend\models\ProductCategory;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$productCategoryResult = ProductCategoryController::createProductCategory();
if(is_a($productCategoryResult, ProductCategory::class))
    echo json_encode($productCategoryResult->_toArray());
else
    echo $productCategoryResult;