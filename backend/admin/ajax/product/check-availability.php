<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 28.01.2018
 * Time: 23:22
 */


use backend\controllers\ProductController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo ProductController::checkProductAvailability();