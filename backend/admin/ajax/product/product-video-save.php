<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 07.08.2017
 * Time: 21:54
 */

use backend\controllers\VideoController;
use backend\models\Video;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$videoResult = VideoController::saveVideo();
if(is_a($videoResult, Video::class)) {
    echo true;
} else {
    echo $videoResult;
}