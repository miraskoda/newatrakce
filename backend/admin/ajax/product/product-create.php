<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 26.03.2017
 * Time: 22:28
 */

use backend\controllers\ProductController;
use backend\models\Product;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$productCreateResult = ProductController::createProduct();
if(is_a($productCreateResult, Product::class))
    echo true;
else
    echo $productCreateResult;