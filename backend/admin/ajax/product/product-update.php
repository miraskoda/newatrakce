<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 27.03.2017
 * Time: 20:21
 */

use backend\controllers\ProductController;
use backend\models\Product;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$productUpdateResult = ProductController::updateProduct();
if(is_a($productUpdateResult, Product::class))
    echo true;
else
    echo $productUpdateResult;