<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 07.11.2017
 * Time: 19:49
 */

use backend\controllers\ProductController;
use backend\models\Product;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$result = ProductController::duplicateProduct();
if(is_a($result, Product::class))
    echo true;
else
    echo $result;