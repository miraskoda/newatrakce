<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 27.03.2017
 * Time: 20:55
 */

use backend\controllers\ProductController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo ProductController::getProduct();