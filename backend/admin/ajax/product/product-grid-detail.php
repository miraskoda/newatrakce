<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 28.03.2017
 * Time: 13:07
 */

use backend\controllers\ProductController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo ProductController::getProduct(ProductController::PRODUCT_DETAIL);