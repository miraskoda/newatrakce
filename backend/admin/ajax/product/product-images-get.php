<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 15.06.2017
 * Time: 1:14
 */

use backend\controllers\ProductImageController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$imageResult = ProductImageController::getImages();
echo $imageResult;