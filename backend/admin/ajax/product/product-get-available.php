<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 06.04.2018
 * Time: 19:19
 */

use backend\controllers\ProductController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo ProductController::getAvailableProducts();