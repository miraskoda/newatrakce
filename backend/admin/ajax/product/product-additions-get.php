<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 21.02.2018
 * Time: 15:15
 */

use backend\controllers\ProductAdditionController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo ProductAdditionController::getProductAdditionalProducts();