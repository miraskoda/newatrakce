<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 07.08.2017
 * Time: 21:42
 */

use backend\controllers\VideoController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo json_encode(VideoController::getVideo());