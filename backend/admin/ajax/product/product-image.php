<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 19.06.2017
 * Time: 13:12
 */

use backend\controllers\ProductImageController;
use backend\models\ProductImage;

require_once __DIR__ . "/../../../modules/app/prepare.php";

$imageResult = ProductImageController::createImage();
if(is_a($imageResult, ProductImage::class))
    echo true;
else
    echo $imageResult;