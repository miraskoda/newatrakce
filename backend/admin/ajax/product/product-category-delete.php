<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 20.06.2017
 * Time: 23:49
 */

use backend\controllers\ProductCategoryController;

require_once __DIR__ . "/../../../modules/app/prepare.php";

echo ProductCategoryController::deleteProductCategory();