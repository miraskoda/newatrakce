<?php
/**
 * Login page of administration
 * pageId = 3
 */

use backend\controllers\UserController;
use backend\models\Url;

$pageId = 3;

require_once __DIR__ . "/../../modules/app/prepare.php";

//before any action check user and redirect to administration if needed
$user = UserController::isLoggedUser();
if($user) {
    header('Location: ../', true);
    die();
}

//head
require_once $root . "/modules/page-parts/head.php";

?>
<body id="admin">
<?php

//header and footer included
require_once $root . "/admin/modules/login.php";

//and js files
echo '<script>';
    require_once $root . "/../js/admin/login.js";
    require_once Url::getPathTo("/js/admin/user/forgotten-password.min.js");
echo '</script>';

?>
</body>
</html>