<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 22.05.2017
 * Time: 13:59
 */

/**
 * Page for managing categories
 * pageId = 8
 * login required
 */

use backend\controllers\UserController;
use backend\models\Password;
use backend\models\Url;

$pageId = 8;

require_once __DIR__ . "/../../modules/app/prepare.php";

//before any action check user and redirect to login if needed
$user = UserController::isLoggedUser();
if(!$user){
    header('Location: /admin/prihlaseni/', true);
    die();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="admin" class="category-management">
<?php
//admin header
require_once Url::getBackendPathTo("/admin/modules/header.php");

require_once Url::getBackendPathTo("/admin/modules/category-management.php");

//this page only js
echo '<script>';
require_once Url::getBackendPathTo("/../js/admin/category/category-show-add.js");
require_once Url::getBackendPathTo("/../js/admin/category/category-create.js");
require_once Url::getBackendPathTo("/../js/admin/category/category-reload.js");
require_once Url::getBackendPathTo("/../js/admin/category/category-delete.js");
require_once Url::getBackendPathTo("/../js/admin/category/category-update.js");
require_once Url::getBackendPathTo("/../js/admin/category/category-add-subcategory.js");
require_once Url::getBackendPathTo("/../js/admin/category/category-sort.js");
require_once Url::getBackendPathTo("/../js/admin/category/dropzone-init.min.js");
require_once Url::getBackendPathTo("/../js/admin/category/category-product-position-sort.min.js");
echo '</script>';
require_once Url::getBackendPathTo("/admin/modules/page-parts/js-footer.php");

?>
</body>
<?php

//modals
require_once Url::getBackendPathTo("/admin/modules/modals/delete-confirm.php");
require_once Url::getBackendPathTo("/admin/modules/modals/category-edit.php");
require_once Url::getBackendPathTo("/admin/modules/modals/category-product-position.php");

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);

?>
</html>