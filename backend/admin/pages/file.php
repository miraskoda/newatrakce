<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 19.05.2017
 * Time: 17:15
 */

/**
 * Page for file management
 * pageId = 7
 * login required
 */

use backend\controllers\UserController;
use backend\models\Url;

$pageId = 46;

require_once __DIR__ . "/../../modules/app/prepare.php";

//before any action check user and redirect to login if needed
$user = UserController::isLoggedUser();
if(!$user){
    header('Location: /admin/prihlaseni/', true);
    die();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="admin" class="product-management">
<?php
//admin header
require_once Url::getBackendPathTo("/admin/modules/header.php");

require_once Url::getBackendPathTo("/admin/modules/files.php");

//this page only js
/*echo '<script>';
echo '</script>';*/
require_once Url::getBackendPathTo("/admin/modules/page-parts/js-footer.php");

?>
</body>
<?php

//modals
require_once Url::getBackendPathTo("/admin/modules/modals/delete-confirm.php");

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);

?>
</html>