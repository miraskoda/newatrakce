<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 16.10.2017
 * Time: 11:02
 */
use backend\controllers\UserController;
use backend\models\Url;
use backend\models\OrderState;


$pageId = 26;

require_once __DIR__ . "/../../modules/app/prepare.php";

//before any action check user and redirect to login if needed
$user = UserController::isLoggedUser();
if(!$user){
    header('Location: /admin/prihlaseni/', true);
    die();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="admin" class="order-detail">
<?php
//admin header
require_once Url::getBackendPathTo("/admin/modules/header.php");

require_once Url::getBackendPathTo("/modules/page-parts/customer/order/detail/detail-loader.php");
require_once Url::getBackendPathTo("/admin/modules/order-detail.php");

require_once Url::getBackendPathTo("/admin/modules/page-parts/js-footer.php");

//this page only js
echo '<script>';
    require_once Url::getPathTo("/js/admin/order/order-state.js");
    require_once Url::getPathTo("/js/admin/order/admin-note.js");
    require_once Url::getPathTo("/js/admin/order/edit-order.js");
require_once Url::getPathTo("/js/admin/order/InvoiceCredit.js");
require_once Url::getPathTo("/js/datetimepicker.js");
    require_once Url::getPathTo("/js/customer/account-data/company-switch-edit.js");
echo '</script>';

?>
</body>
<?php

//modals
require_once Url::getBackendPathTo("/admin/modules/modals/order-state-email.php");

require_once Url::getBackendPathTo("/admin/modules/modals/order/order-edit.php");
require_once Url::getBackendPathTo("/admin/modules/modals/order/invoice-edit.php");
require_once Url::getBackendPathTo("/admin/modules/modals/order/credit-edit.php");

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);

?>
</html>