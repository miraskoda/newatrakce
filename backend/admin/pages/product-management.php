<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 21.03.2017
 * Time: 0:42
 */


/**
 * Page for managing user accounts
 * pageId = 5
 * login required
 */

use backend\controllers\UserController;
use backend\models\Url;

$pageId = 5;

require_once __DIR__ . "/../../modules/app/prepare.php";

//before any action check user and redirect to login if needed
$user = UserController::isLoggedUser();
if(!$user){
    header('Location: /admin/prihlaseni/', true);
    die();
}

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="admin" class="product-management">
<?php
//admin header
require_once Url::getBackendPathTo("/admin/modules/header.php");

require_once Url::getBackendPathTo("/admin/modules/product-management.php");

//this page only js
echo '<script>';
require_once Url::getBackendPathTo("/../js/admin/product/product-form-control.js");
require_once Url::getBackendPathTo("/../js/admin/product/product-grid-create.js");
require_once Url::getBackendPathTo("/../js/admin/product/product-delete.js");
require_once Url::getBackendPathTo("/../js/admin/product/product-grid-delete.js");
require_once Url::getBackendPathTo("/../js/admin/product/product-duplicate.js");
require_once Url::getBackendPathTo("/../js/admin/product/product-grid-update.js");
require_once Url::getBackendPathTo("/../js/admin/product/product-grid-detail.js");
require_once Url::getBackendPathTo("/../js/admin/product/product-search.js");
require_once Url::getBackendPathTo("/../js/admin/product/product-images-dropzone-init.js");
require_once Url::getBackendPathTo("/../js/admin/product/product-video.min.js");
require_once Url::getBackendPathTo("/../js/admin/product/product-categories-init.js");
require_once Url::getBackendPathTo("/../js/admin/product/product-price-included.js");
require_once Url::getBackendPathTo("/../js/admin/product/product-technical.js");
require_once Url::getBackendPathTo("/../js/admin/product/product-additions-init.js");
echo '</script>';
require_once Url::getBackendPathTo("/admin/modules/page-parts/js-footer.php");

?>
</body>
<?php

//modals
require_once Url::getBackendPathTo("/admin/modules/modals/delete-confirm.php");
require_once Url::getBackendPathTo("/admin/modules/modals/product/product-images.php");
require_once Url::getBackendPathTo("/admin/modules/modals/product/product-video.php");
require_once Url::getBackendPathTo("/admin/modules/modals/product/product-categories.php");
require_once Url::getBackendPathTo("/admin/modules/modals/product/product-price-included.php");
require_once Url::getBackendPathTo("/admin/modules/modals/product/product-technical.php");
require_once Url::getBackendPathTo("/admin/modules/modals/product/product-additions.php");

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);

?>
</html>