<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 10.10.2017
 * Time: 1:00
 */

use backend\models\Url;

$pageId = 25;

require_once __DIR__ . "/../../../backend/modules/app/prepare.php";

//head
require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>
<body id="public">
<?php

//public header - categories, pages, cart (search)
//require_once Url::getBackendPathTo("admin/modules/page-parts/header.php");

echo '<div class="container-fluid">
            <div class="row customer panel">';
require_once Url::getBackendPathTo("/admin/modules/forgotten-password.php");
echo '</div>';

//require_once Url::getBackendPathTo("/modules/page-parts/footer.php");
echo '</div>';
echo '<div id="overlay"></div>';

echo '<script>';
require_once Url::getPathTo("/js/admin/user/forgotten-password-change.min.js");
echo '</script>';
require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

//var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
?>
</body>
</html>