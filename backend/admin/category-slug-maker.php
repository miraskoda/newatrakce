<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 26.01.2018
 * Time: 0:00
 */

use backend\models\Category;

require_once __DIR__ . "/../modules/app/prepare.php";

$categories = Category::getCategories(0, true);

foreach ($categories as $category) {
    $tempCategory = new Category($category['categoryId']);
    // disable empty update
    $tempCategory->setCategoryNameSlug('ahoj');
    $tempCategory = $tempCategory->load();
    echo "Kategorie " . $tempCategory->getCategoryId() . '<br>';
    if(is_a($tempCategory->update(), Category::class))
        echo 'Success<br>';
    else
        echo 'Failure<br>';
}