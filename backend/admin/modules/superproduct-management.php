<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 08.04.2018
 * Time: 17:46
 */
?>
<div class="container-fluid">
    <div class="row admin-grid-header">
        <div class="col-md-12 first">
            <div class="box text-center animated zoomIn">
                <h1>Správa superproduktů</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box text-center">
                <div class="row">
                    <div class="col-md-2">
                        <p>ID</p>
                    </div>
                    <div class="col-md-4">
                        <p>Název</p>
                    </div>
                    <div class="col-md-2">
                        <p>Množství</p>
                    </div>
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>
            </div>
        </div>
        <div class="superproduct-container">
        </div>
    </div>
</div>
