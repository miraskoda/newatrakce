<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 05.12.2017
 * Time: 1:03
 */

use backend\models\Reservation;

$reservations = Reservation::getReservations(true);

?>

<div class="container-fluid">
    <div class="row admin-grid-header">
        <div class="col-md-12 first">
            <div class="box text-center animated zoomIn">
                <h1>Správa rezervací</h1>
                <div>
                    <p>Přehled rezervací produktů.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <?php
        if(isset($reservations) && is_array($reservations) && count($reservations) > 0) :
            ?>
            <div class="col-md-12">
                <div class="box text-center animated zoomInDown">
                    <div class="row">
                        <div class="col-md-1">
                            <p>ID</p>
                        </div>
                        <div class="col-md-2">
                            <p>Zákazník</p>
                        </div>
                        <div class="col-md-2">
                            <p>Produkt</p>
                        </div>
                        <div class="col-md-1">
                            <p>Množství</p>
                        </div>
                        <div class="col-md-2">
                            <p>Termín</p>
                        </div>
                        <div class="col-md-1">
                            <p>Počet hodin</p>
                        </div>
                        <div class="col-md-2">
                            <p>Rezervováno do</p>
                        </div>
                        <div class="col-md-1">
                        </div>
                    </div>
                </div>
            </div>
            <?php
            foreach ($reservations as $reservation):
                ?>
                <div class="col-md-12 reservation-row" reservation-id="<?= $reservation->getReservationId() ?>">
                    <div class="box text-center animated zoomInDown">
                        <div class="row">
                            <div class="col-md-1">
                                <p><?= $reservation->getReservationId() ?></p>
                            </div>
                            <div class="col-md-2">
                                <p><?= $reservation->getCustomerId() ?></p>
                            </div>
                            <div class="col-md-2">
                                <p><?= $reservation->getProductId()  ?></p>
                            </div>
                            <div class="col-md-1">
                                <p><?= $reservation->getQuantity() ?></p>
                            </div>
                            <div class="col-md-2">
                                <p><?= $reservation->getFormatedTermStart() ?></p>
                            </div>
                            <div class="col-md-1">
                                <p><?= $reservation->getTermHours() ?></p>
                            </div>
                            <div class="col-md-2">
                                <p><?= $reservation->getReservationEnd() ?></p>
                            </div>
                            <div class="col-md-1">
                                <a class="btn btn-danger delete-reservation"><span class="ti-trash"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            endforeach;
            ?>
        <?php
        else :
            ?>
            <div class="col-md-12 first">
                <div class="box">
                    <p>Žádné rezervace v databázi. <?= ((isset($reservations) && is_string($reservations)) ? $reservations : '') ?></p>
                </div>
            </div>
        <?php
        endif;
        ?>
    </div>
</div>