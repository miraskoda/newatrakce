<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 16.10.2017
 * Time: 11:11
 */

use backend\controllers\OrderController;
use backend\models\Address;
use backend\models\CustomCurrency;
use backend\models\OrderProduct;
use backend\models\OrderState;
use backend\models\OrderTerm;
use backend\models\Payment;
use backend\models\PaymentType;
use backend\models\Validate;
use backend\view\CartGrid;
use backend\controllers\InvoiceCreditController;

?>

<script>

    function Confirm()
    {
        var x = confirm('Nyní bude vygenerován požadovaný doklad');

        if (x)
            return true;
        else
            return false;
    }

    </script>


<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <a href="order-management.php#order-id=<?= $order->getOrderId() ?>">Správa objednávek</a> <span
                        class="ti-arrow-right"></span> Objednávka č. <?= $orderNo ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="box light-blue overflow-auto animated zoomInDown">
                <div class="row" order-id="<?= $order->getOrderId() ?>">
                    <div class="col-md-12 text-center">
                        <label for="order-state">Stav objednávky</label>
                    </div>
                    <div class="col-md-10 col-md-offset-1">
                        <?php
                        $orderStates = OrderState::getOrderStates();
                        ?>
                        <div class="form-group">
                            <select id="order-state" class="form-control order-state">
                                <?php
                                $customerGroupId = $order->getOrderStateId();
                                foreach ($orderStates as $orderState) {
                                    echo '<option value="' . $orderState->getOrderStateId() . '" ' . (($customerGroupId > 0 && $customerGroupId == $orderState->getOrderStateId()) ? 'selected' : '') . '>' . $orderState->getName() . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="box overflow-auto animated zoomInDown" style="background: yellow">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <label for="order-state">CEB status</label>
                    </div>
                    <div class="col-md-10 col-md-offset-1">

                        <div class="form-group">
                                <div class="box" style="display: flex; justify-content: center"> <p style="color:grey;">Platba neevidována</p> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-md-2 animated zoomInDown"><a class="btn btn-success" target="_blank" onclick="return Confirm()"
                                                     href="/prehled-objednavek/detail/<?= $order->getOrderId() ?>/propozice/"><span
                        class="ti-car"></span> Vygenerovat propozice</a></div>
        <?php
        $payment = new Payment($order->getPaymentId());
        $payment = $payment->load();
        $paymentType = new PaymentType($order->getPaymentTypeId());
        $paymentType = $paymentType->load();

        if ($paymentType->getDeposit() > 0) {
            if (InvoiceCreditController::getInvoiceBooleanFromOrderIdDeposit($order->getOrderId())) {
                echo ' <div class="col-md-2 animated zoomInDown"><a class="btn btn-danger" target="_blank" href="/prehled-objednavek/detail/' . $order->getOrderId() . '/faktura/deposit/"><span class="ti-printer"></span> Zálohová číslo ' . InvoiceCreditController::getInvoiceFromOrderIdDeposit($order->getOrderId()) . '</a></div> ';
            } else {
                echo ' <div class="col-md-2 animated zoomInDown"><a class="btn btn-primary" target="_blank" onClick="return Confirm()" href="/prehled-objednavek/detail/' . $order->getOrderId() . '/faktura/deposit/"><span class="ti-printer"></span> Generovat zálohovou</a></div> ';
            }
        }
        ?>


        <?php

        if (InvoiceCreditController::getInvoiceBooleanFromOrderId($order->getOrderId())) {
            echo ' <div class="col-md-2 animated zoomInDown"><a class="btn btn-danger" target="_blank" href="/prehled-objednavek/detail/' . $order->getOrderId() . '/faktura/final/"><span class="ti-printer"></span> Daňový číslo ' . InvoiceCreditController::getInvoiceFromOrderId($order->getOrderId()) . '</a></div> ';




            //dobropis
            if (InvoiceCreditController::getInvoiceBooleanFromOrderIdDobropis($order->getOrderId())) {
                echo ' <div class="col-md-2 animated zoomInDown"><a class="btn btn-danger" target="_blank" href="/prehled-objednavek/detail/' . $order->getOrderId() . '/faktura/dobropis/"><span class="ti-printer"></span> Dobropis číslo ' . InvoiceCreditController::getInvoiceFromOrderIdDobropis($order->getOrderId()) . '</a></div> ';
            } else {
                echo ' <div class="col-md-2 animated zoomInDown"><a class="btn btn-primary" target="_blank" href="/prehled-objednavek/detail/' . $order->getOrderId() . '/faktura/dobropis/" onClick="return Confirm()"><span class="ti-printer"></span> Generovat dobropis</a></div> ';
            }
            //dobropis




        } else {
            echo ' <div class="col-md-2 animated zoomInDown"><a class="btn btn-primary" target="_blank" href="/prehled-objednavek/detail/' . $order->getOrderId() . '/faktura/final/" onClick="return Confirm()"><span class="ti-printer"></span> Generovat daňový</a></div>';

        }


        ?>

        <div class="col-md-2 animated zoomInDown"><button id="credit-btn" class="btn btn-info"><span class="ti-printer"></span> Dodatečný dobropis</button></div>
        <div class="col-md-2 animated zoomInDown"><button id="invoice-btn" class="btn btn-info"><span class="ti-printer"></span> Dodatečný daňový doklad</button></div>

        <?php
        if ($order->getIsCanceled()) {
            echo '<div class="col-md-12">
                    <div class="box bright-red animated zoomInDown">
                        Objednávka byla zrušena!
                    </div>
                </div>';
        } else {
            if (!$order->getIsPaid() && $order->getOrderStateId() != 7) :
                ?>
                <div class="col-md-12">
                    <div class="box bright-red animated zoomInDown">
                        Objednávka není zaplacena!
                    </div>
                </div>
            <?php
            else :
                ?>
                <div class="col-md-12">
                    <div class="box green animated zoomInDown">
                        Objednávka je uhrazena.
                    </div>
                </div>
            <?php
            endif;
        }
        ?>
    </div>

    <div class="row box">
        <div class="title col-md-12">
            <div class="text-center">
                <h3>
                    Vygenerované doklady
                </h3>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-1">
        <div class="panel col-md-12">
            <div class="box text-center">

                <div class="row" style="    background: lightcyan;">
                    <div class="col-md-1 col-md-offset-1">
                        Pořadí
                    </div>
                    <div class="col-md-2">
                        Datum vytvoření
                    </div>
                    <div class="col-md-2">
                        Typ dokumentu
                    </div>
                    <div class="col-md-2">
                        Číslo dokumentu
                    </div>
                    <div class="col-md-2">
                        Typ určení
                    </div>
                    <div class="col-md-2">
                        Celková uvedená cena
                    </div>
                </div>

                <?php
                $countVar = 1;
                echo '<hr>';
//faktury
                foreach(InvoiceCreditController::getInvoiceFromIdVenue($_GET["orderId"]) as $result => $value){

                    if($value["isAdd"]==1){
                        echo '<a target="_blank" href="/prehled-objednavek/detail/' . $value["id"] . '/doplnkova/faktura/faktura/">';
                    }else{
                        echo '<a target="_blank" href="/prehled-objednavek/detail/' . $value["order_id"] . '/faktura/final/">';
                    }

                    echo '<div class="row" id="invoicea">
                   <div class="col-md-1 col-md-offset-1">
                        '.$countVar.'.
                        </div>
                        <div class="col-md-2">
                        '.date('d.m.Y H:i:s',strtotime($value["date"])).'                    
                        </div>
                    <div class="col-md-2">
                        '."Faktura".'                   
                         </div>
                    <div class="col-md-2">
                        '.$value["id"].'                    
                        </div>
                    <div class="col-md-2">
                        ';
                    if($value["isAdd"]==1){
                        echo "Doplňková";
                    }else{
                        echo "Ostrá";
                    }
                    echo '                    
                        </div>
                     <div class="col-md-2">
                        '.( $value["isAdd"]==0 ? CustomCurrency::setCustomCurrencyWithoutDecimals($order->getSumPrice()) : $value["wVAT"]." Kč" ).'                   
                        </div>
                </div></a><hr>';
                    $countVar = $countVar+1;

                }


//dobropisy
                foreach(InvoiceCreditController::getInvoiceFromIdVenueCredit($_GET["orderId"]) as $result => $value){

                    if($value["isAdd"]==1){
                        echo '<a target="_blank" href="/prehled-objednavek/detail/' . $value["id"] . '/doplnkova/faktura/dobropis/">';
                    }else{
                        echo '<a target="_blank" href="/prehled-objednavek/detail/' . $value["order_id"] . '/faktura/final/">';
                    }

                    echo '<div class="row" id="invoicea">
                    <div class="col-md-1 col-md-offset-1">
                        '.$countVar.'.
                        </div>
                        <div class="col-md-2">
                        '.date('d.m.Y H:i:s',strtotime($value["date"])).'                    
                        </div>
                    <div class="col-md-2">
                        '."Dobropis".'                   
                         </div>
                    <div class="col-md-2">
                        '.$value["id"].'                    
                        </div>
                    <div class="col-md-2">
                        ';
                    if($value["isAdd"]==1){
                        echo "Doplňková";
                    }else{
                        echo "Ostrá";
                    }
                    echo '                    
                        </div>
                    <div class="col-md-2">
                        -'.( $value["isAdd"]==0 ? CustomCurrency::setCustomCurrencyWithoutDecimals($order->getSumPrice()) : $value["wVAT"]." Kč" ).'                   
                        </div>
                </div></a><hr>';
                    $countVar = $countVar+1;

                }

//ostatni

                foreach(InvoiceCreditController::getInvoiceFromIdVenueDocument($_GET["orderId"]) as $result => $value){
                    echo '<a target="_blank" href="/prehled-objednavek/detail/' . $order->getOrderId() . '/faktura/deposit/">';
                    echo '<div class="row" id="invoicea">                    
                    <div class="col-md-1 col-md-offset-1">
                        '.$countVar.'.
                        </div>
                        <div class="col-md-2">
                        '.date('d.m.Y H:i:s',strtotime($value["date"])).'                    
                        </div>
                    <div class="col-md-2">
                        '."Zálohová".'                   
                         </div>
                    <div class="col-md-2">
                        '.$value["id"].'                    
                        </div>
                    <div class="col-md-2">
                        Ostrá                   
                        </div>
                    <div class="col-md-2">
                        '.CustomCurrency::setCustomCurrencyWithoutDecimals(round(($order->getSumPrice() / 100) * $paymentType->getDeposit())).'                   
                        </div>
                </div></a><hr>';
                    $countVar = $countVar+1;

                }


                ?>

                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="title col-md-12">
            <div class="box text-center">
                <h3>
                    Objednávka č. <?= $orderNo ?>
                </h3>
                <p>Vytvořena <?= $order->getChanged() ?></p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="title col-md-12">
            <div class="box text-center">
                <div class="page-status"></div>
                <button id="edit-order-btn" class="btn btn-primary" data-target="<?= $order->getOrderId() ?>">Editovat objednávku</button>
            </div>
        </div>
    </div>

    <?php
    $isRequestedChange = $order->getIsRequestedChange();
    if ($isRequestedChange) {
        echo '<div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="row title">
                    <div class="col-md-10 col-md-offset-1">
                        <h4><span class="ti-alert"></span> Požadavek na změnu</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <h4>Požadavek:</h4>
                                <textarea name="request-change" order-id="' . $order->getOrderId() . '" class="form-control">' . $order->getRequestChange() . '</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>';
    } else {
        echo '';
    }
    ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="row title">
                    <div class="col-md-10 col-md-offset-1">
                        <h4>Výpis produktů</h4>
                    </div>
                </div>
                <div class="row admin-cart-list">
                    <?php
                    $stringifiedRentHours = OrderTerm::getRentHoursAsStrings($order);
                    $products = OrderProduct::getOrderProducts($order, false, false);
                    $additionalProducts = OrderProduct::getAdditionalOrderProducts($order);

                    echo CartGrid::generateProductsList($stringifiedRentHours, true, $products, 10, 1, true, $order);
                    ?>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row price-container">
                            <div class="col-md-12 price-block">
                                <?php
                                echo CartGrid::generatePrice($stringifiedRentHours, true, $products, $additionalProducts, true, $order->getDiscount());
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="row title">
                    <div class="col-md-10 col-md-offset-1">
                        <h4>Termíny pronájmu</h4>
                    </div>
                </div>
                <div class="row admin-cart-list">
                    <?php
                    echo CartGrid::generatePublicDates($order, 10, 1);
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box" id="public">
                <div class="row title">
                    <div class="col-md-10 col-md-offset-1">
                        <h4>Místo konání a fakturační údaje</h4>
                    </div>
                </div>
                <div class="row panel">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="col-md-4">
                            <div class="animated zoomIn box with-top">
                                <div class="top top-light-blue">
                                    <h4>Kontaktní osoba v den akce</h4>
                                </div>
                                <div class="content">
                                    <form id="contact-data">
                                        <div class="form-group">
                                            <label class="control-label">Jméno a příjmení</label>
                                            <p><?= $order->getContactName() ?></p>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Telefon (např.: 789456123)</label>
                                            <p><?= $order->getContactPhone() ?></p>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="animated zoomIn box with-top">
                                <div class="top top-blue">
                                    <h4>Adresa místa konání</h4>
                                </div>
                                <div class="content">
                                    <div class="form-group">
                                        <label class="control-label">Ulice a číslo popisné</label>
                                        <p><?= $order->getVenueStreet() ?></p>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Město</label>
                                        <p><?= $order->getVenueCity() ?></p>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">PSČ (např. 45612)</label>
                                        <p><?= $order->getVenueZip() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="animated zoomIn box with-top">
                                <div class="top top-kaky">
                                    <h4>Fakturační údaje</h4>
                                </div>
                                <div class="content">
                                    <?php
                                    $invoiceAddress = new Address();
                                    $invoiceAddress->setOrderId($order->getOrderId());
                                    $invoiceAddress->setAddressType(Address::ADDRESS_INVOICE);
                                    $invoiceAddress = $invoiceAddress->load();

                                    $isAddressLoaded = false;
                                    if (is_a($invoiceAddress, Address::class))
                                        $isAddressLoaded = true;
                                    ?>
                                    <div class="form-group">
                                        <label class="control-label">Jméno / Název firmy</label>
                                        <p><?= ($isAddressLoaded) ? $invoiceAddress->getName() : '' ?></p>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Ulice a číslo popisné</label>
                                        <p><?= ($isAddressLoaded) ? $invoiceAddress->getStreet() : '' ?></p>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Město</label>
                                        <p><?= ($isAddressLoaded) ? $invoiceAddress->getCity() : '' ?></p>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">PSČ (např. 45612)</label>
                                        <p><?= ($isAddressLoaded) ? $invoiceAddress->getZip() : '' ?></p>
                                    </div>
                                    <?php
                                    if ($order->getInvoiceIco() !== null && is_numeric($order->getInvoiceIco()) && $order->getInvoiceIco() > 0):
                                        ?>
                                        <div class="form-group">
                                            <label class="control-label">IČO</label>
                                            <p><?= $order->getInvoiceIco() ?></p>
                                        </div>
                                        <?php
                                        if ((new Validate())->validateNotNull([$order->getInvoiceDic()])):
                                            ?>
                                            <div class="form-group">
                                                <label class="control-label">DIČ</label>
                                                <p><?= $order->getInvoiceDic() ?></p>
                                            </div>
                                        <?php
                                        endif;
                                        ?>
                                    <?php
                                    endif;
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="row title">
                    <div class="col-md-10 col-md-offset-1">
                        <h4>Poznámky</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row">
                            <?php
                            if (strlen($order->getCustomerNote()) > 0):
                                ?>
                                <div class="col-md-6">
                                    <h4>Od zákazníka:</h4>
                                    <p><?= $order->getCustomerNote() ?></p>
                                </div>
                            <?php
                            endif;
                            ?>
                            <div class="col-md-6 form-group">
                                <h4>Od správce:</h4>
                                <textarea name="admin-note" order-id="<?= $order->getOrderId() ?>"
                                          class="form-control"><?= $order->getAdminNote() ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="row title">
                    <div class="col-md-10 col-md-offset-1">
                        <h4>Doprava</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row venue-result">
                            <?php
                            $venueData = OrderController::getVenueCountedDataByOrder($order);
                            if (!is_array($venueData))
                                echo '<div class="col-md-12"><p>Chyba. Nepodařilo se načíst data dopravy. Prosím kotaktujte zákaznickou podporu. Chyba: ' . $venueData . '</p></div>';
                            ?>
                            <div class="col-md-12"><p>Délka cesty z našeho skladu: <span
                                            class="travel-length"><?= (is_array($venueData)) ? $venueData['travel-length'] : '' ?></span>
                                </p></div>
                            <div class="col-md-12"><p>Doba trvání cesty: <span
                                            class="travel-time"><?= (is_array($venueData)) ? $venueData['duration-text'] : '' ?></span>
                                </p></div>
                            <div class="col-md-12">
                                <p class="venue-price">Cena dopravy bez DPH:
                                    <?php
                                    $priceWithoutVat = $order->getProductPrice();//Order::getPriceWithoutVat($stringifiedRentHours, $products);
                                    if ($priceWithoutVat > 7000) {
                                        echo '<span class="green free">ZDARMA (objednávka nad 7 000 Kč bez DPH)</span>';
                                    } else {
                                        echo '<span class="travel-price">' . CustomCurrency::setCustomCurrencyWithoutDecimals((is_array($venueData)) ? $venueData['travel-price'] : '') . '</span>';
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="row title" id="payment">
                    <div class="col-md-10 col-md-offset-1">
                        <h4>Platba</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row venue-result">
                            <div class="col-md-12"><p>Způsob platby: <strong><span
                                                class="<?= $payment->getIcon() ?>"></span> <?= $payment->getName() ?>
                                    </strong></p></div>
                            <div class="col-md-12"><p>Typ platby: <strong><?= $paymentType->getName() ?></strong></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="row title">
                    <div class="col-md-10 col-md-offset-1">
                        <h4>Součet</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="row price-container">
                            <div class="col-md-12 price-block">
                                <?php
                                $discount = $order->getDiscount();
                                $discountAmount = round((floatval($priceWithoutVat) / floatval(100 - $discount)) * floatval($discount));
                                if ($discount > 0)
                                    echo '<p>Sleva ' . $discount . '%: -' . CustomCurrency::setCustomCurrencyWithoutDecimals($discountAmount) . '</p>';
                                ?>
                                <p>Produkty bez DPH:
                                    <strong><?= CustomCurrency::setCustomCurrencyWithoutDecimals($order->getProductPrice()) ?></strong>
                                </p>
                                <p>Doprava a platba bez DPH:
                                    <strong><?= ($priceWithoutVat > 7000) ? 'ZDARMA' : CustomCurrency::setCustomCurrencyWithoutDecimals($order->getDeliveryPaymentPrice()) ?></strong>
                                </p>
                                <p>DPH:
                                    <strong><?= CustomCurrency::setCustomCurrencyWithoutDecimals($order->getVat()) ?></strong>
                                </p>
                                <p class="final-price">Celkem k úhradě:
                                    <strong><?= CustomCurrency::setCustomCurrencyWithoutDecimals($order->getSumPrice()) ?></strong>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>