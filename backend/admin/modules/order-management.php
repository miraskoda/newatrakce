<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 09.10.2017
 * Time: 18:29
 */

use backend\models\Address;
use backend\models\CustomCurrency;
use backend\models\Order;
use backend\models\OrderProduct;
use backend\models\OrderState;


if(!empty($_GET["search"])){
    $orders = Order::getOrdersBySearch($_GET["search"]);

}else{
    $orders = Order::getOrders(true);

}



?>
<div class="container-fluid">
    <div class="row admin-grid-header">
        <div class="col-md-12 first">
            <div class="box text-center animated zoomIn">
                <h1>Správa objednávek</h1>
                <div>
                    <p>Prohlížejte a spravujte objednávky zákazníků.</p>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <?php
        if(isset($orders) && is_array($orders) && count($orders) > 0) :
            ?>
            <div class="col-md-12">
                <div class="box overflow-auto light-blue text-center animated zoomInDown">
                    <div class="row">
                        <div class="col-md-1">
                            <p>Číslo objednávky</p>
                        </div>
                        <div class="col-md-2">
                            <p>Datum vytvoření</p>
                        </div>
                        <div class="col-md-2">
                            <p>Objednavatel</p>
                        </div>
                        <div class="col-md-1">
                            <p>Cena s DPH</p>
                        </div>
                        <div class="col-md-1">
                            <p>Počet produktů v objednávce</p>
                        </div>
                        <div class="col-md-2">
                            <p>Stav platby</p>
                        </div>
                        <div class="col-md-1">
                            <p>Aktuální stav</p>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            foreach ($orders as $order):
                ?>
                <div class="col-md-12">
                    <div class="box text-center animated zoomInDown">
                        <div class="row" order-id="<?= $order->getOrderId() ?>" <?= ($order->getIsCanceled() ? "style='background-color: #A3000055; '" : "" ) ?>   >
                            <div class="col-md-1">
                                <p>
                                    <?php
                                    echo $order->getFormattedOrderId();
                                    ?>
                                </p>
                            </div>
                            <div class="col-md-2">
                                <p><?= $order->getChanged() ?></p>
                            </div>
                            <div class="col-md-2">
                                <p>
                                    <?php
                                    $invoiceAddress = new Address();
                                    $invoiceAddress->setOrderId($order->getOrderId());
                                    $invoiceAddress->setAddressType(Address::ADDRESS_INVOICE);
                                    $invoiceAddress = $invoiceAddress->load();

                                    echo $invoiceAddress->getName();
                                    ?>
                                </p>
                            </div>
                            <div class="col-md-1">
                                <p><?= CustomCurrency::setCustomCurrencyWithoutDecimals($order->getSumPrice()) ?></p>
                            </div>
                            <div class="col-md-1">
                                <p><?= OrderProduct::getSumOfProductsInOrder($order->getOrderId()) ?></p>
                            </div>
                            <div class="col-md-2">
                                <p><?= (($order->getIsPaid() || $order->getOrderStateId() == 7) ? '<span class="green">Zaplaceno</span>' : '<span class="red">Nezaplaceno</span>') ?></p>
                            </div>
                            <div class="col-md-2">
                                <?php
                                /*$orderStateId = $order->getOrderStateId();
                                if(is_numeric($orderStateId) && $orderStateId > 0) {
                                    $orderState = new OrderState($orderStateId);
                                    $orderState = $orderState->load();
                                    if(is_a($orderState, OrderState::class))
                                        echo $orderState->getName();
                                    else
                                        echo 'Neznámý';
                                } else {
                                    echo 'Neznámý';
                                }*/

                                $orderStates = OrderState::getOrderStates();
                                ?>
                                <div class="form-group">
                                    <select class="form-control order-state">
                                        <?php
                                        $orderStateId = $order->getOrderStateId();
                                        foreach ($orderStates as $orderState) {
                                            echo '<option value="' . $orderState->getOrderStateId() . '" ' . (($orderStateId > 0 && $orderStateId == $orderState->getOrderStateId()) ? 'selected' : '') . '>' . $orderState->getName() . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <?php
                                    if ($order->getIsCanceled()) {
                                        echo '<a href="order-detail.php?orderId=' . $order->getOrderId() . '" class="btn btn-danger"><span class="ti-trash"></span> Detail</a>';
                                    } else {
                                        if ($order->getIsRequestedChange()) {
                                            echo '<a href="order-detail.php?orderId=' . $order->getOrderId() . '" class="btn btn-warning"><span class="ti-alert"></span> Detail</a>';
                                        } else {
                                            echo '<a href="order-detail.php?orderId=' . $order->getOrderId() . '" class="btn btn-success"><span class="ti-search"></span> Detail</a>';
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            endforeach;
            ?>
            <?php
        else :
            ?>
            <div class="col-md-12 box">
                <p>Nemáte žádné objednávky. <?= ((isset($orders) && is_string($orders)) ? $orders : '') ?></p>
            </div>
            <?php
        endif;
        ?>
    </div>
</div>