<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 14.03.2017
 * Time: 8:42
 */
?>

<form method="post" id="create">
    <p>Heslo musí mít minimálně 8 znaků a obsahovat číslice, velká a malá písmena.</p>
    <div class="action-alert alert alert-danger hidden animated bounceIn"></div>
    <div class="form-group">
        <label for="name" class="control-label">Jméno</label>
        <input name="name" class="form-control" id="name" placeholder="Jméno">
    </div>
    <div class="form-group required">
        <label for="email" class="control-label">Email</label>
        <input type="email" name="email" class="form-control" id="email" placeholder="Email"
               required>
    </div>
    <div class="form-group required">
        <label for="pswrd" class="control-label">Heslo</label>
        <input type="password" name="password" class="form-control" id="pswrd" placeholder="Heslo"
               required>
    </div>
    <div class="form-group required">
        <label for="pswrd-again" class="control-label">Heslo pro kontrolu</label>
        <input type="password" name="password-again" class="form-control" id="pswrd-again" placeholder="Heslo znovu"
               required>
    </div>
    <input type="submit" class="btn btn-info" value="Vytvořit">
</form>

<form method="post" id="update" style="display: none;">
    <p>Heslo musí mít minimálně 8 znaků a obsahovat číslice, velká a malá písmena.</p>
    <div class="action-alert alert alert-danger hidden animated bounceIn"></div>
    <input type="hidden" name="userid">
    <div class="form-group">
        <label for="name" class="control-label">Jméno</label>
        <input name="name" class="form-control" id="name" placeholder="Jméno">
    </div>
    <div class="form-group required">
        <label for="email" class="control-label">Email</label>
        <input type="email" name="email" class="form-control" id="email" placeholder="Email"
               required>
    </div>
    <input type="submit" class="btn btn-primary" value="Upravit">
    <input type="button" class="btn btn-info cancel" value="Zrušit">
</form>
