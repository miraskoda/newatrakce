<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 24.03.2017
 * Time: 10:55
 */
?>

<div class="row table-ignore">
    <form method="post" id="create-form" class="hidden">
        <div class="action-alert alert alert-danger hidden"></div>
        <div class="col-md-8">
            <div>
                <div class="form-group required">
                    <label for="name" class="control-label">Název</label>
                    <input style="font-size: 20px; font-weight: bold" name="name" class="form-control" id="name" placeholder="Název produktu ..." required>
                </div>
                <div class="form-group required">
                    <label for="description-create" class="control-label">Popis</label>
                    <textarea name="description" class="form-control" rows="15" id="description-create"
                              placeholder="Popis produktu ..."></textarea>
                </div>
                <div class="form-group required">
                    <label for="target-create" class="control-label">Určeno pro</label>
                    <textarea name="target" class="form-control" rows="5" id="target-create"
                              placeholder="Určení produktu ..."></textarea>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div>
                <div class="form-group required">
                    <label for="price" class="control-label">Cena bez DPH</label>
                    <input type="number" min="0" step="0.01" name="price" class="form-control" id="price"
                           placeholder="Cena bez DPH ..." required>
                </div>
                <div class="form-group required">
                    <label for="priceWithVat" class="control-label">Cena s DPH</label>
                    <input type="number" min="0" step="0.01" name="priceWithVat" class="form-control" id="priceWithVat"
                           placeholder="Cena s DPH ..." required>
                </div>
                <div class="form-group">
                    <label for="quantity" class="control-label">Množství skladem</label>
                    <input type="number" min="0" step="1" name="quantity" class="form-control" id="quantity"
                           placeholder="Množství ..." required>
                </div>
                <div class="form-group">
                    <label for="visible" class="control-label">Publikováno</label>
                    <input type="checkbox" name="visible" id="visible" data-toggle="toggle" data-on="Ano" data-off="Ne">
                </div>
                <div class="form-group">
                    <label for="keywords" class="control-label">Klíčová slova</label>
                    <input name="keywords" class="form-control" id="keywords"
                           placeholder="Klíčová slova ..." required>
                </div>
                <input type="submit" class="btn btn-success" value="Vytvořit">
                <input type="button" class="btn btn-danger cancel" value="Zrušit">
            </div>
        </div>
    </form>

    <form method="post" id="update-form" class="hidden">
        <div class="action-alert alert alert-danger hidden"></div>
        <div class="col-md-8">
            <div>
                <input type="hidden" name="productId">
                <div class="form-group required">
                    <label for="name" class="control-label">Název</label>
                    <input style="font-size: 20px; font-weight: bold" name="name" class="form-control" id="name" placeholder="Název produktu ..." required>
                </div>
                <div class="form-group required">
                    <label for="description-update" class="control-label">Popis</label>
                    <textarea name="description" class="form-control" rows="10" id="description-update"
                              placeholder="Popis produktu ..."></textarea>
                </div>
                <div class="form-group required">
                    <label for="target-update" class="control-label">Určeno pro</label>
                    <textarea name="target" class="form-control" rows="5" id="target-update"
                              placeholder="Určení produktu ..."></textarea>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div>
                <div class="form-group required">
                    <label for="price" class="control-label">Cena bez DPH</label>
                    <input type="number" min="0" step="0.01" name="price" class="form-control" id="price"
                           placeholder="Cena bez DPH ..." required>
                </div>
                <div class="form-group required">
                    <label for="priceWithVat" class="control-label">Cena s DPH</label>
                    <input type="number" min="0" step="0.01" name="priceWithVat" class="form-control" id="priceWithVat"
                           placeholder="Cena s DPH ..." required>
                </div>
                <div class="form-group">
                    <label for="quantity" class="control-label">Množství skladem</label>
                    <input type="number" min="0" step="1" name="quantity" class="form-control" id="quantity"
                           placeholder="Množství ..." required>
                </div>
                <div class="form-group required">
                    <label for="visible" class="control-label">Publikováno</label>
                    <input type="checkbox" name="visible" id="visible" data-toggle="toggle" data-on="Ano" data-off="Ne">
                </div>
                <div class="form-group">
                    <label for="keywords" class="control-label">Klíčová slova</label>
                    <input name="keywords" class="form-control" id="keywords"
                           placeholder="Klíčová slova ..." required>
                </div>
                <div class="form-group additions">
                    <p>Hlavní data</p>
                    <input type="button" class="btn btn-primary product-images" value="Fotografie">
                    <input type="button" class="btn btn-primary product-video" value="Video">
                    <input type="button" class="btn btn-primary product-categories" value="Kategorie">
                </div>
                <div class="form-group additions">
                    <p>Ostatní data</p>
                    <input type="button" class="btn btn-primary product-price-included" value="Cena zahrnuje">
                    <input type="button" class="btn btn-primary product-technical" value="Technické požadavky">
                    <input type="button" class="btn btn-primary product-additions" value="Doplňky">
                </div>

                <input type="submit" class="btn btn-success" value="Upravit">
                <input type="button" class="btn btn-danger cancel" value="Zrušit">
            </div>
        </div>
    </form>
</div>
