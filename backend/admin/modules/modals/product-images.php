<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 13.06.2017
 * Time: 15:43
 */

?>

<div class="modal fade" id="product-images-modal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Fotografie k produktu</h4>
            </div>
            <div class="modal-body">
                <form id="category-edit-form">
                    <input type="hidden" name="productId">
                    <p>Fotografie se ukládají automaticky</p>
                    <div class="image-status"><div><p>Nová informace</p></div></div>
                    <div class="form-group">
                        <label class="control-label">Hlavní fotografie (ideálně čtvercová, min. 600px):</label>
                        <div id="product-image-main">
                            <div class="dz-message">
                                <p>Pro nahrání sem přetáhněte soubor nebo klikněte</p>
                            </div>
                            <div class="fallback">
                                <input name="file" type="file" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Další fotografie produktu (min. 600px):</label>
                        <div id="product-images">
                            <div class="dz-message">
                                <p>Pro nahrání sem přetáhněte soubor nebo klikněte</p>
                            </div>
                            <div class="fallback">
                                <input name="file" type="file" multiple />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary" id="cancel">Zpět</button>
            </div>
        </div>

    </div>
</div>