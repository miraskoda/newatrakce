<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 06.04.2018
 * Time: 19:50
 */
?>

<div class="modal fade" id="available-products-modal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Volné atrakce</h4>
            </div>
            <div class="modal-body">
                <form>
                    <ul id="available-products">
                    </ul>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary" id="cancel">Zpět</button>
            </div>
        </div>

    </div>
</div>