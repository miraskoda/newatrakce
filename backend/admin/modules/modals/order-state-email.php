<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 13.10.2017
 * Time: 0:19
 */
?>

<div class="modal fade" id="order-state-email-modal" role="dialog">
    <div class="modal-dialog modal-wrap" style="margin-top: 100px !important;">

        <div class="modal-content">
            <div class="modal-body">
                <p>Chcete informovat zákazníka o změně stavu objednávky?</p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-danger" id="yes">Ano</button>
                <button type="button" data-dismiss="modal" class="btn" id="no">Ne</button>
            </div>
        </div>

    </div>
</div>
