<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 08.04.2018
 * Time: 22:30
 */
?>

<div class="modal fade" id="superproduct-products" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Správa podproduktů</h4>
            </div>
            <div class="modal-body">
                <div class="superproduct-product-status"><div><p></p></div></div>
                <div class="products-container"></div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary" id="cancel">Zrušit</button>
            </div>
        </div>

    </div>
</div>
