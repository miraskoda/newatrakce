<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 08.04.2018
 * Time: 20:26
 */
?>
<div class="modal fade" id="superproduct-edit" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Úprava superproduktu jako celku</h4>
            </div>
            <div class="modal-body">
                <form id="superproduct-edit-form">
                    <div class="form-group required">
                        <label for="name" class="control-label">Název:</label>
                        <input id="name" class="form-control" name="name" maxlength="200" placeholder="Název ..." required>
                    </div>
                    <div class="form-group required">
                        <label for="quantity" class="control-label">Počet kusů:</label>
                        <input type="number" min="1" step="1" id="quantity" class="form-control" name="quantity" required>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-danger" id="save">Uložit</button>
                <button type="button" data-dismiss="modal" class="btn btn-primary" id="cancel">Zrušit</button>
            </div>
        </div>

    </div>
</div>
