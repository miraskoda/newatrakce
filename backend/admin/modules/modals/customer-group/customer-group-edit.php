<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 07.12.2017
 * Time: 14:55
 */
?>

<div class="modal fade" id="customer-group-edit-modal" role="dialog">
	<div class="modal-dialog">

		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Úprava zákaznické skupiny</h4>
			</div>
			<div class="modal-body">

				<form id="customer-group-edit-form">
					<div class="form-group required">
						<label for="name" class="control-label">Název:</label>
						<input id="name" class="form-control" name="name" maxlength="200" placeholder="Název ..." required>
					</div>
					<div class="form-group">
						<label for="normal-payments" class="control-label">Povoleny standardní způsoby platby</label>
                        <input type="checkbox" name="normal-payments" id="normal-payments" data-toggle="toggle" data-on="Ano" data-off="Ne">
					</div>
					<div class="form-group">
						<label for="vip-payments" class="control-label">Povoleny VIP způsoby platby</label>
                        <input type="checkbox" name="vip-payments" id="vip-payments" data-toggle="toggle" data-on="Ano" data-off="Ne">
					</div>
                    <div class="form-group">
                        <label for="discount" class="control-label">Sleva (v procentech)</label>
                        <input type="number" min="0" step="0.01" id="discount" class="form-control" name="discount" placeholder="Sleva ... %">
                    </div>
					<div class="form-group">
						<label for="max-reservation-hours" class="control-label">Maximální délka rezervace</label>
                        <input type="number" min="1" step="1" id="max-reservation-hours" class="form-control" name="max-reservation-hours" placeholder="Maximální délka rezervace ...">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-danger save">Uložit</button>
				<button type="button" data-dismiss="modal" class="btn btn-primary cancel">Zrušit</button>
			</div>
		</div>

	</div>
</div>