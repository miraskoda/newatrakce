<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 07.08.2017
 * Time: 14:56
 */
?>

<div class="modal fade" id="product-category-position-modal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Pořadí produktů v kategorii</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="category-status"><div><p>Nová informace</p></div></div>
                    <div class="form-group">
                        <input type="hidden" name="categoryId">
                    </div>
                    <ul id="category-product-position-list">
                    </ul>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary" id="cancel">Zpět</button>
            </div>
        </div>

    </div>
</div>
