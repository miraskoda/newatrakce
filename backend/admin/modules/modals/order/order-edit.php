<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 1.12.2018
 * Time: 19:50
 */
?>

<?php

use backend\models\Address;
use backend\models\OrderTerm;
use backend\models\Payment;
use backend\controllers\CustomerController;
use backend\models\CustomerGroup;
use backend\models\PaymentType;
use backend\models\Order;
use backend\models\Customer;

?>

<div class="modal fade" id="order-edit-modal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Založení nové objednávky</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="grid-item col-md-12">
                        <h4>Termín pronajmutí</h4>
                        <?php
                        $orderTerm = new OrderTerm();
                        $orderTerm->setOrderId($order->getOrderId());
                        $orderTerm->setTermNo(1);
                        $orderTerm = $orderTerm->load();

                        $isOrderTermLoaded = false;
                        if (is_a($orderTerm, OrderTerm::class))
                            $isOrderTermLoaded = true;
                        ?>

                        <div class="form-group required">
                            <div class="input-group date datetimepickeredit" id="starts">
                                <input class="form-control" name="date" placeholder="Vyberte datum a čas ..."
                                       id="start-term" date-part="date">
                                <span class="input-group-addon">
                                            <span class="ti-calendar"></span>
                                        </span>
                            </div>

                        </div>

                        <script>
                        $( document ).ready(function() {
                        $("#starts").data("DateTimePicker").date("<?=$orderTerm->getStart()?>")
                        })
                        </script>

                        <div class="form-group required">
                            <label for="interval-hours-term-edit"
                                   class="control-label">Počet hodin</label>
                            <input id="numberOfHours"
                                   class="form-control" type="number" min="0"
                                   max="12" step="1"
                                   value="<?= $isOrderTermLoaded ? ($orderTerm->getHours() % 24) : '5' ?>"
                                   placeholder="Zadejte počet hodin"
                                   id="interval-hours-term-edit" date-part="hour">
                        </div>
                        <div class="col-md-12"><p class="date-result"></p></div>
                        <h4>Kontaktní osoba v den akce</h4>
                        <form id="contact-form">
                            <div class="form-group required">
                                <label class="control-label" for="contact-name">Jméno a příjmení</label>
                                <input name="contact-name" class="form-control" id="contact-name"
                                       placeholder="Jméno"
                                       value="<?= (($order->getContactName() !== null) ? ($order->getContactName()) : '') ?>"
                                       required>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="prefix">Předčíslí</label>
                                        <select class="form-control" name="contact-phone-prefix"
                                                id="prefix">
                                            <option<?= ($order->getContactPhonePrefix() == '+420') ? ' selected' : '' ?>>
                                                +420
                                            </option>
                                            <option<?= ($order->getContactPhonePrefix() == '+421') ? ' selected' : '' ?>>
                                                +421
                                            </option>
                                            <option<?= ($order->getContactPhonePrefix() == '+49') ? ' selected' : '' ?>>
                                                +49
                                            </option>
                                            <option<?= ($order->getContactPhonePrefix() == '+48') ? ' selected' : '' ?>>
                                                +48
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group required">
                                        <label class="control-label" for="contact-phone">Telefon (např.:
                                            789456123)</label>
                                        <input type="tel" name="contact-phone" class="form-control"
                                               id="contact-phone" placeholder="789456123" <?php
                                        if ($order->getContactPhone() !== null) {
                                            if ($order->getContactPhone() == 0) {
                                                echo "";
                                            } else {
                                                echo 'value="' . $order->getContactPhone() . '"';
                                            }
                                        }
                                        ?> required>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <h4>Adresa místa konání</h4>
                        <form id="venue-data">
                            <?php
                            $invoiceAddress = new Address();
                            $invoiceAddress->setCustomerId($order->getCustomerId());
                            $invoiceAddress->setAddressType(Address::ADDRESS_INVOICE);
                            $invoiceAddress = $invoiceAddress->load(true);

                            $isAddressLoaded = false;
                            if (is_a($invoiceAddress, Address::class))
                                $isAddressLoaded = true;
                            ?>
                            <div class="image-status venue-data-status">
                                <div><p>Nová informace</p></div>
                            </div>
                            <div class="form-group required">
                                <label class="control-label" for="venue-street">Ulice a číslo
                                    popisné</label>
                                <input name="venue-street" class="form-control" id="venue-street"
                                       placeholder="Ulice 123"
                                       value="<?= (($order->getVenueStreet() !== null) ? ($order->getVenueStreet()) : (($isAddressLoaded) ? $invoiceAddress->getStreet() : '')) ?>"
                                       required>
                            </div>
                            <div class="form-group required">
                                <label class="control-label" for="venue-city">Město</label>
                                <input name="venue-city" class="form-control" id="venue-city"
                                       placeholder="Město"
                                       value="<?= (($order->getVenueCity() !== null) ? ($order->getVenueCity()) : (($isAddressLoaded) ? $invoiceAddress->getCity() : '')) ?>"
                                       required>
                            </div>
                            <div class="form-group required">
                                <label class="control-label" for="venue-zip">PSČ (např. 45612)</label>
                                <input name="venue-zip" class="form-control" id="venue-zip"
                                       placeholder="45612"
                                       value="<?= (($order->getVenueZip() !== null) ? ($order->getVenueZip()) : (($isAddressLoaded) ? $invoiceAddress->getZip() : '')) ?>"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="venue-country">Země</label>
                                <select class="form-control" name="venue-country" id="venue-country">
                                    <option<?= ($order->getVenueCountry() == 'cz') ? ' selected' : '' ?>
                                            value="cz">Česká republika
                                    </option>
                                    <option<?= ($order->getVenueCountry() == 'sk') ? ' selected' : '' ?>
                                            value="sk">Slovensko
                                    </option>
                                    <option<?= ($order->getVenueCountry() == 'pl') ? ' selected' : '' ?>
                                            value="pl">Polsko
                                    </option>
                                    <option<?= ($order->getVenueCountry() == 'de') ? ' selected' : '' ?>
                                            value="de">Německo
                                    </option>
                                </select>
                            </div>
                        </form>
                        <h4>Ověřovací údaje</h4>
                        <?php
                        $customer = new Customer();
                        $customer->setCustomerId($order->getCustomerId());
                        $customer = $customer->load();

                        $isCustomerLoaded = false;
                        if (is_a($customer, Customer::class))
                            $isCustomerLoaded = true;
                        ?>
                        <form id="edit-account-data">
                            <?= $isCustomerLoaded ? '<input type="hidden" name="customer-id" value="' . $customer->getCustomerId() . '">' : '' ?>
                            <div class="form-group required">
                                <label class="control-label" for="company-switch">Jsem firma</label>
                                <input type="checkbox" name="company" id="company-switch" data-toggle="toggle"
                                       data-on="Ano"
                                       data-off="Ne" <?= $isCustomerLoaded ? ($customer->isCompany() ? 'checked' : '') : '' ?>>
                            </div>
                            <div class="person">
                                <div class="form-group required">
                                    <label class="control-label" for="birth-number">Rodné číslo (bez lomítka)</label>
                                    <input name="birth-number" class="form-control" id="birth-number"
                                           placeholder="Rodné číslo"
                                           value="<?= ($isCustomerLoaded) ? $customer->getBirthNumber() : '' ?>">
                                </div>
                            </div>
                            <div class="company" style="display: none;">
                                <div class="form-group required">
                                    <label class="control-label" for="ico">IČO</label>
                                    <input name="ico" class="form-control" id="ico" placeholder="IČO"
                                           value="<?= ($isCustomerLoaded) ? $customer->getIco() : '' ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="dic">DIČ</label>
                                    <input name="dic" class="form-control" id="dic" placeholder="DIČ"
                                           value="<?= ($isCustomerLoaded) ? $customer->getDic() : '' ?>">
                                </div>
                            </div>
                        </form>
                        <h4>Typ platby:</h4>
                        <?php
                        $customerGroup = CustomerController::getCustomerGroup();
                        if (is_a($customerGroup, CustomerGroup::class))
                            $paymentTypes = PaymentType::getPaymentTypes($customerGroup->getVipPaymentsBool());
                        else
                            $paymentTypes = PaymentType::getPaymentTypes();

                        if (is_array($paymentTypes) && is_a($order, Order::class)) {
                            foreach ($paymentTypes as $paymentType) {
                                echo '<div class="radio">
                                <label><input id="payment-type" type="radio" name="payment-type" value="' . $paymentType->getPaymentTypeId() . '" '.($order->getPaymentTypeId() == $paymentType->getPaymentTypeId() ? "checked": "").'><span>' . $paymentType->getName() . '</span></label>
                            </div>';
                            }
                        }
                        ?>
                        <h4>Způsob platby:</h4>
                        <?php
                        if (is_a($customerGroup, CustomerGroup::class))
                            $payments = Payment::getPayments($customerGroup->getVipPaymentsBool());
                        else
                            $payments = Payment::getPayments();

                        if (is_array($payments) && is_a($order, Order::class)) {
                            foreach ($payments as $payment) {
                                echo '<div class="radio">
                                <label><input id="paymentId" type="radio" name="payment" value="' . $payment->getPaymentId() . '" ' . (($order->getPaymentId() == $payment->getPaymentId()) ? 'checked' : null) . '><span class="' . $payment->getIcon() . '"></span> ' . $payment->getName() . '</label>
                            </div>';
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-success" id="accept-edit">Vytvořit
                    objednávku
                </button>
                <button type="button" data-dismiss="modal" class="btn btn-primary" id="cancel-edit">Zpět</button>
            </div>
        </div>
    </div>
</div>