<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 1.12.2018
 * Time: 19:50
 */
?>

<?php

use backend\models\Address;
use backend\models\OrderTerm;
use backend\models\Payment;
use backend\controllers\CustomerController;
use backend\models\CustomerGroup;
use backend\models\PaymentType;
use backend\models\Order;
use backend\models\Customer;

?>

<div class="modal fade" id="modalInvoice" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Vytvoření nové faktury</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="grid-item col-md-12">

                        <form id="invoice-form">
                            <input type="hidden" id="orderId" value="<?= $order->getOrderId() ?>">

                        <div class="form-group required">
                            <label for="interval-hours-term-edit"
                                   class="control-label">Název položky</label>
                            <input type="text" name="names" class="form-control" id="names" placeholder="Název položky ve faktuře" required>


                        </div>


                            <div class="form-group required">
                                <label for="interval-hours-term-edit"
                                       class="control-label">Množství</label>
                                <input type="number" name="mnozstvi" class="form-control" id="mnozstvi" placeholder="Množství na faktuře" required>



                            </div>

                            <div class="form-group required">
                                <label for="interval-hours-term-edit"
                                       class="control-label">Jednotková cena</label>
                                <input type="number" name="jednotkova" class="form-control" id="jednotkova" placeholder="Cena za položku uvedenou ve faktuře" required>



                            </div>

                            <div class="form-group required">
                                <label for="interval-hours-term-edit"
                                       class="control-label">Sleva</label>
                                <input type="number" name="sleva" class="form-control" id="sleva" placeholder="Sleva v % na faktuře" required>



                            </div>

                            <div class="form-group required">
                                <label for="interval-hours-term-edit"
                                       class="control-label">Celkem bez DPH</label>
                                <input type="number" name="bezdph" class="form-control" id="bezdph" onchange="countVATs()" placeholder="Celková cena na faktuře bez DPH" required>



                            </div>

                            <div class="form-group required">
                                <label for="interval-hours-term-edit"
                                       class="control-label">Celkem s DPH</label>
                                <input type="number" name="sdph" class="form-control" id="sdph" onchange="countVATWs()" placeholder="Celková cena na faktuře s DPH" required>


                            </div>

                            <div class="form-group required">
                                <label for="interval-hours-term-edit"
                                       class="control-label">Podrobnosti</label>
                                <input type="text" name="desc" class="form-control" id="desc" placeholder="Nějaké další podrobnosti?" required>


                            </div>

                        </form>


                    </div>
                </div>

            </div>

            <script>

                var bezDph = document.getElementById("bezdph");
                var sDph = document.getElementById("sdph");
                function countVATs() {
                    sDph.value = vatCalcs(bezDph.value).toFixed(2);
                }
                function countVATWs() {
                    bezDph.value = vatCalcWs(sDph.value).toFixed(2);
                }
                function vatCalcs (cost){
                    var vat = 21;
                    return (cost/100)*(vat+100);
                }
                function vatCalcWs (cost){
                    var vat = 21;
                    return (cost*100)/(vat+100);
                }

            </script>


            <div class="modal-footer">
                <button type="button" class="btn btn-success"  id="invoice-edit" >Vytvořit </button>
                <button type="button" data-dismiss="modal" class="btn btn-primary" id="cancel-edit">Zpět</button>
            </div>

        </div>
    </div>
</div>