<div class="container-fluid" >
    <div class="row">
        <div class="col-md-12">
            <div class="box text-center animated zoomIn">
                <h1>Vítejte</h1>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box text-center animated zoomIn">
                <h1>Správa produktů</h1>
                <a href="pages/product-management.php" class="btn btn-primary">Spravovat</a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box text-center animated zoomIn">
                <h1>Objednávky</h1>
                <a href="pages/order-management.php" class="btn btn-primary">Přejít na přehled</a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box text-center animated zoomIn">
                <h1>Rezervace</h1>
                <a href="pages/reservation-management.php" class="btn btn-primary">Přejít na přehled</a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box text-center animated zoomIn">
                <h1>Kalendář</h1>
                <a href="pages/calendar.php" class="btn btn-primary">Jít do kalendáře</a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box text-center animated zoomIn">
                <h1>Správa zákazníků</h1>
                <a href="pages/customer-management.php" class="btn btn-primary">Spravovat</a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box text-center animated zoomIn">
                <h1>Správa uživatelů</h1>
                <a href="pages/user-management.php" class="btn btn-primary">Spravovat</a>
            </div>
        </div>

        <div class="col-md-3">
            <div class="box text-center animated zoomIn">
                <h1>Nastavení</h1>
                <a href="pages/options.php" class="btn btn-primary">Spravovat</a>
            </div>
        </div>


        <div class="col-md-3">
            <div class="box text-center animated zoomIn">
                <h1>File manager</h1>
                <a href="pages/file.php" class="btn btn-primary">Spravovat</a>
            </div>
        </div>

    </div>
</div>