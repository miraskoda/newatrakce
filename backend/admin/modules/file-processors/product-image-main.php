<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 14.06.2017
 * Time: 0:09
 */

header('Content-Type: application/json');

require_once __DIR__ . "/../../../modules/app/prepare.php";
require_once $root . "/plugins/image-uploader/upload.class.php";

$newName = date('YmdHis') . rand(1000, 9999);

$path = $_FILES['image']['name'];
$ext = pathinfo($path, PATHINFO_EXTENSION);

$response = (object) array(
    'code' => 200,
    'newName' => $newName,
    'extension' => $ext,
    'message' => ''
);

$newNameThumbnail = $newName . 'main';
$handleThumbnail = new upload($_FILES['image'], 'cs_CS');
$handleBig = new upload($_FILES['image'], 'cs_CS');
if ($handleThumbnail->uploaded) {
    $handleThumbnail->file_new_name_body   = $newNameThumbnail;
    $handleThumbnail->image_resize         = true;
    $handleThumbnail->image_x              = 500;
    $handleThumbnail->image_ratio_y        = true;
    $handleThumbnail->file_overwrite       = true;
    $handleThumbnail->process($root . '/../assets/images/products');
    if ($handleThumbnail->processed) {
        //echo json_encode($response);
    } else {
        $response['code'] = 400;
        $response['message'] = $handleThumbnail->error;

        echo json_encode($response);
        exit;
    }
}

if ($handleBig->uploaded) {
    $handleBig->file_new_name_body   = $newName;
    $handleBig->image_resize         = true;
    $handleBig->image_x              = 600;
    $handleBig->image_ratio_y        = true;
    $handleBig->file_overwrite       = true;
    $handleBig->process($root . '/../assets/images/products');
    if ($handleBig->processed) {
        echo json_encode($response);

        $handleThumbnail->clean();
        $handleBig->clean();
    } else {
        $response['code'] = 400;
        $response['message'] = $handleBig->error;

        echo json_encode($response);
    }
}