<?php
use backend\models\Url;
?>
<nav class="navbar navbar-inverse" style="background-color: black; border-radius: 0px;position: fixed;
    width: 100vw;
    z-index: 999999999999;">
    <div class="container-fluid" style="margin: 5px">


        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar"><span
                        class="ti-menu"></span></button>
            <a class="navbar-brand" href="<?= Url::getAbsUrlTo($root . '/admin/') ?>">OnlineAtrakce.cz</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar">
        <ul class="nav navbar-nav">
            <li class="active"><a href="<?= Url::getAbsUrlTo($root . '/admin/') ?>">Administrace</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/../') ?>">Na web</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right tip">


            <?php
            if($pageId==5 || $pageId==24)
             echo '<li style="margin-top: 7px" >      <form style="display: flex">
                     <button style="float: right" class="btn btn-default" type="submit"><span class="ti-search"></span></button>
                     <input style="width: 75%" size="24" type="search" name="search" class="form-control" placeholder="Hledej">
                 </form>

             </li>';
            ?>
            <li><a href="#"><span class="ti-user"></span> &nbsp; Uživatel Administrátor</a></li>
            <li><a href="#"><span class="ti-settings"></span><span>&nbsp;Nastavení</span></a></li>
            <li><a class="admin-logout" href="#"><span class="ti-power-off"></span> <span>&nbsp;Odhlásit se</span></a></li>
        </ul>
    </div>
</nav>
</div>
<nav class="sidebar " style="width: 54px">
    <div>
        <ul>
            <li><a href="#" class="hamburger-trigger"><span class="ti-menu"></span> Menu</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/user-management.php') ?>"><span class="ti-user"></span> Uživatelé</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/product-management.php?layout=table') ?>"><span class="ti-package"></span> Produkty</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/superproduct-management.php') ?>"><span class="ti-layers-alt"></span> Superprodukty</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/category-management.php') ?>"><span class="ti-direction-alt"></span> Kategorie</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/customer-management.php') ?>"><span class="ti-id-badge"></span> Zákazníci</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/customer-group-management.php') ?>"><span class="ti-folder"></span> Zák. skupiny</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/order-management.php') ?>"><span class="ti-truck"></span> Objednávky</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/reservation-management.php') ?>"><span class="ti-timer"></span> Rezervace</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/calendar.php') ?>"><span class="ti-calendar"></span> Kalendář akcí</a></li>
            <li><a href="#"><span class="ti-stats-up"></span> Analytika</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/options.php') ?>"><span class="ti-settings"></span> Nastavení</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/logs.php') ?>"><span class="ti-exchange-vertical"></span> Logy</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/foxydesk-chat.php') ?>"><span class="ti-comments"></span> Foxydesk</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/admin/pages/file.php') ?>"><span class="ti-save"></span>File manager</a></li>

        </ul>
    </div>
</nav>

<div class="wrapper" style="padding-top: 60px"></div>