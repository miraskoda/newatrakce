<div class="container-fluid" style="margin-left: 0px">
    <div class="block-wrap">
        <header class="animated bounceInDown">
            <h1>OnlineAtrakce.cz</h1>
        </header>
        <div class="animated flipInX" style="background-color: black">
            <h2>Přihlášení</h2>
            <form method="post">
                <div class="action-alert alert alert-danger hidden animated bounceIn"></div>
                <div class="form-group required">
                    <label class="control-label" for="usr">Email:</label>
                    <input type="email" name="email" class="form-control" id="usr" placeholder="Email" required>
                </div>
                <div class="form-group required">
                    <label class="control-label" for="pswrd">Heslo:</label>
                    <input type="password" name="password" class="form-control" id="pswrd" placeholder="Heslo" required>
                </div>
                <input type="submit" class="btn btn-info" value="Přihlásit se">
            </form>
        </div>
        <footer class="animated fadeInUp text-center">
            <p><a class="forgotten-password" href="#">Zapomenuté heslo</a></p>
        </footer>
    </div>
</div>