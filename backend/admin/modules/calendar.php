<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 23.01.2018
 * Time: 11:24
 */
?>

<div class="container-fluid calendar-view">
    <div class="row admin-grid-header calendar-header">
        <div class="col-md-3">
            <div class="animated zoomIn">
                <h2>Kalendář akcí</h2>
            </div>
        </div>
        <div class="col-md-3 text-center">
            <p class="current-month">Červen 2018</p>
        </div>
        <div class="col-md-3 text-center">
            <p>
                Zobrazena data <span class="dates bold"></span>
            </p>
        </div>
        <div class="col-md-3 text-center form-group">
            <div class="row">
                <div class="col-md-4">
                    <a class="btn previous"><span class="ti-angle-left" onclick="nextSwitch(false)"></span></a>
                    <a class="btn next"><span class="ti-angle-right" onclick="nextSwitch(true)"></span></a>
                </div>
                <div class="col-md-8">
                    <select name="calendar-grid-layout" class="form-control">
                        <option value="week">Týden</option>
                        <option value="three-day">3 dny</option>
                        <option value="day">Den</option>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="row calendar-data">
        <div class="col-md-2">
            <div class="small-calendar-header">
                <span class="pull-left small-current-month">Červen 2018</span>
                <span class="pull-right">
                    <span class="ti-angle-left previous-month btn" onclick="nextSmallSwitch(false)"></span>
                    <span class="ti-angle-right next-month btn" onclick="nextSmallSwitch(true)"></span>
                </span>
            </div>
            <div class="small-calendar-grid">
            </div>
            <div class="">
                <div class="row">
                    <div class="col-md-12">
                        <p>Celkem objednávek: <span class="order-count bold"></span></p>
                    </div>
                    <div class="col-md-12">
                        <p>Celkem rezervací: <span class="reservation-count bold"></span></p>
                    </div>
                </div>
            </div>
            <div class="legend">
                <h4>Legenda</h4>
                <p>Objednávky:</p>
                <ul>
                    <li class="order unpaid">Nezaplacená</li>
                    <li class="order confirmed">Potvrzená</li>
                    <li class="order sentInvoice">Faktura odeslána</li>
                    <li class="order cancelled">Stornovaná</li>
                    <li class="order finished">Dokončeno</li>
                </ul>
                <p class="reserv reservation">Rezervace</p>
            </div>
        </div>

        <div class="col-md-10 calendar-grid-container">
            <div class="calendar-grid">
            </div>
        </div>
    </div>
</div>