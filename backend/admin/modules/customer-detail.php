<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 16.02.2018
 * Time: 17:38
 */

use backend\models\Customer;
use backend\models\CustomerGroup;
use backend\models\Url;

if(isset($_GET['customerId'])) {
    $customerId = $_GET['customerId'];
    $customer = new Customer($customerId);
    $customer = $customer->load();
    if(!is_a($customer, Customer::class))
        die('Chyba! ' . $customer);
} else {
    die('Nebylo možné načíst zákazníka.');
}
?>

<div class="container-fluid">
	<div class="row admin-grid-header">
		<div class="col-md-12 first">
			<div class="box text-center animated zoomIn">
                <a href="customer-management.php#customer-id=<?= $customerId ?>">Správa zákazníků</a> <span class="ti-arrow-right"></span> Zákazník č. <?= $customerId ?>
			</div>
		</div>
	</div>

	<div class="row">
        <div class="col-md-6">
            <div class="box light-blue overflow-auto animated zoomInDown">
                <div class="row" customer-id="<?= $customerId ?>">
                    <div class="col-md-12 text-center">
                        <label for="order-state">Uživatelská skupina</label>
                    </div>
                    <div class="col-md-10 col-md-offset-1">
                        <?php
                        $customerGroups = CustomerGroup::getCustomerGroups();
                        ?>
                        <div class="form-group">
                            <select class="form-control customer-group">
                                <?php
                                $customerGroupId = $customer->getCustomerGroupId();
                                foreach ($customerGroups as $customerGroup) {
                                    echo '<option value="' . $customerGroup->getCustomerGroupId() . '" ' . (($customerGroupId > 0 && $customerGroupId == $customerGroup->getCustomerGroupId()) ? 'selected' : '') . '>' . $customerGroup->getName() . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box animated zoomInDown">
                <p>
                    <?= (($customer->isCompany()) ? 'Je firma.' : 'Není firma.') ?>
                </p>
                <p>
                    <?= (($customer->isEmailConfirmed()) ? 'Zákazník je ověřen.' : 'Zákazník není ověřen!') ?>
                </p>
            </div>
        </div>
    </div>
    <div class="row customer">
        <?php require_once Url::getBackendPathTo("/modules/page-parts/customer/account-data/account-data-content.php"); ?>
    </div>
</div>