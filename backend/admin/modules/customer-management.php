<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 15.02.2018
 * Time: 12:01
 */

use backend\models\Customer;
use backend\models\CustomerGroup;
use backend\models\Order;
use backend\models\Reservation;

$customers = Customer::getCustomers();
?>
<div class="container-fluid">
	<div class="row admin-grid-header">
		<div class="col-md-12 first">
			<div class="box text-center animated zoomIn">
				<h1>Správa zákazníků</h1>
			</div>
		</div>
	</div>

	<div class="row">
		<?php
		if(isset($customers) && is_array($customers) && count($customers) > 0) :
			?>
			<div class="col-md-12">
				<div class="box overflow-auto light-blue text-center animated zoomInDown">
					<div class="row">
						<div class="col-md-1">
							<p>Číslo zákazníka</p>
						</div>
						<div class="col-md-3">
							<p>Jméno / Název</p>
						</div>
                        <div class="col-md-1">
                            <p>Ověřen</p>
                        </div>
                        <div class="col-md-1">
                            <p>Firma</p>
                        </div>
                        <div class="col-md-2">
                            <p>Počet objednávek</p>
                        </div>
                        <div class="col-md-1">
                            <p>Počet rezervací</p>
                        </div>
						<div class="col-md-2">
							<p>Uživatelská skupina</p>
						</div>
						<div class="col-md-1">
						</div>
					</div>
				</div>
			</div>
			<?php
			foreach ($customers as $customer):
				?>
				<div class="col-md-12">
					<div class="box text-center animated zoomInDown">
						<div class="row" customer-id="<?= $customer->getCustomerId() ?>">
							<div class="col-md-1">
								<p>
									<?= $customer->getCustomerId() ?>
								</p>
							</div>
							<div class="col-md-3">
								<p><?= $customer->getName() ?></p>
							</div>
							<div class="col-md-1">
                                <p>
                                    <?= (($customer->isEmailConfirmed()) ? 'Ano' : 'Ne') ?>
                                </p>
							</div>
                            <div class="col-md-1">
                                <p>
                                    <?= (($customer->isCompany()) ? 'Ano' : 'Ne') ?>
                                </p>
                            </div>
							<div class="col-md-2">
                                <p>
                                    <?= Order::getOrderCountByCustomer($customer->getCustomerId()); ?>
                                </p>
							</div>
                            <div class="col-md-1">
                                <p>
                                    <?= Reservation::getReservationCountByCustomer($customer->getCustomerId()); ?>
                                </p>
                            </div>
							<div class="col-md-2">
                                <?php
                                $customerGroups = CustomerGroup::getCustomerGroups();
                                ?>
                                <div class="form-group">
                                    <select class="form-control customer-group">
                                        <?php
                                        $customerGroupId = $customer->getCustomerGroupId();
                                        foreach ($customerGroups as $customerGroup) {
                                            echo '<option value="' . $customerGroup->getCustomerGroupId() . '" ' . (($customerGroupId > 0 && $customerGroupId == $customerGroup->getCustomerGroupId()) ? 'selected' : '') . '>' . $customerGroup->getName() . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
							</div>
							<div class="col-md-1">
								<a href="customer-detail.php?customerId=<?= $customer->getCustomerId() ?>" class="btn btn-success"><span class="ti-search"></span></a>
							</div>
						</div>
					</div>
				</div>
			<?php
			endforeach;
			?>
		<?php
		else :
			?>
			<div class="col-md-12 box">
				<p>Nebyli nalezeni žádní zákazníci. <?= ((isset($customers) && is_string($customers)) ? $customers : '') ?></p>
			</div>
		<?php
		endif;
		?>
	</div>
</div>