<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 25.01.2018
 * Time: 23:29
 */


use backend\models\Product;

require_once __DIR__ . "/../../modules/app/prepare.php";

$products = Product::getProducts();

foreach ($products as $product) {
    echo "Produkt " . $product->getProductId();
    if(is_a($product->update(), Product::class))
        echo 'Success';
    else
        echo 'Failure';
}