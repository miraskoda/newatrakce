<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 19.04.2018
 * Time: 12:00
 */

use backend\models\Customer;
use backend\models\Log;
use backend\models\MailHelper;
use backend\models\Reservation;

require_once __DIR__ . "/../../modules/app/prepare.php";

$reservations = Reservation::getReservations(true);
$killCount = 0;
foreach ($reservations as $reservation) {
    if($reservation->isReservationOver()){
        $reservation = $reservation->load();
        $reservation->setActive(0);
        $reservation = $reservation->update();
        if(!is_a($reservation, Reservation::class))
            echo $reservation;

        $killCount++;
    }
}

Log::insert(0, 'Spuštěn reservation killer. Deaktivováno ' . $killCount . ' rezervací.', Log::LOG_SYSTEM, Log::LOG_STATE_SUCCESS);
echo 'Spuštěn reservation killer. Deaktivováno ' . $killCount . ' rezervací.';

$reservations = Reservation::getReservations(true);
$mailer = new MailHelper();
foreach ($reservations as $reservation) {
    if($reservation->isReservationOverInCloseHours(2, 3)) {
        $reservation = $reservation->load();
        $customer = new Customer($reservation->getCustomerId());
        $customer = $customer->load();

        if(is_a($customer, Customer::class)) {
            $reservationWarn = $mailer->sendReservationWarning($customer->getEmail(), $reservation);
            if($reservationWarn !== true) {
                Log::insert(0, 'Chyba odeslání varování pro rezervaci ID:' . $reservation->getReservationId(), Log::LOG_SYSTEM, Log::LOG_STATE_ERROR, $reservationWarn);
                echo '<br>Chyba odeslání varování pro rezervaci ID:' . $reservation->getReservationId() . '. Error: ' . $reservationWarn;
            } else {
                echo '<br>Odesláno varování na email ' . $customer->getEmail();
            }
        } else {
            Log::insert(0, 'Chyba načtení zákazníka pro rezervaci ID:' . $reservation->getReservationId(), Log::LOG_SYSTEM, Log::LOG_STATE_ERROR, $customer);
            echo '<br>Chyba načtení zákazníka pro rezervaci ID:' . $reservation->getReservationId();
        }
    }
}