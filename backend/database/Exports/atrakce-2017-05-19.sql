-- phpMyAdmin SQL Dump
-- version 4.4.15.1
-- http://www.phpmyadmin.net
--
-- Počítač: wm145.wedos.net:3306
-- Vytvořeno: Pát 19. kvě 2017, 17:27
-- Verze serveru: 10.1.19-MariaDB
-- Verze PHP: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `d160030_sql`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `Address`
--

CREATE TABLE IF NOT EXISTS `Address` (
  `addressId` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `street` varchar(100) NOT NULL,
  `cp` varchar(50) NOT NULL,
  `city` varchar(100) NOT NULL,
  `zip` varchar(10) NOT NULL,
  `customerId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `Customer`
--

CREATE TABLE IF NOT EXISTS `Customer` (
  `customerId` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `birthNumber` varchar(20) NOT NULL,
  `ico` varchar(20) NOT NULL,
  `dic` varchar(20) NOT NULL,
  `bankAccountNumber` varchar(50) NOT NULL,
  `bankCode` varchar(4) NOT NULL,
  `registrationDate` datetime NOT NULL,
  `lastLoginDate` datetime NOT NULL,
  `password` varchar(260) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `mns_customer_chat_auto_message`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_auto_message` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text,
  `config` text,
  `state` varchar(16) NOT NULL,
  `token` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `mns_customer_chat_data`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_data` (
  `id` int(10) unsigned NOT NULL,
  `type` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `mns_customer_chat_department`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_department` (
  `id` smallint(5) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `mns_customer_chat_message`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_message` (
  `id` int(10) unsigned NOT NULL,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `body` text NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `talk_id` int(10) unsigned NOT NULL,
  `extra` text
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `mns_customer_chat_message`
--

INSERT INTO `mns_customer_chat_message` (`id`, `from_id`, `to_id`, `body`, `datetime`, `talk_id`, `extra`) VALUES
(1, 4, -1, '', '2017-05-19 15:02:02', 1, '{"type":"talk_start"}'),
(2, 1, 4, 'Dobrý den', '2017-05-19 15:02:39', 1, NULL),
(3, -2, -1, 'OndrejSimecek teď řídí konverzaci', '2017-05-19 15:02:39', 1, '{"type":"talk_owner","id":1}'),
(4, 4, -1, 'Ahoj', '2017-05-19 15:02:52', 1, NULL),
(5, 1, 4, 'Co chcete?', '2017-05-19 15:03:31', 1, NULL),
(6, -2, -1, 'OndrejSimecek ukončil konverzaci', '2017-05-19 15:05:22', 1, '{"type":"talk_close"}');

-- --------------------------------------------------------

--
-- Struktura tabulky `mns_customer_chat_shared_file`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_shared_file` (
  `id` int(10) unsigned NOT NULL,
  `original_name` varchar(255) NOT NULL,
  `name` varchar(32) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `size` int(10) unsigned DEFAULT NULL,
  `upload_id` int(10) unsigned NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `mns_customer_chat_stats`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_stats` (
  `id` int(10) unsigned NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `mns_customer_chat_stats`
--

INSERT INTO `mns_customer_chat_stats` (`id`, `datetime`, `type`, `value`) VALUES
(1, '2017-05-19 15:02:39', 'timeToAnswer', '37');

-- --------------------------------------------------------

--
-- Struktura tabulky `mns_customer_chat_talk`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_talk` (
  `id` int(10) unsigned NOT NULL,
  `state` varchar(32) DEFAULT NULL,
  `department_id` smallint(5) unsigned DEFAULT NULL,
  `owner` int(11) DEFAULT NULL,
  `last_activity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `extra` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `mns_customer_chat_talk`
--

INSERT INTO `mns_customer_chat_talk` (`id`, `state`, `department_id`, `owner`, `last_activity`, `extra`) VALUES
(1, 'closed', NULL, 1, '2017-05-19 15:03:31', NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `mns_customer_chat_upload`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_upload` (
  `id` int(10) unsigned NOT NULL,
  `message_id` int(10) unsigned NOT NULL,
  `state` varchar(16) NOT NULL,
  `files_info` text,
  `size` int(10) unsigned DEFAULT NULL,
  `progress` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `mns_customer_chat_user`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_user` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `mail` varchar(64) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `info` text,
  `roles` varchar(128) DEFAULT NULL,
  `last_activity` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `mns_customer_chat_user`
--

INSERT INTO `mns_customer_chat_user` (`id`, `name`, `mail`, `password`, `image`, `info`, `roles`, `last_activity`) VALUES
(1, 'OndrejSimecek', 'info@butapps.com', '669a7edda165d0ac66b2ec9187c4650cb524b2a3', NULL, '{"ip":"86.49.187.190"}', 'ADMIN,OPERATOR', '2017-05-19 15:24:21'),
(2, 'MiraSkoda', 'miraskoda@seznam.cz', '1f82ea75c5cc526729e2d581aeb3aeccfef4407e', NULL, NULL, 'OPERATOR', '0000-00-00 00:00:00'),
(3, 'PepaRybar', 'josef@pronajematrakce.cz', '1f82ea75c5cc526729e2d581aeb3aeccfef4407e', NULL, NULL, 'OPERATOR', '0000-00-00 00:00:00'),
(4, 'Test-1495206122', 'info@butapps.com', 'x', NULL, '{"ip":"86.49.187.190","referer":"http://www.onlineatrakce.cz/","userAgent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36","browserName":"chrome","browserVersion":"58.0","os":"windows","engine":"webkit","language":"en","geoloc":{"countryCode":"CZ","countryName":"Czech Republic","regionCode":"KR","regionName":"Kralovehradecky kraj","city":"Jičín","zipCode":"506 01","timeZone":"Europe/Prague","latitude":50.4358,"longitude":15.3556,"metroCode":null,"utcOffset":-120}}', 'GUEST', '2017-05-19 15:04:52');

-- --------------------------------------------------------

--
-- Struktura tabulky `mns_customer_chat_user_department`
--

CREATE TABLE IF NOT EXISTS `mns_customer_chat_user_department` (
  `user_id` int(11) NOT NULL,
  `department_id` smallint(5) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `Page`
--

CREATE TABLE IF NOT EXISTS `Page` (
  `pageId` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(400) NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `allowIndex` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `Page`
--

INSERT INTO `Page` (`pageId`, `title`, `description`, `keywords`, `allowIndex`) VALUES
(1, 'Hlavní strana', 'Toto je hlavní strana webu', 'web', 1),
(2, 'Hlavní strana administrace', 'Toto je administrace', 'admin', 0),
(3, 'Přihlášení do administrace', 'Přihlášení do správy webu', 'admin', 0),
(4, 'Správa uživatelů', 'Strana administrace pro správu uživatelů.', 'admin', 0),
(5, 'Správa produktů', 'Strana administrace pro správu produktů.', 'admin', 0),
(6, 'Dev log - info o vývoji', 'Dev log', 'dev', 0),
(7, 'Chat - Foxydesk', 'Konzole pro správu online chatu se zákazníky - Foxydesk.', 'admin, chat, online, foxydesk', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `Product`
--

CREATE TABLE IF NOT EXISTS `Product` (
  `productId` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `keywords` varchar(300) DEFAULT NULL,
  `price` double(10,2) NOT NULL,
  `priceWithVat` double(10,2) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `changed` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `Product`
--

INSERT INTO `Product` (`productId`, `name`, `description`, `keywords`, `price`, `priceWithVat`, `visible`, `created`, `changed`) VALUES
(18, 'Střelnice', '<p style="text-align: center;">Str&aacute;nka s atrakc&iacute;</p>', 'Laserová střelnice', 7500.00, 9075.00, 1, '2017-04-25 23:46:57', '1990-01-01 00:00:00');

-- --------------------------------------------------------

--
-- Struktura tabulky `User`
--

CREATE TABLE IF NOT EXISTS `User` (
  `userId` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `password` varchar(260) NOT NULL,
  `lastLoginDate` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `User`
--

INSERT INTO `User` (`userId`, `name`, `email`, `isAdmin`, `password`, `lastLoginDate`) VALUES
(2, 'Ondra', 'ondrasimecek@seznam.cz', 1, '$2y$10$nV9qAEWrh563e8coyLVEyu9DvZzde54CFu0IlKxGg1uFqdjQ7Cgam', '2017-04-25'),
(10, 'Míra Škoda', 'miraskoda@seznam.cz', 0, '$2y$10$sCfAHMi61SDjgZ.m3ZXtqO9JZeiS10pC3sPGcp9j6/IAI.QSXP8de', '2017-04-25'),
(11, 'Pepa', 'josef@pronajematrakce.cz', 0, '$2y$10$HYZT7ML.OdBM4Ua4D9DXkOnl6agVQ8O0Xlhg5SuMrxOi4smQTJVRO', '2017-04-26');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `Address`
--
ALTER TABLE `Address`
  ADD PRIMARY KEY (`addressId`),
  ADD KEY `fk_address_customer` (`customerId`);

--
-- Klíče pro tabulku `Customer`
--
ALTER TABLE `Customer`
  ADD PRIMARY KEY (`customerId`);

--
-- Klíče pro tabulku `mns_customer_chat_auto_message`
--
ALTER TABLE `mns_customer_chat_auto_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auto_message_state_ix` (`state`);

--
-- Klíče pro tabulku `mns_customer_chat_data`
--
ALTER TABLE `mns_customer_chat_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_type_ix` (`type`);

--
-- Klíče pro tabulku `mns_customer_chat_department`
--
ALTER TABLE `mns_customer_chat_department`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `mns_customer_chat_message`
--
ALTER TABLE `mns_customer_chat_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `message_fk_talk` (`talk_id`),
  ADD KEY `message_from_id_ix` (`from_id`),
  ADD KEY `message_to_id_ix` (`to_id`),
  ADD KEY `message_datetime_ix` (`datetime`);

--
-- Klíče pro tabulku `mns_customer_chat_shared_file`
--
ALTER TABLE `mns_customer_chat_shared_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shared_file_fk_upload` (`upload_id`);

--
-- Klíče pro tabulku `mns_customer_chat_stats`
--
ALTER TABLE `mns_customer_chat_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stats_type_ix` (`type`),
  ADD KEY `stats_datetime_ix` (`datetime`);

--
-- Klíče pro tabulku `mns_customer_chat_talk`
--
ALTER TABLE `mns_customer_chat_talk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `talk_fk_department` (`department_id`),
  ADD KEY `talk_owner_ix` (`owner`),
  ADD KEY `talk_last_activity_ix` (`last_activity`);

--
-- Klíče pro tabulku `mns_customer_chat_upload`
--
ALTER TABLE `mns_customer_chat_upload`
  ADD PRIMARY KEY (`id`),
  ADD KEY `upload_fk_message` (`message_id`);

--
-- Klíče pro tabulku `mns_customer_chat_user`
--
ALTER TABLE `mns_customer_chat_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_mail_ix` (`mail`),
  ADD KEY `user_last_activity_ix` (`last_activity`);

--
-- Klíče pro tabulku `mns_customer_chat_user_department`
--
ALTER TABLE `mns_customer_chat_user_department`
  ADD UNIQUE KEY `user_department_uq` (`user_id`,`department_id`),
  ADD KEY `user_department_fk_department` (`department_id`);

--
-- Klíče pro tabulku `Page`
--
ALTER TABLE `Page`
  ADD PRIMARY KEY (`pageId`);

--
-- Klíče pro tabulku `Product`
--
ALTER TABLE `Product`
  ADD PRIMARY KEY (`productId`);

--
-- Klíče pro tabulku `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`userId`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `Address`
--
ALTER TABLE `Address`
  MODIFY `addressId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `Customer`
--
ALTER TABLE `Customer`
  MODIFY `customerId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `mns_customer_chat_auto_message`
--
ALTER TABLE `mns_customer_chat_auto_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `mns_customer_chat_data`
--
ALTER TABLE `mns_customer_chat_data`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `mns_customer_chat_department`
--
ALTER TABLE `mns_customer_chat_department`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `mns_customer_chat_message`
--
ALTER TABLE `mns_customer_chat_message`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pro tabulku `mns_customer_chat_shared_file`
--
ALTER TABLE `mns_customer_chat_shared_file`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `mns_customer_chat_stats`
--
ALTER TABLE `mns_customer_chat_stats`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pro tabulku `mns_customer_chat_talk`
--
ALTER TABLE `mns_customer_chat_talk`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pro tabulku `mns_customer_chat_upload`
--
ALTER TABLE `mns_customer_chat_upload`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `mns_customer_chat_user`
--
ALTER TABLE `mns_customer_chat_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pro tabulku `Page`
--
ALTER TABLE `Page`
  MODIFY `pageId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pro tabulku `Product`
--
ALTER TABLE `Product`
  MODIFY `productId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT pro tabulku `User`
--
ALTER TABLE `User`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `Address`
--
ALTER TABLE `Address`
  ADD CONSTRAINT `fk_address_customer` FOREIGN KEY (`customerId`) REFERENCES `Customer` (`customerId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `mns_customer_chat_message`
--
ALTER TABLE `mns_customer_chat_message`
  ADD CONSTRAINT `message_fk_talk` FOREIGN KEY (`talk_id`) REFERENCES `mns_customer_chat_talk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `mns_customer_chat_shared_file`
--
ALTER TABLE `mns_customer_chat_shared_file`
  ADD CONSTRAINT `shared_file_fk_upload` FOREIGN KEY (`upload_id`) REFERENCES `mns_customer_chat_upload` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `mns_customer_chat_talk`
--
ALTER TABLE `mns_customer_chat_talk`
  ADD CONSTRAINT `talk_fk_department` FOREIGN KEY (`department_id`) REFERENCES `mns_customer_chat_department` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Omezení pro tabulku `mns_customer_chat_upload`
--
ALTER TABLE `mns_customer_chat_upload`
  ADD CONSTRAINT `upload_fk_message` FOREIGN KEY (`message_id`) REFERENCES `mns_customer_chat_message` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `mns_customer_chat_user_department`
--
ALTER TABLE `mns_customer_chat_user_department`
  ADD CONSTRAINT `user_department_fk_department` FOREIGN KEY (`department_id`) REFERENCES `mns_customer_chat_department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_department_fk_user` FOREIGN KEY (`user_id`) REFERENCES `mns_customer_chat_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
