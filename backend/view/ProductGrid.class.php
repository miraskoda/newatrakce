<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 23.03.2017
 * Time: 19:01
 */

namespace backend\view;

use backend\controllers\CategoryController;
use backend\models\Category;
use backend\models\CustomCurrency;
use backend\models\Product;
use backend\models\ProductCategory;
use backend\models\ProductImage;
use backend\models\Url;
use backend\models\Video;

class ProductGrid
{
    const PRODUCT_GRID_LAYOUT = 'grid';
    const PRODUCT_TABLE_LAYOUT = 'table';

    private static $newProductHtml = '<h2>Nový produkt</h2>
                                        <i class="ti-plus"></i>';
    private static $emptyCellHtml = '<p>Nenalezeny žádné výsledky</p>';
    private static $errorHtml = '<p>Nepodařilo se načíst produkty</p>';
    private static $layout = self::PRODUCT_GRID_LAYOUT;

    /**
     *  Get layout from GET
     */
    public static function setLayout()
    {
        if (isset($_GET['layout'])) {
            self::$layout = $_GET['layout'];
            if (self::$layout == self::PRODUCT_GRID_LAYOUT)
                $_SESSION['product-layout'] = self::PRODUCT_GRID_LAYOUT;
            else if (self::$layout == self::PRODUCT_TABLE_LAYOUT)
                $_SESSION['product-layout'] = self::PRODUCT_TABLE_LAYOUT;
            else {
                $_SESSION['product-layout'] = self::PRODUCT_GRID_LAYOUT;
                self::$layout = self::PRODUCT_GRID_LAYOUT;
            }
        }
    }

    /**
     * @param $products
     * @return string
     */
    public static function generateProductGrid($products)
    {
        //get layout from session
        self::$layout = self::PRODUCT_GRID_LAYOUT;
        if (isset($_SESSION['product-layout'])) {
            if ($_SESSION['product-layout'] == self::PRODUCT_TABLE_LAYOUT)
                self::$layout = self::PRODUCT_TABLE_LAYOUT;
        } else {
            $_SESSION['product-layout'] = self::PRODUCT_GRID_LAYOUT;
        }

        $html = "";

        //add new product button
        if (self::$layout == self::PRODUCT_GRID_LAYOUT)
            $html .= self::wrapGridCell(self::$newProductHtml, ['first-box', 'create-trigger']);
        else
            $html .= self::wrapTableCell(self::$newProductHtml, ['first-box', 'create-trigger']);

        if (self::$layout == self::PRODUCT_GRID_LAYOUT)
            $html .= '<div class="col-md-9 first hidden" id="form-box">';
        else
            $html .= '<div class="col-md-12 first hidden" id="form-box">';
        $html .= '<div class="box animated fadeInLeft text-center">
                        <h2>Nový produkt</h2>';
        $html .= file_get_contents(Url::getBackendPathTo('/admin/modules/page-parts/product-form.php'));
        $html .= '</div>
                </div>';

        //generate products
        if (is_array($products)) {
            if (count($products) > 0) {
                foreach ($products as $key => $product) {
                    if (self::$layout == self::PRODUCT_GRID_LAYOUT)
                        $html .= self::generateProductCell($product, false, self::PRODUCT_GRID_LAYOUT);
                    else
                        $html .= self::generateProductCell($product, false, self::PRODUCT_TABLE_LAYOUT);
                }
            } else {
                if (self::$layout == self::PRODUCT_GRID_LAYOUT)
                    $html .= self::wrapGridCell(self::$emptyCellHtml);
                else
                    $html .= self::wrapTableCell(self::$emptyCellHtml);
            }
        } else {
            if (self::$layout == self::PRODUCT_GRID_LAYOUT)
                $html .= self::wrapGridCell(self::$errorHtml);
            else
                $html .= self::wrapTableCell(self::$errorHtml);
        }

        //finally wrap everything together
        if (self::$layout == self::PRODUCT_GRID_LAYOUT)
            $html = '<div class="row admin-grid"">' . $html . '</div>';
        else
            $html = '<div class="row admin-table" ">' . $html . '</div>';

        return $html;
    }

    /**
     * Creates cell for grid layout
     * @param $content
     * @param array $classes
     * @param string $id
     * @return string
     */
    private static function wrapGridCell($content, $classes = [], $id = "")
    {
        $html = '<div class="col-md-3 data-cell ' . implode(' ', $classes) . '" id="' . $id . '">
                    <div class="box text-center">';

        $html .= $content;

        $html .= '</div>
                </div>';

        return $html;
    }

    /**
     * @param $content
     * @param array $classes
     * @param string $id
     * @return string
     */
    private static function wrapGridDetailCell($content, $classes = [], $id = "")
    {
        $html = '<div class="col-md-6 data-cell ' . implode(' ', $classes) . '" id="' . $id . '">
                    <div class="box text-center">';

        $html .= $content;

        $html .= '</div>
                </div>';

        return $html;
    }

    /**
     * Creates cell for table layout (full width lines)
     * @param $content
     * @param array $classes
     * @param string $id
     * @return string
     */
    private static function wrapTableCell($content, $classes = [], $id = "")
    {
        $html = '<div class="col-md-12 data-cell ' . implode(' ', $classes) . '" id="' . $id . '">
                    <div class="box text-center">';

        $html .= $content;

        $html .= '</div>
                </div>';

        return $html;
    }

    private static function wrapTableDetailCell($content, $classes = [], $id = "")
    {
        $html = '<div class="col-md-12 data-cell ' . implode(' ', $classes) . '" id="' . $id . '">
                    <div class="box text-center">';

        $html .= $content;

        $html .= '</div>
                </div>';

        return $html;
    }

    /**
     * @param Product $product
     * @param bool $autoLayout
     * @param string $layout
     * @return string
     */
    public static function generateProductCell(Product $product, $autoLayout = true, $layout = 'grid')
    {
        if ($autoLayout) {
            $layout = self::PRODUCT_GRID_LAYOUT;
            if (isset($_SESSION['product-layout'])) {
                if ($_SESSION['product-layout'] == self::PRODUCT_TABLE_LAYOUT)
                    $layout = self::PRODUCT_TABLE_LAYOUT;
            } else {
                $_SESSION['product-layout'] = self::PRODUCT_GRID_LAYOUT;
            }
        }

        $additionsCategory = CategoryController::getCategoryBySlug('doplnky');
        $isProductAddition = ProductCategory::isProductInCategory($additionsCategory->getCategoryId(), $product->getProductId());

        if ($layout == self::PRODUCT_GRID_LAYOUT) {
            $html = '<div class="action-alert alert alert-danger hidden animated bounceIn"></div>
                <span class="product-visible animated bounce" data-toggle="tooltip" data-placement="right" title="' . (($product->getVisible()) ? 'Je publikován' : 'Není publikován') . '"><i class="' . (($product->getVisible()) ? 'ti-world' : 'ti-lock') . '"></i><br>ID: ' . $product->getProductId() . '</span>
                <span class="product-image animated bounce" style="float:right;" data-toggle="tooltip" data-placement="right" title="Počet obrázků produktu"><i class="ti-image"></i><br>' . ProductImage::getProductImagesCount($product->getProductId()) . '</span>
                ' . (($isProductAddition) ? '<span class="product-addition animated bounce pull-right" style="float:right;" data-toggle="tooltip" data-placement="right" title="Je doplněk"><i class="ti-clip"></i></span>' : '') . '
                </h class="product-name"><strong><a href="https://www.onlineatrakce.cz/produkty/'.$product->getNameSlug().'/" target="_blank">' . $product->getName() . '</a> </strong> <i style="float: right"> '.$product->getChanged().' </i></h4>
                <p class="product-price"><strong>Cena: ' . Product::convertToCurrency($product->getPrice()) . '</strong></p>
                <p class="product-priceWithVat"><strong>Cena s DPH: ' . Product::convertToCurrency($product->getPriceWithVat()) . '</strong></p>
                <div>
                    <button type="button" class="btn btn-primary update-product"><i class="ti-pencil"></i> Upravit</button>
                    <button type="button" class="btn btn-warning duplicate-product"><i class="ti-layers"></i> Duplikovat</button>
                    <button type="button" class="btn btn-danger delete-product"><i class="ti-trash"></i> Smazat</button>
                </div>';
            $html = self::wrapGridCell($html, [], $product->getProductId());
        } else {
            $html = '<div class="row" style="min-width: 600px;">
                 
                    <span class="col-md-4"><h4 class="product-name"><strong><a href="https://www.onlineatrakce.cz/produkty/'.$product->getNameSlug().'/" target="_blank" >' . $product->getName() . '</a> </strong> <i style="float: right"> '.$product->getChanged().' </i></h4></span>
                    
                   
                    <span class="col-md-2"><p class="product-price"><strong>Cena: ' . Product::convertToCurrency($product->getPrice()) . '</strong></p></span>           
                    <span class="col-md-2"><p class="product-priceWithVat"><strong>Cena s DPH: ' . Product::convertToCurrency($product->getPriceWithVat()) . '</strong></p></span>
                    
                        <span class="col-md-1 product-visible animated bounce" data-toggle="tooltip" data-placement="right" title="' . (($product->getVisible()) ? 'Je publikován' : 'Není publikován') . '"><i class="' . (($product->getVisible()) ? 'ti-world' : 'ti-lock') . '"></i><br>ID: ' . $product->getProductId() . '</span>
                    <span class="col-md-1 product-visible animated bounce">
                        <span class="row">
                            <span class="col-md-6" data-toggle="tooltip" data-placement="right" title="Počet obrázků produktu">
                                <i class="ti-image"></i>
                                <br>' . ProductImage::getProductImagesCount($product->getProductId()) . '
                            </span>
                            <span class="col-md-6">
                                ' . (($isProductAddition) ? '<span class="product-visible" data-toggle="tooltip" data-placement="right" title="Je doplněk"><i class="ti-clip"></i></span>' : '') . '
                            </span>
                        </span>
                    </span>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-primary update-product"><i class="ti-pencil"></i></button>
                        <button type="button" class="btn btn-warning duplicate-product"><i class="ti-layers"></i></button>
                        <button type="button" class="btn btn-danger delete-product"><i class="ti-trash"></i></button>
                        
                        
                    </div>
                    
                </div>';
            $html = self::wrapTableCell($html, [], $product->getProductId());
        }

        return $html;
    }

    /**
     * @param Product $product
     * @param bool $autoLayout
     * @param string $layout
     * @return string
     */
    public static function generateProductDetailCell(Product $product, $autoLayout = true, $layout = 'grid')
    {
        if ($autoLayout) {
            $layout = self::PRODUCT_GRID_LAYOUT;
            if (isset($_SESSION['product-layout'])) {
                if ($_SESSION['product-layout'] == self::PRODUCT_TABLE_LAYOUT)
                    $layout = self::PRODUCT_TABLE_LAYOUT;
            } else {
                $_SESSION['product-layout'] = self::PRODUCT_GRID_LAYOUT;
            }
        }

        $html = '<div class="action-alert alert alert-danger hidden animated bounceIn"></div>
                <span class="product-visible animated bounce" data-toggle="tooltip" data-placement="right" title="' . (($product->getVisible()) ? 'Je publikován' : 'Není publikován') . '"><i class="' . (($product->getVisible()) ? 'ti-world' : 'ti-lock') . '"></i><br>ID: ' . $product->getProductId() . '</span>
                <div class="row">
                    <div class="col-md-8">
                        <h4><strong>' . $product->getName() . '</strong></h4>
                        <div>' . $product->getDescription() . '</div>
                    </div>
                    <div class="col-md-4">
                        <p>Cena: ' . Product::convertToCurrency($product->getPrice()) . '</strong></p>
                        <p>Cena s DPH: ' . Product::convertToCurrency($product->getPriceWithVat()) . '</strong></p>
                        <p>Klíčová slova: ' . $product->getKeywords() . '</p>
                        <div>
                            <button type="button" class="btn btn-success cancel-detail-product"><i class="ti-search"></i> Zpět</button>
                        </div>
                    </div>
                </div>';
        if ($layout == self::PRODUCT_GRID_LAYOUT)
            $html = self::wrapGridDetailCell($html, ['product-detail'], $product->getProductId());
        else
            $html = self::wrapTableDetailCell($html, ['product-detail'], $product->getProductId());

        return $html;
    }

    // PUBLIC WEB

    /**
     * @param array $products
     * @param int $colSize
     * @param int $colSMSize
     * @param int $colXSSize
     * @return string
     */
    public static function generatePublicProductGrid(array $products, $colSize = 3, $colSMSize = 6, $colXSSize = 12) {
        $html = '';

        if(count($products) < 1) {
            $html .= '<div class="col-md-12 text-center">
                    <h3>Omlouváme se. Pro toto vyhledávání jsme nenašli žádné výsledky.</h3>
                    <p>Zkuste použít jiné klíčové slovo nebo přeformulovat zadání.</p>
                    <a href="/nase-produkty/" class="btn btn-primary">Prohlédnout si celou nabídku produktů</a>
                </div>';
        }

        foreach ($products as $product) {
            $productImage = ProductImage::getProductImages($product->getProductId(), ProductImage::TYPE_MAIN);
            $productVideo = Video::getProductVideo($product->getProductId());
            $html .= '<div class="item-box col-lg-3 col-xs-6 category-border"';

                                    if(is_array($productVideo)) {

                                        $html .= ' ontouchstart="myFunction(\''.$productVideo["hash"].'\',\'swap'.$product->getProductId().'\')" 
                                        onmouseenter="myFunction(\''.$productVideo["hash"].'\',\'swap'.$product->getProductId().'\')" ';
                                    }


            $html .= 'product-id="' . $product->getProductId() . '">
                        <div class="images">                                
                            <a href="/produkty/' . $product->getNameSlug() . '/">';
                                    if(is_array($productVideo)) {
                                        $html .= '<div style="width: 100%" id="swap'.$product->getProductId(). '"><img src = "https://img.youtube.com/vi/'. $productVideo["hash"].'/hqdefault.jpg"></div>';


                                    } else {
                                        $html .= ((count($productImage) > 0) ? '<img src = "/assets/images/products/' . $productImage[0]['name'] . '" > ' : '<img src = "/images/products/rocking-horse/rh1.jpg" > ');
                                    }
                        $html .= '</a>
                        </div>
                        <div class="details">
                            <div class="text">                                
                                <a href="/produkty/' . $product->getNameSlug() . '/">
                                    <h4><b>' . $product->getName() . '</b></h4>
                                    <p>' . CustomCurrency::setCustomCurrencyWithoutDecimals($product->getPrice()) . '</p>
                                    <p class="green"><span class="ti-check-box"></span> Aktuálně volné</p>
                                </a>
                            </div>
                        </div>
                    </div>';
        }

        return $html;
    }


    public static function generatePublicProductGridMain(array $products, $colSize = 3, $colSMSize = 6, $colXSSize = 12) {
        $html = '';

        if(count($products) < 1) {
            $html .= '<div class="col-md-12 text-center">
                    <h3>Omlouváme se. Pro toto vyhledávání jsme nenašli žádné výsledky.</h3>
                    <p>Zkuste použít jiné klíčové slovo nebo přeformulovat zadání.</p>
                    <a href="/nase-produkty/" class="btn btn-primary">Prohlédnout si celou nabídku produktů</a>
                </div>';
        }

        foreach ($products as $product) {
            $productImage = ProductImage::getProductImages($product->getProductId(), ProductImage::TYPE_MAIN);
            $productVideo = Video::getProductVideo($product->getProductId());

            $html .= '<div class="item-box carousel-celll"';

                                        if(is_array($productVideo)) {
                                        $html .= 'ontouchstart="myFunction(\''.$productVideo["hash"].'\',\'swap'.$product->getProductId().'\')" 
                                       onmouseenter="myFunction(\''.$productVideo["hash"].'\',\'swap'.$product->getProductId().'\')" ';
                                    }

            $html .= 'product-id="' . $product->getProductId() . '">
                        <div class="images">                                
                            <a href="/produkty/' . $product->getNameSlug() . '/">';
            if(is_array($productVideo)) {
                $html .= '<div style="width: 100%" id="swap'.$product->getProductId(). '"><img src = "https://img.youtube.com/vi/'. $productVideo["hash"].'/hqdefault.jpg"></div>';
            } else {
                $html .= ((count($productImage) > 0) ? '<img src = "/assets/images/products/' . $productImage[0]['name'] . '" > ' : '<img src = "/images/products/rocking-horse/rh1.jpg" > ');
            }
            $html .= '</a>
                        </div>
                        <div class="details">
                            <div class="text paddingl">                                
                                <a href="/produkty/' . $product->getNameSlug() . '/">
                                    <h4><b>' . $product->getName() . '</b></h4>
                                    <p>' . CustomCurrency::setCustomCurrencyWithoutDecimals($product->getPrice()) . '</p>
                                    <p class="green"><span class="ti-check-box"></span> Aktuálně volné</p>
                                </a>
                            </div>
                        </div>
                    </div>';
        }

        return $html;
    }


    public static function generatePublicProductGridMainProduct(array $products) {
        $html = '';

        if(count($products) < 1) {
            $html .= '<div class="col-md-12 text-center">
                    <h3>Omlouváme se. Pro toto vyhledávání jsme nenašli žádné výsledky.</h3>
                    <p>Zkuste použít jiné klíčové slovo nebo přeformulovat zadání.</p>
                    <a href="/nase-produkty/" class="btn btn-primary">Prohlédnout si celou nabídku produktů</a>
                </div>';
        }

        foreach ($products as $product) {

            $productImage = ProductImage::getProductImages($product->getProductId(), ProductImage::TYPE_MAIN);
            $productVideo = Video::getProductVideo($product->getProductId());

            $html .= '<div class="item-box carousel-celll"';

            if(is_array($productVideo)) {
                $html .= 'ontouchstart="myFunction(\''.$productVideo["hash"].'\',\'swap'.$product->getProductId().'\')" 
                                       onmouseenter="myFunction(\''.$productVideo["hash"].'\',\'swap'.$product->getProductId().'\')" ';
            }

            $html .= 'product-id="' . $product->getProductId() . '">
                        <div class="images">                                
                            <a href="/produkty/' . $product->getNameSlug() . '/">';
            if(is_array($productVideo)) {
                $html .= '<div style="width: 100%" id="swap'.$product->getProductId(). '"><img src = "https://img.youtube.com/vi/'. $productVideo["hash"].'/hqdefault.jpg"></div>';
            } else {
                $html .= ((count($productImage) > 0) ? '<img src = "/assets/images/products/' . $productImage[0]['name'] . '" > ' : '<img src = "/images/products/rocking-horse/rh1.jpg" > ');
            }
            $html .= '</a>
                        </div>
                        <div class="details">
                            <div class="text paddingl">                                
                                <a href="/produkty/' . $product->getNameSlug() . '/">
                                    <h4><b>' . $product->getName() . '</b></h4>
                                    <p>' . CustomCurrency::setCustomCurrencyWithoutDecimals($product->getPriceWithVat()) . '</p>
                                    <p class="green"><span class="ti-check-box"></span> Aktuálně volné</p>
                                </a>
                            </div>
                        </div>
                    </div>';
        }

        return $html;
    }

    /**
     * @param array $products
     * @return string
     */
    public static function generatePublicProductList(array $products) {
        $html = '';

        if(count($products) < 1) {
            $html .= '<div class="col-md-12 text-center">
                    <h3>Omlouváme se. Pro toto vyhledávání jsme nenašli žádné výsledky.</h3>
                    <p>Zkuste použít jiné klíčové slovo nebo přeformulovat zadání.</p>
                    <a href="/nase-produkty/" class="btn btn-primary">Prohlédnout si celou nabídku produktů</a>
                </div>';
        }

        foreach ($products as $product) {
            $productImage = ProductImage::getProductImages($product->getProductId(), ProductImage::TYPE_MAIN);
            $productImageOther = ProductImage::getProductImages($product->getProductId(), ProductImage::TYPE_OTHER);
            $productVideo = Video::getProductVideo($product->getProductId());

            $html .= '<div class="col-md-12">
                    <div class="row item-box big"';
                    
                                        if(is_array($productVideo)) {

                                        $html .= ' ontouchstart="myFunction(\''.$productVideo["hash"].'\',\'swap'.$product->getProductId().'\')" 
                                        onmouseenter="myFunction(\''.$productVideo["hash"].'\',\'swap'.$product->getProductId().'\')" ';
                                    }


                    $html .= ' product-id="' . $product->getProductId() . '">
                        <div class="col-md-4">
                            <div class="images">
                                <a href="/produkty/' . $product->getNameSlug() . '/">';
                                    if(is_array($productVideo)) {
                                        $html .= '<div style="width: 100%" id="swap'.$product->getProductId(). '"><img src = "https://img.youtube.com/vi/'. $productVideo["hash"].'/hqdefault.jpg"></div>';
                                    } else {
                                        $html .= (((count($productImage) > 0) ? '<img src = "/assets/images/products/' . $productImage[0]['name'] . '" > ' : '<img src = "/images/products/rocking-horse/rh1.jpg" > ') .
                                        ((count($productImageOther) > 0) ? '<img src = "/assets/images/products/' . $productImageOther[0]['name'] . '" class="image-swap" > ' : '<img src = "/images/products/rocking-horse/rh1.jpg" > '));
                                    }
                        $html .= '</a>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="details">
                                <div class="wide">                                
                                    <a href="/produkty/' . $product->getNameSlug() . '/">
                                        <h4> <b>' . $product->getName() . '</b></h4>
                                        <p>' . CustomCurrency::setCustomCurrencyWithoutDecimals($product->getPrice()) . '</p>
                                        <p class="green"><span class="ti-check-box"></span> Aktuálně volné</p>
                                        <p class="description">' . $product->getDescriptionSlug() . '</p>
                                    </a>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>';
        }

        return $html;
    }
}