<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 22.05.2017
 * Time: 14:29
 */

namespace backend\view;

use backend\controllers\ProductCategoryController;
use backend\models\Category;
use backend\models\Product;
use backend\models\ProductCategory;
use backend\models\Url;

class CategoryList
{
    public static function generateCategoryList($isPublic = "admin") {
        $categories = Category::getCategories();
        switch ($isPublic){
            case "admin":
                if (is_array($categories))
                    return CategoryList::getListOfCategories($categories);
                else
                    return CategoryList::getEmptyList();
                break;
            case "public-list":
                if(is_array($categories))
                    return CategoryList::getPublicListOfCategories($categories);
                break;
            case "public-line":
                if(is_array($categories))
                    return CategoryList::getPublicLineOfCategories($categories);
                break;
        }

        return null;
    }

    /**
     * @param array $categories
     * @return string
     */
    private static function getListOfCategories($categories) {
        $htmlList = "";

        $htmlList .= "<ul>";
        foreach ($categories as $category) {
            $subCategories = Category::getCategories($category->getCategoryId());

            $htmlList .= "<li id='" . $category->getCategoryId() . "'>
                <div>
                    <p><span class='ti-control-record'></span> " . $category->getName() . "</p>
                    <button type=\"button\" class=\"btn btn-danger delete-category\"><i class=\"ti-trash\"></i></button>
                    <button type=\"button\" class=\"btn btn-primary update-category\"><i class=\"ti-pencil\"></i></button>
                    <button type=\"button\" class=\"btn btn-success list-category\"><i class=\"ti-menu-alt\"></i></button>";
            if((!is_array($subCategories) || empty($subCategories)) && $category->getParentCategoryId() == 0)
                $htmlList .= "<button type=\"button\" class=\"btn btn-success add-sub-category\"><i class=\"ti-plus\"></i></button>";
            $htmlList .= "</div>
            </li>";
            if(is_array($subCategories))
                $htmlList .= "<li>" . CategoryList::getListOfCategories($subCategories) . "</li>";
        }
        if(is_array($categories) && !empty($categories) && is_a($categories[0], Category::class)) {
            $htmlList .= self::addPlusLi($categories[0]->getParentCategoryId());
        }
        $htmlList .= "</ul>";

        return $htmlList;
    }

    private static function getEmptyList() {
        $htmlList = "<ul>";
        $htmlList .= CategoryList::addPlusLi();
        $htmlList .= "</ul>";

        return $htmlList;
    }

    private static function addPlusLi($parentCategoryId = "") {
        return "
        <li class='new-category'>
            <p><span class='ti-plus'></span> Přidat novou kategorii</p>
            <div style='display: none'>
                <form class='new-category-form' method='post'>
                    <div class=\"form-group required\">
                    <label for=\"name\" class=\"control-label\">Název</label>
                    <input name=\"name\" class=\"form-control\" id=\"name\" placeholder=\"Název kategorie ...\"
                           required>
                    <input type='hidden' name='parentCategoryId' value='" . $parentCategoryId . "'>
                    <input type=\"submit\" class=\"btn btn-primary\" value=\"Přidat\">
                    <input type=\"button\" class=\"btn btn-danger\" value=\"Zrušit\">
                </div>
                </form>
            </div>
        </li>";
    }

    // PUBLIC WEB

    private static function getPublicListOfCategories(array $categories) {
        $htmlList = "<ul class=\"nav navbar-nav\">";
        foreach ($categories as $category) {
            $htmlList .= "<li class=\"active\" id=\"" . $category->getCategoryNameSlug() . "\">
                        <a class=\"dropdown-toggle\" data-target=\"#" . $category->getCategoryNameSlug() . "\" href=\"/kategorie/" . $category->getCategoryNameSlug() . "/\">" . $category->getName() . "<span class=\"ti-arrow-circle-down\"></span></a>";
            $subCategories = Category::getCategories($category->getCategoryId());

            if(is_array($subCategories) && !empty($subCategories)) {
                $htmlList .= "<ul class=\"dropdown-menu\" role=\"menu\">";
                // all subcategories
                foreach ($subCategories as $subCategory) {

                    $categoryName = $subCategory->getName();

                    $categoryProducts = ProductCategory::getProductCategories(0, $subCategory->getCategoryId(), 'product');
                    $productListHtml = '<ul>';
                    foreach ($categoryProducts as $product) {
                        $productListHtml .= '<li><a href="/produkty/' . $product->getNameSlug() . '">' . $product->getName() . '</a></li>';
                    }

                    $htmlList .= "<li data-submenu-id=\"submenu-" . Url::nameToUrl($categoryName) . "\">
                                    <a href=\"/kategorie/" . $subCategory->getCategoryNameSlug() . "/\">" . $categoryName . "</a>
                                    </li>";
                }
                $htmlList .= "</ul>";
            }
            $htmlList .= "</li>";
        }
        $htmlList .= "</ul>";

        return $htmlList;
    }

    private static function getPublicLineOfCategories(array $categories) {
        $htmlList = "";
        foreach ($categories as $category) {
            $htmlList .= "<div class=\"col-md-12 animated slideInDown\">
                            <h4>" . $category->getName() . "</h4>
                        </div>";
            $subCategories = Category::getCategories($category->getCategoryId());

            if(is_array($subCategories) && !empty($subCategories)) {
                $htmlList .= "<div class=\"col-md-12\">
                        <div class=\"row\">";
                foreach ($subCategories as $subCategory) {
                    $categoryName = $subCategory->getName();
                    $htmlList .= "<div class=\"col-md-3 scroll-anim animated zoomInUp\">
                                    <div>
                                        <a href='/kategorie/" . $subCategory->getCategoryNameSlug() . "/'><h5>" . $categoryName . "</h5></a>
                        </div>
                    </div>";
                }
                $htmlList .= "</div>
                    </div>";
            }
        }

        return $htmlList;
    }
}