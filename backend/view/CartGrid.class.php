<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 17.08.2017
 * Time: 12:41
 */

namespace backend\view;


use backend\controllers\CartController;
use backend\controllers\CustomerController;
use backend\models\Cart;
use backend\models\CustomCurrency;
use backend\models\Customer;
use backend\models\CustomerGroup;
use backend\models\Order;
use backend\models\OrderProduct;
use backend\models\OrderTerm;
use backend\models\ProductAddition;
use backend\models\Product;

class CartGrid
{
    /**
     * @param $rentHours
     * @param bool $isSummary
     * @param null $products
     * @param int $colSize
     * @param int $offsetSize
     * @param bool $isOrder
     * @param int $order
     * @return string
     */
    public static function generateProductsList($rentHours, $isSummary = false, $products = null, $colSize = 8, $offsetSize = 2, $isOrder = false, $order = 0)
    {
        if (is_null($products))
            $products = CartController::getCartProducts(false);

        $numOfTerms = count(array_filter($rentHours, function ($x) {
            return ($x > 0);
        }));
        if ($numOfTerms < 1)
            $numOfTerms = 1;
        else if ($numOfTerms > 5)
            $numOfTerms = 5;

        if (is_array($products) && count($products) > 0) {
            $html = '';
            $html .= '<div class="col-md-' . $colSize . ' col-md-offset-' . $offsetSize . ' animated">
                        <div class="row header">
                            <div class="col-md-2"></div>
                            <div class="col-md-' . (($isSummary) ? 5 : 4) . '"><p>Název produktu</p></div>
                            <div class="col-md-2"><p>Základní cena bez DPH</p></div>
                            <div class="col-md-1"><p>Množství</p></div>
                            <div class="col-md-2"><p>Celková cena bez DPH</p></div>
                            ' . (($isSummary) ? '' : '<div class="col-md-1"></div>') . '
                        </div>
                    </div>';

            foreach ($products as $index => $product) {
                // calculating sum
                $sumPrice = 0;
                foreach ($rentHours as $rentHour) {
                    if ($rentHour > 0) {
                        $sumPrice += (($rentHour <= 5) ?
                            (($product['price']) * $product['quantity'])
                            :
                            ((($product['price']) + (($product['price'] / 100) * (5 * ($rentHour - (($rentHour > 5) ? 5 : $rentHour))))) * $product['quantity'])
                        );
                    }
                }

                $html .= '<div product-id="' . $product['productId'] . '" class="col-md-' . $colSize . ' col-md-offset-' . $offsetSize . ' animated cart-product ' . (($numOfTerms > 1 && $index > 0) ? 'top-border' : '') . '">
                        <div class="row">
                            <div class="col-md-2"><img src="/assets/images/products/' . ((isset($product['image'])) ? $product['image'] : '') . '"></div>
                            <div class="col-md-1"><a href="#" class="btn btn-additional" data-target="' . $product['productId'] . '" data-name="' . $product['name'] . '"><span class="ti-plus"></span></a></div>              
                            <div class="col-md-' . (($isSummary) ? 5 : 4) . '"><p class="productNameParagraph"><a href="/produkty/' . $product['nameSlug'] . '">' . $product['name'] . '</a></p></div>      
                            <div class="col-md-2"><p>' . CustomCurrency::setCustomCurrencyWithoutDecimals($product['price']) . '</p></div>
                            <div class="col-md-1">' . (($isSummary) ? '<p>' . $product['quantity'] . '</p>' : '<input class="form-control" type="number" min="1" step="1" name="quantity" value="' . $product['quantity'] . '">') . '</div>
                            <div class="col-md-2"><p>' . CustomCurrency::setCustomCurrencyWithoutDecimals($sumPrice) . '</p></div>
                            ' . (($isSummary) ? '' : '<div class="col-md-1"><span class="ti-close"></span></div>') . '
                        </div>
                    </div>';

                if ($numOfTerms > 1) {
                    $counter = 1;
                    foreach ($rentHours as $rentHour) {
                        if ($rentHour > 0) {
                            $html .= '<div class="col-md-' . $colSize . ' col-md-offset-' . $offsetSize . ' animated term-info">
                                        <div class="row">
                                            <div class="col-md-' . (($isSummary) ? 5 : 4) . ' col-md-offset-2"><p>' . $counter . '. termín</p></div>
                                            <div class="col-md-2 col-md-offset-3"><p>' .
                                (($rentHour <= 5) ?
                                    CustomCurrency::setCustomCurrencyWithoutDecimals(($product['price']) * $product['quantity'])
                                    :
                                    CustomCurrency::setCustomCurrencyWithoutDecimals((($product['price']) + (($product['price'] / 100) * (5 * ($rentHour - (($rentHour > 5) ? 5 : $rentHour))))) * $product['quantity'])
                                )
                                . '</p></div>
                                        </div>
                                    </div>';
                            $counter++;
                        }
                    }
                }

                if (!$isOrder)
                    $additionalProducts = CartController::getAdditionalCartProducts($product['productId']);
                else
                    $additionalProducts = OrderProduct::getAdditionalOrderProductsToProductId($order, false, $product['productId']);
                if (is_array($additionalProducts) && count($additionalProducts) > 0) {
                    $html .= '<div class="col-md-' . ($colSize - 2) . ' col-md-offset-' . ($offsetSize + 1) . '"><hr><h5>Doplňky k produktu ' . $product['name'] . '</h5></div>';
                    foreach ($additionalProducts as $additionIndex => $additionalProduct) {
                        $sumPrice = 0;
                        foreach ($rentHours as $rentHour) {
                            if ($rentHour > 0) {
                                $sumPrice += (($rentHour <= 5) ?
                                    (($additionalProduct['price']) * $additionalProduct['quantity'])
                                    :
                                    ((($additionalProduct['price']) + (($additionalProduct['price'] / 100) * (5 * ($rentHour - (($rentHour > 5) ? 5 : $rentHour))))) * $additionalProduct['quantity'])
                                );
                            }
                        }

                        $html .= '<div product-id="' . $additionalProduct['productId'] . '" addition-to-product-id="' . $product['productId'] . '" class="col-md-' . ($colSize - 2) . ' col-md-offset-' . ($offsetSize + 1) . ' animated fadeIn additional-cart-product ' . (($numOfTerms > 1 && $additionIndex > 0) ? 'top-border' : '') . '">
                            <div class="row">
                                <div class="col-md-2"><img src="/assets/images/products/' . ((isset($additionalProduct['image'])) ? $additionalProduct['image'] : '') . '"></div>
                                <div class="col-md-' . (($isSummary) ? 5 : 4) . '"><p>' . $additionalProduct['name'] . '</p></div>      
                                <div class="col-md-2"><p>' . CustomCurrency::setCustomCurrencyWithoutDecimals($additionalProduct['price']) . '</p></div>
                                <div class="col-md-1">' . (($isSummary) ? '<p>' . $additionalProduct['quantity'] . '</p>' : '<input class="form-control" type="number" min="1" step="1" name="quantity" value="' . $additionalProduct['quantity'] . '">') . '</div>
                                <div class="col-md-2"><p>' . CustomCurrency::setCustomCurrencyWithoutDecimals($sumPrice) . '</p></div>
                                ' . (($isSummary) ? '' : '<div class="col-md-1"><span class="ti-close"></span></div>') . '
                            </div>
                        </div>';

                        if ($numOfTerms > 1) {
                            $counter = 1;
                            foreach ($rentHours as $rentHour) {
                                if ($rentHour > 0) {
                                    $html .= '<div class="col-md-' . ($colSize - 2) . ' col-md-offset-' . ($offsetSize + 1) . ' animated fadeIn term-info">
                                        <div class="row">
                                            <div class="col-md-' . (($isSummary) ? 5 : 4) . ' col-md-offset-2"><p>' . $counter . '. termín</p></div>
                                            <div class="col-md-2 col-md-offset-3"><p>' .
                                        (($rentHour <= 5) ?
                                            CustomCurrency::setCustomCurrencyWithoutDecimals(($additionalProduct['price']) * $additionalProduct['quantity'])
                                            :
                                            CustomCurrency::setCustomCurrencyWithoutDecimals((($additionalProduct['price']) + (($additionalProduct['price'] / 100) * (5 * ($rentHour - (($rentHour > 5) ? 5 : $rentHour))))) * $additionalProduct['quantity'])
                                        )
                                        . '</p></div>
                                        </div>
                                    </div>';
                                    $counter++;
                                }
                            }
                        }
                    }
                    $html .= '<div class="col-md-' . ($colSize - 2) . ' col-md-offset-' . ($offsetSize + 1) . '"><hr></div>';
                }
            }

            return $html;
        } else {
            return '<div class="col-md-' . $colSize . ' col-md-offset-' . $offsetSize . '"><p>V košíku není žádný produkt</p><a class="back-to-shop btn" href="/">Pokračovat v nákupu</a></div>';
        }
    }

    /**
     * @param $number
     * @return string
     */
    public static function generateDate($number)
    {
        $orderTerm = OrderTerm::getOrderTerm($number);


        $isOrderLoaded = false;
        if (is_a($orderTerm, OrderTerm::class))
            $isOrderLoaded = true;

        $html = '<div class="col-md-12 date-title animated fadeIn"><h5>' . $number . '. termín</h5></div>
                    <div class="col-md-12 date-content animated fadeIn" date-no="' . $number . '">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group required">
                                    <label for="start-' . $number . '" class="control-label">Počátek</label>
                                    <div class="input-group date cart-datetimepicker" id="start-' . $number . '">
                                        <input class="form-control" name="date" placeholder="Vyberte datum a čas ..."
                                               id="start-' . $number . '" date-part="date">
                                        <span class="input-group-addon">
                                            <span class="ti-calendar"></span>
                                        </span>
                                        <script>
                                        $( document ).ready(function() {
                                           $("#start-' . $number . '").data("DateTimePicker").date("' . (($isOrderLoaded) ? ($orderTerm->getStart()) : '') . '") 
                                        })                                                                       
                                        </script>              
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="form-group required">
                                        <!--<div class="col-md-12"><label class="control-label">Trvání</label></div>-->
                                        ' . /*<div class="col-md-6">
                                            <label for="interval-days-' . $number . '" class="control-label">Počet dní</label>
                                            <input class="form-control" type="number" min="0" max="21" step="1"
                                                   value="' . (($isOrderLoaded) ? round($orderTerm->getHours() / 24) : '0') . '"
                                                   placeholder="Zadejte počet dní" id="interval-days-' . $number . '" date-part="day">
                                        </div>*/
            '<div class="col-md-12">
                                            <label for="interval-hours-' . $number . '" class="control-label">Počet hodin</label>
                                            <input id="numberOfHours" oninput="changeValueByNumber(this.value)" class="form-control" type="number" min="0" max="12" step="1"
                                                   value="' . (($isOrderLoaded) ? ($orderTerm->getHours() % 24) : '5') . '"
                                                   placeholder="Zadejte počet hodin" id="interval-hours-' . $number . '" date-part="hour">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12"><p class="date-result"></p></div>
                        </div>
                    </div>';

        return $html;
    }

    public static function generatePublicDates($order = null, $colSize = 8, $offsetSize = 2)
    {
        if (is_null($order))
            $orderTerms = OrderTerm::getOrderTerms();
        else
            $orderTerms = OrderTerm::getOrderTermsByOrder($order);

        $html = '';
        if (is_array($orderTerms) && count($orderTerms) > 0) {
            // headings
            $html .= '<div class="col-md-' . $colSize . ' col-md-offset-' . $offsetSize . '">
                            <div class="row header">
                                <div class="col-md-4"><p>Začátek</p></div>
                                <div class="col-md-4"><p>Konec</p></div>
                                <div class="col-md-4"><p>Počet hodin</p></div>
                            </div>
                        </div>';

            foreach ($orderTerms as $orderTerm) {
                $html .= '<div class="col-md-' . $colSize . ' col-md-offset-' . $offsetSize . '">
                            <div class="row">
                                <div class="col-md-4"><p>' . $orderTerm->getStart() . '</p></div>
                                <div class="col-md-4"><p>' . $orderTerm->getEnd() . '</p></div>
                                <div class="col-md-4"><p>' . $orderTerm->getHours() . ' ' . self::czechHourMaker($orderTerm->getHours()) . '</p></div>
                            </div>
                        </div>';
            }
        }

        return $html;
    }

    /**
     * @param $rentHours
     * @param bool $isSummary
     * @param null $products
     * @param null $additionalProducts
     * @param bool $discountSet
     * @param int $discount
     * @return string
     */
    public static function generatePrice($rentHours, $isSummary = false, $products = null, $additionalProducts = null, $discountSet = false, $discount = 0)
    {
        // add additional products to price
        if ($additionalProducts != null)
            $products = array_merge($products, $additionalProducts);

        $customerGroup = CustomerController::getCustomerGroup();
        $discountAmount = 0;
        if (!$discountSet) {
            if (is_a($customerGroup, CustomerGroup::class)) {
                $discount = $customerGroup->getDiscount();
            }
        }

        $priceWithoutVat = Order::getPriceWithoutVat($rentHours, $products);
        if ($discount > 0) {
            $discountAmount = ($priceWithoutVat / 100) * $discount;
            $priceWithoutVat = round(($priceWithoutVat / 100) * (100 - $discount));
        }

        $priceWithVat = round(($priceWithoutVat / 100) * 121);

        $html = '';

        if ($discount > 0) {
            $html .= '<p>Sleva ' . $discount . '%: -' . CustomCurrency::setCustomCurrencyWithoutDecimals($discountAmount) . '</p>';
        }

        $html .= '<p>Celková cena bez DPH: <strong>' . CustomCurrency::setCustomCurrencyWithoutDecimals($priceWithoutVat) . '</strong></p>
                <p>DPH 21%: <strong>' . CustomCurrency::setCustomCurrencyWithoutDecimals($priceWithVat - $priceWithoutVat) . '</strong></p>
                <p class="final-price"><strong>' . (($isSummary) ? 'Celkem' : 'Celkem k úhradě') . ': ' . CustomCurrency::setCustomCurrencyWithoutDecimals($priceWithVat) . '</strong></p>';

        return $html;
    }

    /**
     * @param $num
     * @return string
     */
    public static function czechHourMaker($num)
    {
        switch ($num) {
            case 1:
                return 'hodina';
                break;
            case 2:
            case 3:
            case 4:
                return 'hodiny';
            default:
                return 'hodin';
                break;
        }
    }

    /**
     * @param $num
     * @return string
     */
    public static function czechMinuteMaker($num)
    {
        switch ($num) {
            case 1:
                return 'minuta';
                break;
            case 2:
            case 3:
            case 4:
                return 'minuty';
            default:
                return 'minut';
                break;
        }
    }

    /**
     * @param $rentHours
     * @return string
     */
    public static function generateAdditionalList($productId = null)
    {
        $product = new Product($productId);
        $product = $product->load();

        $html = null;

        $html .= '<ul class="product-additions">';

        $additionalProducts = ProductAddition::getAdditionalProductsByMainProduct($productId);

        if (count($additionalProducts) > 0) {
            foreach ($additionalProducts as $additionalProduct) {
                $isInCart = new Cart(0, CartController::getCustomerId(), $additionalProduct->getProductId(), $product->getProductId());
                $isInCart = $isInCart->load();
                if (is_a($isInCart, Cart::class))
                    $isInCart = $isInCart->isDuplicate();
                else
                    $isInCart = false;
                $html .= '<li class="checkbox" product-id="' . $additionalProduct->getProductId() . '" addition-to="' . $product->getProductId() . '"><input type="checkbox" ' . (($isInCart) ? 'checked' : '') . '><a href="/produkty/' . $additionalProduct->getNameSlug() . '">' . $additionalProduct->getName() . '</a></li>';
            }
            $html .= '</ul>';

            return $html;
        } else {
            return '<h3>Požadovaný produkt nemá žádné produkty k dokoupení</h3>';
        }
    }
}