<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 20.03.2017
 * Time: 16:42
 */

namespace backend\view;

use backend\models\User;
use backend\models\Validate;

class UserGrid
{
    public static function generateUserGrid()
    {
        $users = User::getUsers();

        $html = "";
        if (is_array($users)) {
            if (count($users) > 0) {
                foreach ($users as $key => $user) {
                    $html .= self::generateUserGridCell($user);
                }
            } else {
                $html .=
                '<div class="col-md-3" >
                    <div class="box text-center animated zoomIn" >
                        <p> Nenalezeny žádné výsledky </p >
                    </div>
                </div>';
            }
        } else {
            //TODO error alert
        }

        return $html;
    }

    public static function generateUserGridCell(User $user)
    {
        return '<div class="col-md-3" id="' . $user->getUserId() . '">
                        <div class="box text-center animated zoomIn">
                            <div class="action-alert alert alert-danger hidden animated bounceIn"></div>
                            <span class="product-visible animated bounce">ID: ' . $user->getUserId() . '</span>' .
                                (((new Validate())->validateNotEmpty([$user->getName()])) ? '<p class="user-name">' . $user->getName() . "</p>" : '') .
                                '<p class="user-email">' . $user->getEmail() . '</p>
                                <div>
                                <button type="button" class="btn btn-primary update-user"><i class="ti-pencil"></i> Upravit</button>
                                <button type="button" class="btn btn-danger delete-user"><i class="ti-trash"></i> Smazat</button>
                            </div>
                        </div>
                    </div>';
    }
}