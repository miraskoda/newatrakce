<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 03.02.2018
 * Time: 19:06
 */

namespace backend\models;


use backend\database\DatabaseError;
use backend\database\MyDB;

class Option
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;

    private $db;
    private $scenario;

    private $optionId;
	private $name;
	private $slug;
    private $description;
    private $value;
    private $public;

	/**
	 * Option constructor.
	 *
	 * @param int $optionId
	 * @param string $name
	 * @param string $slug
	 * @param string $description
	 * @param string $value
	 * @param string $public
	 */
    public function __construct($optionId = 0,  $name = '', $slug = '', $description = '', $value = '', $public = '')
    {
        $this->db = MyDB::getConnection();

        $this->optionId = $optionId;
        $this->name = $name;
        $this->slug = $slug;
        $this->description = $description;
        $this->value = $value;
        $this->public = $public;
    }

    /**
     * Loads Option from DB by optionId or name or slug.
     * @return Option|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
            $optionData = $this->db->queryOne("SELECT * FROM `option` WHERE optionId = ? OR name = ? OR slug = ?", [
                $this->optionId,
                $this->name,
                $this->slug
            ]);

            if (!(new Validate())->validateNotNull([$optionData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst nastavení z DB');

            $this->optionId = $optionData['optionId'];
            $this->name = $optionData['name'];
            $this->slug = $optionData['slug'];
            $this->description = $optionData['description'];
            $this->value = $optionData['value'];
            $this->public = $optionData['public'];

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Inserts new Option to DB.
     * @return Option|string
     */
    public function create()
    {
        $this->scenario = self::SCENARIO_CREATE;

        $this->slug = Product::createSlug($this->name);

        if ($this->validate()) {
            $insert = $this->db->query("INSERT INTO `option` (name, slug, description, value, public) VALUES (?,?,?,?,?)", [
                $this->name,
                $this->slug,
                $this->description,
                $this->value,
                $this->public
            ]);

            if ($insert > 0) {
                $this->optionId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Updates Option by optionId.
     * @return Option|string
     */
    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;

        $this->slug = Product::createSlug($this->name);

        if ($this->validate()) {
            $update = $this->db->query("UPDATE `option` SET name = ?, slug = ?, description = ?, value = ?, public = ? WHERE optionId = ?", [
                $this->name,
                $this->slug,
                $this->description,
                $this->value,
                $this->public,
                $this->optionId
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Deletes Option by optionId.
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->query("DELETE FROM `option` WHERE optionId = ?", [
            $this->optionId
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * @param bool $onlyPublic
     * @return array|string
     */
    public static function getOptions($onlyPublic = true)
    {
        $db = MyDB::getConnection();
        if($onlyPublic)
            $optionData = $db->queryAll("SELECT * FROM `option` WHERE public = 1 ORDER BY name", []);
        else
            $optionData = $db->queryAll("SELECT * FROM `option` ORDER BY name", []);

        if (count($optionData) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$optionData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst nastavení z DB');

        $options = [];
        foreach ($optionData as $option) {
            $options[] = (new Option(
                $option['optionId'],
                $option['name'],
                $option['slug'],
                $option['description'],
                $option['value'],
                $option['public']
            ))->_toArray();
        }

        return $options;
    }

    /**
     * Validates if Option attributes are complete and valid.
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        if ($this->scenario == self::SCENARIO_UPDATE) {
            $validation->validateRequired([
                'optionId' => $this->optionId,
            ]);
            $validation->validateNumeric([
                'optionId' => $this->optionId,
            ]);
        }

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'optionId' => $this->optionId,
                'název' => $this->name
            ]);

            $validation->validateNumeric([
                'optionId' => $this->optionId
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return array
     */
    public function _toArray()
    {
        return [
            'optionId' => $this->optionId,
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'value' => $this->value,
            'public' => $this->public
        ];
    }

    /**
     * @return int
     */
    public function getOptionId()
    {
        return $this->optionId;
    }

    /**
     * @param int $optionId
     */
    public function setOptionId($optionId)
    {
        $this->optionId = $optionId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * @param string $public
     */
    public function setPublic($public)
    {
        $this->public = $public;
    }
}