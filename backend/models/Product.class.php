<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 23.03.2017
 * Time: 13:38
 */

namespace backend\models;

use backend\controllers\CustomerController;
use backend\view\ProductGrid;
use backend\controllers\UserController;
use backend\database\DatabaseError;
use backend\database\MyDB;
use DateTime;

class Product
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_DELETE = 3;
    const SCENARIO_LOAD = 4;

    private $db;
    private $scenario;

    private $productId;
    private $name;
    private $nameSlug;
    private $description;
    private $keywords;
    private $price;
    private $priceWithVat;
    private $quantity;
    private $visible;
    private $created;
    private $changed;
    private $target;

    //These keys have to be equal to database
    /*private static $attributes = [
        'productId' => 'ID produktu',
        'name' => 'Název',
        'description' => 'Popis',
        'keywords' => 'Klíčová slova',
        'price' => 'Cena bez DPH',
        'priceWithVat' => 'Cena s DPH',
        'visible' => 'Publikováno',
        'created' => 'Vytvořeno',
        'changed' => 'Upraveno',
    ];*/

    /**
     * Product constructor.
     * @param int $productId
     * @param string $name
     * @param string $nameSlug
     * @param string $description
     * @param string $target
     * @param string $keywords
     * @param float $price
     * @param float $priceWithVat
     * @param int $quantity
     * @param bool $visible
     * @param string $created
     * @param string $changed
     */
    public function __construct($productId = 0, $name = "", $nameSlug = "", $description = "", $target = "", $keywords = "", $price = 0.00, $priceWithVat = 0.00, $quantity = 0, $visible = false, $created = "1990-01-01", $changed = "1990-01-01")
    {
        $this->db = MyDB::getConnection();
        $this->productId = $productId;
        $this->created = $created;
        $this->changed = $changed;

        $this->nameSlug = $nameSlug;

        $this->setProduct($name, $description, $target, $keywords, $price, $priceWithVat, $quantity, $visible);
    }

    /**
     * @return Product|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
            $productData = $this->db->queryOne("SELECT * FROM product WHERE productId = ? OR nameSlug = ? OR name = ?", [
                $this->productId,
                $this->nameSlug,
                $this->name,
            ]);

            if (!(new Validate())->validateNotNull([$productData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst produkt z DB');

            $this->productId = $productData['productId'];
            $this->created = $productData['created'];
            $this->changed = $productData['changed'];

            $this->nameSlug = $productData['nameSlug'];

            $this->setProduct(
                $productData['name'],
                $productData['description'],
                $productData['target'],
                $productData['keywords'],
                $productData['price'],
                $productData['priceWithVat'],
                $productData['quantity'],
                $productData['visible']
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return Product|string
     */
    public function create()
    {
        $this->scenario = self::SCENARIO_CREATE;
        $this->nameSlug = self::createSlug($this->name);

        if ($this->validate()) {

            $insert = $this->db->query("INSERT INTO product (name, nameSlug, description, target, keywords, price, priceWithVat, quantity, visible, created, changed) VALUES (?,?,?,?,?,?,?,?,?,?,?)", [
                $this->name,
                $this->nameSlug,
                $this->description,
                $this->target,
                $this->keywords,
                $this->price,
                $this->priceWithVat,
                $this->quantity,
                $this->visible,
                date('Y-m-d H:i:s'),
                $this->changed
            ]);

            if ($insert > 0) {
                $this->productId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return Product|string
     */
    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;
        $this->nameSlug = self::createSlug($this->name);

        if ($this->validate()) {
            $update = $this->db->query("UPDATE product SET name = ?, nameSlug = ?, description = ?, target = ?, keywords = ?, price = ?, priceWithVat = ?, quantity = ?, visible = ?, created = ?, changed = ? WHERE productId = ?", [
                $this->name,
                $this->nameSlug,
                $this->description,
                $this->target,
                $this->keywords,
                $this->price,
                $this->priceWithVat,
                $this->quantity,
                $this->visible,
                $this->created,
                date('Y-m-d H:i:s'),
                $this->productId
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->query("DELETE FROM product WHERE productId = ?", [
            $this->productId
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        if ($this->scenario == self::SCENARIO_CREATE || $this->scenario == self::SCENARIO_UPDATE) {
            $validation->validateRequired([
                'name' => $this->name,
                'nameSlug' => $this->nameSlug,
                'price' => $this->price,
                'priceWithVat' => $this->priceWithVat,
            ]);
            /*$validation->validateNotEmptyHTML([
                'description' => $this->description,
            ]);*/
            $validation->validateNumeric([
                'price' => $this->price,
                'priceWithVat' => $this->priceWithVat,
            ]);
            $validation->validateBoolean([
                'visible' => $this->visible,
            ]);
        }

        if ($this->scenario == self::SCENARIO_LOAD || $this->scenario == self::SCENARIO_DELETE || $this->scenario == self::SCENARIO_UPDATE) {
            /*$validation->validateRequired([
                'productId' => $this->productId
            ]);*/
            $validation->validateNumeric([
                'productId' => $this->productId
            ]);
            $validation->validateBoolean([
                'visible' => $this->visible,
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    /**
     * Creates czech currency text
     * @param $money
     * @return string
     */
    public static function convertToCurrency($money)
    {
        return number_format($money, 2, '.', ' ') . ' Kč';
    }

    /**
     * @return string
     */
    public static function getLastCreatedProduct()
    {
        if (is_a(UserController::isLoggedUser(), User::class)) {
            $productData = MyDB::getConnection()->queryOne("SELECT * FROM product WHERE productId = (SELECT MAX(productId) FROM product)", []);

            if (!(new Validate())->validateNotNull([$productData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst produkt z DB');

            $product = new Product($productData['productId']);
            $product = $product->load();
            if (!is_a($product, Product::class))
                return $product;

            return ProductGrid::generateProductCell($product);
        }

        return false;
    }

    /**
     * @return array
     */
    public function _toArray()
    {
        return [
            'productId' => $this->productId,
            'name' => $this->name,
            'nameSlug' => $this->nameSlug,
            'description' => $this->description,
            'target' => $this->target,
            'keywords' => $this->keywords,
            'price' => $this->price,
            'priceWithVat' => $this->priceWithVat,
            'quantity' => $this->quantity,
            'visible' => $this->visible,
            'created' => $this->created,
            'changed' => $this->changed
        ];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode($this->_toArray());
    }

    /**
     * @return array|string
     */
    public static function getProducts()
    {
        $db = MyDB::getConnection();
        $productsData = $db->queryAll("SELECT * FROM product", []);

        if (count($productsData) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$productsData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst produkty z DB');

        $products = [];
        foreach ($productsData as $productData) {
            $products[] = new Product(
                $productData['productId'],
                $productData['name'],
                $productData['nameSlug'],
                $productData['description'],
                $productData['target'],
                $productData['keywords'],
                $productData['price'],
                $productData['priceWithVat'],
                $productData['quantity'],
                $productData['visible'],
                $productData['created'],
                $productData['changed']
            );
        }

        return $products;
    }

    /**
     * @param null $products
     * @return array|string
     */
	public static function getProductsAsArray($products = null) {
	    if($products == null)
    	    $products = self::getProducts();

		if(is_array($products) && count($products) > 0) {
    		$productsArray = [];
    		foreach ($products as $product) {
    			$productsArray[] = $product->_toArray();
		    }

		    return $productsArray;
	    } else {
    		return $products;
	    }
    }

    /**
     * @param int $categoryId
     * @param string $orderBy
     * @param int $numOfRecords
     * @param int $offset
     * @param bool $onlyThisCategory
     * @return array|string
     */
    public static function getProductsWithParams($categoryId = 0, $orderBy = "", $numOfRecords = 12, $offset = 0, $onlyThisCategory = false)
    {
        $db = MyDB::getConnection();

        $orderByString = '';
        if ($orderBy != "" && $orderBy != null)
            $orderByString = ' ORDER BY ' . $orderBy . ', name';

        $limitString = '';
        if (is_numeric($numOfRecords) && is_numeric($offset) && $numOfRecords > 0)
            $limitString = ' LIMIT ' . $offset . ', ' . $numOfRecords;

        if (is_numeric($categoryId) && $categoryId > 0) {
            // order by admin set positions
            if ($orderByString == "" || $orderByString == null)
                $orderByString = ' ORDER BY pc.position, name';

            if (!$onlyThisCategory) {
                $productsData = $db->queryAll('SELECT DISTINCT p.* FROM product AS p INNER JOIN product_category AS pc ON p.productId = pc.productId WHERE p.visible = 1 AND (pc.categoryId = ? OR pc.categoryId IN (SELECT categoryId FROM category WHERE parentCategoryId = ?))' . $orderByString . $limitString, [
                    $categoryId,
                    $categoryId
                ]);
            } else {
                $productsData = $db->queryAll('SELECT DISTINCT p.* FROM product AS p INNER JOIN product_category AS pc ON p.productId = pc.productId WHERE p.visible = 1 AND pc.categoryId = ?' . $orderByString . $limitString, [
                    $categoryId
                ]);
            }
        } else {
            if($orderBy == 'pc.position asc'){
                $orderByString = ' ORDER BY name';
            }

            $productsData = $db->queryAll('SELECT DISTINCT * FROM product WHERE product.visible = 1' . $orderByString . $limitString, []);
        }

        if (count($productsData) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$productsData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst produkty z DB');

        $products = [];
        foreach ($productsData as $productData) {
            $products[] = new Product(
                $productData['productId'],
                $productData['name'],
                $productData['nameSlug'],
                $productData['description'],
                $productData['target'],
                $productData['keywords'],
                $productData['price'],
                $productData['priceWithVat'],
                $productData['quantity'],
                $productData['visible'],
                $productData['created'],
                $productData['changed']
            );
        }

        return $products;
    }

    /**
     * @param int $categoryId
     * @return mixed
     */
    public static function getProductsCount($categoryId = 0)
    {
        $db = MyDB::getConnection();

        if (is_numeric($categoryId) && $categoryId > 0)
            $productsCount = $db->queryOne('SELECT COUNT(DISTINCT p.productId) AS pCount FROM product AS p INNER JOIN product_category AS pc ON p.productId = pc.productId WHERE p.visible = 1 AND (pc.categoryId = ? OR pc.categoryId IN (SELECT categoryId FROM category WHERE parentCategoryId = ?))', [
                $categoryId,
                $categoryId
            ]);
        else
            $productsCount = $db->queryOne('SELECT COUNT(DISTINCT productId) AS pCount FROM product WHERE product.visible = 1', []);

        return $productsCount['pCount'];
    }

	/**
	 * @param $wantedPieces
	 * @param $date
	 * @param $hours
	 *
	 * @return string
	 */
    public function getProductAvailability($wantedPieces, $date, $hours) {
        $db = MyDB::getConnection();

	    $startDate = DateTime::createFromFormat( 'd. m. Y H:i', $date );
	    $endDate   = DateTime::createFromFormat( 'd. m. Y H:i', $date );
	    $endDate->modify( '+' . $hours . ' hours' );

        $availablePieces = 0;
        if(boolval(SuperproductProduct::isProductInSuperproduct($this->productId))) {
        	$availablePieces = Superproduct::getAvailableSuperproductPieces($this->productId, $date, $hours);
        	if(!is_int($availablePieces)) {
		        return json_encode( [
			        'result' => 'danger',
			        'text'   => ( 'Chyba ve výpočtu superproduktu. Chyba: ' . $availablePieces )
		        ] );
	        }

        	if($availablePieces < 1) {
		        return json_encode( [
			        'result' => 'danger',
			        'text'   => 'Produkt je v tomto termínu již plně vyprodán.'
		        ] );
	        }
        } else {
	        $ordersCount = $db->queryOne( 'SELECT MAX(op.quantity) AS maxQuantityInTerm FROM 
                                            order_product AS op INNER JOIN `order` AS o 
                                            ON op.orderId = o.orderId 
                                            INNER JOIN order_term AS ot 
                                            ON o.orderId = ot.orderId 
                                            WHERE productId = ?
                                            AND o.type = \'finished\'
                                            AND DATE_ADD(ot.start, INTERVAL ot.hours HOUR) >= ?
                                            AND ot.start <= ?', [
		        $this->productId,
		        $startDate->format( 'Y-m-d H:i:s' ),
		        $endDate->format( 'Y-m-d H:i:s' )
	        ] );

	        $quantityInOrders = $ordersCount['maxQuantityInTerm'];

	        $availablePieces = ($this->quantity - $quantityInOrders);

	        if ($availablePieces < 1) {
		        return json_encode( [
			        'result' => 'danger',
			        'text'   => 'Produkt je v tomto termínu již plně vyprodán.'
		        ] );
	        }
        }

        $reservationsCount = $this->getNumberOfReservedPieces($date, $hours);
        if($reservationsCount == 0){
        	if($wantedPieces > $availablePieces) {
		        return json_encode(['result' => 'danger', 'text' => 'Tento produkt momentálně není skladem']);
	        }
            return json_encode(['result' => 'success', 'text' => 'Produkt je v tomto termínu volný.']);
        } else {
            return json_encode(['result' => 'warning', 'text' => 'Tento produkt je již na tento termín rezervovaný.']);
        }
    }

    /**
     * @param $date
     * @param $hours
     *
     * @return int
     */
    public function getNumberOfReservedPieces($date, $hours) {
        $db = MyDB::getConnection();
        $startDate = DateTime::createFromFormat( 'd. m. Y H:i', $date );
        $endDate   = DateTime::createFromFormat( 'd. m. Y H:i', $date );
        $endDate->modify( '+' . $hours . ' hours' );
        $startDate->format( 'Y-m-d H:i:s' );
        $dateTo = date("Y-m-d H:i:s", strtotime(sprintf("+%d hours", $hours), strtotime($startDate->format( 'Y-m-d H:i:s' ))));
        $reservationsQuantity = $db->queryAll("SELECT reservationId FROM reservation WHERE productId = ? AND active = 1 AND termStart BETWEEN ? AND ?;", [
            $this->productId,
            $startDate->format( 'Y-m-d H:i:s' ),
            $dateTo
        ]);
         return count($reservationsQuantity) || 0;
    }

	/**
	 * @param $date
	 * @param $hours
	 *
	 * @return int
	 */
	public function getNumberOfOrderedPieces($date, $hours) {
	    $db = MyDB::getConnection();

	    $startDate = DateTime::createFromFormat('d. m. Y H:i', $date);
	    $endDate = DateTime::createFromFormat('d. m. Y H:i', $date);
	    $endDate->modify('+' . $hours . ' hours');

	    $ordersCount = $db->queryOne('SELECT SUM(op.quantity) AS maxQuantityInTerm FROM 
                                            order_product AS op INNER JOIN `order` AS o 
                                            ON op.orderId = o.orderId 
                                            INNER JOIN order_term AS ot 
                                            ON o.orderId = ot.orderId 
                                            WHERE productId = ?
                                            AND o.type = \'finished\'
                                            AND DATE_ADD(ot.start, INTERVAL ot.hours HOUR) >= ?
                                            AND ot.start <= ?', [
		    $this->productId,
		    $startDate->format('Y-m-d H:i:s'),
		    $endDate->format('Y-m-d H:i:s')
	    ]);

	    return intval($ordersCount['maxQuantityInTerm']);
    }

    /**
     * @param $date
     * @param $hours
     * @param bool $asArray
     * @return array|string
     */
    public static function getAvailableProducts($date, $hours, $asArray = false) {
        $db = MyDB::getConnection();

        $startDate = DateTime::createFromFormat('d. m. Y H:i', $date);
        $endDate = DateTime::createFromFormat('d. m. Y H:i', $date);
        $endDate->modify('+' . $hours . ' hours');

        $orderProducts = $db->queryAll('SELECT * FROM product
                                            WHERE product.productId NOT IN 
                                            (SELECT DISTINCT op.productId FROM 
                                            order_product AS op
                                            INNER JOIN `order` AS o
                                            ON op.orderId = o.orderId
                                            INNER JOIN order_term AS ot
                                            ON ot.orderId = o.orderId
                                            WHERE o.type = \'finished\'
                                            AND DATE_ADD(ot.start, INTERVAL ot.hours HOUR) >= ?
                                            AND ot.start <= ?)
                                            AND product.productId NOT IN 
                                            (SELECT DISTINCT productId FROM
                                            reservation AS r
                                            WHERE r.active = 1
                                            AND DATE_ADD(r.termStart, INTERVAL r.termHours HOUR) >= ?
                                            AND DATE(r.termStart) <= ?)', [
            $startDate->format('Y-m-d H:i:s'),
            $endDate->format('Y-m-d H:i:s'),
            $startDate->format('Y-m-d H:i:s'),
            $endDate->format('Y-m-d H:i:s')
        ]);

        if (count($orderProducts) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$orderProducts]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst produkty z DB');

        $products = [];
        foreach ($orderProducts as $productData) {
            $products[] = new Product(
                $productData['productId'],
                $productData['name'],
                $productData['nameSlug'],
                $productData['description'],
                $productData['target'],
                $productData['keywords'],
                $productData['price'],
                $productData['priceWithVat'],
                $productData['quantity'],
                $productData['visible'],
                $productData['created'],
                $productData['changed']
            );
        }

        if(!$asArray)
            return $products;

        $productsAsArray = [];
        foreach ($products as $product) {
            $productsAsArray[] = $product->_toArray();
        }

        return $productsAsArray;
    }

    /**
     * @param $date
     * @param $hours
     * @param bool $asArray
     * @return array|string
     */
    public static function getUnavailableProducts($date, $hours, $asArray = false) {
        $db = MyDB::getConnection();

        $startDate = DateTime::createFromFormat('d. m. Y H:i', $date);
        $endDate = DateTime::createFromFormat('d. m. Y H:i', $date);
        $endDate->modify('+' . $hours . ' hours');

        $orderProducts = $db->queryAll('SELECT DISTINCT * FROM product
                                            WHERE product.productId IN 
                                            (SELECT DISTINCT op.productId FROM 
                                            order_product AS op
                                            INNER JOIN `order` AS o
                                            ON op.orderId = o.orderId
                                            INNER JOIN order_term AS ot
                                            ON ot.orderId = o.orderId
                                            WHERE o.type = \'finished\'
                                            AND DATE_ADD(ot.start, INTERVAL ot.hours HOUR) >= ?
                                            AND ot.start <= ?)
                                            OR product.productId IN 
                                            (SELECT DISTINCT productId FROM
                                            reservation AS r
                                            WHERE r.active = 1
                                            AND DATE_ADD(r.termStart, INTERVAL r.termHours HOUR) >= ?
                                            AND DATE(r.termStart) <= ?)', [
            $startDate->format('Y-m-d H:i:s'),
            $endDate->format('Y-m-d H:i:s'),
            $startDate->format('Y-m-d H:i:s'),
            $endDate->format('Y-m-d H:i:s')
        ]);

        if (count($orderProducts) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$orderProducts]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst produkty z DB');

        $products = [];
        foreach ($orderProducts as $productData) {
            $products[] = new Product(
                $productData['productId'],
                $productData['name'],
                $productData['nameSlug'],
                $productData['description'],
                $productData['target'],
                $productData['keywords'],
                $productData['price'],
                $productData['priceWithVat'],
                $productData['quantity'],
                $productData['visible'],
                $productData['created'],
                $productData['changed']
            );
        }

        if(!$asArray)
            return $products;

        $productsAsArray = [];
        foreach ($products as $product) {
            $productsAsArray[] = $product->_toArray();
        }

        return $productsAsArray;
    }

    /**
     * @param $text
     * @return null|string|string[]
     */
    public static function createSlug($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        setlocale(LC_ALL, 'czech');

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    /**
     * Group setter
     * @param $name
     * @param $description
     * @param $target
     * @param $keywords
     * @param $price
     * @param $priceWithVat
     * @param $quantity
     * @param $visible
     */
    public function setProduct($name, $description, $target, $keywords, $price, $priceWithVat, $quantity, $visible)
    {
        $this->name = $name;
        $this->description = $description;
        $this->keywords = $keywords;
        $this->price = $price;
        $this->priceWithVat = $priceWithVat;
        $this->quantity = $quantity;
        $this->target = $target;
        $this->visible = boolval($visible);
    }

    /**
     * @return string
     */
    public function getNameSlug()
    {
        return $this->nameSlug;
    }

    /**
     * @param string $nameSlug
     */
    public function setNameSlug($nameSlug)
    {
        $this->nameSlug = $nameSlug;
    }

    /**
     * @return string
     */
    public function getDescriptionSlug()
    {
        $slug = strip_tags($this->description);

        $slug = preg_replace("/&nbsp;/", ' ', $slug);

        // only first 150 chars
        $slug = substr($slug, 0, 265);

        // find space
        $splitChar = strrpos($slug, ' ');
        return substr($slug, 0, $splitChar) . ' ...';
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPageTitle() {
        if(strlen($this->name) < 50)
            return $this->name . ' | OnlineAtrakce.cz - atrakce k pronájmu';
        else
            return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

   /**
    * @return mixed
    */

    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @return mixed
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getPriceWithVat()
    {
        return $this->priceWithVat;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param mixed $keywords
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @param mixed $priceWithVat
     */
    public function setPriceWithVat($priceWithVat)
    {
        $this->priceWithVat = $priceWithVat;
    }

    /**
     * @param $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return bool
     */
    public function getVisible()
{
    return $this->visible;
}

    public function getChanged()
    {
        $date = new DateTime($this->changed);
        $currentDate = date_create();

        $delta = date_diff($date, $currentDate);


        return $delta->format("%a dny");
    }

    public function setTarget($target) {
        $this->target = $target;
    }

    /**
     * @param mixed $visible
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    }
}