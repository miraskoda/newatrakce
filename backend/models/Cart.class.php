<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 14.08.2017
 * Time: 11:27
 */

namespace backend\models;

use backend\database\DatabaseError;
use backend\database\MyDB;

class Cart
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;

    private $db;
    private $scenario;

    private $cartId;
    private $customerId;
    private $productId;
    private $additionToProductId;
    private $quantity;

	/**
	 * Cart constructor.
	 *
	 * @param int $cartId
	 * @param int $customerId
	 * @param int $productId
	 * @param int $additionToProductId
	 * @param int $quantity
	 */
    public function __construct($cartId = 0, $customerId = 0, $productId = 0, $additionToProductId = 0, $quantity = 0)
    {
        $this->db = MyDB::getConnection();
        $this->cartId = $cartId;
        $this->customerId = $customerId;
        $this->productId = $productId;
        $this->additionToProductId = $additionToProductId;

        $this->setCart($quantity);
    }

    /**
     * Loads Cart from DB by cartId or customerId.
     * @return Cart|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
            $cartData = $this->db->queryOne("SELECT * FROM cart WHERE cartId = ? OR (customerId = ? AND productId = ? AND additionToProductId = ?)", [
                $this->cartId,
                $this->customerId,
                $this->productId,
	            $this->additionToProductId
            ]);

            if (!(new Validate())->validateNotNull([$cartData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst košík z DB');

            $this->cartId = $cartData['cartId'];
            $this->customerId = $cartData['customerId'];
            $this->productId = $cartData['productId'];
            $this->additionToProductId = $cartData['additionToProductId'];
            $this->setCart(
                $cartData['quantity']
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Inserts new Cart to DB.
     * No params required because all data are gained inside constructor.
     * @return Cart|string
     */
    public function create()
    {
        $this->scenario = self::SCENARIO_CREATE;

        if ($this->validate()) {
            if (!$this->isDuplicate()) {
                $insert = $this->db->query("INSERT INTO cart (customerId, productId, additionToProductId, quantity) VALUES (?,?,?,?)", [
                    $this->customerId,
                    $this->productId,
                    $this->additionToProductId,
                    $this->quantity
                ]);

                if ($insert > 0) {
                    $this->cartId = $this->db->lastInsertId();
                    return $this;
                } else {
                    return DatabaseError::getErrorDescription(DatabaseError::INSERT);
                }
            } else {
                return 'Produkt je již v košíku';
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Updates Cart by cartId.
     * @return Cart|string
     */
    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;

        if ($this->validate()) {
            $update = $this->db->query("UPDATE cart SET customerId = ?, productId = ?, additionToProductId = ?, quantity = ? WHERE cartId = ?", [
                $this->customerId,
                $this->productId,
                $this->additionToProductId,
                $this->quantity,
                $this->cartId
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Deletes Cart by cartId.
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->query("DELETE FROM cart WHERE cartId = ?", [
            $this->cartId
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * @return bool
     */
    public function isDuplicate()
    {
        $isDuplicate = $this->db->query("SELECT cartId FROM cart WHERE productId = ? AND customerId = ? AND additionToProductId = ?", [
            $this->productId,
            $this->customerId,
            $this->additionToProductId
        ]);

        return ($isDuplicate > 0);
    }

    /**
     * @param $tempId
     * @param $customerId
     * @return bool
     */
    public static function transferTempCart($tempId, $customerId)
    {
        $db = MyDB::getConnection();

        $order = new Order();
        $order->setCustomerId($tempId);
        $order->setType(Order::TYPE_DRAFT);
        $order = $order->load();

        // if temp customer created order -> override old
        if(is_a($order, Order::class)) {
            // delete cart products from previous temp orders
            $db->query("DELETE FROM cart WHERE customerId = ?", [
                $customerId
            ]);
        }

        $transfer = $db->query("UPDATE cart SET customerId = ? WHERE customerId = ?", [
            $customerId,
            $tempId
        ]);

        if ($transfer > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $customerId
     * @param bool $getAdditions
     * @param bool $onlyAdditions
     * @return array|string
     */
    public static function getCartProducts($customerId, $getAdditions = true, $onlyAdditions = false)
    {
        $db = MyDB::getConnection();
        if($getAdditions && !$onlyAdditions) {
            // additions and main products together
            $productsData = $db->queryAll("SELECT *, cart.quantity AS cquantity FROM cart INNER JOIN product ON cart.productId = product.productId WHERE customerId = ? ORDER BY name", [
                $customerId
            ]);
        } else if(!$getAdditions) {
            // only main products
            $productsData = $db->queryAll("SELECT *, cart.quantity AS cquantity FROM cart INNER JOIN product ON cart.productId = product.productId WHERE customerId = ? AND additionToProductId = 0 ORDER BY name", [
                $customerId
            ]);
        } else {
            // only additions
            $productsData = $db->queryAll("SELECT *, cart.quantity AS cquantity FROM cart INNER JOIN product ON cart.productId = product.productId WHERE customerId = ? AND additionToProductId <> 0 ORDER BY name", [
                $customerId
            ]);
        }

        if (count($productsData) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$productsData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst produkty v košíku z DB');

        $products = [];
        foreach ($productsData as $index => $productData) {
            $products[] = (new Product(
                $productData['productId'],
                $productData['name'],
                $productData['nameSlug'],
                $productData['additionToProductId'], // warning - field for description is used to carry additionToProductId value
                '','',
                $productData['price'],
                $productData['priceWithVat'],
                $productData['cquantity'],
                $productData['visible'],
                $productData['created'],
                $productData['changed']
            ))->_toArray();

            // load image
            $productImage = ProductImage::getProductImages($productData['productId'], ProductImage::TYPE_MAIN);
            (count($productImage) > 0) ? $products[$index]['image'] = $productImage[0]['name'] : null;
        }

        return $products;
    }

    public static function removeCartProducts($customerId, MyDB $db){
        $delete = $db->query("DELETE FROM cart WHERE customerId = ?", [
            $customerId
        ]);

        return boolval($delete);
    }

    public static function getAdditionalCartProducts($customerId, $mainProduct)
    {
        $db = MyDB::getConnection();
        $productsData = $db->queryAll("SELECT *, cart.quantity AS cquantity FROM cart INNER JOIN product ON cart.productId = product.productId WHERE customerId = ? AND additionToProductId = ? ORDER BY name", [
            $customerId,
            $mainProduct
        ]);

        if (count($productsData) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$productsData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst produkty v košíku z DB');

        $products = [];
        foreach ($productsData as $index => $productData) {
            $products[] = (new Product(
                $productData['productId'],
                $productData['name'],
                $productData['nameSlug'],
                '',
                '',
                $productData['price'],
                $productData['priceWithVat'],
                $productData['cquantity'],
                $productData['visible'],
                $productData['created'],
                $productData['changed']
            ))->_toArray();

            // load image
            $productImage = ProductImage::getProductImages($productData['productId'], ProductImage::TYPE_MAIN);
            (count($productImage) > 0) ? $products[$index]['image'] = $productImage[0]['name'] : null;
        }

        return $products;
    }

    /**
     * Validates if Cart attributes are complete and valid.
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        if ($this->scenario == self::SCENARIO_LOAD) {
            $validation->validateRequired([
                'customerId' => $this->customerId,
            ]);
            $validation->validateNumeric([
                'customerId' => $this->customerId,
            ]);
        }

        if ($this->scenario == self::SCENARIO_UPDATE) {
            $validation->validateRequired([
                'cartId' => $this->cartId,
            ]);
            $validation->validateNumeric([
                'cartId' => $this->cartId,
            ]);
        }

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'productId' => $this->productId,
                'množství' => $this->quantity,
                'customerId' => $this->customerId,
            ]);

            $validation->validateNumeric([
                'productId' => $this->productId,
                'množství' => $this->quantity,
                'customerId' => $this->customerId,
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    /**
     * Group setter
     * @param $quantity
     */
    public function setCart($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @param $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * @param $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @param $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getQuantity() {
        return $this->quantity;
    }

	/**
	 * @return int
	 */
	public function getCartId() {
		return $this->cartId;
	}

	/**
	 * @param int $cartId
	 */
	public function setCartId( $cartId ) {
		$this->cartId = $cartId;
	}

	/**
	 * @return int
	 */
	public function getAdditionToProductId() {
		return $this->additionToProductId;
	}

	/**
	 * @param int $additionToProductId
	 */
	public function setAdditionToProductId( $additionToProductId ) {
		$this->additionToProductId = $additionToProductId;
	}

}