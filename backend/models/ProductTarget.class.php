<?php
/**
 * Created by PhpStorm.
 * User: adamk
 * Date: 25/10/2018
 * Time: 18:34
 */

namespace backend\models;

use backend\database\DatabaseError;
use backend\database\MyDB;

class ProductTarget
{

    private static $tableName = 'product_target';

    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_DELETE = 3;
    const SCENARIO_LOAD = 4;

    private $db;
    private $scenario;

    private $productDataId;
    private $description;

    public function __construct($productDataId = 0, $description = "")
    {
        $this->db = MyDB::getConnection();
        $this->productDataId = $productDataId;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return $this|mixed|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;
        if ($this->validate()) {
            $result = $this->db->queryOne('SELECT * FROM ' . ProductTarget::$tableName . ' WHERE productDataId  = ?', [$this->productDataId]);

            if(!(new Validate())->validateNotNull([$result]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst data produktu z DB');

            $this->productDataId = $result['id'];
            $this->description = $result['description'];

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }


    /**
     * @return $this|mixed|string
     */
    public function create()
    {
        if ($this->validate()) {
            $insert = $this->db->queryOne('INSERT INTO ' . ProductTarget::$tableName . '(description) values(?)', [$this->description]);
            if ($insert > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return ProductTarget
     */
    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;

        if ($this->validate()) {
            $update = $this->db->queryOne('UPDATE ' . ProductTarget::$tableName . ' SET description = ? WHERE id = ?', [
                $this->description,
                $this->productDataId,
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Deletes ProductTargets.
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->queryOne('DELETE FROM ' . ProductTarget::$tableName . ' WHERE productDataId = ?', [$this->productDataId]);
        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * Validates if ProductTargets attributes are complete and valid.
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();
        if ($this->scenario == self::SCENARIO_CREATE || $this->scenario == self::SCENARIO_UPDATE) {
            $validation->validateRequired([
                'id' => $this->productDataId,
                'description' => $this->description,
            ]);
            $validation->validateNumeric([
                'id' => $this->productDataId,
            ]);
        }
        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }



//need refactoring code
    public static function getTargetByMainProduct($productId) {
        $db = MyDB::getConnection();
        $data = $db->queryAll('SELECT * FROM ' . ProductTarget::$tableName . ' WHERE productDataId = ?', [$productId]);

        if (count($data) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$data]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst kategorie produktů z DB');

        $result = [];
        foreach ($data as $category) {
            $categoryObject = new ProductTarget(
                $category['productDataId'],
                $category['description']
            );
            $result[] = $categoryObject;
        }

        return $result;
    }

}