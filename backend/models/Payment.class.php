<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 05.09.2017
 * Time: 9:45
 */

namespace backend\models;


use backend\database\DatabaseError;
use backend\database\MyDB;

class Payment
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;

    private $db;
    private $scenario;

    private $paymentId;
    private $name;
    private $icon;
    private $isVIP;
    private $slug;

	/**
	 * Payment constructor.
	 *
	 * @param int $paymentId
	 * @param string $name
	 * @param string $icon
	 * @param bool $isVIP
	 * @param string $slug
	 */
    public function __construct($paymentId = 0, $name = "", $icon = "", $isVIP = false, $slug = '')
    {
        $this->db = MyDB::getConnection();
        $this->paymentId = $paymentId;

        $this->isVIP = $isVIP;
        $this->slug = $slug;
        $this->setPayment($name, $icon);
    }

    /**
     * @return Payment|string
     */
    public function load(){
        $this->scenario = self::SCENARIO_LOAD;

        if($this->validate()){
            $paymentData = $this->db->queryOne("SELECT * FROM payment WHERE paymentId = ?", [
                $this->paymentId
            ]);

            if(!(new Validate())->validateNotNull([$paymentData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst platbu z DB');

            $this->isVIP = boolval($paymentData['isVIP']);
            $this->slug = $paymentData['slug'];
            $this->setPayment(
                $paymentData['name'],
                $paymentData['icon']
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return Payment|string
     */
    public function create(){
        $this->scenario = self::SCENARIO_CREATE;

        if($this->validate()){
            $insert = $this->db->query("INSERT INTO payment (name, icon, isVIP, slug) VALUES (?,?,?,?)", [
                $this->name,
                $this->icon,
	            (($this->isVIP) ? 1 : 0),
	            $this->slug
            ]);

            if($insert > 0){
                $this->paymentId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return Payment|string
     */
    public function update(){
        $this->scenario = self::SCENARIO_UPDATE;

        if($this->validate()){
            $update = $this->db->query("UPDATE payment SET name = ?, icon = ?, isVIP = ?, slug = ? WHERE paymentId = ?", [
                $this->name,
                $this->icon,
	            (($this->isVIP) ? 1 : 0),
                $this->slug,
                $this->paymentId
            ]);

            if($update > 0){
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return bool|string
     */
    public function delete(){
        $delete = $this->db->query("DELETE FROM payment WHERE paymentId = ?", [
            $this->paymentId
        ]);

        if($delete > 0){
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_LOAD) {
            $validation->validateRequired([
                'paymentId' => $this->paymentId,
            ]);
            $validation->validateNumeric([
                'paymentId' => $this->paymentId,
            ]);
        }

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'název' => $this->name,
                'ikona' => $this->icon,
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

	/**
	 * @param bool $isVIP
	 *
	 * @return array|string
	 */
    public static function getPayments($isVIP = false) {
        $db = MyDB::getConnection();
        if(!$isVIP)
            $paymentData = $db->queryAll("SELECT * FROM payment WHERE isVIP = 0", []);
        else
            $paymentData = $db->queryAll("SELECT * FROM payment", []);

        if (count($paymentData) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$paymentData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst platby z DB');

        $payments = [];
        foreach ($paymentData as $payment) {
            $payments[] = new Payment(
                $payment['paymentId'],
                $payment['name'],
                boolval($payment['icon']),
                $payment['slug']
            );
        }

        return $payments;
    }

    /**
     * Group setter
     * @param $name
     * @param $icon
     */
    public function setPayment($name, $icon){
        $this->name = $name;
        $this->icon = $icon;
    }

    /**
     * @return int
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

	/**
	 * @return bool
	 */
	public function isVIP() {
		return $this->isVIP;
	}

	/**
	 * @param bool $isVIP
	 */
	public function setIsVIP( $isVIP ) {
		$this->isVIP = $isVIP;
	}

	/**
	 * @return string
	 */
	public function getSlug() {
		return $this->slug;
	}

	/**
	 * @param string $slug
	 */
	public function setSlug( $slug ) {
		$this->slug = $slug;
	}
}