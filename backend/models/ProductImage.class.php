<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 15.06.2017
 * Time: 0:12
 */

namespace backend\models;

use backend\database\DatabaseError;
use backend\database\MyDB;

class ProductImage extends Image
{
    protected $tableName = 'product_image';
    private $type;
    private $productId;

    const TYPE_THUMBNAIL = 'thumbnail';
    const TYPE_MAIN = 'main';
    const TYPE_OTHER = 'other';

    /**
     * ProductImage constructor.
     * @param int $imageId
     * @param string $name
     * @param string $type
     * @param string $description
     * @param int $productId
     */
    public function __construct($imageId = 0, $name = "", $type = self::TYPE_OTHER, $description = '', $productId = 0)
    {
        parent::__construct($imageId, $name);
        $this->description = $description;
        $this->setProductImage($name, $type, $productId);
    }

    /**
     * @return ProductImage|mixed|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
            $imageData = $this->db->queryOne('SELECT * FROM product_image WHERE productImageId = ?', [
                $this->imageId
            ]);

            if (!(new Validate())->validateNotNull([$imageData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst obrázek z DB');

            $this->description = $imageData['description'];
            $this->setProductImage(
                $imageData['name'],
                $imageData['type'],
                $imageData['productId']
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return ProductImage|mixed|string
     */
    public function create()
    {
        $this->scenario = self::SCENARIO_CREATE;

        if ($this->validate()) {
            $insert = $this->db->query("INSERT INTO product_image (name, type, description, productId) VALUES (?, ?, ?, ?)", [
                $this->name,
                $this->type,
                $this->description,
                $this->productId
            ]);

            if ($insert > 0) {
                $this->imageId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return ProductImage|mixed|string
     */
    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;

        if ($this->validate()) {
            $update = $this->db->query("UPDATE product_image SET name = ?, type = ?, description = ?, productId = ? WHERE productImageId = ?", [
                $this->name,
                $this->type,
                $this->description,
                $this->productId,
                $this->imageId
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->query("DELETE FROM product_image WHERE productImageId = ?", [
            $this->imageId
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * @return bool|string
     */
    public function deleteByName()
    {
        $delete = $this->db->query("DELETE FROM product_image WHERE name = ?", [
            $this->name
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * @param $productId
     * @param $type
     * @return array|string
     */
    public static function getProductImages($productId, $type) {
        $db = MyDB::getConnection();
        $productImagesData = $db->queryAll("SELECT * FROM product_image WHERE productId = ? AND type = ?", [
            $productId,
            $type
        ]);

        if (count($productImagesData) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$productImagesData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst fotografie produktů z DB');

        $productImages = [];
        foreach ($productImagesData as $productImage) {
            $productImageObject = new ProductImage(
                $productImage['productImageId'],
                $productImage['name'],
                $productImage['type'],
                $productImage['description'],
                $productImage['productId']
            );
            $productImages[] = $productImageObject->_toArray();
        }

        return $productImages;
    }
    public static function getProductImagesByName($productName, $type) {


        $db = MyDB::getConnection();
        $productImagesData = $db->queryAll("SELECT product_image.name, product_image.type, product_image.productId, product.productId FROM product_image INNER JOIN product ON product_image.productId = product.productId WHERE product.name = ? AND product_image.type = ?", [
            $productName,
            $type
        ]);

        if (count($productImagesData) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$productImagesData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst fotografie produktů z DB');

        return $productImagesData;
    }


    /**
     * @param $productId
     * @return int
     */
    public static function getProductImagesCount($productId) {
        $db = MyDB::getConnection();
        $productImageCount = $db->queryOne("SELECT COUNT(*) AS imageCount FROM product_image WHERE productId = ?", [
            $productId
        ]);

        if (count($productImageCount) < 1)
            return 0;
        else if (!(new Validate())->validateNotNull([$productImageCount]))
            return 0;

        return $productImageCount['imageCount'];
    }

    /**
     * @return array
     */
    public function _toArray(){
        return [
            'productImageId' => $this->imageId,
            'name' => $this->name,
            'type' => $this->type,
            'description' => $this->description,
            'productId' => $this->productId
        ];
    }

    /**
     * @return array
     */
    public function jsonSerialize() {
        return [
            'productImageId' => $this->imageId,
            'name' => $this->name,
            'type' => $this->type,
            'description' => $this->description,
            'productId' => $this->productId
        ];
    }

    /**
     * Group setter
     * @param $name
     * @param $type
     * @param $productId
     */
    public function setProductImage($name, $type, $productId) {
        $this->setImage($name);
        $this->type = $type;
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
}