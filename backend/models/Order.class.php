<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 21.08.2017
 * Time: 18:04
 */

namespace backend\models;

use backend\controllers\CartController;
use backend\database\DatabaseError;
use backend\database\MyDB;
use DateTime;

class Order
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;

    private $db;
    private $scenario;

    private $orderId;
    private $customerId;
    private $type;

    private $contactName;
    private $contactPhonePrefix;
    private $contactPhone;

    private $invoiceIco;
    private $invoiceDic;

    private $venueStreet;
    private $venueCity;
    private $venueZip;
    private $venueCountry;

    private $paymentId;
    private $paymentTypeId;
    private $depositPaymentId;
    private $finishPaymentId;

    private $productPrice;
    private $deliveryPaymentPrice;
    private $vat;

    private $orderStateId;
    private $isPaid;
    private $isCanceled;
    private $requestChange;

    private $discount;

    private $adminNote;
    private $customerNote;

    private $changed;

    const TYPE_DRAFT = 'draft';
    const TYPE_FINISHED = 'finished';

    /**
     * Order constructor.
     *
     * @param int $orderId
     * @param int $customerId
     * @param string $type
     * @param string $contactName
     * @param string $contactPhonePrefix
     * @param string $contactPhone
     * @param int $invoiceIco
     * @param string $invoiceDic
     * @param string $venueStreet
     * @param string $venueCity
     * @param string $venueZip
     * @param string $venueCountry
     * @param int $paymentId
     * @param int $paymentTypeId
     * @param int $depositPaymentId
     * @param int $finishPaymentId
     * @param float $productPrice
     * @param float $deliveryPaymentPrice
     * @param int $vat
     * @param int $orderStateId
     * @param int $isPaid
     * @param int $discount
     * @param string $adminNote
     * @param string $customerNote
     * @param string $changed
     * @param int $isCanceled
     * @param null $requestChange
     */
    public function __construct($orderId = 0, $customerId = 0, $type = self::TYPE_DRAFT, $contactName = '', $contactPhonePrefix = '+420', $contactPhone = '', $invoiceIco = 0, $invoiceDic = '', $venueStreet = '', $venueCity = '', $venueZip = '', $venueCountry = Address::COUNTRY_CZ, $paymentId = 0, $paymentTypeId = 0, $depositPaymentId = 0, $finishPaymentId = 0, $productPrice = 0.0, $deliveryPaymentPrice = 0.0, $vat = 0, $orderStateId = 0, $isPaid = 0, $discount = 0, $adminNote = '', $customerNote = '', $changed = '1990-01-01 00:00:00', $isCanceled = 0, $requestChange = NULL)
    {
        $this->db = MyDB::getConnection();
        $this->orderId = $orderId;
        $this->customerId = $customerId;
        $this->type = $type;

        $this->paymentId = $paymentId;
        $this->paymentTypeId = $paymentTypeId;
        $this->depositPaymentId = $depositPaymentId;
        $this->finishPaymentId = $finishPaymentId;

        $this->productPrice = $productPrice;
        $this->deliveryPaymentPrice = $deliveryPaymentPrice;
        $this->vat = $vat;

        $this->invoiceIco = $invoiceIco;
        $this->invoiceDic = $invoiceDic;

        $this->orderStateId = $orderStateId;
        $this->isPaid = $isPaid;
        $this->isCanceled = $isCanceled;
        $this->requestChange = $requestChange;

        $this->discount = $discount;

        $this->adminNote = $adminNote;
        $this->customerNote = $customerNote;

        $this->setOrder($type, $contactName, $contactPhonePrefix, $contactPhone, $venueStreet, $venueCity, $venueZip, $venueCountry, $changed, $isCanceled, $requestChange);
    }

    /**
     * Loads Order from DB by orderId or customerId and type.
     * @return Order|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
            $orderData = $this->db->queryOne("SELECT * FROM `order` WHERE orderId = ? OR (customerId = ? AND type = ?) ORDER BY orderId DESC", [
                $this->orderId,
                $this->customerId,
                $this->type
            ]);

            if (!(new Validate())->validateNotNull([$orderData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst objednávku z DB');

            $this->orderId = $orderData['orderId'];
            $this->customerId = $orderData['customerId'];

            $this->paymentId = $orderData['paymentId'];
            $this->paymentTypeId = $orderData['paymentTypeId'];
            $this->depositPaymentId = $orderData['depositPaymentId'];
            $this->finishPaymentId = $orderData['finishPaymentId'];

            $this->productPrice = $orderData['productPrice'];
            $this->deliveryPaymentPrice = $orderData['deliveryPaymentPrice'];
            $this->vat = $orderData['vat'];

            $this->invoiceIco = $orderData['invoiceIco'];
            $this->invoiceDic = $orderData['invoiceDic'];

            $this->orderStateId = $orderData['orderStateId'];
            $this->isPaid = $orderData['isPaid'];
            $this->isCanceled = $orderData['isCanceled'];
            $this->requestChange = $orderData['requestChange'];

            $this->discount = $orderData['discount'];

            $this->adminNote = $orderData['adminNote'];
            $this->customerNote = $orderData['customerNote'];

            $this->setOrder(
                $orderData['type'],
                $orderData['contactName'],
                $orderData['contactPhonePrefix'],
                $orderData['contactPhone'],
                $orderData['venueStreet'],
                $orderData['venueCity'],
                $orderData['venueZip'],
                $orderData['venueCountry'],
                $orderData['changed'],
                $orderData['isCanceled'],
                $orderData['requestChange']
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }


    /**
     * Inserts new Order to DB.
     * No params required because all data are gained inside constructor.
     * @return Order|string
     */
    public function create()
    {
        $this->scenario = self::SCENARIO_CREATE;

        if ($this->validate()) {
            $insert = $this->db->query("INSERT INTO `order` (customerId, type, contactName, contactPhonePrefix, contactPhone, invoiceIco, invoiceDic, venueStreet, venueCity, venueZip, venueCountry, paymentId, paymentTypeId, depositPaymentId, finishPaymentId, productPrice, deliveryPaymentPrice, vat, orderStateId, isPaid, discount, adminNote, customerNote, isCanceled, requestChange, changed) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [
                $this->customerId,
                $this->type,
                $this->contactName,
                $this->contactPhonePrefix,
                $this->contactPhone,
                $this->invoiceIco,
                $this->invoiceDic,
                $this->venueStreet,
                $this->venueCity,
                $this->venueZip,
                $this->venueCountry,
                $this->paymentId,
                $this->paymentTypeId,
                $this->depositPaymentId,
                $this->finishPaymentId,
                $this->productPrice,
                $this->deliveryPaymentPrice,
                $this->vat,
                $this->orderStateId,
                $this->isPaid,
                $this->discount,
                $this->adminNote,
                $this->customerNote,
                $this->isCanceled,
                $this->requestChange,
                (($this->type == self::TYPE_FINISHED) ? $this->changed : date('Y-m-d H:i:s'))
            ]);

            if ($insert > 0) {
                $this->orderId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Updates Order by orderId.
     * @return Order|string
     */
    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;

        if ($this->validate()) {
            $update = $this->db->query("UPDATE `order` SET customerId = ?, type = ?, contactName = ?, contactPhonePrefix = ?, contactPhone = ?, invoiceIco = ?, invoiceDic = ?, venueStreet = ?, venueCity = ?, venueZip = ?, venueCountry = ?, paymentId = ?, paymentTypeId = ?, depositPaymentId = ?, finishPaymentId = ?, productPrice = ?, deliveryPaymentPrice = ?, vat = ?, orderStateId = ?, isPaid = ?, discount = ?, adminNote = ?, customerNote = ?, isCanceled = ?, requestChange = ?, changed = ? WHERE orderId = ?", [
                $this->customerId,
                $this->type,
                $this->contactName,
                $this->contactPhonePrefix,
                $this->contactPhone,
                $this->invoiceIco,
                $this->invoiceDic,
                $this->venueStreet,
                $this->venueCity,
                $this->venueZip,
                $this->venueCountry,
                $this->paymentId,
                $this->paymentTypeId,
                $this->depositPaymentId,
                $this->finishPaymentId,
                $this->productPrice,
                $this->deliveryPaymentPrice,
                $this->vat,
                $this->orderStateId,
                $this->isPaid,
                $this->discount,
                $this->adminNote,
                $this->customerNote,
                $this->isCanceled,
                $this->requestChange,
                (($this->type == self::TYPE_FINISHED) ? $this->changed : date('Y-m-d H:i:s')),
                $this->orderId
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Deletes Order by orderId.
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->query("DELETE FROM `order` WHERE orderId = ?", [
            $this->orderId
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * @return bool
     */
    public function isDuplicate()
    {
        $isDuplicate = $this->db->query("SELECT orderId FROM `order` WHERE type = ? AND customerId = ?", [
            $this->type,
            $this->customerId
        ]);

        if ($isDuplicate > 0) {
            return true;
        } else {
            return false;
        }
    }



    public static function setOrderStateByDocument($docId, $orderStateId = 7){

        $db = MyDB::getConnection();

        // find orderId by documentID
        $orderId = $db->query("SELECT * FROM `order_document` WHERE id = ?", [
            $docId,
        ]);

        if ($orderId > 0) {
            $tempId = $orderId["order_id"];
        } else {
            return false;
        }


        // update order state of order - need orderID
        $transfer = $db->query("UPDATE `order` SET orderStateId = ? WHERE orderId = ?", [
            $orderStateId,
            $tempId
        ]);

        if ($transfer > 0) {

            //odeslani emailu
            $order = new Order($tempId);
            $order = $order->load();
            $mailer = new MailHelper();

            $c = new Customer($order -> getCustomerId());
            $c -> load();

            $mailer->sendConfirmation( $c -> getEmail(), $order);

            return true;
        } else {
            return false;

        }

    }


    /**
     * @param $tempId
     * @param $customerId
     * @return bool
     */
    public static function transferTempOrder($tempId, $customerId)
    {
        $db = MyDB::getConnection();

        // update temp customer
        $transfer = $db->query("UPDATE `order` SET customerId = ? WHERE customerId = ?", [
            $customerId,
            $tempId
        ]);

        self::deleteOldTempOrders();

        if ($transfer > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete old drafts
     */
    public static function deleteOldTempOrders()
    {
        $customer = CartController::getCustomerId();

        $order = new Order();
        $order->setCustomerId($customer);
        $order->setType(Order::TYPE_DRAFT);
        $order = $order->load();

        if (is_a($order, Order::class)) {
            $db = MyDB::getConnection();
            // delete terms of old temp orders
            $db->query("DELETE FROM order_term WHERE orderId IN (SELECT orderId FROM `order` WHERE customerId = ? AND orderId <> ?)", [
                $customer,
                $order->getOrderId()
            ]);

            // delete old orders
            $db->query("DELETE FROM `order` WHERE orderId IN (SELECT * FROM (SELECT orderId FROM `order` WHERE customerId = ? AND orderId <> ?) as o)", [
                $customer,
                $order->getOrderId()
            ]);
        }
    }

    /**
     * @param $rentHours
     * @param null $products
     * @return float|int
     */
    public static function getPriceWithoutVat($rentHours, $products = null)
    {
        if (is_null($products))
            $products = CartController::getCartProducts();

        $priceWithoutVat = 0;
        foreach ($products as $product) {
            foreach ($rentHours as $rentHour) {
                if ($rentHour > 0) {
                    $priceWithoutVat += (($rentHour <= 5) ?
                        (($product['price']) * $product['quantity'])
                        :
                        ((($product['price']) + (($product['price'] / 100) * (5 * ($rentHour - (($rentHour > 5) ? 5 : $rentHour))))) * $product['quantity'])
                    );
                }
            }
        }
        return $priceWithoutVat;
    }

    /**
     * @param $order
     * @param $rentHours
     * @param null $products
     * @return float|int
     */
    public static function getPriceWithoutVatByOrder($order, $rentHours, $products = null)
    {
        if (is_null($products))
            $products = OrderProduct::getOrderProducts($order);
        $priceWithoutVat = 0;
        foreach ($products as $product) {
            foreach ($rentHours as $rentHour) {
                if ($rentHour > 0) {
                    $priceWithoutVat += (($rentHour <= 5) ?
                        (($product['price']) * $product['quantity'])
                        :
                        ((($product['price']) + (($product['price'] / 100) * (5 * ($rentHour - (($rentHour > 5) ? 5 : $rentHour))))) * $product['quantity'])
                    );
                }
            }
        }
        return $priceWithoutVat;
    }

    /**
     * @param bool $isAdmin
     * @return array|string
     */
    public static function getOrders($isAdmin = false)
    {
        $customerId = CartController::getCustomerId();

        if ($isAdmin || (isset($customerId) && is_numeric($customerId))) {
            $db = MyDB::getConnection();
            if (!$isAdmin) {
                $ordersData = $db->queryAll("SELECT * FROM `order` WHERE customerId = ? AND type = ? ORDER BY changed DESC", [
                    $customerId,
                    Order::TYPE_FINISHED
                ]);
            } else {
                $ordersData = $db->queryAll("SELECT * FROM `order` WHERE type = ? ORDER BY changed DESC", [
                    Order::TYPE_FINISHED
                ]);
            }

            if (count($ordersData) < 1)
                return [];
            else if (!(new Validate())->validateNotNull([$ordersData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst objednávky z DB');

            $orders = [];
            foreach ($ordersData as $orderData) {
                $orders[] = new Order(
                    $orderData['orderId'],
                    $orderData['customerId'],
                    $orderData['type'],
                    $orderData['contactName'],
                    $orderData['contactPhonePrefix'],
                    $orderData['contactPhone'],
                    $orderData['invoiceIco'],
                    $orderData['invoiceDic'],
                    $orderData['venueStreet'],
                    $orderData['venueCity'],
                    $orderData['venueZip'],
                    $orderData['venueCountry'],
                    $orderData['paymentId'],
                    $orderData['paymentTypeId'],
                    $orderData['depositPaymentId'],
                    $orderData['finishPaymentId'],
                    $orderData['productPrice'],
                    $orderData['deliveryPaymentPrice'],
                    $orderData['vat'],
                    $orderData['orderStateId'],
                    $orderData['isPaid'],
                    $orderData['discount'],
                    $orderData['adminNote'],
                    $orderData['customerNote'],
                    $orderData['changed'],
                    $orderData['isCanceled'],
                    $orderData['requestChange']
                );
            }

            return $orders;
        } else {
            return [];
        }
    }

    public static function getOrdersBySearch($phrase)
    {

            $db = MyDB::getConnection();

            // for searchin between names and id of orders
                $ordersDataNames = $db->queryAll("SELECT o.*, a.orderId, a.name FROM `order` as o, `address` as a WHERE a.orderId = o.orderId AND type = ? AND (o.contactName LIKE concat('%', ?, '%') OR o.orderId LIKE concat('%', ?, '%') OR a.name LIKE concat('%', ?, '%')) ORDER BY `o`.`orderId` DESC", [Order::TYPE_FINISHED,$phrase,$phrase,$phrase]);

            // for searching between invoices
                $ordersDataInvoice = $db->queryAll("SELECT o.*, i.order_id, i.id FROM `order` as o, `order_invoice` as i WHERE i.order_id = o.orderId AND type = ? AND (i.id LIKE concat('%', ?, '%')) ORDER BY `o`.`orderId` DESC", [Order::TYPE_FINISHED,$phrase]);

            // for searching between credits
                $ordersDataCredit = $db->queryAll("SELECT o.*, i.order_id, i.id FROM `order` as o, `order_credit` as i WHERE i.order_id = o.orderId AND type = ? AND (i.id LIKE concat('%', ?, '%')) ORDER BY `o`.`orderId` DESC", [Order::TYPE_FINISHED,$phrase]);

            // for searching between credits
                $ordersDataDocument = $db->queryAll("SELECT o.*, i.order_id, i.id FROM `order` as o, `order_document` as i WHERE i.order_id = o.orderId AND type = ? AND (i.id LIKE concat('%', ?, '%')) ORDER BY `o`.`orderId` DESC", [Order::TYPE_FINISHED,$phrase]);

            // for searching between atractions
                $ordersDataAttractions = $db->queryAll("SELECT o.*, p.orderId, p.name FROM `order` as o, `order_product` as p WHERE p.orderId = o.orderId AND type = ? AND (p.name LIKE concat('%', ?, '%')) ORDER BY `o`.`orderId` DESC", [Order::TYPE_FINISHED,$phrase]);

            // for searhing between date
                $ordersDataDate = $db->queryAll("SELECT o.* FROM `order` as o WHERE type = ? AND DATE_FORMAT(changed , '%e.%c.%Y') = ? ORDER BY `o`.`orderId` DESC", [Order::TYPE_FINISHED,$phrase]);


        $ordersData = [];
                if(count($ordersDataNames)>0){
                    $ordersData = $ordersDataNames;
                }
                else if(count($ordersDataInvoice)>0) {
                    $ordersData = $ordersDataInvoice;
                }
                else if(count($ordersDataCredit)>0) {
                    $ordersData = $ordersDataCredit;
                }
                else if(count($ordersDataDocument)>0) {
                    $ordersData = $ordersDataDocument;
                }
                else if(count($ordersDataAttractions)>0) {
                    $ordersData = $ordersDataAttractions;
                }
                else if(count($ordersDataDate)>0) {
                    $ordersData = $ordersDataDate;
                }




                        if (count($ordersData) < 1)
                return [];
            else if (!(new Validate())->validateNotNull([$ordersData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Hledání neodpovídá žádný záznam');

            $orders = [];
            foreach ($ordersData as $orderData) {
                $orders[] = new Order(
                    $orderData['orderId'],
                    $orderData['customerId'],
                    $orderData['type'],
                    $orderData['contactName'],
                    $orderData['contactPhonePrefix'],
                    $orderData['contactPhone'],
                    $orderData['invoiceIco'],
                    $orderData['invoiceDic'],
                    $orderData['venueStreet'],
                    $orderData['venueCity'],
                    $orderData['venueZip'],
                    $orderData['venueCountry'],
                    $orderData['paymentId'],
                    $orderData['paymentTypeId'],
                    $orderData['depositPaymentId'],
                    $orderData['finishPaymentId'],
                    $orderData['productPrice'],
                    $orderData['deliveryPaymentPrice'],
                    $orderData['vat'],
                    $orderData['orderStateId'],
                    $orderData['isPaid'],
                    $orderData['discount'],
                    $orderData['adminNote'],
                    $orderData['customerNote'],
                    $orderData['changed'],
                    $orderData['isCanceled'],
                    $orderData['requestChange']
                );
            }

            return $orders;

    }


    /**
     * Returns array with orders and terms, grouped by terms.
     *
     * @param $date
     * @return array|string
     */
    public static function getOrdersByDate($date)
    {
        if ($date == "")
            return [];

        $db = MyDB::getConnection();
        $ordersData = $db->queryAll("SELECT o.*, ot.* FROM `order` AS o INNER JOIN order_term AS ot ON o.orderId = ot.orderId WHERE DATE(start) = ? AND type = ? ORDER BY ot.start", [
            $date,
            Order::TYPE_FINISHED
        ]);

        if (count($ordersData) < 1) {
            return [
                'orders' => [],
                'orderTerms' => []
            ];
        } else if (!(new Validate())->validateNotNull([$ordersData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst objednávky z DB');


        $orders = [];
        $ordersProducts = [];
        $orderTerms = [];
        foreach ($ordersData as $orderData) {
            $orders[] = (new Order(
                $orderData['orderId'],
                $orderData['customerId'],
                $orderData['type'],
                $orderData['contactName'],
                $orderData['contactPhonePrefix'],
                $orderData['contactPhone'],
                $orderData['invoiceIco'],
                $orderData['invoiceDic'],
                $orderData['venueStreet'],
                $orderData['venueCity'],
                $orderData['venueZip'],
                $orderData['venueCountry'],
                $orderData['paymentId'],
                $orderData['paymentTypeId'],
                $orderData['depositPaymentId'],
                $orderData['finishPaymentId'],
                $orderData['productPrice'],
                $orderData['deliveryPaymentPrice'],
                $orderData['vat'],
                $orderData['orderStateId'],
                $orderData['isPaid'],
                $orderData['discount'],
                $orderData['adminNote'],
                $orderData['customerNote'],
                $orderData['changed'],
                $orderData['isCanceled'],
                $orderData['requestChange']
            ))->_toArray();
            $orderTerms[] = (new OrderTerm(
                $orderData['orderTermId'],
                $orderData['orderId'],
                $orderData['termNo'],
                $orderData['start'],
                $orderData['hours']
            ))->_toArray();

            //get order products and format them
            $orderProducts = OrderProduct::getOrderProducts($orderData['orderId'], true);
            $orderProductsOutput = [];
            if (is_array($orderProducts)) {
                foreach ($orderProducts as $product) {
                    $orderProductsOutput[] = $product['quantity'] . 'x ' . $product['name'];
                }
            }

            // add order products to array
            $ordersProducts[] = $orderProductsOutput;
        }

        return [
            'orders' => $orders,
            'orderProducts' => $ordersProducts,
            'orderTerms' => $orderTerms
        ];
    }

    /**
     * @param $date
     * @return int
     */
    public static function getOrderCountByDate($date)
    {
        if ($date == "")
            return 0;

        $db = MyDB::getConnection();
        $ordersCount = $db->query("SELECT o.*, ot.* FROM `order` AS o INNER JOIN order_term AS ot ON o.orderId = ot.orderId WHERE DATE(start) = ? AND type = ?", [
            $date,
            Order::TYPE_FINISHED
        ]);

        return $ordersCount;
    }

    /**
     * @param $customerId
     * @return null
     */
    public static function getOrderCountByCustomer($customerId)
    {
        if ($customerId == "" || $customerId == 0)
            return null;

        $db = MyDB::getConnection();
        $ordersCount = $db->queryOne("SELECT COUNT(orderId) AS customerOrderCount FROM `order` WHERE customerId = ?", [
            $customerId
        ]);

        return $ordersCount['customerOrderCount'];
    }

    /**
     * Validates if Order attributes are complete and valid.
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        /*if ($this->scenario == self::SCENARIO_LOAD) {
            $validation->validateRequired([
                'orderId' => $this->orderId,
            ]);
            $validation->validateNumeric([
                'orderId' => $this->orderId,
            ]);
        }*/

        if ($this->scenario == self::SCENARIO_UPDATE) {
            $validation->validateRequired([
                'orderId' => $this->orderId,
            ]);
            $validation->validateNumeric([
                'orderId' => $this->orderId,
            ]);
        }

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'typ' => $this->type,
                'customerId' => $this->customerId,
            ]);

            $validation->validateNumeric([
                'customerId' => $this->customerId,
            ]);

            $tempValidate = new Validate();
            if ($tempValidate->validateRequired([$this->contactPhone]))
                $validation->validatePhone([
                    'telefon' => $this->contactPhone,
                ]);

            if ($tempValidate->validateRequired([$this->venueZip]))
                $validation->validateZIP([
                    'PSČ' => $this->venueZip,
                ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return array
     */
    public function _toArray()
    {
        return [
            'orderId' => $this->orderId,
            'customerId' => $this->customerId,
            'type' => $this->type,
            'contactName' => $this->contactName,
            'contactPhonePrefix' => $this->contactPhonePrefix,
            'contactPhone' => $this->contactPhone,
            'invoiceIco' => $this->invoiceIco,
            'invoiceDic' => $this->invoiceDic,
            'venueStreet' => $this->venueStreet,
            'venueCity' => $this->venueCity,
            'venueZip' => $this->venueZip,
            'venueCountry' => $this->venueCountry,
            'paymentId' => $this->paymentId,
            'paymentTypeId' => $this->paymentTypeId,
            'depositPaymentId' => $this->depositPaymentId,
            'finishPaymentId' => $this->finishPaymentId,
            'productPrice' => $this->productPrice,
            'deliveryPaymentPrice' => $this->deliveryPaymentPrice,
            'vat' => $this->vat,
            'orderStateId' => $this->orderStateId,
            'isPaid' => $this->isPaid,
            'discount' => $this->discount,
            'adminNote' => $this->adminNote,
            'customerNote' => $this->customerNote,
            'changed' => $this->changed,
            'isCanceled' => $this->isCanceled,
            'requestChange' => $this->requestChange
        ];
    }

    /**
     * Group setter
     * @param $type
     * @param $contactName
     * @param $contactPhonePrefix
     * @param $contactPhone
     * @param $venueStreet
     * @param $venueCity
     * @param $venueZip
     * @param $venueCountry
     * @param $isCanceled
     * @param $changed
     */
    public function setOrder($type, $contactName, $contactPhonePrefix, $contactPhone, $venueStreet, $venueCity, $venueZip, $venueCountry, $changed, $isCanceled, $requestChange)
    {
        $this->type = $type;

        $this->contactName = $contactName;
        $this->contactPhonePrefix = $contactPhonePrefix;
        $this->contactPhone = $contactPhone;

        $this->venueStreet = $venueStreet;
        $this->venueCity = $venueCity;
        $this->venueZip = $venueZip;
        $this->venueCountry = $venueCountry;

        $this->isCanceled = $isCanceled;
        $this->changed = $changed;
        $this->requestChange = $requestChange;
    }

    /**
     * @param $isCanceled
     */
    public function setIsCanceled($isCanceled)
    {
        if ($isCanceled > 0) {
            $this->isCanceled = 1;
        } else {
            $this->isCanceled = 0;
        }
    }

    public static function isCorrectAmount($getAmount, $getProductPrice)
    {
        if( round($getAmount, -2) == round($getProductPrice, -2)){

            return true;

        }else{

            return false;
        }

    }


    /**
     * @return bool
     */
    public function getIsCanceled()
    {
        return $this->isCanceled > 0;
    }

    /**
     * @return string
     */
    public function getRequestChange()
    {
        return $this->requestChange;
    }

    /**
     * @return bool
     */
    public function getIsRequestedChange()
    {
        return ($this->requestChange != NULL);
    }

    /**
     * @param MyDB $db
     */
    public function setDb($db)
    {
        $this->db = $db;
    }

    /**
     * @param $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param mixed $contactName
     */
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;
    }

    /**
     * @param mixed $contactPhonePrefix
     */
    public function setContactPhonePrefix($contactPhonePrefix)
    {
        $this->contactPhonePrefix = $contactPhonePrefix;
    }

    /**
     * @param mixed $contactPhone
     */
    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;
    }

    /**
     * @param int $invoiceIco
     */
    public function setInvoiceIco($invoiceIco)
    {
        $this->invoiceIco = $invoiceIco;
    }

    /**
     * @param string $invoiceDic
     */
    public function setInvoiceDic($invoiceDic)
    {
        $this->invoiceDic = $invoiceDic;
    }

    /**
     * @param mixed $venueStreet
     */
    public function setVenueStreet($venueStreet)
    {
        $this->venueStreet = $venueStreet;
    }

    /**
     * @param mixed $venueCity
     */
    public function setVenueCity($venueCity)
    {
        $this->venueCity = $venueCity;
    }

    /**
     * @param mixed $venueZip
     */
    public function setVenueZip($venueZip)
    {
        $this->venueZip = $venueZip;
    }

    /**
     * @param mixed $venueCountry
     */
    public function setVenueCountry($venueCountry)
    {
        $this->venueCountry = $venueCountry;
    }

    /**
     * @param int $paymentId
     */
    public function setPaymentId($paymentId)
    {
        $this->paymentId = $paymentId;
    }

    /**
     * @param int $paymentTypeId
     */
    public function setPaymentTypeId($paymentTypeId)
    {
        $this->paymentTypeId = $paymentTypeId;
    }

    /**
     * @return float
     */
    public function getProductPrice()
    {
        return $this->productPrice;
    }

    /**
     * @return float
     */
    public function getDeliveryPaymentPrice()
    {
        return $this->deliveryPaymentPrice;
    }

    /**
     * @return int
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @return bool
     */
    public function getIsPaid()
    {
        return boolval($this->isPaid);
    }

    /**
     * @param int $isPaid
     */
    public function setIsPaid($isPaid)
    {
        $this->isPaid = $isPaid;
    }

    /**
     * @return mixed
     */
    public function getContactName()
    {
        return $this->contactName;
    }

    /**
     * @return mixed
     */
    public function getContactPhonePrefix()
    {
        return $this->contactPhonePrefix;
    }

    /**
     * @return mixed
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * @return mixed
     */
    public function getVenueStreet()
    {
        return $this->venueStreet;
    }

    /**
     * @return mixed
     */
    public function getVenueCity()
    {
        return $this->venueCity;
    }

    /**
     * @return mixed
     */
    public function getVenueZip()
    {
        return $this->venueZip;
    }

    /**
     * @return mixed
     */
    public function getVenueCountry()
    {
        return $this->venueCountry;
    }

    /**
     * @return int
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * @return int
     */
    public function getPaymentTypeId()
    {
        return $this->paymentTypeId;
    }

    /**
     * @param int $orderStateId
     */
    public function setOrderStateId($orderStateId)
    {
        $this->orderStateId = $orderStateId;
    }

    /**
     * @return int
     */
    public function getOrderStateId()
    {
        return $this->orderStateId;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }


    /**
     * @return int
     */
    public function getFormattedOrderId()
    {


        $ret = $this->orderId;;


        //$ret = 999999;
        $out = "";
        if ($ret < 100) {
            $out .= '0000' . $ret;

        } elseif ($ret < 1000) {
            $out .= '000' . $ret;


        } elseif ($ret < 10000) {
            $out .= '00' . $ret;


        } elseif ($ret < 100000) {

            $out .= '0' . $ret;

        } elseif ($ret < 1000000) {

            $out .= $ret;

        }


        return $out;
    }


    /**
     * @return int
     */
    public function getInvoiceIco()
    {
        return $this->invoiceIco;
    }

    /**
     * @return string
     */
    public function getInvoiceDic()
    {
        return $this->invoiceDic;
    }

    /**
     * @return mixed
     */
    public function getChanged()
    {
        $dateTime = DateTime::createFromFormat('Y-m-d H:i:s', $this->changed);
        return $dateTime->format('d. m. Y H:i:s');
    }

    /**
     * @return string
     */
    public function getChangedDate()
    {
        $dateTime = DateTime::createFromFormat('Y-m-d H:i:s', $this->changed);
        return $dateTime->format('d. m. Y');
    }

    /**
     * @return mixed
     */
    public function getChangedRaw()
    {
        return $this->changed;
    }

    /**
     * @return string
     */
    public function getPayDate()
    {
        $dateTime = DateTime::createFromFormat('Y-m-d H:i:s', $this->changed);
        $dateTime->modify('+4 days');
        return $dateTime->format('d. m. Y');
    }

    /**
     * @return float|int
     */
    public function getSumPrice()
    {
        return $this->productPrice + $this->deliveryPaymentPrice + $this->vat;
    }

    /**
     * @param float $productPrice
     */
    public function setProductPrice($productPrice)
    {
        $this->productPrice = $productPrice;
    }

    /**
     * @param float $deliveryPaymentPrice
     */
    public function setDeliveryPaymentPrice($deliveryPaymentPrice)
    {
        $this->deliveryPaymentPrice = $deliveryPaymentPrice;
    }

    /**
     * @param int $vat
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
    }

    /**
     * @return int
     */
    public function getDiscount()
    {
        return $this->discount;
    }


    /**
     * @param int $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return string
     */
    public function getAdminNote()
    {
        return $this->adminNote;
    }

    /**
     * @param string $adminNote
     */
    public function setAdminNote($adminNote)
    {
        $this->adminNote = $adminNote;
    }

    /**
     * @return string
     */
    public function getCustomerNote()
    {
        return $this->customerNote;
    }

    /**
     * @param string $customerNote
     */
    public function setCustomerNote($customerNote)
    {
        $this->customerNote = $customerNote;
    }

    /**
     * @return int
     */
    public function getDepositPaymentId()
    {
        return $this->depositPaymentId;
    }

    /**
     * @param int $depositPaymentId
     */
    public function setDepositPaymentId($depositPaymentId)
    {
        $this->depositPaymentId = $depositPaymentId;
    }

    /**
     * @return int
     */
    public function getFinishPaymentId()
    {
        return $this->finishPaymentId;
    }

    /**
     * @param int $finishPaymentId
     */
    public function setFinishPaymentId($finishPaymentId)
    {
        $this->finishPaymentId = $finishPaymentId;
    }

    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @param $requestChange
     */
    public function setRequestChange($requestChange)
    {
        $this->requestChange = $requestChange;
    }

    /**
     * @param $changed
     */
    public function setChanged($changed) {
        $this->changed = $changed;
    }
}