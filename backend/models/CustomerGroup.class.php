<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 05.12.2017
 * Time: 20:03
 */

namespace backend\models;


use backend\database\DatabaseError;
use backend\database\MyDB;

class CustomerGroup {
	const SCENARIO_CREATE = 1;
	const SCENARIO_UPDATE = 2;
	const SCENARIO_LOAD = 4;

	private $db;
	private $scenario;

	private $customerGroupId;
	private $name;
	private $normalPayments;
	private $vipPayments;
	private $discount;
	private $maxReservationHours;

	/**
	 * CustomerGroup constructor.
	 *
	 * @param int $customerGroupId
	 * @param string $name
	 * @param int $normalPayments
	 * @param int $vipPayments
	 * @param float $discount
	 * @param int $maxReservationHours
	 */
	public function __construct($customerGroupId = 0, $name = '', $normalPayments = 1, $vipPayments = 0, $discount = 0.0, $maxReservationHours = 1)
	{
		$this->db = MyDB::getConnection();
		$this->customerGroupId = $customerGroupId;
		$this->name = $name;
		$this->normalPayments = $normalPayments;
		$this->vipPayments = $vipPayments;
		$this->discount = $discount;
		$this->maxReservationHours = $maxReservationHours;
	}

	/**
	 * Loads CustomerGroup from DB by customerGroupId.
	 * @return CustomerGroup|string
	 */
	public function load()
	{
		$this->scenario = self::SCENARIO_LOAD;

		if ($this->validate()) {
			$customerGroupData = $this->db->queryOne("SELECT * FROM customer_group WHERE customerGroupId = ?", [
				$this->customerGroupId
			]);

			if (!(new Validate())->validateNotNull([$customerGroupData]))
				return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst zákaznickou skupinu z DB');

			$this->customerGroupId = $customerGroupData['customerGroupId'];
			$this->name = $customerGroupData['name'];
			$this->normalPayments = $customerGroupData['normalPayments'];
			$this->vipPayments = $customerGroupData['vipPayments'];
			$this->discount = $customerGroupData['discount'];
			$this->maxReservationHours = $customerGroupData['maxReservationHours'];

			return $this;
		} else {
			return Validate::getValidationSummaryText();
		}
	}

	/**
	 * Inserts new CustomerGroup to DB.
	 * @return CustomerGroup|string
	 */
	public function create()
	{
		$this->scenario = self::SCENARIO_CREATE;

		if ($this->validate()) {
			$insert = $this->db->query("INSERT INTO customer_group (name, normalPayments, vipPayments, discount, maxReservationHours) VALUES (?,?,?,?,?)", [
				$this->name,
                $this->normalPayments,
                $this->vipPayments,
                $this->discount,
                $this->maxReservationHours
			]);

			if ($insert > 0) {
				$this->customerGroupId = $this->db->lastInsertId();
				return $this;
			} else {
				return DatabaseError::getErrorDescription(DatabaseError::INSERT);
			}
		} else {
			return Validate::getValidationSummaryText();
		}
	}

	/**
	 * Updates CustomerGroup by customerGroupId.
	 * @return CustomerGroup|string
	 */
	public function update()
	{
		$this->scenario = self::SCENARIO_UPDATE;

		if ($this->validate()) {
			$update = $this->db->query("UPDATE customer_group SET name = ?, normalPayments = ?, vipPayments = ?, discount = ?, maxReservationHours = ? WHERE customerGroupId = ?", [
				$this->name,
                $this->normalPayments,
                $this->vipPayments,
                $this->discount,
                $this->maxReservationHours,
                $this->customerGroupId
			]);

			if ($update > 0) {
				return $this;
			} else {
				return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
			}
		} else {
			return Validate::getValidationSummaryText();
		}
	}

	/**
	 * Deletes CustomerGroup by customerGroupId.
	 * @return bool|string
	 */
	public function delete()
	{
		$delete = $this->db->query("DELETE FROM customer_group WHERE customerGroupId = ?", [
			$this->customerGroupId
		]);

		if ($delete > 0) {
			return true;
		} else {
			return DatabaseError::getErrorDescription(DatabaseError::DELETE);
		}
	}

	/**
	 * Validates if CustomerGroup attributes are complete and valid.
	 * @return bool
	 */
	public function validate()
	{
		$validation = new Validate();

		if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_LOAD) {
			$validation->validateRequired([
				'customerGroupId' => $this->customerGroupId,
			]);
			$validation->validateNumeric([
				'customerGroupId' => $this->customerGroupId,
			]);
		}

		if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
			$validation->validateRequired([
				'jméno' => $this->name
			]);

			$validation->validateNumeric([
				'maximální délka rezervace' => $this->maxReservationHours,
			]);
		}

		if (!$validation->isValidationResult()) {
			unset($_SESSION['validationSummary']);
			$_SESSION['validationSummary'] = $validation->getValidationSummary();
			return false;
		} else {
			return true;
		}
	}

	/**
	 * @return array|string
	 */
	public static function getCustomerGroups() {
		$db = MyDB::getConnection();
		$customerGroupData = $db->queryAll("SELECT * FROM customer_group", [
		]);

		if (count($customerGroupData) < 1)
			return [];
		else if (!(new Validate())->validateNotNull([$customerGroupData]))
			return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst zákaznické skupiny z DB');

		$customerGroups = [];
		foreach ($customerGroupData as $customerGroup) {
			$customerGroups[] = new CustomerGroup(
				$customerGroup['customerGroupId'],
				$customerGroup['name'],
				$customerGroup['normalPayments'],
				$customerGroup['vipPayments'],
				$customerGroup['discount'],
				$customerGroup['maxReservationHours']
			);
		}

		return $customerGroups;
	}

    /**
     * @return array
     */
    public function _toArray()
    {
        return [
            'customerGroupId' => $this->customerGroupId,
            'name' => $this->name,
            'normalPayments' => $this->normalPayments,
            'vipPayments' => $this->vipPayments,
            'discount' => $this->discount,
            'maxReservationHours' => $this->maxReservationHours
        ];
    }

    /**
     * @return int
     */
    public function getCustomerGroupId()
    {
        return $this->customerGroupId;
    }

    /**
     * @param int $customerGroupId
     */
    public function setCustomerGroupId($customerGroupId)
    {
        $this->customerGroupId = $customerGroupId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getNormalPayments()
    {
        return $this->normalPayments;
    }

    /**
     * @param int $normalPayments
     */
    public function setNormalPayments($normalPayments)
    {
        $this->normalPayments = $normalPayments;
    }

    /**
     * @return int
     */
    public function getVipPayments()
    {
        return $this->vipPayments;
    }

	/**
	 * @return bool
	 */
	public function getVipPaymentsBool() {
    	return boolval($this->vipPayments);
    }

    /**
     * @param int $vipPayments
     */
    public function setVipPayments($vipPayments)
    {
        $this->vipPayments = $vipPayments;
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return int
     */
    public function getMaxReservationHours()
    {
        return $this->maxReservationHours;
    }

    /**
     * @param int $maxReservationHours
     */
    public function setMaxReservationHours($maxReservationHours)
    {
        $this->maxReservationHours = $maxReservationHours;
    }
}