<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 14.09.2017
 * Time: 19:04
 */

namespace backend\models;


use backend\controllers\CartController;
use backend\database\DatabaseError;
use backend\database\MyDB;

class OrderProduct
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;

    private $db;
    private $scenario;

    private $orderProductId;
    private $productId;
    private $orderId;
    private $additionToProductId;
    private $name;
    private $price;
    private $priceWithVat;
    private $quantity;

	/**
	 * OrderProduct constructor.
	 *
	 * @param int $orderProductId
	 * @param int $productId
	 * @param int $orderId
	 * @param int $additionToProductId
	 * @param string $name
	 * @param float $price
	 * @param float $priceWithVat
	 * @param int $quantity
	 */
    public function __construct($orderProductId = 0, $productId = 0, $orderId = 0, $additionToProductId = 0, $name = "", $price = 0.0, $priceWithVat = 0.0, $quantity = 0)
    {

        $this->db = MyDB::getConnection();
        $this->orderProductId = $orderProductId;
        $this->productId = $productId;
        $this->orderId = $orderId;
        $this->additionToProductId = $additionToProductId;

        $this->setOrderProduct($name, $price, $priceWithVat, $quantity);
    }

    /**
     * Loads OrderProduct from DB by orderProductId.
     * @return OrderProduct|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
            $orderProductData = $this->db->queryOne("SELECT * FROM order_product WHERE orderProductId = ?", [
                $this->orderProductId
            ]);

            if (!(new Validate())->validateNotNull([$orderProductData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst produkt objednávky z DB');

            $this->orderProductId = $orderProductData['orderProductId'];
            $this->productId = $orderProductData['productId'];
            $this->orderId = $orderProductData['orderId'];
            $this->additionToProductId = $orderProductData['additionToProductId'];
            $this->setOrderProduct(
                $orderProductData['name'],
                $orderProductData['price'],
                $orderProductData['priceWithVat'],
                $orderProductData['quantity']
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Inserts new OrderProduct to DB.
     * No params required because all data are gained inside constructor.
     * @return OrderProduct|string
     */
    public function create()
    {
        $this->scenario = self::SCENARIO_CREATE;

        if ($this->validate()) {
            $insert = $this->db->query("INSERT INTO order_product (productId, orderId, additionToProductId, name, price, priceWithVat, quantity) VALUES (?,?,?,?,?,?,?)", [
                $this->productId,
                $this->orderId,
                $this->additionToProductId,
                $this->name,
                $this->price,
                $this->priceWithVat,
                $this->quantity
            ]);

            if ($insert > 0) {
                $this->orderProductId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Updates OrderProduct by orderProductId.
     * @return OrderProduct|string
     */
    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;

        if ($this->validate()) {
            $update = $this->db->query("UPDATE order_product SET productId = ?, orderId = ?, additionToProductId = ?, name = ?, price = ?, priceWithVat = ?, quantity = ? WHERE orderProductId = ?", [
                $this->productId,
                $this->orderId,
                $this->additionToProductId,
                $this->name,
                $this->price,
                $this->priceWithVat,
                $this->quantity,
                $this->orderProductId
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Deletes OrderProduct by orderProductId.
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->query("DELETE FROM order_product WHERE orderProductId = ?", [
            $this->orderProductId
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * Validates if OrderProduct attributes are complete and valid.
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        /*if ($this->scenario == self::SCENARIO_LOAD) {
            $validation->validateRequired([
                'orderTermId' => $this->orderTermId,
            ]);
            $validation->validateNumeric([
                'orderTermId' => $this->orderTermId,
            ]);
        }*/

        if ($this->scenario == self::SCENARIO_UPDATE) {
            $validation->validateRequired([
                'orderProductId' => $this->orderProductId,
            ]);
            $validation->validateNumeric([
                'orderProductId' => $this->orderProductId,
            ]);
        }

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'productId' => $this->productId,
                'orderId' => $this->orderId,
                'název' => $this->name,
                'cena' => $this->price,
                'cena bez DPH' => $this->priceWithVat,
                'množství' => $this->quantity,
            ]);

            $validation->validateNumeric([
                'productId' => $this->productId,
                'orderId' => $this->orderId,
                'cena' => $this->price,
                'cena bez DPH' => $this->priceWithVat,
                'množství' => $this->quantity,
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param $order
     * @param bool $isOrderId
     * @param bool $withAdditions
     * @return array|string
     */
    public static function getOrderProducts($order, $isOrderId = false, $withAdditions = true)
    {
        $db = MyDB::getConnection();
        if($withAdditions) {
            $productsData = $db->queryAll("SELECT *, 
                order_product.name AS opname,  
                order_product.price AS opprice,
                order_product.priceWithVat AS oppricewithvat,
                order_product.quantity AS opquantity 
                FROM order_product LEFT JOIN product ON order_product.productId = product.productId WHERE orderId = ? ORDER BY opname", [
                (($isOrderId) ? $order : $order->getOrderId())
            ]);
        } else {
            $productsData = $db->queryAll("SELECT *, 
                order_product.name AS opname,  
                order_product.price AS opprice,
                order_product.priceWithVat AS oppricewithvat,
                order_product.quantity AS opquantity 
                FROM order_product LEFT JOIN product ON order_product.productId = product.productId WHERE orderId = ? AND additionToProductId = 0 ORDER BY opname", [
                (($isOrderId) ? $order : $order->getOrderId())
            ]);
        }

        if (count($productsData) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$productsData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst produkty v košíku z DB');

        $products = [];
        foreach ($productsData as $index => $productData) {
            $products[] = (new Product(
                $productData['productId'],
                $productData['opname'],
                $productData['nameSlug'],
                $productData['additionToProductId'], // warning - description field is used to carry additionToProductId
                '','',
                $productData['opprice'],
                $productData['oppricewithvat'],
                $productData['opquantity'],
                $productData['visible'],
                $productData['created'],
                $productData['changed']
            ))->_toArray();

            // load image
            if(isset($productData['productId']) && is_numeric($productData['productId'])) {
                $productImage = ProductImage::getProductImages($productData['productId'], ProductImage::TYPE_MAIN);
                (count($productImage) > 0) ? $products[$index]['image'] = $productImage[0]['name'] : null;
            }
        }

        return $products;
    }

    /**
     * @param $order
     * @param bool $isOrderId
     * @return array|string
     */
    public static function getAdditionalOrderProducts($order, $isOrderId = false)
    {
        $db = MyDB::getConnection();
        $productsData = $db->queryAll("SELECT *, 
                order_product.name AS opname,  
                order_product.price AS opprice,
                order_product.priceWithVat AS oppricewithvat,
                order_product.quantity AS opquantity 
                FROM order_product LEFT JOIN product ON order_product.productId = product.productId WHERE orderId = ? AND additionToProductId <> 0 ORDER BY opname", [
            (($isOrderId) ? $order : $order->getOrderId())
        ]);

        if (count($productsData) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$productsData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst produkty v košíku z DB');

        $products = [];
        foreach ($productsData as $index => $productData) {
            $products[] = (new Product(
                $productData['productId'],
                $productData['opname'],
                $productData['nameSlug'],
                $productData['additionToProductId'], // warning - description field is used to carry additionToProductId
                '',
                $productData['opprice'],
                $productData['oppricewithvat'],
                $productData['opquantity'],
                $productData['visible'],
                $productData['created'],
                $productData['changed']
            ))->_toArray();

            // load image
            if(isset($productData['productId']) && is_numeric($productData['productId'])) {
                $productImage = ProductImage::getProductImages($productData['productId'], ProductImage::TYPE_MAIN);
                (count($productImage) > 0) ? $products[$index]['image'] = $productImage[0]['name'] : null;
            }
        }

        return $products;
    }

    /**
     * @param $order
     * @param bool $isOrderId
     * @param $productId
     * @return array|string
     */
    public static function getAdditionalOrderProductsToProductId($order, $isOrderId = false, $productId)
    {
        $db = MyDB::getConnection();
        $productsData = $db->queryAll("SELECT *, 
                order_product.name AS opname,  
                order_product.price AS opprice,
                order_product.priceWithVat AS oppricewithvat,
                order_product.quantity AS opquantity 
                FROM order_product LEFT JOIN product ON order_product.productId = product.productId WHERE orderId = ? AND additionToProductId = ? ORDER BY opname", [
            (($isOrderId) ? $order : $order->getOrderId()),
            $productId
        ]);

        if (count($productsData) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$productsData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst produkty v košíku z DB');

        $products = [];
        foreach ($productsData as $index => $productData) {
            $products[] = (new Product(
                $productData['productId'],
                $productData['opname'],
                $productData['nameSlug'],
                $productData['additionToProductId'], // warning - description field is used to carry additionToProductId
                '',
                $productData['opprice'],
                $productData['oppricewithvat'],
                $productData['opquantity'],
                $productData['visible'],
                $productData['created'],
                $productData['changed']
            ))->_toArray();

            // load image
            if(isset($productData['productId']) && is_numeric($productData['productId'])) {
                $productImage = ProductImage::getProductImages($productData['productId'], ProductImage::TYPE_MAIN);
                (count($productImage) > 0) ? $products[$index]['image'] = $productImage[0]['name'] : null;
            }
        }

        return $products;
    }

    /**
     * @param $customerId
     * @param $orderId
     * @param $db
     * @return array|bool|string
     */
    public static function copyCartToOrder($customerId, $orderId, $db) {

        $cartProducts = CartController::getCartProducts();

        //var_dump($cartProducts);

        if(is_array($cartProducts)){
            $result = true;
            $error = '';
            foreach ($cartProducts as $cartProduct) {
                $orderProduct = new OrderProduct(0, $cartProduct['productId'], $orderId, $cartProduct['description'], $cartProduct['name'], $cartProduct['price'], $cartProduct['priceWithVat'], $cartProduct['quantity']);
                // same connection for transaction
                $orderProduct->setDb($db);
                // and create new order product
                $orderProduct = $orderProduct->create();
                if(!is_a($orderProduct, OrderProduct::class)) {
                    $result = false;
                    $error .= $orderProduct;
                }
            }

            // delete old cart products
            $removeResult = Cart::removeCartProducts($customerId, $db);

            if($removeResult && $result){
                return true;
            } else {
                return 'Chyba. Nepodařilo se přesunout data o produktech. ' . $error;
            }
        } else {
            return $cartProducts;
        }
    }

    /**
     * @param $orderId
     * @param $oldOrderId
     * @param $db
     * @return array|bool|string
     */
    public static function copyFromOrder($orderId, $oldOrderId, $db) {
        $oldOrder = new Order();
        $oldOrder->setOrderId($oldOrderId);
        $oldOrder = $oldOrder->load();
        if(is_a($oldOrder,Order::class)) {
            $orderProducts = OrderProduct::getOrderProducts($oldOrder);

            if (is_array($orderProducts)) {
                $result = true;
                $error = '';
                foreach ($orderProducts as $cartProduct) {
                    $orderProduct = new OrderProduct(0, $cartProduct['productId'], $orderId, $cartProduct['description'], $cartProduct['name'], $cartProduct['price'], $cartProduct['priceWithVat'], $cartProduct['quantity']);
                    // same connection for transaction
                    $orderProduct->setDb($db);
                    // and create new order product
                    $orderProduct = $orderProduct->create();
                    if (!is_a($orderProduct, OrderProduct::class)) {
                        $result = false;
                        $error .= $orderProduct;
                    }
                }

                if ($result) {
                    return true;
                } else {
                    return 'Chyba. Nepodařilo se přesunout data o produktech. ' . $error;
                }
            } else {
                return $orderProducts;
            }
        } else {
            return 'Chyba. Nepodařilo se najít objednávku.';
        }
    }

    /**
     * @param $orderId
     * @return int
     */
    public static function getSumOfProductsInOrder($orderId) {
        $customerId = CartController::getCustomerId();

        if(isset($customerId) && is_numeric($customerId)) {
            $db = MyDB::getConnection();
            $sum = $db->queryOne("SELECT SUM(quantity) as sum FROM order_product WHERE orderId = ?", [
                $orderId
            ]);

            return $sum['sum'];
        } else {
            return 0;
        }
    }

    /**
     * Group setter
     * @param $name
     * @param $price
     * @param $priceWithVat
     * @param $quantity
     */
    public function setOrderProduct($name, $price, $priceWithVat, $quantity)
    {
        $this->name = $name;
        $this->price = $price;
        $this->priceWithVat = $priceWithVat;
        $this->quantity = $quantity;
    }

    /**
     * @param MyDB $db
     */
    public function setDb($db)
    {
        $this->db = $db;
    }
}