<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 07.08.2017
 * Time: 21:10
 */

namespace backend\models;

use backend\database\DatabaseError;
use backend\database\MyDB;

class Video
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_LOAD = 4;

    protected $db;
    protected $scenario;

    protected $videoId;
    protected $productId;
    protected $hash;

    protected $tableName = 'video';

    /**
     * Video constructor.
     * @param int $videoId
     * @param int $productId
     * @param string $hash
     */
    public function __construct($videoId = 0, $productId = 0, $hash = "")
    {
        $this->db = MyDB::getConnection();
        $this->videoId = $videoId;
        $this->productId = $productId;

        $this->setVideo($hash);
    }

    /**
     * Loads Video from DB by videoId.
     * @return Video|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
            $videoData = $this->db->queryOne('SELECT * FROM video WHERE videoId = ? OR productId = ?', [
                $this->videoId,
                $this->productId
            ]);

            if (!(new Validate())->validateNotNull([$videoData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst video z DB');

            $this->videoId = $videoData['videoId'];
            $this->productId = $videoData['productId'];
            $this->setVideo(
                $videoData['hash']
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Inserts new Video to DB.
     * No params required because all data are gained inside constructor.
     * @return Video|string
     */
    public function create()
    {
        $this->scenario = self::SCENARIO_CREATE;

        if ($this->validate()) {
            $insert = $this->db->query("INSERT INTO video (productId, hash) VALUES (?,?)", [
                $this->productId,
                $this->hash
            ]);

            if ($insert > 0) {
                $this->videoId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Deletes Video by videoId.
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->query("DELETE FROM video WHERE videoId = ?", [
            $this->videoId
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * Validates if Video attributes are complete and valid.
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        /*if ($this->scenario == self::SCENARIO_LOAD) {
            $validation->validateRequired([
                'videoId' => $this->videoId,
            ]);
            $validation->validateNumeric([
                'videoId' => $this->videoId,
            ]);
        }*/

        if ($this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'productId' => $this->productId,
                //'hash' => $this->hash
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    public function _toArray(){
        return [
            'videoId' => $this->videoId,
            'productId' => $this->productId,
            'hash' => $this->hash
        ];
    }

    /**
     * Group setter
     * @param $hash
     */
    public function setVideo($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @param $productId
     * @return array|string
     */
    public static function getProductVideo($productId) {
        $db = MyDB::getConnection();
        $productVideoData = $db->queryOne("SELECT * FROM video WHERE productId = ? LIMIT 1", [
            $productId
        ]);

        if (count(array($productVideoData)) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$productVideoData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst videa produktů z DB');

        return (new Video($productVideoData['videoId'], $productVideoData['productId'], $productVideoData['hash']))->_toArray();
    }
}