<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 28.07.2017
 * Time: 7:59
 */

namespace backend\models;


use backend\database\DatabaseError;
use backend\database\MyDB;

class ProductData
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;

    protected $db;
    protected $scenario;

    protected $tableName = 'product_data';
    private $type;
    private $data;
    private $productId;
    private $productDataId;

    const TYPE_PRICE_INCLUDED = 'price_included';
    const TYPE_TECHNICAL = 'technical';

    /**
     * ProductData constructor.
     * @param int $productDataId
     * @param int $productId
     * @param string $type
     * @param string $data
     */
    public function __construct($productDataId = 0, $productId = 0, $type = self::TYPE_PRICE_INCLUDED, $data = '')
    {
        $this->db = MyDB::getConnection();

        $this->productDataId = $productDataId;
        $this->productId = $productId;
        $this->setProductData($type, $data);
    }

    /**
     * @return ProductData|mixed|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
            $productData = $this->db->queryOne('SELECT * FROM product_data WHERE productDataId = ?', [
                $this->productDataId
            ]);

            if (!(new Validate())->validateNotNull([$productData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst data produktu z DB');

            $this->productId = $productData['productId'];
            $this->setProductData(
                $productData['type'],
                $productData['data']
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return ProductData|mixed|string
     */
    public function create()
    {
        $this->scenario = self::SCENARIO_CREATE;

        if ($this->validate()) {
            $insert = $this->db->query("INSERT INTO product_data (productId, type, data) VALUES (?, ?, ?)", [
                $this->productId,
                $this->type,
                $this->data
            ]);

            if ($insert > 0) {
                $this->productDataId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return ProductData|mixed|string
     */
    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;

        if ($this->validate()) {
            $update = $this->db->query("UPDATE product_data SET productId = ?, type = ?, data = ? WHERE productDataId= ?", [
                $this->productId,
                $this->type,
                $this->data,
                $this->productDataId
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->query("DELETE FROM product_data WHERE productDataId = ?", [
            $this->productDataId
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * Validates if ProductData attributes are complete and valid.
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_LOAD) {
            $validation->validateRequired([
                'productDataId' => $this->productDataId,
            ]);
            $validation->validateNumeric([
                'productDataId' => $this->productDataId,
            ]);
        }

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'productId' => $this->productId,
                'type' => $this->type,
                'data' => $this->data,
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param $productId
     * @param $type
     * @param bool $asArray
     * @return array|string
     */
    public static function getProductData($productId, $type, $asArray = true) {
        $db = MyDB::getConnection();
        $productData = $db->queryAll("SELECT * FROM product_data WHERE productId = ? AND type = ?", [
            $productId,
            $type
        ]);

        if (count($productData) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$productData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst fotografie produktů z DB');

        $productDataArray = [];
        foreach ($productData as $product) {
            $productDataObject = new ProductData(
                $product['productDataId'],
                $product['productId'],
                $product['type'],
                $product['data']
            );
            if($asArray)
                $productDataArray[] = $productDataObject->_toArray();
            else
                $productDataArray[] = $productDataObject;
        }

        return $productDataArray;
    }

    /**
     * @return array
     */
    public function _toArray(){
        return [
            'productDataId' => $this->productDataId,
            'productId' => $this->productId,
            'type' => $this->type,
            'data' => $this->data
        ];
    }

    /**
     * @return array
     */
    public function jsonSerialize() {
        return [
            'productDataId' => $this->productDataId,
            'productId' => $this->productId,
            'type' => $this->type,
            'data' => $this->data
        ];
    }

    /**
     * Group setter
     * @param $type
     * @param $data
     */
    public function setProductData($type, $data) {
        $this->type = $type;
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return int
     */
    public function getProductDataId()
    {
        return $this->productDataId;
    }

    /**
     * @param int $productDataId
     */
    public function setProductDataId($productDataId)
    {
        $this->productDataId = $productDataId;
    }

}