<?php
class Mailer{

	private $addresses;
	private $subject;
	private $message;
	private $from;
	private $headers;

	//pokud hledám chybu tak je v tom, že neposílám adresy jako array

	public function __construct($addresses){
		$this->addresses = $addresses;
		if(count($addresses) < 1){
			die("Mailer: Nebyla obdržena žádná emailová adresa.");
		}

        // Always set content-type when sending HTML email
        $this->headers = "MIME-Version: 1.0" . "\r\n";
        $this->headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	}

	public function sendLostPassword($token){
		$subject = "Obnovení hesla pro PouzdraProMobily.cz";
		$message = '<html>
					    <head>
                            <title>Obnovení hesla pro eshop PouzdraProMobily.cz</title>
                        </head>
                        <body style="margin: 0px auto; font-family: \'Trebuchet MS\', \'Lucida Sans Unicode\', sans-serif; overflow: auto; text-align: center; width: 800px;">
                        	<h2 style="background-color: #3D2914; color: white; margin: 0px; padding: 5px;">Zapomenuté heslo - www.PouzdraProMobily.cz</h2>
                        	<p>Na eshopu www.PouzdraProMobily.cz bylo vyžádáno <b>obnovení hesla</b> k účtu s Vaší emailovou adresou.<br /><br/>
                        	Heslo si můžete změnit po kliknutí na následující odkaz: <a href="http://www.pouzdrapromobily.cz/pages/lost-password.php?token='.$token.'&email='.$this->addresses[0].'" title="Změna hesla">http://www.pouzdrapromobily.cz/pages/lost-password.php?token='.$token.'&email='.$this->addresses[0].'</a>
                        	</p>

                        	<p><small>K vašim službám<br />eshop www.PouzdraProMobily.cz</small><br /><br /><small>Pokud jste nežádali o změnu hesla, tento email ignorujte.</small></p>
                        </body>
					</html>';

		return $this->sendEmail($subject, $message, null);
	}

	public function sendChangePassword($token){
		$subject = "Změna hesla na PouzdraProMobily.cz";
		$message = '<html>
					    <head>
                            <title>Změna hesla pro eshop PouzdraProMobily.cz</title>
                        </head>
                        <body style="margin: 0px auto; font-family: \'Trebuchet MS\', \'Lucida Sans Unicode\', sans-serif; overflow: auto; text-align: center; width: 800px;">
                        	<h2 style="background-color: #3D2914; color: white; margin: 0px; padding: 5px;">Změna hesla - www.PouzdraProMobily.cz</h2>
                        	<p>Na eshopu www.PouzdraProMobily.cz byla <b>provedena změna hesla</b> k účtu s Vaší emailovou adresou.<br /><br/>
                        	Pokud jste změnu hesla neprováděli, pravděpodobně někdo získal přístup k Vašemu účtu. 
                        	Abyste zabránili zneužití, vytvořte si nové heslo po kliknutí na následující odkaz: <a href="http://www.pouzdrapromobily.cz/pages/lost-password.php?token='.$token.'&email='.$this->addresses[0].'" title="Změna hesla">http://www.pouzdrapromobily.cz/pages/lost-password.php?token='.$token.'&email='.$this->addresses[0].'</a> Nezadávejte již použité heslo, ale vytvořte si nové a bezpečné (min. 8 znaků, velká a malá písmena, číslice a případně speciální znaky).
                        	<br /><br />Pokud jste změnu hesla provedli vy, tento email ignorujte.
                        	</p>

                        	<p><small>K vašim službám<br />eshop www.PouzdraProMobily.cz</small></p>
                        </body>
					</html>';

		return $this->sendEmail($subject, $message, null);
	}

	//!warning! pevná hodnota DPH - původně
	public function sendOrder($customerId, $orderId, $kosik, $fakturacniUdaje, $dodaciUdaje, $kontaktniUdaje, $doprava, $platba, $dopravaPlatbaCena, $maxDeliveryTime, $note, $kosikCena){
		$subject = "Potvrzení objednávky - PouzdraProMobily.cz";
		$message = '<html>
				      <head>
				            <title>Potvrzení objednávky v eshopu PouzdraProMobily.cz</title>
				      </head>
				      <body style="margin: 0px auto; font-family: \'Trebuchet MS\', \'Lucida Sans Unicode\', sans-serif; overflow: auto; text-align: center; width: 800px;">
				            <h2 style="background-color: #3D2914; color: white; margin: 0px; padding: 5px;">Potvrzení objednávky - <a href="http://www.PouzdraProMobily.cz" title="Pouzdra, kryty a obaly na mobilní telefony" style="color: white">www.PouzdraProMobily.cz</a></h2>
				            <p>Vaše objednávka (ID: '.md5($orderId).') byla přijata a vyřizuje se. O jejím průběhu Vás budeme informovat.</p>
				            <p><b>Online náhled této objednávky</b> včetně aktuálního stavu <b>naleznete ';
				            if($customerId == '0' || !is_numeric($customerId)){ 
				            	$message .= 'na adrese <a href="http://www.pouzdrapromobily.cz/pages/order/show-order.php?token='.md5(md5($orderId)).'" title="Online náhled objednávky">http://www.pouzdrapromobily.cz/pages/order/show-order.php?token='.md5(md5($orderId)).'</a> .';
				            } else {
				            	$message .= 'v administraci svého účtu ve složce Objednávky.';
				            }
				            $message .= '</b></p>

				            <h3 style="background-color: #3D2914; color: white; margin: 5px; padding: 5px;">Informace o objednávce</h3>
				            <h4 style="background-color: #FFCD83; color: #3D2914; margin: 5px; padding: 5px;">Kontaktní údaje</h4>
				            '.$kontaktniUdaje.'
				            <h4 style="background-color: #FFCD83; color: #3D2914; margin: 5px; padding: 5px;">Fakturační údaje</h4>
				            '.$fakturacniUdaje.'
				            <h4 style="background-color: #FFCD83; color: #3D2914; margin: 5px; padding: 5px;">Dodací údaje</h4>
				            '.$dodaciUdaje.'

				            <h3 style="background-color: #3D2914; color: white; margin: 5px; padding: 5px;">Výpis položek objednávky (prodejce není plátce DPH)</h3>
				            <table style="margin: 0px auto; text-align: center;">
				                <tr><th>Produkt</th><th>Počet</th><th>Cena za ks</th><th>Cena celkem</th></tr>';
				            	foreach ($kosik as $product){
				            		$message .= '<tr><td style="padding: 5px 10px; text-align: center;">'.$product["ProductName"].'</td><td style="padding: 5px 10px; text-align: center;">'.$product["Quantity"].' ks</td><td style="padding: 5px 10px; text-align: center;">'.$product["Price"].' Kč</td><td style="padding: 5px 10px; text-align: center;">'.($product["Price"]*$product["Quantity"]).' Kč</td></tr>';
				            	}
				                //zjisteni kuponu
				                //pokud je aktivovaný slevový kod tak ho pouziju
								if($_SESSION["couponName"] != null && $_SESSION["couponName"] != "" && $_SESSION["couponValue"] != null && $_SESSION["couponValue"] != "" && $_SESSION["couponType"] != null && $_SESSION["couponType"] != ""){
									if($_SESSION["couponType"] == "Percent"){
										//odečtu procenta
										if($_SESSION["couponValue"] <= 100){
											$kosikCena = (($kosikCena/100)*(100-$_SESSION["couponValue"]));
											$message .= '<tr><td colspan="4"><b>Sleva: '.round($_SESSION["couponValue"],0).'%</b></td></tr>';
										}
									} else if($_SESSION["couponType"] == "Money"){
										//odečtu prachy
										if($_SESSION["couponValue"] <= $kosikCena){
											$kosikCena = ($kosikCena-$_SESSION["couponValue"]);
											$message .= '<tr><td colspan="4"><b>Sleva: '.round($_SESSION["couponValue"], 0).' Kč</b></td></tr>';
										} else {
											//pokud je sleva vyšší než objednávka = 0				
											$message .= '<tr><td colspan="4"><b>Sleva: '.round($kosikCena, 0).' Kč</b></td></tr>';
										}
									}
								}
								$message .= '<tr><td colspan="4"><b>Celková cena za zboží: '.$kosikCena.' Kč</b></td></tr>';
				            $message .= '</table>

				            <h3 style="background-color: #3D2914; color: white; margin: 5px; padding: 5px;">Informace k platbě a dopravě</h3>
				            <p>Doprava: '.$doprava["Name"].'</p>
				            <p>Plaba: '.$platba["Name"].'</p>
				            <p><b>Cena za dopravu a platbu: '.$dopravaPlatbaCena.' Kč</b></p>
				            <p>Dodatečné informace: '.$platba["Description"].'</p>
				            <p>';
							if($maxDeliveryTime > 0){
								$message .= 'Omlouváme se, ale některé položky ve Vaší objednávce nyní nejsou skladem. Budeme se snažit dostat je k Vám co nejdříve. U této objednávky by to nemělo trvat déle než ';
								switch ($maxDeliveryTime) {
									case 1:
										$message .= 'jeden den.';
										break;
									case 2:
									case 3:
									case 4:
										$message .= $maxDeliveryTime.' dny.';
										break;
									default:
										$message .= $maxDeliveryTime.' dní.';
										break;
								}
							} else {
								$message .= 'Všechny položky z Vaší objednávky máme nyní skladem.';
							}
							$message .= '</p>

				            <h3 style="background-color: #3D2914; color: white; margin: 5px; padding: 5px;">Shrnutí</h3>
				            <p><b>Cena celkem: '.round(($kosikCena+$dopravaPlatbaCena), 0).' Kč</b></p>';
				            //!warning! vyhozeno DPH
				            //<p><b>Bez DPH: '.round((($kosikCena+$dopravaPlatbaCena)/1.21), 0).' Kč</b></p>';

				            if($note != "" && $note != null)
				            	$message .= '<p>Poznámka: '.$note.'</p>';

				            $message .= '<p><br /><b>Děkujeme za Váš nákup<br />eshop www.PouzdraProMobily.cz</b></p>
				      </body>
				</html>';

		return $this->sendEmail($subject, $message, null);
	}

	public function sendOrderInfo($orderId, $status){
		$subject = "Změna stavu objednávky z eshopu PouzdraProMobily.cz";
		$message = '<html>
				      <head>
				            <title>Změna stavu objednávky z eshopu PouzdraProMobily.cz</title>
				      </head>
				      <body style="margin: 0px auto; font-family: \'Trebuchet MS\', \'Lucida Sans Unicode\', sans-serif; overflow: auto; text-align: center; width: 800px;">
				            <h2 style="background-color: #3D2914; color: white; margin: 0px; padding: 5px;">Změna stavu objednávky č. '.$orderId.'</h2>
				            <p>Nový stav objednávky je <b>'.$status.'</b></p>
				            <p><br /><br />eshop www.PouzdraProMobily.cz</b></p>
				      </body>
				</html>';

		return $this->sendEmail($subject, $message, null);
	}

	public function sendAdminEmail($subject, $messageText){
		$message = '<html>
				      <head>
				            <title>'.$subject.'</title>
				      </head>
				      <body style="margin: 0px auto; font-family: \'Trebuchet MS\', \'Lucida Sans Unicode\', sans-serif; overflow: auto; text-align: center; width: 800px;">
				            <h2 style="background-color: #3D2914; color: white; margin: 0px; padding: 5px;">'.$subject.'</h2>
				            <p>'.$messageText.'</p>
				            <p><br /><br />eshop www.PouzdraProMobily.cz</b></p>
				      </body>
				</html>';

		return $this->sendEmail($subject, $message, null);
	}

	public function sendContactMessage($contactFrom, $contactMessage){
		$subject = "Dotaz z eshopu PouzdraProMobily.cz";
		$message = '<html>
				      <head>
				            <title>Dotaz z eshopu PouzdraProMobily.cz</title>
				      </head>
				      <body style="margin: 0px auto; font-family: \'Trebuchet MS\', \'Lucida Sans Unicode\', sans-serif; overflow: auto; text-align: center; width: 800px;">
				            <h2 style="background-color: #3D2914; color: white; margin: 0px; padding: 5px;">Nová zpráva - <a href="http://www.PouzdraProMobily.cz" title="Pouzdra, kryty a obaly na mobilní telefony" style="color: white">www.PouzdraProMobily.cz</a></h2>
				            <p>Od '.$contactFrom.' byla přijata nová zpráva</p>
				            
				            <h3 style="background-color: #3D2914; color: white; margin: 5px; padding: 5px;">Obsah zprávy:</h3>
				            <p>'.$contactMessage.'</p>
				            <p><br /><br />eshop www.PouzdraProMobily.cz</b></p>
				      </body>
				</html>';

		return $this->sendEmail($subject, $message, $contactFrom);
	}

	public function sendRequestMessage($requestFrom, $requestMessage){
		$subject = "Dotaz z eshopu PouzdraProMobily.cz";
		$message = '<html>
				      <head>
				            <title>Dotaz z eshopu PouzdraProMobily.cz</title>
				      </head>
				      <body style="margin: 0px auto; font-family: \'Trebuchet MS\', \'Lucida Sans Unicode\', sans-serif; overflow: auto; text-align: center; width: 800px;">
				            <h2 style="background-color: #3D2914; color: white; margin: 0px; padding: 5px;">Nový dotaz - <a href="http://www.PouzdraProMobily.cz" title="Pouzdra, kryty a obaly na mobilní telefony" style="color: white">www.PouzdraProMobily.cz</a></h2>
				            <p>Od '.$requestFrom.' byl přijat nový dotaz na zboží.</p>
				            
				            <h3 style="background-color: #3D2914; color: white; margin: 5px; padding: 5px;">Obsah dotazu:</h3>
				            <p>'.$requestMessage.'</p>
				            <p><br /><br />eshop www.PouzdraProMobily.cz</b></p>
				      </body>
				</html>';

		return $this->sendEmail($subject, $message, $requestFrom);
	}

	public function sendEmail($subject, $message, $from){
		$this->subject = $subject;
		$this->message = $message;
		$this->from = $from;

		return $this->doSend();
	}

	//!warning! pevné hodnoty emailů
	private function doSend(){	
	    //odesilatel
		if($this->from != "" && $this->from != null){		
	        $this->headers .= 'From: <'.$this->from.'>' . "\r\n";
		} else {			
	        $this->headers .= 'From: Eshop www.PouzdraProMobily.cz <info@pouzdrapromobily.cz>' . "\r\n";
		}	

		//do to vypisu vsechny adresy		
		$to = implode(", ", $this->addresses);
		/*$counter = 0;
		foreach ($this->addresses as $address) {
			if($counter > 0){
				$to .= ", ".$address;
			} else {
				$to .= $address;
			}
		}*/

		//zjistim jestli jsem v adresach - jestli ne tak poslu sobe skrytou kopii
		if(strpos($to, "ondrasek.simecek@gmail.com") !== false){}
		else
			$this->headers .= 'Bcc: ondrasek.simecek@gmail.com\r\n';
		//echo $this->headers;

		//a poslu
        return mail($to,$this->subject,$this->message,$this->headers);
        /*if($send != false){
            return true;
        } else {
        	return false;
        }*/
	}
}
?>