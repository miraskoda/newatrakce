<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 27.11.2017
 * Time: 20:55
 */

namespace backend\models;


use backend\controllers\CartController;
use backend\database\DatabaseError;
use backend\database\MyDB;
use DateTime;

class Reservation
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;

    private $db;
    private $scenario;

    private $reservationId;
    private $customerId;
    private $productId;
    private $quantity;
    private $termStart;
    private $termHours;
    private $reservationStart;
    private $reservationHours;
    private $active;

    /**
     * Reservation constructor.
     * @param int $reservationId
     * @param int $customerId
     * @param int $productId
     * @param int $quantity
     * @param string $termStart
     * @param int $termHours
     * @param string $reservationStart
     * @param int $reservationHours
     * @param int $active
     */
    public function __construct($reservationId = 0, $customerId = 0, $productId = 0, $quantity = 1, $termStart = '1970-01-01 00:00:00', $termHours = 1, $reservationStart = '1970-01-01 00:00:00', $reservationHours = 1, $active = 1)
    {
        $this->db = MyDB::getConnection();

        $this->reservationId = $reservationId;
        $this->customerId = $customerId;
        $this->productId = $productId;
        $this->active = $active;

        $this->setReservation($quantity, $termStart, $termHours, $reservationStart, $reservationHours);
    }

    /**
     *
     * Returns an array of IDs that are reserved between provided time
     *
     * @param $productId
     * @param $dateFrom
     * @param $termHours
     *
     * @return array
     */
    public function areThereConflicts($productId, $dateFrom, $termHours) {
        $dateTo = date("Y-m-d H:i:s", strtotime(sprintf("+%d hours", $termHours), strtotime($dateFrom)));
        $reservationData = $this->db->queryAll( "SELECT reservationId FROM reservation WHERE active = 1 AND productId = ? AND termStart BETWEEN ? AND ?;", [
            $productId,
            $dateFrom,
            $dateTo,
        ] );
        return $reservationData;
    }

	/**
	 * Loads Reservation from DB by reservationId.
	 *
	 * @param bool $customerRequired
	 *
	 * @return Reservation|string
	 */
    public function load($customerRequired = false)
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
        	if(!$customerRequired) {
		        $reservationData = $this->db->queryOne( "SELECT * FROM reservation WHERE reservationId = ?", [
			        $this->reservationId
		        ] );
	        } else {
		        $reservationData = $this->db->queryOne( "SELECT * FROM reservation WHERE reservationId = ? AND customerId = ?", [
			        $this->reservationId,
			        $this->customerId
		        ] );
	        }

            if (!(new Validate())->validateNotNull([$reservationData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst rezervaci z DB');

            $this->reservationId = $reservationData['reservationId'];
            $this->customerId = $reservationData['customerId'];
            $this->productId = $reservationData['productId'];
            $this->active = $reservationData['active'];
            $this->setReservation(
                $reservationData['quantity'],
                $reservationData['termStart'],
                $reservationData['termHours'],
                $reservationData['reservationStart'],
                $reservationData['reservationHours']
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Inserts new Reservation to DB.
     * @return Reservation|string
     */
    public function create()
    {
        $this->scenario = self::SCENARIO_CREATE;

        if ($this->validate()) {
            $this->reservationStart = date("Y-m-d H:i:s");
            $insert = $this->db->query("INSERT INTO reservation (customerId, productId, quantity, termStart, termHours, reservationStart, reservationHours, active) VALUES (?,?,?,?,?,?,?,?)", [
                $this->customerId,
                $this->productId,
                $this->quantity,
                $this->termStart,
                $this->termHours,
                $this->reservationStart,
                $this->reservationHours,
                $this->active
            ]);

            if ($insert > 0) {
                $this->reservationId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Updates Reservation by reservationId.
     * @return Reservation|string
     */
    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;

        if ($this->validate()) {
            $update = $this->db->query("UPDATE reservation SET customerId = ?, productId = ?, quantity = ?, termStart = ?, termHours = ?, reservationStart = ?, reservationHours = ?, active = ? WHERE reservationId = ?", [
                $this->customerId,
                $this->productId,
                $this->quantity,
                $this->termStart,
                $this->termHours,
                $this->reservationStart,
                $this->reservationHours,
                $this->active,
                $this->reservationId
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Deletes Reservation by reservationId.
     * @return bool|string
     */
    public function delete()
    {
        /*$delete = $this->db->query("DELETE FROM reservation WHERE reservationId = ?", [
            $this->reservationId
        ]);*/

        // no delete only deactivate
	    $delete = $this->db->query("UPDATE reservation SET active = 0 WHERE reservationId = ?", [
		    $this->reservationId
	    ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * Validates if Reservation attributes are complete and valid.
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_LOAD) {
            $validation->validateRequired([
                'reservationId' => $this->reservationId,
            ]);
            $validation->validateNumeric([
                'reservationId' => $this->reservationId,
            ]);
        }

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'zákazník' => $this->customerId,
                'produkt' => $this->productId,
                'počet' => $this->quantity,
                'začátek termínu' => $this->termStart,
                'počet hodin termínu' => $this->termHours,
                'začátek rezervace' => $this->reservationStart,
                'počet hodin rezervace' => $this->reservationHours
            ]);

            $validation->validateNumeric([
                'zákazník' => $this->customerId,
                'produkt' => $this->productId,
                'počet' => $this->quantity,
                'počet hodin termínu' => $this->termHours,
                'počet hodin rezervace' => $this->reservationHours,
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param $quantity
     * @param $termStart
     * @param $termHours
     * @param $reservationStart
     * @param $reservationHours
     */
    private function setReservation($quantity, $termStart, $termHours, $reservationStart, $reservationHours) {
        $this->quantity = $quantity;
        $this->termStart = $termStart;
        $this->termHours = $termHours;
        $this->reservationStart = $reservationStart;
        $this->reservationHours = $reservationHours;
    }

    /**
     * @param bool $isAdmin
     * @return array|string
     */
    public static function getReservations($isAdmin = false) {
        $customerId = CartController::getCustomerId();

        if($isAdmin || (isset($customerId) && is_numeric($customerId))) {
            $db = MyDB::getConnection();
            if(!$isAdmin) {
                $reservationData = $db->queryAll("SELECT *, reservation.quantity AS rq, customerId AS cn, product.name AS pn 
														FROM reservation INNER JOIN product ON reservation.productId = product.productId 
														WHERE customerId = ? AND reservation.active = 1 
														ORDER BY reservationStart DESC", [
                    $customerId
                ]);
            } else {
                $reservationData = $db->queryAll("SELECT *, reservation.quantity AS rq, customer.name AS cn, product.name AS pn
														FROM 
														(reservation INNER JOIN product ON reservation.productId = product.productId)
														INNER JOIN customer ON reservation.customerId = customer.customerId 
														WHERE reservation.active = 1
														ORDER BY reservationStart DESC", [
                ]);
            }

            if (count($reservationData) < 1)
                return [];
            else if (!(new Validate())->validateNotNull([$reservationData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst rezervace z DB');

            $reservations = [];
            foreach ($reservationData as $reservation) {
                $reservations[] = new Reservation(
                    $reservation['reservationId'],
                    $reservation['cn'] . ' | ' . $reservation['customerId'],
                    $reservation['pn'],
                    $reservation['rq'],
                    $reservation['termStart'],
                    $reservation['termHours'],
                    $reservation['reservationStart'],
                    $reservation['reservationHours'],
                    $reservation['active']
                );
            }

            return $reservations;
        } else {
            return [];
        }
    }

    /**
     * Returns reservations in date.
     * Date should be in format YYYY-MM-DD
     *
     * @param $date
     * @return array|string
     */
    public static function getReservationsByDate($date) {
        $db = MyDB::getConnection();
        $reservationData = $db->queryAll("SELECT *, reservation.quantity AS rq, customer.name AS cn, product.name AS pn
                                                    FROM 
                                                    (reservation INNER JOIN product ON reservation.productId = product.productId)
                                                    INNER JOIN customer ON reservation.customerId = customer.customerId 
                                                    WHERE reservation.active = 1 AND DATE(termStart) = ?
                                                    ORDER BY termStart DESC", [
                $date
        ]);

        if (count($reservationData) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$reservationData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst rezervace z DB');

        $reservations = [];
        foreach ($reservationData as $reservation) {
            $reservations[] = (new Reservation(
                $reservation['reservationId'],
                $reservation['cn'] . ' | ' . $reservation['customerId'],
                $reservation['pn'],
                $reservation['rq'],
                $reservation['termStart'],
                $reservation['termHours'],
                $reservation['reservationStart'],
                $reservation['reservationHours'],
                $reservation['active']
            ))->_toArray();
        }

        return $reservations;
    }

    /**
     * @param $date
     * @return int
     */
    public static function getReservationCountByDate($date) {
        if($date == "")
            return 0;

        $db = MyDB::getConnection();
        $reservationCount = $db->query("SELECT *, reservation.quantity AS rq, customer.name AS cn, product.name AS pn
                                                    FROM 
                                                    (reservation INNER JOIN product ON reservation.productId = product.productId)
                                                    INNER JOIN customer ON reservation.customerId = customer.customerId 
                                                    WHERE reservation.active = 1 AND DATE(termStart) = ?
                                                    ORDER BY reservationStart DESC", [
            $date
        ]);

        return $reservationCount;
    }

    /**
     * @param $customerId
     * @return mixed
     */
    public static function getReservationCountByCustomer($customerId) {
        $db = MyDB::getConnection();
        $reservationCount = $db->queryOne("SELECT COUNT(reservationId) AS reservationCount FROM reservation WHERE customerId = ?", [
            $customerId
        ]);

        return $reservationCount['reservationCount'];
    }

    /**
     * @param $date
     * @param $hours
     * @param $customerId
     * @return array|string
     */
    public static function getReservationsByDateAndCustomer($date, $hours, $customerId) {
        $db = MyDB::getConnection();

        $startDate = DateTime::createFromFormat('d. m. Y H:i', $date);
        $endDate = DateTime::createFromFormat('d. m. Y H:i', $date);
        $endDate->modify('+' . $hours . ' hours');

        $reservationData = $db->queryAll("SELECT *, reservation.quantity AS rq, customer.name AS cn, product.name AS pn
                                                    FROM 
                                                    (reservation INNER JOIN product ON reservation.productId = product.productId)
                                                    INNER JOIN customer ON reservation.customerId = customer.customerId 
                                                    WHERE reservation.active = 1 
                                                    AND reservation.customerId = ?
                                                    AND DATE_ADD(reservation.termStart, INTERVAL reservation.termHours HOUR) >= ?
                                                    AND DATE(reservation.termStart) <= ?", [
            $customerId,
            $startDate->format('Y-m-d H:i:s'),
            $endDate->format('Y-m-d H:i:s')
        ]);

        if (count($reservationData) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$reservationData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst rezervace z DB');

        $reservations = [];
        foreach ($reservationData as $reservation) {
            $reservations[] = (new Reservation(
                $reservation['reservationId'],
                $reservation['customerId'],
                $reservation['pn'],
                $reservation['rq'],
                $reservation['termStart'],
                $reservation['termHours'],
                $reservation['reservationStart'],
                $reservation['reservationHours'],
                $reservation['active']
            ));
        }

        return $reservations;
    }

    /**
     * Returns if reservation end is in past.
     * @return bool
     */
    public function isReservationOver() {
        $dateNow = new DateTime();

        $date = new DateTime($this->reservationStart);
        $date->modify('+' . $this->reservationHours . ' hours');

        return $dateNow > $date;
    }

    /**
     * @param $minHour
     * @param $maxHour
     * @return bool
     */
    public function isReservationOverInCloseHours($minHour, $maxHour) {
        $dateNow = new DateTime();

        $date = new DateTime($this->reservationStart);
        $date->modify('+' . $this->reservationHours . ' hours');

        $diffInterval = $date->diff($dateNow, true);

        return ($diffInterval->h > $minHour && $diffInterval->h <= $maxHour);
    }

    /**
     * @return array
     */
    public function _toArray(){
        return [
            'reservationId' => $this->reservationId,
            'customerId' => $this->customerId,
            'productId' => $this->productId,
            'quantity' => $this->quantity,
            'termStart' => $this->termStart,
            'termHours' => $this->termHours,
            'reservationStart' => $this->reservationStart,
            'reservationHours' => $this->reservationHours,
            'active' => $this->active
        ];
    }

	/**
	 * @return int
	 */
	public function getReservationId() {
		return $this->reservationId;
	}

	/**
	 * @param int $reservationId
	 */
	public function setReservationId( $reservationId ) {
		$this->reservationId = $reservationId;
	}

	/**
	 * @return int
	 */
	public function getCustomerId() {
		return $this->customerId;
	}

	/**
	 * @param int $customerId
	 */
	public function setCustomerId( $customerId ) {
		$this->customerId = $customerId;
	}

	/**
	 * @return int
	 */
	public function getProductId() {
		return $this->productId;
	}

	/**
	 * @param int $productId
	 */
	public function setProductId( $productId ) {
		$this->productId = $productId;
	}

	/**
	 * @return mixed
	 */
	public function getQuantity() {
		return $this->quantity;
	}

	/**
	 * @param mixed $quantity
	 */
	public function setQuantity( $quantity ) {
		$this->quantity = $quantity;
	}

	/**
	 * @return mixed
	 */
	public function getTermStart() {
		return $this->termStart;
	}

    /**
     * @return string
     */
    public function getFormatedTermStart() {
        $date = new DateTime($this->termStart);
        return $date->format('d. m. Y H:i');
    }

	/**
	 * @param mixed $termStart
	 */
	public function setTermStart( $termStart ) {
		$this->termStart = $termStart;
	}

	/**
	 * @return mixed
	 */
	public function getTermHours() {
		return $this->termHours;
	}

	/**
	 * @param mixed $termHours
	 */
	public function setTermHours( $termHours ) {
		$this->termHours = $termHours;
	}

	/**
	 * @return mixed
	 */
	public function getReservationStart() {
		return $this->reservationStart;
	}

	/**
	 * @param mixed $reservationStart
	 */
	public function setReservationStart( $reservationStart ) {
		$this->reservationStart = $reservationStart;
	}

	/**
	 * @return mixed
	 */
	public function getReservationHours() {
		return $this->reservationHours;
	}

	/**
	 * @param mixed $reservationHours
	 */
	public function setReservationHours( $reservationHours ) {
		$this->reservationHours = $reservationHours;
	}

    /**
     * @return string
     */
    public function getReservationEnd() {
        $date = new DateTime($this->reservationStart);
        $date->modify('+' . $this->reservationHours . ' hours');
        return $date->format('d. m. Y H:i');
    }

    /**
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param int $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }
}