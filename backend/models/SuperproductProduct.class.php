<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 08.04.2018
 * Time: 16:33
 */

namespace backend\models;


use backend\database\DatabaseError;
use backend\database\MyDB;

class SuperproductProduct
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;

    protected $db;
    protected $scenario;

    private $superproductProductId;
    private $superproductId;
    private $productId;
    private $quantity;

    /**
     * SuperproductProduct constructor.
     * @param int $superproductProductId
     * @param int $superproductId
     * @param int $productId
     * @param int $quantity
     */
    public function __construct($superproductProductId = 0, $superproductId = 0, $productId = 0, $quantity = 0)
    {
        $this->db = MyDB::getConnection();

        $this->superproductProductId = $superproductProductId;
        $this->superproductId = $superproductId;
        $this->productId = $productId;
        $this->quantity = $quantity;
    }

    /**
     * @return SuperproductProduct|mixed|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
            $productData = $this->db->queryOne('SELECT * FROM superproduct_product WHERE superproductProductId = ? OR (superproductId = ? AND productId = ?)', [
                $this->superproductProductId,
	            $this->superproductId,
	            $this->productId
            ]);

            if (!(new Validate())->validateNotNull([$productData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst data superproduktu z DB');

            $this->superproductProductId = $productData['superproductProductId'];
            $this->superproductId = $productData['superproductId'];
            $this->productId = $productData['productId'];
            $this->quantity = $productData['quantity'];

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return SuperproductProduct|mixed|string
     */
    public function create()
    {
        $this->scenario = self::SCENARIO_CREATE;

        if ($this->validate()) {
            $insert = $this->db->query("INSERT INTO superproduct_product (superproductId, productId, quantity) VALUES (?, ?, ?)", [
                $this->superproductId,
                $this->productId,
                $this->quantity
            ]);

            if ($insert > 0) {
                $this->superproductProductId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return SuperproductProduct|mixed|string
     */
    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;

        if ($this->validate()) {
            $update = $this->db->query("UPDATE superproduct_product SET superproductId = ?, productId = ?, quantity = ? WHERE superproductProductId = ?", [
                $this->superproductId,
                $this->productId,
                $this->quantity,
                $this->superproductProductId
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->query("DELETE FROM superproduct_product WHERE superproductProductId = ?", [
            $this->superproductProductId
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        /*if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_LOAD) {
            $validation->validateRequired([
                'superproductProductId' => $this->superproductProductId
            ]);
        }*/

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'superproductId' => $this->superproductId,
                'productId' => $this->productId
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

	/**
	 * @param $superproductId
	 * @param bool $asArray
	 *
	 * @return array|string
	 */
	public static function getProductsBySuperproduct($superproductId, $asArray = true) {
		$db = MyDB::getConnection();
		$data = $db->queryAll("SELECT p.productId, p.name, p.nameSlug, p.price, p.priceWithVat, p.visible,p.created,p.changed, superproduct_product.quantity AS quantity
									FROM product AS p INNER JOIN superproduct_product ON p.productId = superproduct_product.productId 
									WHERE superproduct_product.superproductId = ?", [
			$superproductId
		]);
		if (count($data) < 1)
			return [];
		else if (!(new Validate())->validateNotNull([$data]))
			return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst produkty superproduktu z DB');

		$products = [];
		foreach ($data as $product) {
			$productObj = new Product(
				$product['productId'],
				$product['name'],
				$product['nameSlug'],
              '',
				'',
                '',
				$product['price'],
				$product['priceWithVat'],
				$product['quantity'],
				$product['visible'],
				$product['created'],
				$product['changed']
			);
			if($asArray)
				$products[] = $productObj->_toArray();
			else
				$products[] = $productObj;
		}

		return $products;
	}

	/**
	 * @param $productId
	 *
	 * @return bool
	 */
	public static function isProductInSuperproduct($productId) {
		$db = MyDB::getConnection();
		$data = $db->query("SELECT * FROM superproduct_product WHERE productId = ?", [
			$productId
		]);

		return $data > 0;
	}

	/**
	 * @param $productId
	 *
	 * @return mixed
	 */
	public static function getSuperproductByProduct($productId) {
		$db = MyDB::getConnection();
		$data = $db->queryOne("SELECT superproductId FROM superproduct_product WHERE productId = ?", [
			$productId
		]);

		return $data['superproductId'];
	}

    /**
     * @return array
     */
    public function _toArray(){
        return [
            'superproductProductId' => $this->superproductProductId,
            'superproductId' => $this->superproductId,
            'productId' => $this->productId,
            'quantity' => $this->quantity
        ];
    }

	/**
	 * @return int
	 */
	public function getSuperproductProductId() {
		return $this->superproductProductId;
	}

	/**
	 * @param int $superproductProductId
	 */
	public function setSuperproductProductId( $superproductProductId ) {
		$this->superproductProductId = $superproductProductId;
	}

	/**
	 * @return int
	 */
	public function getSuperproductId() {
		return $this->superproductId;
	}

	/**
	 * @param int $superproductId
	 */
	public function setSuperproductId( $superproductId ) {
		$this->superproductId = $superproductId;
	}

	/**
	 * @return int
	 */
	public function getProductId() {
		return $this->productId;
	}

	/**
	 * @param int $productId
	 */
	public function setProductId( $productId ) {
		$this->productId = $productId;
	}

	/**
	 * @return int
	 */
	public function getQuantity() {
		return $this->quantity;
	}

	/**
	 * @param int $quantity
	 */
	public function setQuantity( $quantity ) {
		$this->quantity = $quantity;
	}

}