<?php

/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 02.03.2017
 * Time: 22:13
 */

namespace backend\models;

use backend\database\MyDB;

class Validate
{
    private $validationSummary = [];
    private static $validationSummaryText;
    /*Each function has its own local result for simple use purposes*/
    private $validationResult = true;

    /**
     * Validates if value is not null, empty or 0
     * @param array $valuesForValidation
     * @return bool
     */
    public function validateRequired(array $valuesForValidation)
    {
        $result = true;

        foreach ($valuesForValidation as $key => $value) {
            if (!isset($value) || $value == null || empty($value)) {
                $this->validationSummary[] = new ValidationError(ValidationError::REQUIRED, $key);
                $this->validationResult = false;
                $result = false;
            }
        }

        return $result;
    }

    /**
     * @param array $valuesForValidation
     * @return bool
     */
    public function validateNotNull(array $valuesForValidation)
    {
        return self::validateRequired($valuesForValidation);
    }

    /**
     * @param array $valuesForValidation
     * @return bool
     */
    public function validateNotEmpty(array $valuesForValidation)
    {
        return self::validateRequired($valuesForValidation);
    }

    /**
     * Validates if value is integer.
     * @param array $valuesForValidation
     * @return bool
     */
    public function validateNumeric(array $valuesForValidation)
    {
        $result = true;

        foreach ($valuesForValidation as $key => $value) {
            if (!is_numeric($value)) {
                $this->validationSummary[] = new ValidationError(ValidationError::INTEGER, $key);
                $this->validationResult = false;
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Validates if var is boolean.
     * @param array $valuesForValidation
     * @return bool
     */
    public function validateBoolean(array $valuesForValidation)
    {
        $result = true;

        foreach ($valuesForValidation as $key => $value) {
            if (!is_bool($value)) {
                $this->validationSummary[] = new ValidationError(ValidationError::BOOLEAN, $key);
                $this->validationResult = false;
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Password validation.
     * Min 8 chars, numbers, small and big letters.
     * @param array $valuesForValidation
     * @return bool
     */
    public function validatePassword(array $valuesForValidation)
    {
        $result = true;

        foreach ($valuesForValidation as $key => $value) {
            if (strlen($value) < 8 || !preg_match("^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*^", $value)) {
                $this->validationSummary[] = new ValidationError(ValidationError::PASSWORD, $key);
                $this->validationResult = false;
                $result = false;
            }
        }

        return $result;
    }


    /**
     * Password validation.
     * Min 6 chars, numbers, small letters.
     * @param array $valuesForValidation
     * @return bool
     */
    public function validatePasswordWeak(array $valuesForValidation)
    {
        $result = true;

        foreach ($valuesForValidation as $key => $value) {
            if (strlen($value) < 6 || !preg_match("^\S*(?=\S{6,})(?=\S*[a-z])(?=\S*[\d])\S*^", $value)) {
                $this->validationSummary[] = new ValidationError(ValidationError::PASSWORD_WEAK, $key);
                $this->validationResult = false;
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Validates if ZIP is in correct format (XXXXX)
     * @param array $valuesForValidation
     * @return bool
     */
    public function validateZIP(array $valuesForValidation)
    {
        $result = true;

        foreach ($valuesForValidation as $key => $value) {
            if (!preg_match('/^(\d{5})$/i', $value)) {
                $this->validationSummary[] = new ValidationError(ValidationError::ZIP, $key);
                $this->validationResult = false;
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Validates if email is correct.
     * @param array $valuesForValidation
     * @return bool
     */
    public function validateEmail(array $valuesForValidation)
    {
        $result = true;

        foreach ($valuesForValidation as $key => $value) {
            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $this->validationSummary[] = new ValidationError(ValidationError::EMAIL, $key);
                $this->validationResult = false;
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Validates if phone is correct.
     * Possible value is 'XXXXXXXXX'
     * @param array $valuesForValidation
     * @return bool
     */
    public function validatePhone(array $valuesForValidation)
    {
        $result = true;

        foreach ($valuesForValidation as $key => $value) {
            if (!preg_match('/^(\d{3})(\d{3})(\d{3})$/i', $value)) {
                $this->validationSummary[] = new ValidationError(ValidationError::PHONE, $key);
                $this->validationResult = false;
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Validates if ICO is valid.
     * ICO contains 8 numbers.
     * @param array $valuesForValidation
     * @return bool
     */
    public function validateICO(array $valuesForValidation)
    {
        $result = true;

        foreach ($valuesForValidation as $key => $value) {
            if (!preg_match('/^(\d{8})$/i', $value)) {
                $this->validationSummary[] = new ValidationError(ValidationError::ICO, $key);
                $this->validationResult = false;
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Validates DIČ.
     * Contains CZ and 7-14 numbers.
     * @param array $valuesForValidation
     * @return bool
     */
    public function validateDIC(array $valuesForValidation)
    {
        $result = true;

        foreach ($valuesForValidation as $key => $value) {
            if (!preg_match('/([C][Z])(\d{7,14})$/i', $value)) {
                $this->validationSummary[] = new ValidationError(ValidationError::DIC, $key);
                $this->validationResult = false;
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Validates czech birth number.
     * Contains 6 numbers and 3 or 4 numbers.
     * @param array $valuesForValidation
     * @return bool
     */
    public function validateBirthNumber(array $valuesForValidation)
    {
        $result = true;

        foreach ($valuesForValidation as $key => $value) {
            if (!preg_match('/^(\d{6})(\d{3,4})$/i', $value)) {
                $this->validationSummary[] = new ValidationError(ValidationError::BIRTH_NUMBER, $key);
                $this->validationResult = false;
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Validates bank code.
     * Contains 4 numbers.
     * @param array $valuesForValidation
     * @return bool
     */
    public function validateBankCode(array $valuesForValidation)
    {
        $result = true;

        foreach ($valuesForValidation as $key => $value) {
            if (!preg_match('/(^\d{4}$)/i', $value)) {
                $this->validationSummary[] = new ValidationError(ValidationError::BANK_CODE, $key);
                $this->validationResult = false;
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Validates if var is smaller or equal to maxLength.
     * @param array $valuesForValidation
     * @param int $maxLength - included (only bigger numbers are not allowed)
     * @return bool
     */
    public function validateMaxLength(array $valuesForValidation, $maxLength = 1)
    {
        $result = true;

        foreach ($valuesForValidation as $key => $value) {
            if (count($value) > $maxLength) {
                $this->validationSummary[] = new ValidationError(ValidationError::LENGTH, $key);
                $this->validationResult = false;
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Validates if all array values are identical to others
     * @param array $valuesForValidation
     * @return bool
     */
    public function validateIdentical(array $valuesForValidation)
    {
        $result = true;

        $count = count($valuesForValidation);
        $keys = array_keys($valuesForValidation);
        for ($i = 0; $i < $count - 1; $i++) {
            if ($valuesForValidation[$keys[$i]] !== $valuesForValidation[$keys[$i + 1]]) {
                $this->validationSummary[] = new ValidationError(ValidationError::NOT_IDENTICAL, key($valuesForValidation));
                $this->validationResult = false;
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Validates if values are not in database
     * @param array $valuesForValidation
     * @param $table - database table
     * @param $column - column in database table
     * @return bool
     */
    public function validateUnique(array $valuesForValidation, $table, $column)
    {
        $result = true;

        //table and column are set in class - safe
        //sending table and column as parametr throws error
        $uniqueSelect = MyDB::getConnection()->queryOne("SELECT COUNT(?) AS uniqueCount FROM " . $table . " WHERE " . $column . " IN (?)", [$column, implode(", ", $valuesForValidation)]);
        if ($uniqueSelect['uniqueCount'] > 0) {
            $this->validationSummary[] = new ValidationError(ValidationError::UNIQUE, key($valuesForValidation));
            $this->validationResult = false;
            $result = false;
        }

        return $result;
    }

    /**
     * Validates if in values is text except HTML
     * @param array $valuesForValidation
     * @return bool
     */
    public function validateNotEmptyHTML(array $valuesForValidation)
    {
        $result = true;

        foreach ($valuesForValidation as $key => $value) {
            //removes html tags
            $value = strip_tags($value);
            //checks if it contains any text
            if (!isset($value) || $value == null || empty($value)) {
                $this->validationSummary[] = new ValidationError(ValidationError::REQUIRED, $key);
                $this->validationResult = false;
                $result = false;
            }
        }

        return $result;
    }

    /**
     * @param ValidationError $error
     */
    public function addValidationError(ValidationError $error)
    {
        $this->validationSummary[] = $error;
        $this->validationResult = false;
    }

    /**
     * @return bool
     */
    public function isValidationResult()
    {
        return $this->validationResult;
    }

    /**
     * @return string
     */
    public function getValidationSummary()
    {
        self::$validationSummaryText = "<p>";
        self::$validationSummaryText .= implode("</p><p>", $this->validationSummary);
        self::$validationSummaryText .= "</p>";
        return self::$validationSummaryText;
    }

    /**
     * @return mixed
     */
    public static function getValidationSummaryText()
    {
        return self::$validationSummaryText;
    }
}

class ValidationError
{
    const REQUIRED = 1;
    const INTEGER = 2;
    const ZIP = 3;
    const EMAIL = 4;
    const LENGTH = 5;
    const PHONE = 6;
    const ICO = 7;
    const DIC = 8;
    const BIRTH_NUMBER = 9;
    const BANK_CODE = 10;
    const PASSWORD = 11;
    const BOOLEAN = 12;
    const LOGIN = 13;
    const NOT_IDENTICAL = 14;
    const UNIQUE = 15;
    const DUPLICATE_EMAIL = 16;
    const PASSWORD_WEAK = 17;
    private $errorCode;
    private $nameOfAttribute;

    /**
     * ValidationError constructor.
     * @param $errorCode
     * @param string $nameOfAttribute
     */
    public function __construct($errorCode, $nameOfAttribute = "")
    {
        $this->errorCode = $errorCode;
        $this->nameOfAttribute = $nameOfAttribute;
    }

    /**
     * Returns default description for error.
     * @param string $nameOfAttribute
     * @param string $message
     * @return string
     */
    public function getErrorDescription($nameOfAttribute = "", $message = "")
    {
        if (!(new Validate())->validateNotEmpty([$nameOfAttribute]))
            $nameOfAttribute = $this->nameOfAttribute;

        switch ($this->errorCode) {
            case self::REQUIRED:
                return "Pole " . $nameOfAttribute . " nesmí být prázdné nebo nulové. ";
            case self::INTEGER:
                return "Obsah pole " . $nameOfAttribute . " musí být číselná hodnota. ";
            case self::ZIP:
                return "Pole " . $nameOfAttribute . " musí obsahovat PSČ ve formátu XXXXX. Např.: 50003. ";
            case self::EMAIL:
                return "Pole " . $nameOfAttribute . " musí obsahovat email ve formátu jmeno@domena.cz. Např.: info@example.com. ";
            case self::LENGTH:
                return "Obsah pole " . $nameOfAttribute . " nesmí být delší než " . $message . " znaků. ";
            case self::PHONE:
                return "Obsah pole " . $nameOfAttribute . " se musí skládat z 9 číslic ve formátu XXXXXXXXX. Např.: 789456123. ";
            case self::ICO:
                return "Obsah pole " . $nameOfAttribute . " se musí skládat z 8 číslic. ";
            case self::DIC:
                return "Obsah pole " . $nameOfAttribute . " musí začínat na CZ, které následuje (bez mezery) 7 - 14 číslic. ";
            case self::BIRTH_NUMBER:
                return "Obsah pole " . $nameOfAttribute . " se musí skládat z 9 nebo 10 číslic. Např.: 1234561234. ";
            case self::BANK_CODE:
                return "Obsah pole " . $nameOfAttribute . " se musí skládat ze 4 číslic ve formátu XXXX. Např.: 0100. ";
            case self::PASSWORD:
                return "Heslo musí být delší než 7 znaků. Musí obsahovat číslice, velká, malá písmena a ideálně i speciální znaky. ";
            case self::PASSWORD_WEAK:
                return "Heslo musí mít alespoň 6 znaků. Musí obsahovat číslice a písmena. Doporučujeme použít i speciální znaky a velká písmena.";
            case self::BOOLEAN:
                return "Pole " . $nameOfAttribute . " musí být pravda nebo nepravda. ";
            case self::LOGIN:
                return "Nesprávné heslo nebo email. ";
            case self::NOT_IDENTICAL:
                return $nameOfAttribute . " a jeho potvrzení musí být stejné. ";
            case self::UNIQUE:
                return "Obsah pole " . $nameOfAttribute . " již v databázi existuje. ";
            case self::DUPLICATE_EMAIL:
                return "Uživatel s daným emailem již v databázi existuje. ";
            default:
                return "Chyba při validaci. Zkontrolujte zadané údaje. ";
        }
    }

    public function __toString()
    {
        return $this->getErrorDescription();
    }
}