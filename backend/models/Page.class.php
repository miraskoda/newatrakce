<?php
namespace backend\models;

use backend\database\DatabaseError;
use backend\database\MyDB;

class Page
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;

    const PAGE_DEFAULT = 'default';
    const PAGE_EDITABLE = 'editable';

    private $db;
    private $scenario;

	private $pageId;
	private $title;
	private $description;
	private $keywords;
	private $allowIndex = true;
	private $type;
	private $content;

	/**
	 * Page constructor.
	 *
	 * @param int $pageId
	 * @param string $title
	 * @param string $description
	 * @param string $keywords
	 * @param bool $allowIndex
	 * @param string $type
	 * @param string $content
	 */
    public function __construct($pageId = 0, $title = "", $description = "", $keywords = "", $allowIndex = true, $type = self::PAGE_DEFAULT, $content = '')
    {
        $this->db = MyDB::getConnection();
        $this->pageId = $pageId;

        $this->type = $type;
        $this->content = $content;

        $this->setPage($title, $description, $keywords, $allowIndex);
    }

    /**
     * @return Page|string
     */
    public function load(){
        $this->scenario = self::SCENARIO_LOAD;

        if($this->validate()){
            $pageData = $this->db->queryOne("SELECT * FROM page WHERE pageId = ?", [
                $this->pageId
            ]);

            if(!(new Validate())->validateNotNull([$pageData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst stránku z DB');

            if(strlen($pageData['title']) < 40 && strpos($pageData['title'], 'OnlineAtrakce.cz') === false){
                $pageData['title'] .= ' | OnlineAtrakce.cz';
            }

            $this->type = $pageData['type'];
            $this->content = $pageData['content'];

            $this->setPage(
                $pageData['title'],
                $pageData['description'],
                $pageData['keywords'],
                ($pageData['allowIndex'] == 0) ? false : true
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    public function loadID($id){
        $this->scenario = self::SCENARIO_LOAD;

        if($this->validate()){
            $pageData = $this->db->queryOne("SELECT * FROM page WHERE pageId = ?", [
                $id
            ]);

            if(!(new Validate())->validateNotNull([$pageData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst stránku z DB');

            if(strlen($pageData['title']) < 40 && strpos($pageData['title'], 'OnlineAtrakce.cz') === false){
                $pageData['title'] .= ' | OnlineAtrakce.cz';
            }

            $this->type = $pageData['type'];
            $this->content = $pageData['content'];

            $this->setPage(
                $pageData['title'],
                $pageData['description'],
                $pageData['keywords'],
                ($pageData['allowIndex'] == 0) ? false : true
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }


    /**
     * @return Page|string
     */
    public function create(){
        $this->scenario = self::SCENARIO_CREATE;

        if($this->validate()){
            $insert = $this->db->query("INSERT INTO page (title, description, keywords, allowIndex, type, content) VALUES (?,?,?,?,?,?)", [
                $this->title,
                $this->description,
                $this->keywords,
                ($this->allowIndex) ? 1 : 0,
	            $this->type,
	            $this->content
            ]);

            if($insert > 0){
                $this->pageId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return Page|string
     */
    public function update(){
        $this->scenario = self::SCENARIO_UPDATE;

        if($this->validate()){
            $update = $this->db->query("UPDATE page SET title = ?, description = ?, keywords = ?, allowIndex = ?, type = ?, content = ? WHERE pageId = ?", [
                $this->title,
                $this->description,
                $this->keywords,
                ($this->allowIndex) ? 1 : 0,
                $this->type,
                $this->content,
                $this->pageId
            ]);

            if($update > 0){
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return bool|string
     */
    public function delete(){
        $delete = $this->db->query("DELETE FROM page WHERE pageId = ?", [
            $this->pageId
        ]);

        if($delete > 0){
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_LOAD) {
            $validation->validateRequired([
                'pageId' => $this->pageId,
            ]);
            $validation->validateNumeric([
                'pageId' => $this->pageId,
            ]);
        }

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'title' => $this->title,
                'description' => $this->description,
            ]);

            $tempValidation = new Validate();

            if($tempValidation->validateNotNull([$this->allowIndex]))
                $validation->validateBoolean(['allowIndex' => $this->allowIndex]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    /**
     * Group setter
     * @param $title
     * @param $description
     * @param $keywords
     * @param $allowIndex
     */
    public function setPage($title, $description, $keywords, $allowIndex = true){
        $this->title = $title;
        $this->description = $description;
        $this->keywords = $keywords;
        $this->allowIndex = $allowIndex;
    }

	public function getId()
    {
	    return $this->pageId;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @return bool
     */
    public function isAllowIndex()
    {
        return $this->allowIndex;
    }

	/**
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType( $type ) {
		$this->type = $type;
	}

	/**
	 * @return string
	 */
	public function getContent() {
		return $this->content;
	}

	/**
	 * @param string $content
	 */
	public function setContent( $content ) {
		$this->content = $content;
	}
}