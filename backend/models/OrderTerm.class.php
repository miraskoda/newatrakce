<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 21.08.2017
 * Time: 18:26
 */

namespace backend\models;


use backend\controllers\OrderController;
use backend\database\DatabaseError;
use backend\database\MyDB;
use DateTime;

class OrderTerm
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;

    private $db;
    private $scenario;

    private $orderTermId;
    private $orderId;
    private $termNo;
    private $start;
    private $hours;

    const TYPE_DRAFT = 'draft';
    const TYPE_FINISHED = 'finished';

    /**
     * OrderTerm constructor.
     * @param int $orderTermId
     * @param int $orderId
     * @param int $termNo
     * @param string $start
     * @param int $hours
     */
    public function __construct($orderTermId = 0, $orderId = 0, $termNo = 0, $start = '1990-01-01 00:00:00', $hours = 0)
    {
        $this->db = MyDB::getConnection();
        $this->orderTermId = $orderTermId;
        $this->orderId = $orderId;

        $this->setOrderTerm($termNo, $start, $hours);
    }

    /**
     * Loads OrderTerm from DB by orderTermId or orderId and $termNo.
     * @return OrderTerm|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
            $orderTermData = $this->db->queryOne("SELECT * FROM order_term WHERE orderTermId = ? OR (orderId = ? AND termNo = ?)", [
                $this->orderTermId,
                $this->orderId,
                $this->termNo
            ]);

            if (!(new Validate())->validateNotNull([$orderTermData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst termín objednávky z DB');

            $this->orderTermId = $orderTermData['orderTermId'];
            $this->orderId = $orderTermData['orderId'];
            $this->setOrderTerm(
                $orderTermData['termNo'],
                $orderTermData['start'],
                $orderTermData['hours']
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Inserts new OrderTerm to DB.
     * No params required because all data are gained inside constructor.
     * @return OrderTerm|string
     */
    public function create()
    {
        $this->scenario = self::SCENARIO_CREATE;

        if ($this->validate()) {
            $insert = $this->db->query("INSERT INTO order_term (orderId, termNo, start, hours) VALUES (?,?,?,?)", [
                $this->orderId,
                $this->termNo,
                $this->start,
                $this->hours
            ]);

            if ($insert > 0) {
                $this->orderTermId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Updates OrderTerm by orderTermId.
     * @return OrderTerm|string
     */
    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;

        if ($this->validate()) {
            $update = $this->db->query("UPDATE order_term SET orderId = ?, termNo = ?, start = ?, hours = ? WHERE orderTermId = ?", [
                $this->orderId,
                $this->termNo,
                $this->start,
                $this->hours,
                $this->orderTermId
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Deletes OrderTerm by orderTermId.
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->query("DELETE FROM order_term WHERE orderTermId = ?", [
            $this->orderTermId
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * Deletes term with highest no in order
     * @return bool|string
     */
    public function deleteLast() {
        $delete = $this->db->query("DELETE FROM order_term WHERE termNo IN (SELECT * FROM (SELECT MAX(termNo) FROM order_term WHERE orderId = ?) AS o) AND orderId = ?", [
            $this->orderId,
            $this->orderId
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * @return bool
     */
    public function isDuplicate()
    {
        $isDuplicate = $this->db->query("SELECT orderTermId FROM order_term WHERE orderId = ? AND termNo = ?", [
            $this->orderId,
            $this->termNo
        ]);

        if ($isDuplicate > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $termNo
     * @return OrderTerm|bool|string
     */
    public static function getOrderTerm($termNo) {
        if(OrderController::existsDraftOrder()) {
            $order = OrderController::getDraftOrder();

            if(is_a($order, Order::class)) {
                $orderTerm = new OrderTerm();
                $orderTerm->setOrderId($order->getOrderId());
                $orderTerm->setTermNo($termNo);

                $orderTerm = $orderTerm->load();
                if(is_a($orderTerm, OrderTerm::class))
                    return $orderTerm;
            }
        }

        return false;
    }

    /**
     * @return array|bool|string
     */
    public static function getOrderTerms() {
        if(OrderController::existsDraftOrder()) {
            $order = OrderController::getDraftOrder();

            return self::getOrderTermsByOrder($order);
        }

        return false;
    }

    /**
     * @param $order
     * @return array|bool|string
     */
    public static function getOrderTermsByOrder($order) {
        if(is_a($order, Order::class)) {
            $db = MyDB::getConnection();
            $orderTermData = $db->queryAll("SELECT * FROM order_term WHERE orderId = ?", [
                $order->getOrderId()
            ]);

            if (count($orderTermData) < 1)
                return false;
            else if (!(new Validate())->validateNotNull([$orderTermData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst termíny pronájmu z DB. Prosím kontaktujte zákaznickou podporu.');

            $orderTerms = [];
            foreach ($orderTermData as $orderTerm) {
                $orderTerms[] = new OrderTerm(
                    $orderTerm['orderTermId'],
                    $orderTerm['orderId'],
                    $orderTerm['termNo'],
                    $orderTerm['start'],
                    $orderTerm['hours']
                );
            }

            return $orderTerms;
        }

        return [];
    }

    /**
     * Get OrderTerms by date.
     * Date should be in YYYY-MM-DD format.
     * @param $date
     * @return array|bool|string
     */
    public static function getOrderTermsByDate($date) {
        if($date == "")
            return [];

        $db = MyDB::getConnection();
        $orderTermData = $db->queryAll("SELECT * FROM order_term WHERE DATE(start) = ?", [
            $date
        ]);

        if (count($orderTermData) < 1)
            return false;
        else if (!(new Validate())->validateNotNull([$orderTermData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst termíny pronájmu z DB. Prosím kontaktujte zákaznickou podporu.');

        $orderTerms = [];
        foreach ($orderTermData as $orderTerm) {
            $orderTerms[] = new OrderTerm(
                $orderTerm['orderTermId'],
                $orderTerm['orderId'],
                $orderTerm['termNo'],
                $orderTerm['start'],
                $orderTerm['hours']
            );
        }

        return $orderTerms;
    }

    /**
     * @return int
     */
    public static function getCountOfTerms() {
        if(OrderController::existsDraftOrder()) {
            $order = OrderController::getDraftOrder();

            if(is_a($order, Order::class)) {
                $db = MyDB::getConnection();
                $count = $db->query("SELECT orderTermId FROM order_term WHERE orderId = ? ORDER BY termNo", [
                    $order->getOrderId()
                ]);

                return $count;
            }
        }

        return 0;
    }

    /**
     * @return array
     */
    public static function getRentHours() {
        if(OrderController::existsDraftOrder()) {
            $order = OrderController::getDraftOrder();

            return self::getRentHoursByOrder($order);
        }

        return [];
    }

    /**
     * @param $order
     * @return array
     */
    public static function getRentHoursByOrder($order) {
        if(is_a($order, Order::class)) {
            $db = MyDB::getConnection();
            $rentHours = $db->queryAll("SELECT hours FROM order_term WHERE orderId = ? ORDER BY termNo", [
                $order->getOrderId()
            ]);

            return $rentHours;
        }

        return [];
    }

    /**
     * @param null $order
     * @return array
     */
    public static function getRentHoursAsStrings($order = null) {
        if(!is_null($order))
            $rentHours = self::getRentHoursByOrder($order);
        else
            $rentHours = self::getRentHours();
        $stringifiedRentHours = [];
        foreach ($rentHours as $rentHour) {
            $stringifiedRentHours[] = $rentHour['hours'];
        }

        return $stringifiedRentHours;
    }

    /**
     * Validates if Order attributes are complete and valid.
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        /*if ($this->scenario == self::SCENARIO_LOAD) {
            $validation->validateRequired([
                'orderTermId' => $this->orderTermId,
            ]);
            $validation->validateNumeric([
                'orderTermId' => $this->orderTermId,
            ]);
        }*/

        if ($this->scenario == self::SCENARIO_UPDATE) {
            $validation->validateRequired([
                'orderTermId' => $this->orderTermId,
            ]);
            $validation->validateNumeric([
                'orderTermId' => $this->orderTermId,
            ]);
        }

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'orderId' => $this->orderId,
                'číslo termínu' => $this->termNo,
                'start' => $this->start,
                'hodiny' => $this->hours,
            ]);

            $validation->validateNumeric([
                'orderId' => $this->orderId,
                'číslo termínu' => $this->termNo,
                'hodiny' => $this->hours,
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return array
     */
    public function _toArray(){
        return [
            'orderTermId' => $this->orderTermId,
            'orderId' => $this->orderId,
            'termNo' => $this->termNo,
            'start' => $this->start,
            'hours' => $this->hours
        ];
    }

    /**
     * Group setter
     * @param $termNo
     * @param $start
     * @param $hours
     */
    public function setOrderTerm($termNo, $start, $hours)
    {
        $this->termNo = $termNo;
        $this->start = $start;
        $this->hours = $hours;
    }

    /**
     * @param $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @param $termNo
     */
    public function setTermNo($termNo)
    {
        $this->termNo = $termNo;
    }

    /**
     * @param $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @param $hours
     */
    public function setHours($hours)
    {
        $this->hours = $hours;
    }

    /**
     * @return string
     */
    public function getStart()
    {
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $this->start);
        $date = $date->format('d. m. Y H:i');
        return $date;
    }


    /**
     * @return string
     */
    public function getStartWithoutDay()
    {
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $this->start);
        $date = $date->format('H:i');
        return $date;
    }


    /**
     * @param int $buildTime
     * @return string
     */
    public function getStartWithoutDayMinusOne($buildTime = 60)
    {
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $this->start);
        $date -> modify('-'.$buildTime.' minutes');
        $date = $date->format('H:i');

        return $date;
    }
    /**
     * @return mixed
     */
    public function getStartRaw() {
        return $this->start;
    }


    /**
     * @param $var
     * @param int $buildTime
     * @param int $offset
     * @return string
     * Method to calculating actual time arriving from Syrovatka
     */
    public function TimeArrived($var,$buildTime = 60,$offset = 15){

        $parsedDate = new DateTime("@$var");
        $datHour = $parsedDate -> format('H');
        $datMinutes = round($parsedDate -> format('i')/ $offset) * $offset;
        $dateLayout = DateTime::createFromFormat('Y-m-d H:i:s', $this->start);
        $final = $dateLayout -> modify('-'.$datHour.' hour')->modify('-'.$datMinutes.' minutes');
        return $final->modify('-'.$buildTime.' minutes')->format('H:i');
    }

    /**
     * @return bool|DateTime|string
     */
    public function getEnd() {
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $this->start);
        $date->modify('+' . $this->getHours() . ' hours');
        $date = $date->format('d. m. Y H:i');
        return $date;
    }

    /**
     * @return mixed
     */
    public function getHours()
    {
        return $this->hours;
    }
}