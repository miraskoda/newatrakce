<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 14.03.2017
 * Time: 16:54
 */

namespace backend\models;

class Url
{
    /**
     * Returns absolute url to path.
     * @param $path
     * @return mixed
     */
    public static function getAbsUrlTo($path) {
        $configs = include $_SERVER['DOCUMENT_ROOT'] . '/backend/config/config.php';
        return $configs->protocol . '://' . str_replace($configs->localRoot, $configs->domain, $path);
    }

    public static function getPathTo($path, $extraFolder = '') {
        $configs = include $_SERVER['DOCUMENT_ROOT'] . '/backend/config/config.php';
        return $configs->localRoot . $extraFolder . $path;
    }

    public static function getPathToHome() {
	    $configs = include $_SERVER['DOCUMENT_ROOT'] . '/backend/config/config.php';
	    return $configs->protocol . '://' . $configs->domain . '/';
    }

    public static function getBackendPathTo($path) {
        return self::getPathTo($path, "/backend");
    }

    public static function nameToUrl($name) {
        $name = str_replace('ě', 'e', $name);
        $name = str_replace('š', 's', $name);
        $name = str_replace('č', 'c', $name);
        $name = str_replace('ř', 'r', $name);
        $name = str_replace('ž', 'z', $name);
        $name = str_replace('ů', 'u', $name);
        $name = str_replace('ň', 'n', $name);
        return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($name, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
    }
}