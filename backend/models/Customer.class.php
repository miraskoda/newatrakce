<?php
namespace backend\models;

use backend\controllers\CartController;
use backend\database\DatabaseError;
use backend\database\MyDB;

class Customer
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;
    const SCENARIO_CHANGE_PASSWORD = 5;
    const SCENARIO_LOGIN = 6;

    private $db;
    private $scenario;

    private $customerId;
    private $name;
    private $phonePrefix;
    private $phone;
    private $email;
    private $birthNumber;
    private $ico;
    private $dic;
    private $isCompany;
    private $bankAccountNumber;
    private $bankCode;

    private $registrationDate;
    private $lastLoginDate;

    private $plainPassword;
    private $plainPasswordConfirm;
    private $hashedPassword;

    private $isEmailConfirmed;
    private $emailConfirmedHash;
    private $forgottenPasswordHash;
    private $customerGroupId;

	/**
	 * Customer constructor.
	 *
	 * @param int $customerId
	 * @param string $name
	 * @param string $phonePrefix
	 * @param string $phone
	 * @param string $email
	 * @param string $birthNumber
	 * @param string $ico
	 * @param string $dic
	 * @param int $isCompany
	 * @param string $bankAccountNumber
	 * @param string $bankCode
	 * @param string $registrationDate
	 * @param string $lastLoginDate
	 * @param string $plainPassword
	 * @param string $plainPasswordConfirm
	 * @param int $isEmailConfirmed
	 * @param string $emailConfirmedHash
	 * @param string $forgottenPasswordHash
	 * @param int $customerGroupId
	 */
    public function __construct($customerId = 0, $name = "", $phonePrefix = '+420', $phone = "", $email = "", $birthNumber = "", $ico = "", $dic = "", $isCompany = 0, $bankAccountNumber = "", $bankCode = "", $registrationDate = "1990-01-01", $lastLoginDate = "1990-01-01", $plainPassword = "", $plainPasswordConfirm = "", $isEmailConfirmed = 0, $emailConfirmedHash = "", $forgottenPasswordHash = "", $customerGroupId = 1)
    {
        $this->db = MyDB::getConnection();
        $this->customerId = $customerId;
        $this->registrationDate = $registrationDate;
        $this->lastLoginDate = $lastLoginDate;

        $this->plainPassword = $plainPassword;
        $this->plainPasswordConfirm = $plainPasswordConfirm;
        if ((new Validate())->validateNotNull([$this->plainPassword]) && (new Validate())->validateNotNull([$this->plainPasswordConfirm]) && (new Validate())->validateIdentical([$this->plainPassword, $this->plainPasswordConfirm]))
            $this->hashedPassword = Password::hashPassword($this->plainPassword);

        $this->isEmailConfirmed = $isEmailConfirmed;
        $this->emailConfirmedHash = $emailConfirmedHash;
        $this->forgottenPasswordHash = $forgottenPasswordHash;

        $this->customerGroupId = $customerGroupId;

        $this->setCustomer($name, $phonePrefix, $phone, $email, $birthNumber, $ico, $dic, $isCompany, $bankAccountNumber, $bankCode);
    }

    /**
     * Loads data to customer from DB by customerId or email.
     * @return Customer|string
     */
    public function load(){
        $this->scenario = self::SCENARIO_LOAD;

        if($this->validate()){
            $customerData = $this->db->queryOne("SELECT * FROM customer WHERE email = ? OR customerId = ? OR emailConfirmedHash = ?", [
                $this->email,
                $this->customerId,
                $this->emailConfirmedHash
            ]);

            if(!(new Validate())->validateNotNull([$customerData])) {
                if((new Validate())->validateNotNull([$this->email]))
                    return 'Uživatel s těmito údaji neexistuje. Prosím zkontrolujte svůj email a heslo.';
                else
                    return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst zákazníka z DB');
            }

            $this->customerId = $customerData['customerId'];
            $this->registrationDate = $customerData['registrationDate'];
            $this->lastLoginDate = $customerData['lastLoginDate'];

            $this->hashedPassword = $customerData['password'];

            $this->isEmailConfirmed = $customerData['isEmailConfirmed'];
            $this->emailConfirmedHash = $customerData['emailConfirmedHash'];
            $this->forgottenPasswordHash = $customerData['forgottenPasswordHash'];

	        $this->customerGroupId = $customerData['customerGroupId'];

	        $this->setCustomer(
                $customerData['name'],
                $customerData['phonePrefix'],
                $customerData['phone'],
                $customerData['email'],
                $customerData['birthNumber'],
                $customerData['ico'],
                $customerData['dic'],
                $customerData['isCompany'],
                $customerData['bankAccountNumber'],
                $customerData['bankCode']
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return Customer|string
     */
    public function create(){
        $this->scenario = self::SCENARIO_CREATE;

        if($this->checkForDuplicate()){
            $error = new ValidationError(ValidationError::DUPLICATE_EMAIL);
            return $error->getErrorDescription();
        }

        if($this->validate()){
            //if hashedPassword is not set - try to create
            if (!(new Validate())->validateNotNull([$this->hashedPassword]))
                $this->hashedPassword = Password::hashPassword($this->plainPassword);

            /* is done in send email function
             * if (!(new Validate())->validateNotNull([$this->emailConfirmedHash]))
                $this->emailConfirmedHash = $this->generateCustomerHash();*/

            $emailSendResult = $this->sendEmailConfirmEmail();
            if ($emailSendResult != true)
                return $emailSendResult;

            $insert = $this->db->query("INSERT INTO customer (name, phonePrefix, phone, email, birthNumber, ico, dic, isCompany, bankAccountNumber, bankCode, password, registrationDate, lastLoginDate, isEmailConfirmed, emailConfirmedHash, forgottenPasswordHash, customerGroupId) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [
                $this->name,
                $this->phonePrefix,
                $this->phone,
                $this->email,
                $this->birthNumber,
                $this->ico,
                $this->dic,
                $this->isCompany,
                $this->bankAccountNumber,
                $this->bankCode,
                $this->hashedPassword,
                date('Y-m-d H:i:s'),
                date('Y-m-d H:i:s'),
                $this->isEmailConfirmed,
                $this->emailConfirmedHash,
                $this->forgottenPasswordHash,
	            $this->customerGroupId
            ]);

            if($insert > 0){
                $this->customerId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return Customer|string
     */
    public function update(){
        $this->scenario = self::SCENARIO_UPDATE;

        if($this->validate()){
            $update = $this->db->query("UPDATE customer SET name = ?, phonePrefix = ?, phone = ?, email = ?, birthNumber = ?, ico = ?, dic = ?, isCompany = ?, bankAccountNumber = ?, bankCode = ?, lastLoginDate = ?, isEmailConfirmed = ?, emailConfirmedHash = ?, forgottenPasswordHash = ?, customerGroupId = ? WHERE customerId = ?", [
                $this->name,
                $this->phonePrefix,
                $this->phone,
                $this->email,
                $this->birthNumber,
                $this->ico,
                $this->dic,
                $this->isCompany,
                $this->bankAccountNumber,
                $this->bankCode,
                date('Y-m-d H:i:s'),
                $this->isEmailConfirmed,
                $this->emailConfirmedHash,
                $this->forgottenPasswordHash,
                $this->customerGroupId,
                $this->customerId
            ]);

            if($update > 0){
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return bool|string
     */
    public function delete(){
        $delete = $this->db->query("DELETE FROM customer WHERE customerId = ?", [
            $this->customerId
        ]);

        if($delete > 0){
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * @return bool|mixed
     */
    public function login()
    {
        $this->scenario = self::SCENARIO_LOGIN;

        $tempId = CartController::getCustomerId();

        if ($this->validate()) {
            $_SESSION['customer'] = $this->customerId;

            //change last login in db - no result required
            $this->db->query("UPDATE customer SET lastLoginDate = ? WHERE customerId = ?", [
                date('Y-m-d H:i:s'),
                $this->customerId
            ]);

            // transfer cart of temp customer to logged customer
            // transfer orders to customer
            if(is_numeric($tempId) && $tempId > 0) {
                Cart::transferTempCart($tempId, $this->customerId);
                Order::transferTempOrder($tempId, $this->customerId);
            }

            return true;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return Customer|string
     */
    public function changePassword(){
        $this->scenario = self::SCENARIO_CHANGE_PASSWORD;

        if($this->validate()){
            // must rehash password because plain changed
            $this->hashedPassword = Password::hashPassword($this->plainPassword);

            $update = $this->db->query("UPDATE customer SET password = ? WHERE customerId = ?", [
                $this->hashedPassword,
                $this->customerId
            ]);

            if($update > 0){
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return bool|string
     */
    public function sendEmailConfirmEmail() {
        $this->emailConfirmedHash = $this->generateCustomerHash();
        $this->update();

        $mail = new MailHelper();
        return $mail->sendEmailConfirm($this->email, $this->emailConfirmedHash);
    }

    /**
     * @return string
     */
    private function generateCustomerHash() {
        return md5($this->email . date('Y-m-d-H-i-s') . rand(1111, 9999));
    }

    /**
     * @return bool|string
     */
    public function sendForgottenPasswordEmail() {
        $this->forgottenPasswordHash = Password::generatePasswordHash($this->email);
        $this->update();

        $mail = new MailHelper();
        return $mail->sendForgottenPassword($this->email, $this->forgottenPasswordHash);
    }

    /**
     * Generates customerId for cart of not logged customer
     * @return string
     */
    public static function generateTempCustomerId() {
        return date('ymdHis') . rand(1111, 9999);
    }

    /**
     * Check for customer duplicates by email
     * Returns true if customer exists
     * @return bool
     */
    private function checkForDuplicate() {
        if ((new Validate())->validateNotNull([$this->email])) {
            $check = $this->db->query("SELECT customerId FROM customer WHERE email = ?", [
                $this->email
            ]);

            if($check > 0)
                return true;
            else
                return false;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function validate(){
        $validation = new Validate();

        if($this->scenario == self::SCENARIO_CHANGE_PASSWORD || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'password' => $this->plainPassword,
            ]);
            $validation->validatePasswordWeak([
                'password' => $this->plainPassword,
            ]);

            $validation->validateIdentical([
                'Heslo' => $this->plainPassword,
                'Potvrzení hesla' => $this->plainPasswordConfirm,
            ]);
        }

        if ($this->scenario == self::SCENARIO_LOGIN) {
            $validation->validateRequired([
                'email' => $this->email,
            ]);
            $validation->validateEmail([
                'email' => $this->email,
            ]);

            $validation->validateRequired([
                'password' => $this->plainPassword,
            ]);

            $validation->validateRequired([
                'password' => $this->hashedPassword,
            ]);

            if (!Password::verifyPassword($this->plainPassword, $this->hashedPassword)) {
                $validation->addValidationError(new ValidationError(ValidationError::LOGIN));
            }
        }

        if($this->scenario == self::SCENARIO_LOAD) {
            $tempValidation = new Validate();
            $oneRequiredResult = false;

            //if customerId is not null
            if ($tempValidation->validateNotNull([$this->customerId])) {
                $validation->validateNumeric(['customerId' => $this->customerId]);
                $oneRequiredResult = true;
            }

            //if email is not null
            if ($tempValidation->validateNotNull([$this->email])) {
                $validation->validateEmail(['email' => $this->email]);
                $oneRequiredResult = true;
            }

            //if emailConfirmedHash is not null
            if ($tempValidation->validateNotNull([$this->emailConfirmedHash])) {
                $validation->validateNotNull(['emailConfirmedHash' => $this->emailConfirmedHash]);
                $oneRequiredResult = true;
            }

            //if all of them are null send error validation to validation
            if (!$oneRequiredResult)
                $validation->validateNotNull([$this->email]);
        }

        if($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'email' => $this->email,
            ]);

            $validation->validateEmail([
                'email' => $this->email,
            ]);
        }

        if($this->scenario == self::SCENARIO_UPDATE){
            $validation->validateRequired([
                'customerId' => $this->customerId,
            ]);
            $validation->validateNumeric([
                'customerId' => $this->customerId,
            ]);

            //validation only for not required attributes
            //use only for testing if they are not null - because result is global in class
            //otherwise use normal validation
            $tempValidation = new Validate();

            if($tempValidation->validateNotNull([$this->phone]))
                $validation->validatePhone(['telefon' => $this->phone]);

            if($this->isCompany == 0) {
                if($tempValidation->validateNotNull([$this->birthNumber]))
                    $validation->validateBirthNumber(['rodné číslo' => $this->birthNumber]);
            } else {
                if ($tempValidation->validateNotNull([$this->ico]))
                    $validation->validateICO(['IČO' => $this->ico]);

                if ($tempValidation->validateNotNull([$this->dic]))
                    $validation->validateDIC(['DIČ' => $this->dic]);
            }

            if ($tempValidation->validateNotNull([$this->bankCode]))
                $validation->validateBankCode(['kód banky' => $this->bankCode]);

            //ico or birthNumber have to be filled
            //if both are null then throw validation error
            //TODO only one can be not null
            /*if(!$tempValidation->validateNotNull([$this->ico]) && !$tempValidation->validateNotNull([$this->birthNumber])) {
                $validation->validateNotNull(['ico' => $this->ico]);
                $validation->validateNotNull(['birthNumber' => $this->birthNumber]);
            }*/
        }

        if(!$validation->isValidationResult()){
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

	/**
	 * @return array|string
	 */
	public static function getCustomers() {
		$db = MyDB::getConnection();
		$customersData = $db->queryAll("SELECT * FROM customer ORDER BY name", [
		]);

		if (count($customersData) < 1)
			return [];
		else if (!(new Validate())->validateNotNull([$customersData]))
			return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst zákazníky z DB');

		$customers = [];
		foreach ($customersData as $customerData) {
			$customers[] = new Customer(
				$customerData['customerId'],
				$customerData['name'],
				$customerData['phonePrefix'],
				$customerData['phone'],
				$customerData['email'],
				$customerData['birthNumber'],
				$customerData['ico'],
				$customerData['dic'],
				$customerData['isCompany'],
				$customerData['bankAccountNumber'],
				$customerData['bankCode'],
				$customerData['registrationDate'],
				$customerData['lastLoginDate'],
				'',
				'',
				$customerData['isEmailConfirmed'],
				'',
				'',
				$customerData['customerGroupId']
			);
		}

		return $customers;
	}

    /**
     * Group setter
     * @param $name
     * @param $phonePrefix
     * @param $phone
     * @param $email
     * @param $birthNumber
     * @param $ico
     * @param $dic
     * @param $isCompany
     * @param $bankAccountNumber
     * @param $bankCode
     */
    public function setCustomer($name, $phonePrefix, $phone, $email, $birthNumber, $ico, $dic, $isCompany, $bankAccountNumber, $bankCode){
        $this->name = $name;
        $this->phonePrefix = $phonePrefix;
        $this->phone = $phone;
        $this->email = $email;
        $this->birthNumber = $birthNumber;
        $this->ico = $ico;
        $this->dic = $dic;
        $this->isCompany = $isCompany;
        $this->bankAccountNumber = $bankAccountNumber;
        $this->bankCode = $bankCode;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setCustomerId($customerId) {
        $this->customerId = $customerId;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * @param mixed $phonePrefix
     */
    public function setPhonePrefix($phonePrefix)
    {
        $this->phonePrefix = $phonePrefix;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    public function setBankAccountNumber($bankAccountNumber) {
        $this->bankAccountNumber = $bankAccountNumber;
    }

    public function setBankCode($bankCode) {
        $this->bankCode = $bankCode;
    }

    public function setIsCompany($isCompany) {
        $this->isCompany = $isCompany;
    }

    public function setBirthNumber($birthNumber) {
        $this->birthNumber = $birthNumber;
    }

    public function setIco($ico) {
        $this->ico = $ico;
    }

    public function setDic($dic) {
        $this->dic = $dic;
    }

    public function setPlainPassword($plainPassword){
        $this->plainPassword = $plainPassword;
    }

    public function setPlainPasswordConfirm($plainPasswordConfirm) {
        $this->plainPasswordConfirm = $plainPasswordConfirm;
    }

    public function setEmailConfirmedHash($emailConfirmedHash) {
        $this->emailConfirmedHash = $emailConfirmedHash;
    }

    public function setIsEmailConfirmed($isEmailConfirmed) {
        $this->isEmailConfirmed = $isEmailConfirmed;
    }

    public function setForgottenPasswordHash($forgottenPasswordHash) {
        $this->forgottenPasswordHash = $forgottenPasswordHash;
    }

    public function getCustomerId()
    {
        return $this->customerId;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPhonePrefix()
    {
        return $this->phonePrefix;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getForgottenPasswordHash()
    {
        return $this->forgottenPasswordHash;
    }

    /**
     * @return mixed
     */
    public function getIco()
    {
        return $this->ico;
    }

    /**
     * @return mixed
     */
    public function getDic()
    {
        return $this->dic;
    }

    /**
     * @return mixed
     */
    public function getBirthNumber()
    {
        return $this->birthNumber;
    }

    /**
     * @return mixed
     */
    public function getBankAccountNumber()
    {
        return $this->bankAccountNumber;
    }

    /**
     * @return mixed
     */
    public function getBankCode()
    {
        return $this->bankCode;
    }

    /**
     * @return bool|string
     */
    public function getHashedPassword() {
        return $this->hashedPassword;
    }

    /**
     * @return bool
     */
    public function isCompany() {
        if($this->isCompany == 0)
            return false;
        else
            return true;
    }

    /**
     * @return bool
     */
    public function isEmailConfirmed() {
        if($this->isEmailConfirmed == 0)
            return false;
        else
            return true;
    }

	/**
	 * @return int
	 */
	public function getCustomerGroupId() {
		return $this->customerGroupId;
	}

	/**
	 * @param int $customerGroupId
	 */
	public function setCustomerGroupId( $customerGroupId ) {
		$this->customerGroupId = $customerGroupId;
	}
}

?>