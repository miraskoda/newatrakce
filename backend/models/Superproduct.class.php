<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 08.04.2018
 * Time: 16:33
 */

namespace backend\models;


use backend\controllers\ProductController;
use backend\database\DatabaseError;
use backend\database\MyDB;

class Superproduct
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;

    protected $db;
    protected $scenario;

    private $superproductId;
    private $name;
    private $quantity;

    /**
     * Superproduct constructor.
     * @param int $superproductId
     * @param string $name
     * @param int $quantity
     */
    public function __construct($superproductId = 0, $name = '', $quantity = 0)
    {
        $this->db = MyDB::getConnection();

        $this->superproductId = $superproductId;
        $this->name = $name;
        $this->quantity = $quantity;
    }

    /**
     * @return Superproduct|mixed|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
            $productData = $this->db->queryOne('SELECT * FROM superproduct WHERE superproductId = ?', [
                $this->superproductId
            ]);

            if (!(new Validate())->validateNotNull([$productData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst superprodukt z DB');

            $this->superproductId = $productData['superproductId'];
            $this->name = $productData['name'];
            $this->quantity = $productData['quantity'];

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return Superproduct|mixed|string
     */
    public function create()
    {
        $this->scenario = self::SCENARIO_CREATE;

        if ($this->validate()) {
            $insert = $this->db->query("INSERT INTO superproduct (name, quantity) VALUES (?, ?)", [
                $this->name,
                $this->quantity
            ]);

            if ($insert > 0) {
                $this->superproductId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return Superproduct|mixed|string
     */
    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;

        if ($this->validate()) {
            $update = $this->db->query("UPDATE superproduct SET name = ?, quantity = ? WHERE superproductId = ?", [
                $this->name,
                $this->quantity,
                $this->superproductId
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->query("DELETE FROM superproduct WHERE superproductId = ?", [
            $this->superproductId
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * Validates if Superproduct attributes are complete and valid.
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_LOAD) {
            $validation->validateRequired([
                'superproductId' => $this->superproductId
            ]);
        }

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'name' => $this->name
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param bool $asArray
     * @return array|string
     */
    public static function getSuperproducts($asArray = true) {
        $db = MyDB::getConnection();
        $data = $db->queryAll("SELECT * FROM superproduct", []);

        if (count($data) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$data]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst superprodukty z DB');

        $superproducts = [];
        foreach ($data as $superproduct) {
            $superproductObj = new Superproduct(
                $superproduct['superproductId'],
                $superproduct['name'],
                $superproduct['quantity']
            );
            if($asArray)
                $superproducts[] = $superproductObj->_toArray();
            else
                $superproducts[] = $superproductObj;
        }

        return $superproducts;
    }

    /**
     * @param $productId
     * @param $date
     * @param $hours
     *
     * @param bool $divideByProductQuantity
     * @return Superproduct|SuperproductProduct|float|int
     */
	public static function getAvailableSuperproductPieces($productId, $date, $hours, $divideByProductQuantity = true) {
		$db = MyDB::getConnection();

		$superproduct = new Superproduct(SuperproductProduct::getSuperproductByProduct($productId));
		$superproduct = $superproduct->load();

		$sumNumberOfOrderedProducts = 0;
		if(is_a($superproduct, Superproduct::class)) {
		 	$superproductProduct = new SuperproductProduct(0, $superproduct->getSuperproductId(), $productId);
			$superproductProduct = $superproductProduct->load();

			if(!is_a($superproductProduct, SuperproductProduct::class))
				return $superproductProduct;

			$products = SuperproductProduct::getProductsBySuperproduct( $superproduct->getSuperproductId(), false );

			foreach ( $products as $product ) {
				// warning in getQuantity is superproduct_product quantity
				$sumNumberOfOrderedProducts += ($product->getNumberOfOrderedPieces($date, $hours) * $product->getQuantity());
                $sumNumberOfOrderedProducts += $product->getNumberOfReservedPieces($date, $hours);
			}

			if($divideByProductQuantity)
			    return intval(floor(($superproduct->getQuantity() - $sumNumberOfOrderedProducts) / $superproductProduct->getQuantity()));
            else
                return intval(floor(($superproduct->getQuantity() - $sumNumberOfOrderedProducts)));
        } else {
			return $superproduct;
		}
	}

    /**
     * @return array
     */
    public function _toArray(){
        return [
            'superproductId' => $this->superproductId,
            'name' => $this->name,
            'quantity' => $this->quantity
        ];
    }

    /**
     * @return int
     */
    public function getSuperproductId()
    {
        return $this->superproductId;
    }

    /**
     * @param int $superproductId
     */
    public function setSuperproductId($superproductId)
    {
        $this->superproductId = $superproductId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
}