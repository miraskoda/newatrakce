<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 16.09.2017
 * Time: 18:44
 */

namespace backend\models;


use backend\database\DatabaseError;
use backend\database\MyDB;

class OrderState
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;

    private $db;
    private $scenario;

    private $orderStateId;
    private $name;
    private $slug;
    private $position;




	/**
	 * OrderState constructor.
	 *
	 * @param int $orderStateId
	 * @param string $name
	 * @param string $slug
	 * @param int $position
	 */
    public function __construct($orderStateId = 0, $name = '', $slug = '', $position = 0)
    {
        $this->db = MyDB::getConnection();
        $this->orderStateId = $orderStateId;
        $this->name = $name;
        $this->slug = $slug;
        $this->position = $position;
    }

    /**
     * Loads OrderState from DB by orderStateId.
     * @return OrderState|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
            $orderStateData = $this->db->queryOne("SELECT * FROM order_state WHERE orderStateId = ?", [
                $this->orderStateId
            ]);

            if (!(new Validate())->validateNotNull([$orderStateData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst status objednávky z DB');

            $this->name = $orderStateData['name'];
            $this->slug = $orderStateData['slug'];
            $this->position = $orderStateData['position'];

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Inserts new OrderState to DB.
     * No params required because all data are gained inside constructor.
     * @return OrderState|string
     */
    public function create()
    {
        $this->scenario = self::SCENARIO_CREATE;

        if ($this->validate()) {
            $insert = $this->db->query("INSERT INTO order_state (name, slug, position) VALUES (?,?,?)", [
                $this->name,
	            $this->slug,
	            $this->position
            ]);

            if ($insert > 0) {
                $this->orderStateId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Updates OrderState by orderStateId.
     * @return OrderState|string
     */
    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;

        if ($this->validate()) {
            $update = $this->db->query("UPDATE order_state SET name = ?, slug = ?, position = ? WHERE orderStateId = ?", [
                $this->name,
                $this->slug,
                $this->position,
                $this->orderStateId
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * Deletes OrderState by orderStateId.
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->query("DELETE FROM order_state WHERE orderStateId = ?", [
            $this->orderStateId
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * Validates if Order attributes are complete and valid.
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        if ($this->scenario == self::SCENARIO_LOAD || $this->scenario == self::SCENARIO_UPDATE) {
            $validation->validateRequired([
                'orderStateId' => $this->orderStateId,
            ]);
            $validation->validateNumeric([
                'orderStateId' => $this->orderStateId,
            ]);
        }

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'název' => $this->name,
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return array|string
     */
    public static function getOrderStates() {
        $db = MyDB::getConnection();
        $orderStateData = $db->queryAll("SELECT * FROM order_state ORDER BY position", []);

        if (count($orderStateData) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$orderStateData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst stavy objednávky z DB');

        $orderStates = [];
        foreach ($orderStateData as $index => $orderState) {
            $orderStates[] = new OrderState(
                $orderState['orderStateId'],
                $orderState['name'],
                $orderState['slug'],
                $orderState['position']
            );
        }

        return $orderStates;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getOrderStateId()
    {
        return $this->orderStateId;
    }

	/**
	 * @return string
	 */
	public function getSlug() {
		return $this->slug;
	}

	/**
	 * @param string $slug
	 */
	public function setSlug( $slug ) {
		$this->slug = $slug;
	}

	/**
	 * @return int
	 */
	public function getPosition() {
		return $this->position;
	}

	/**
	 * @param int $position
	 */
	public function setPosition( $position ) {
		$this->position = $position;
	}
}