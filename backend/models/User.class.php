<?php
namespace backend\models;

use backend\view\UserGrid;
use backend\controllers\UserController;
use backend\database\DatabaseError;
use backend\database\MyDB;

class User
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_DELETE = 3;
    const SCENARIO_LOAD = 4;
    const SCENARIO_CHANGE_PASSWORD = 5;
    const SCENARIO_LOGIN = 6;

    private $db;
    private $scenario;

    private $userId;
    private $name;
    private $email;
    private $isAdmin;
    private $lastLoginDate;

    private $plainPassword;
    private $plainPasswordConfirm;
    private $hashedPassword;

    private $forgottenPasswordHash;

    //These keys have to be equal to database
    /*private static $attributes = [
        'name' => 'Jméno',
        'email' => 'Email',
        'isAdmin' => 'Je admin',
        'lastLoginDate' => 'Datum posledního přihlášení',
    ];*/

    /**
     * User constructor.
     * @param int $userId
     * @param string $name
     * @param string $email
     * @param bool $isAdmin
     * @param string $lastLoginDate
     * @param string $plainPassword
     * @param string $plainPasswordConfirm
     * @param string $forgottenPasswordHash
     */
    public function __construct($userId = 0, $name = "", $email = "", $isAdmin = false, $lastLoginDate = "1990-01-01", $plainPassword = "", $plainPasswordConfirm = "", $forgottenPasswordHash = "")
    {
        $this->db = MyDB::getConnection();
        $this->userId = $userId;
        $this->lastLoginDate = $lastLoginDate;

        $this->plainPassword = $plainPassword;
        $this->plainPasswordConfirm = $plainPasswordConfirm;
        if ((new Validate())->validateNotNull([$this->plainPassword]) && (new Validate())->validateNotNull([$this->plainPasswordConfirm]) && (new Validate())->validateIdentical([$this->plainPassword, $this->plainPasswordConfirm]))
            $this->hashedPassword = Password::hashPassword($this->plainPassword);

        $this->forgottenPasswordHash = $forgottenPasswordHash;

        $this->setUser($name, $email, $isAdmin);
    }

    /**
     * @return User|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
            //email is preferred
            $userData = $this->db->queryOne("SELECT * FROM user WHERE email = ? OR userId = ?", [
                $this->email,
                $this->userId
            ]);

            if (!(new Validate())->validateNotNull([$userData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst uživatele z DB');

            $this->userId = $userData['userId'];
            $this->lastLoginDate = $userData['lastLoginDate'];

            $this->hashedPassword = $userData['password'];

            $this->forgottenPasswordHash = $userData['forgottenPasswordHash'];

            $this->setUser(
                $userData['name'],
                $userData['email'],
                $userData['isAdmin']
            );

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return User|string
     */
    public function create()
    {
        $this->scenario = self::SCENARIO_CREATE;

        if ($this->validate()) {
            //if hashedPassword is not set - try to create
            if (!(new Validate())->validateNotNull([$this->hashedPassword]))
                $this->hashedPassword = Password::hashPassword($this->plainPassword);

            $insert = $this->db->query("INSERT INTO user (name, email, isAdmin, password, forgottenPasswordHash, lastLoginDate) VALUES (?,?,?,?,?,?)", [
                $this->name,
                $this->email,
                $this->isAdmin,
                $this->hashedPassword,
                $this->forgottenPasswordHash,
                date('Y-m-d H:i:s')
            ]);

            if ($insert > 0) {
                $this->userId = $this->db->lastInsertId();
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::INSERT);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return User|string
     */
    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;

        if ($this->validate()) {
            $update = $this->db->query("UPDATE user SET name = ?, email = ?, isAdmin = ?, forgottenPasswordHash = ?, lastLoginDate = ? WHERE userId = ? AND password = ?", [
                $this->name,
                $this->email,
                $this->isAdmin,
                $this->forgottenPasswordHash,
                date('Y-m-d H:i:s'),
                $this->userId,
                $this->hashedPassword
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return bool|string
     */
    public function delete()
    {
        $delete = $this->db->query("DELETE FROM user WHERE userId = ? AND password = ?", [
            $this->userId,
            $this->hashedPassword
        ]);

        if ($delete > 0) {
            return true;
        } else {
            return DatabaseError::getErrorDescription(DatabaseError::DELETE);
        }
    }

    /**
     * @return bool|mixed
     */
    public function login()
    {
        $this->scenario = self::SCENARIO_LOGIN;

        if ($this->validate()) {
            $_SESSION['user'] = $this->userId;

            //change last login in db - no result required
            $this->db->query("UPDATE user SET lastLoginDate = ? WHERE userId = ?", [
                date('Y-m-d H:i:s'),
                $this->userId
            ]);

            return true;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return User|string
     */
    public function changePassword()
    {
        $this->scenario = self::SCENARIO_CHANGE_PASSWORD;

        if ($this->validate()) {
            // must rehash password because plain changed
            $this->hashedPassword = Password::hashPassword($this->plainPassword);

            $update = $this->db->query("UPDATE user SET password = ? WHERE userId = ?", [
                $this->hashedPassword,
                $this->userId
            ]);

            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return bool|string
     */
    public function sendForgottenPasswordEmail() {
        $this->forgottenPasswordHash = Password::generatePasswordHash($this->email);
        $this->update();

        $mail = new MailHelper();
        return $mail->sendForgottenPassword($this->email, $this->forgottenPasswordHash, true);
    }

    /**
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        if ($this->scenario == self::SCENARIO_CHANGE_PASSWORD || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'password' => $this->plainPassword,
            ]);
            $validation->validatePassword([
                'password' => $this->plainPassword,
            ]);
        }

        if ($this->scenario == self::SCENARIO_LOGIN) {
            $validation->validateRequired([
                'email' => $this->email,
            ]);
            $validation->validateEmail([
                'email' => $this->email,
            ]);

            $validation->validateRequired([
                'password' => $this->plainPassword,
            ]);

            $validation->validateRequired([
                'password' => $this->hashedPassword,
            ]);

            if (!Password::verifyPassword($this->plainPassword, $this->hashedPassword)) {
                $validation->addValidationError(new ValidationError(ValidationError::LOGIN));
            }
        }

        //userId or email is required
        if ($this->scenario == self::SCENARIO_LOAD) {
            $tempValidation = new Validate();
            $oneRequiredResult = false;

            //if userId is not null
            if ($tempValidation->validateNotNull([$this->userId])) {
                $validation->validateNumeric(['userId' => $this->userId]);
                $oneRequiredResult = true;
            }

            //if email is not null
            if ($tempValidation->validateNotNull([$this->email])) {
                $validation->validateEmail(['email' => $this->email]);
                $oneRequiredResult = true;
            }

            //if both are null send error validation to validation
            if (!$oneRequiredResult)
                $validation->validateNotNull([$this->email]);
        }

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_DELETE) {
            $validation->validateRequired([
                'userId' => $this->userId,
            ]);
            $validation->validateNumeric([
                'userId' => $this->userId,
            ]);

            $validation->validateRequired([
                'password' => $this->hashedPassword,
            ]);
        }

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_CREATE) {
            $validation->validateRequired([
                'email' => $this->email,
            ]);
            $validation->validateEmail([
                'email' => $this->email,
            ]);

            //validation only for not required attributes
            //use only for testing if they are not null - because result is global in class
            //otherwise use normal validation
            $tempValidation = new Validate();

            /*if ($tempValidation->validateNotNull([$this->isAdmin])) {
                $validation->validateBoolean(['isAdmin' => $this->isAdmin]);*/
        }

        if ($this->scenario == self::SCENARIO_CREATE) {
            $validation->validateUnique([
                'email' => $this->email,
            ], 'user', 'email');

            $validation->validateRequired([
                'password' => $this->plainPassword,
            ]);
            $validation->validateRequired([
                'password-again' => $this->plainPasswordConfirm,
            ]);

            $validation->validateIdentical([
                'password' => $this->plainPassword,
                'password-again' => $this->plainPasswordConfirm,
            ]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    /**
     * TODO add user params
     * @return array|string
     */
    public static function getUsers()
    {
        //select only users with isAdmin = 0 (not hidden admins)
        $db = MyDB::getConnection();
        $usersData = $db->queryAll("SELECT * FROM user WHERE isAdmin = 0", []);

        if (count($usersData) < 1)
            return [];
        else if (!(new Validate())->validateNotNull([$usersData]))
            return DatabaseError::getErrorDescription(DatabaseError::QUERY_ALL, 'Nebylo možné načíst uživatele z DB');

        $users = [];
        foreach ($usersData as $userData) {
            $users[] = new User(
                $userData['userId'],
                $userData['name'],
                $userData['email'],
                $userData['isAdmin'],
                $userData['lastLoginDate']
            );
        }

        return $users;
    }

    /**
     * @return string
     */
    public static function getLastCreatedUser()
    {
        if (is_a(UserController::isLoggedUser(), User::class)) {
            $userData = MyDB::getConnection()->queryOne("SELECT * FROM user WHERE userId = (SELECT MAX(userId) FROM user)", []);

            if (!(new Validate())->validateNotNull([$userData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst uživatele z DB');

            $user = new User($userData['userId'], $userData['name'], $userData['email'], $userData['isAdmin'], $userData['lastLoginDate']);
            $user = $user->load();
            if (!is_a($user, User::class))
                return $user;

            return UserGrid::generateUserGridCell($user);
        }

        return false;
    }

    /**
     * Group setter
     * @param $name
     * @param $email
     * @param $isAdmin
     */
    public function setUser($name, $email, $isAdmin)
    {
        $this->name = $name;
        $this->email = $email;
        $this->isAdmin = $isAdmin;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getEmail()
    {
        return $this->email;
    }

    //TODO remake
    public function getLastVisit()
    {
        $time = strtotime($this->lastLoginDate);
        if (date("Y", $time) < 2010)
            return date("d.m.Y");
        else
            return date("d.m.Y", $time);
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param string $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param $plainPasswordConfirm
     */
    public function setPlainPasswordConfirm($plainPasswordConfirm)
    {
        $this->plainPasswordConfirm = $plainPasswordConfirm;
    }

    /**
     * @param string $forgottenPasswordHash
     */
    public function setForgottenPasswordHash($forgottenPasswordHash)
    {
        $this->forgottenPasswordHash = $forgottenPasswordHash;
    }

    /**
     * @return string
     */
    public function getForgottenPasswordHash()
    {
        return $this->forgottenPasswordHash;
    }
}

?>