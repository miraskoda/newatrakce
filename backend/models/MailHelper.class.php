<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 08.08.2017
 * Time: 11:29
 */

namespace backend\models;

use backend\controllers\CustomerController;
use DateTime;
use PHPMailer;
use backend\models\ProductImage;


class MailHelper
{
    private $mail;


    public function __construct() {
        $this->initMailer();
    }

    private function initMailer() {
        $this->mail = new PHPMailer();
        $this->mail->CharSet = 'UTF-8';
        $this->mail->setLanguage('cs');
        $this->mail->isHTML(true);
    }

    /**
     * @param $email
     * @param $hash
     * @return bool|string
     */
    public function sendEmailConfirm($email, $hash) {
        //From email address and name
        $this->mail->From = "no-reply@onlineatrakce.cz";
        $this->mail->FromName = "OnlineAtrakce.cz";
        //Address to which recipient will reply
        $this->mail->addReplyTo("info@onlineatrakce.cz", "OnlineAtrakce.cz");

        //To address and name
        $this->mail->addAddress($email);

        $this->mail->Subject = "Ověřovací email z OnlineAtrakce.cz";
        $this->mail->Body = $this->getConfirmEmailBody($email, $hash);

        if(!$this->mail->send())
        {
            return "Nastala chyba v odesílání emailů: " . $this->mail->ErrorInfo;
        }
        else
        {
            return true;
        }
    }

    /**
     * @param $email
     * @param $hash
     * @param bool $isAdmin
     * @return bool|string
     */
    public function sendForgottenPassword($email, $hash, $isAdmin = false) {
        //From email address and name
        $this->mail->From = "no-reply@onlineatrakce.cz";
        $this->mail->FromName = "OnlineAtrakce.cz";
        //Address to which recipient will reply
        $this->mail->addReplyTo("info@onlineatrakce.cz", "OnlineAtrakce.cz");

        //To address and name
        $this->mail->addAddress($email);

        $this->mail->Subject = "Zapomenuté heslo na OnlineAtrakce.cz";
        $this->mail->Body = $this->getForgottenPasswordBody($email, $hash, $isAdmin);

        if(!$this->mail->send())
        {
            return "Nastala chyba v odesílání emailů: " . $this->mail->ErrorInfo;
        }
        else
        {
            return true;
        }
    }

    /**
     * @param $email
     * @param $orderNo
     * @param $newState
     * @return bool|string
     */
    public function sendOrderStatusUpdate($email, $orderNo, $newState) {
        //From email address and name
        $this->mail->From = "no-reply@onlineatrakce.cz";
        $this->mail->FromName = "OnlineAtrakce.cz";
        //Address to which recipient will reply
        $this->mail->addReplyTo("info@onlineatrakce.cz", "OnlineAtrakce.cz");

        //To address and name
        $this->mail->addAddress($email);

        $this->mail->Subject = "Aktualizace stavu objednávky č. " . $orderNo . " na OnlineAtrakce.cz";
        $this->mail->Body = $this->getOrderStatusUpdateBody($orderNo, $newState);

        if(!$this->mail->send())
        {
            return "Nastala chyba v odesílání emailů: " . $this->mail->ErrorInfo;
        }
        else
        {
            return true;
        }
    }

	/**
	 * @param $email
	 * @param $orderNo
	 * @param Order $order
	 *
	 * @return PaymentType|bool|string
	 */
	public function sendOrder($email, $orderNo, $order) {
		//From email address and name
		$this->mail->From = "no-reply@onlineatrakce.cz";
		$this->mail->FromName = "OnlineAtrakce.cz";
		//Address to which recipient will reply
		$this->mail->addReplyTo("info@onlineatrakce.cz", "OnlineAtrakce.cz");

		//To address and name
		$this->mail->addAddress($email);

		$this->mail->Subject = "Nová objednávka č. " . $orderNo . " na OnlineAtrakce.cz";
		$this->mail->Body = $this->getOrderBody($orderNo, $order);

		$paymentType = new PaymentType($order->getPaymentTypeId());
		$paymentType = $paymentType->load();

		if(!is_a($paymentType, PaymentType::class))
			return $paymentType;

        if($paymentType->getDeposit() > 0) {

            $pdfContent = file_get_contents('https://'.$_SERVER["SERVER_NAME"].'/pages/customer/order/files/generate-invoice.php?orderId=' . $order->getOrderId());

            $this->mail->addStringAttachment($pdfContent, 'doklad' . DateTime::createFromFormat('Y-m-d H:i:s', $order->getChangedRaw())->format('y') . $order->getOrderId() . '01.pdf',
                $encoding = 'base64', $type = 'application/pdf');
        }

		if(!$this->mail->send())
		{
			return "Nastala chyba v odesílání emailů: " . $this->mail->ErrorInfo;
		}
		else
		{
			return true;
		}
	}



    public function sendConfirmation($email, $order) {
        //From email address and name
        $this->mail->From = "no-reply@onlineatrakce.cz";
        $this->mail->FromName = "OnlineAtrakce.cz";
        //Address to which recipient will reply
        $this->mail->addReplyTo("info@onlineatrakce.cz", "OnlineAtrakce.cz");

        //To address and name
        $this->mail->addAddress($email);

        $this->mail->Subject = "Doklad o platbě za objednávku č. " . $order->getOrderId() . " - OnlineAtrakce.cz";
        $this->mail->Body = $this->getOrderBodyConf($order->getOrderId(), $order);



            $pdfContent = file_get_contents('https://'.$_SERVER["SERVER_NAME"].'/pages/customer/order/files/generate-confirmation.php?orderId=' . $order->getOrderId());

            $this->mail->addStringAttachment($pdfContent, 'OnlineAtrakce.cz-dokl' . $order->getOrderId() . '.pdf',
                $encoding = 'base64', $type = 'application/pdf');


        if(!$this->mail->send())
        {
            return "Nastala chyba v odesílání emailů: " . $this->mail->ErrorInfo;
        }
        else
        {
            return true;
        }
    }

    /**
     * @param $email
     * @param Reservation $reservation
     * @return bool|string
     */
    public function sendNewReservation($email, $reservation) {
        //From email address and name
        $this->mail->From = "no-reply@onlineatrakce.cz";
        $this->mail->FromName = "OnlineAtrakce.cz";
        //Address to which recipient will reply
        $this->mail->addReplyTo("info@onlineatrakce.cz", "OnlineAtrakce.cz");

        //To address and name
        $this->mail->addAddress($email);

        $this->mail->Subject = "Nová rezervace na OnlineAtrakce.cz";
        $this->mail->Body = $this->getNewReservation($reservation);

        if(!$this->mail->send())
        {
            return "Nastala chyba v odesílání emailů: " . $this->mail->ErrorInfo;
        }
        else
        {
            return true;
        }
    }

    /**
     * @param $email
     * @param Reservation $reservation
     * @return bool|string
     */
    public function sendReservationWarning($email, $reservation) {
        //From email address and name
        $this->mail->From = "no-reply@onlineatrakce.cz";
        $this->mail->FromName = "OnlineAtrakce.cz";
        //Address to which recipient will reply
        $this->mail->addReplyTo("info@onlineatrakce.cz", "OnlineAtrakce.cz");

        //To address and name
        $this->mail->addAddress($email);

        $this->mail->Subject = "Vaše rezervace na OnlineAtrakce.cz již brzy vyprší";
        $this->mail->Body = $this->getReservationWarning($reservation);

        if(!$this->mail->send())
        {
            return "Nastala chyba v odesílání emailů: " . $this->mail->ErrorInfo;
        }
        else
        {
            return true;
        }
    }


    public function getOrderBodyConf($orderNo, $order) {


        $html = '<head>
            <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700&subset=latin,latin-ext" rel="stylesheet">
            <meta charset="UTF-8">            
            <style>
                ' . file_get_contents(__DIR__ . "/../../styles/file-style.css") . '
                .border-around {
                	border: 1px solid black;
                }
            </style>
        </head>
        <body>
            <h2>Doklad o platbě za objednávku č. ' . $orderNo . ' - OnlineAtrakce.cz</h2>';

        $discount = $order->getDiscount();

        $html .= '<h4>Doklad o platbě</h4>';

        $stringifiedRentHours = OrderTerm::getRentHoursAsStrings($order);
        $products = OrderProduct::getOrderProducts($order);

        /*$invoiceAddress = new Address();
        $invoiceAddress->setOrderId($order->getOrderId());
        $invoiceAddress->setAddressType(Address::ADDRESS_INVOICE);
        $invoiceAddress = $invoiceAddress->load();*/

        $payment = new Payment($order->getPaymentId());
        $payment = $payment->load();
        $paymentType = new PaymentType($order->getPaymentTypeId());
        $paymentType = $paymentType->load();

        $terms = OrderTerm::getOrderTermsByOrder($order);
        $numOfTerms = count(array_filter($stringifiedRentHours, function($x) {return ($x > 0);}));
        if($numOfTerms < 1)
            $numOfTerms = 1;
        else if ($numOfTerms > 5)
            $numOfTerms = 5;

        $html .= '<table class="border-around">';

        $html .= '<thead>
            <tr class="padding-around">
                <td class="center bold first">Fotografie</td>
                <td class="center bold first">Název</td>
                <td class="center bold first last">Množství</td>
                <td class="center bold first last">Jednotková cena</td>
                <td class="center bold first last">Sleva</td>
                <td class="center bold first last">Celkem</td>
            </tr>
            <tr class="line-with-spaces"><td colspan="5"></td></tr>
          </thead>';

        foreach ( $products as $index => $product ) {
            // only for long output testing
            /*for ($i = 0; $i < 30; $i++) {
                $product = $products[0];*/

            $sumPrice = 0;
            foreach ( $stringifiedRentHours as $rentHour ) {
                if ( $rentHour > 0 ) {
                    $sumPrice += ( ( $rentHour <= 5 ) ?
                        ( ( $product['price'] ) * $product['quantity'] )
                        :
                        ( ( ( $product['price'] ) + ( ( $product['price'] / 100 ) * ( 5 * ( $rentHour - ( ( $rentHour > 5 ) ? 5 : $rentHour ) ) ) ) ) * $product['quantity'] )
                    );
                }
            }
            $productImageMain = ProductImage::getProductImagesByName($product['name'], ProductImage::TYPE_MAIN);

            $html .= '<tr class="padding-around">
            <td class="left bold first"> 

            <img style="height: 70px;border:0;" height="70" src="https://'.$_SERVER["SERVER_NAME"] .'/assets/images/products/' . $productImageMain[0]['name'] .'"/></td>
            
            <td class="right first last">' . $product['name'] . '</td>
            <td class="right first last">' . $product['quantity'] . ' ks</td>
            <td class="right first last">' . CustomCurrency::setDecimals( round( $product['price'] ) ) . '</td>
            <td class="right first last">' . $discount . '%</td>
            <td class="right first last">' . CustomCurrency::setDecimals( round( $sumPrice ) ) . '</td>
          </tr>';

            if ( $numOfTerms > 1 ) {
                $counter = 1;
                foreach ( $stringifiedRentHours as $rentHour ) {
                    if ( $rentHour > 0 ) {
                        $html .= '<tr class="padding-around"><td class="right bold">' . $counter . '. termín</td><td colspan="3"></td><td class="right first last">' .
                            ( ( $rentHour <= 5 ) ?
                                CustomCurrency::setDecimals( round( ( $product['price'] ) * $product['quantity'] ) )
                                :
                                CustomCurrency::setDecimals( round( ( ( $product['price'] ) + ( ( $product['price'] / 100 ) * ( 5 * ( $rentHour - ( ( $rentHour > 5 ) ? 5 : $rentHour ) ) ) ) ) * $product['quantity'] ) )
                            )
                            . '</td></tr>';
                        $counter ++;
                    }
                }
                $html .= '<tr class="line-with-spaces"><td colspan="5"></td></tr>';
            }
        }

        $html .= '<tr class="padding-around">
            <td class="left bold first padding-bottom"></td>

            <td class="left bold first padding-bottom">Doprava</td>
            <td class="right first last padding-bottom">' . (($order->getDeliveryPaymentPrice() > 0) ? CustomCurrency::setDecimals($order->getDeliveryPaymentPrice() / 6) : 0) . ' km</td>
            <td class="right first last padding-bottom">' . CustomCurrency::setDecimals(6) . '</td>
            <td class="right first last padding-bottom">' . (($order->getDeliveryPaymentPrice() > 0) ? '0%' : '100%') . '</td>
            <td class="right first last padding-bottom">' . CustomCurrency::setDecimals($order->getDeliveryPaymentPrice()) . '</td>
          </tr>';

        $html .= '</table>';

        // sum price
        $html .= '<table class="border-around">';

        $html .= '<br>';

        $html .= '<tr class="padding-around">
        <td colspan="3" class="right bold first last padding-bottom bigger-font"><h4>Celkem zaplaceno:</h4></td>
        <td class="right bold fist last padding-bottom bigger-font"><h4>' . CustomCurrency::setCustomCurrencyWithoutDecimals( round( $order->getSumPrice() ) ) . '</h4></td>
      </tr>';

        $html .= '</table>';

        $html .= '<p>Další detaily objednávky najdete <a href="https://www.onlineatrakce.cz/prehled-objednavek/detail/' . $order->getOrderId() . '/">ve svém účtu na OnlineAtrakce.cz</a>.</p>';
        $html .= '<p>Informace k platbě najdete <a href="https://www.onlineatrakce.cz/prehled-objednavek/detail/' . $order->getOrderId() . '/platba/">zde</a>.</p>';

        if($paymentType->getDeposit() > 0)
            $html .= '<p>V příloze tohoto emailu naleznete dokumenty k této objednávce.</p>';

        $html .= '<br>
            <p>Děkujeme za Váš nákup.<br>S pozdravem<br>OnlineAtrakce.cz</p>
        </body>';

        return $html;
    }



    /**
     * @param $email
     * @param $hash
     * @return string
     */
    private function getConfirmEmailBody($email, $hash) {
        return '<head>
            <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700&subset=latin,latin-ext" rel="stylesheet">
            <meta charset="UTF-8">            
            <style>
                body {
                    font-family: \'Comfortaa\', cursive;
                }
            </style>
        </head>
        <body>
            <p>Na emailovou adresu ' . $email . ' byl vytvořen nový účet na webu OnlineAtrakce.cz.</p>
            <br>
            <p><strong>Pro ověření tohoto emailu přejděte na odkaz <a href="https://www.onlineatrakce.cz/overeni-emailu/' . $hash . '">http://www.onlineatrakce.cz/overeni-emailu/' . $hash . '</a></strong></p>
            <br>
            <p>Pokud kliknutí nefunguje, zkopírujte celou adresu bez mezer do prohlížeče.</p>
            <p>Pokud jste se neregistrovali, tento email ignorujte.</p>
            <br>
            <p>Děkujeme<br>OnlineAtrakce.cz</p>
        </body>';
    }

    /**
     * @param $email
     * @param $hash
     * @param bool $isAdmin
     * @return string
     */
    private function getForgottenPasswordBody($email, $hash, $isAdmin = false) {
        return '<head>
            <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700&subset=latin,latin-ext" rel="stylesheet">
            <meta charset="UTF-8">            
            <style>
                body {
                    font-family: \'Comfortaa\', cursive;
                }
            </style>
        </head>
        <body>
            <p>Pro účet na OnlineAtrakce.cz s emailem ' . $email . ' byla vyžádána změna hesla.</p>
            <br>
            <p><strong>Pokud chcete heslo změnit, přejděte na odkaz <a href="https://www.onlineatrakce.cz/' . (($isAdmin) ? 'admin/' : '' ) . 'zapomenute-heslo/' . $email . '/' . $hash . '">http://www.onlineatrakce.cz/' . (($isAdmin) ? 'admin/' : '' ) . 'zapomenute-heslo/' . $email . '/' . $hash . '</a></strong></p>
            <br>
            <p>Pokud kliknutí nefunguje, zkopírujte celou adresu bez mezer do prohlížeče.</p>
            <p>Pomocí tohoto odkazu je možné heslo změnit pouze 30 minut po odeslání emailu, poté bude nutné vyžádat nový.</p>
            <p>Pokud jste změnu hesla nevyžádali, tento email ignorujte.</p>
            <br>
            <p>Děkujeme<br>OnlineAtrakce.cz</p>
        </body>';
    }

    private function getOrderStatusUpdateBody($orderNo, $newState) {
        return '<head>
            <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700&subset=latin,latin-ext" rel="stylesheet">
            <meta charset="UTF-8">            
            <style>
                body {
                    font-family: \'Comfortaa\', cursive;
                }
            </style>
        </head>
        <body>
            <p>Stav objednávky s číslem ' . $orderNo . ' byl aktualizován.</p>
            <br>
            <p><strong>Nový stav je: ' . $newState . '</strong></p>
            <br>
            <br>
            <p>S pozdravem<br>OnlineAtrakce.cz</p>
        </body>';
    }

	/**
	 * @param $orderNo
	 * @param Order $order
	 *
	 * @return string
	 */
	public function getOrderBody($orderNo, $order) {


        $html = '<head>
            <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700&subset=latin,latin-ext" rel="stylesheet">
            <meta charset="UTF-8">            
            <style>
                ' . file_get_contents(__DIR__ . "/../../styles/file-style.css") . '
                .border-around {
                	border: 1px solid black;
                }
            </style>
        </head>
        <body>
            <h2>Nová objednávka č. ' . $orderNo . ' na OnlineAtrakce.cz</h2>';

		$discount = $order->getDiscount();

		$html .= '<h4>Výpis objednávky</h4>';

		$stringifiedRentHours = OrderTerm::getRentHoursAsStrings($order);
		$products = OrderProduct::getOrderProducts($order);

		/*$invoiceAddress = new Address();
		$invoiceAddress->setOrderId($order->getOrderId());
		$invoiceAddress->setAddressType(Address::ADDRESS_INVOICE);
		$invoiceAddress = $invoiceAddress->load();*/

		$payment = new Payment($order->getPaymentId());
		$payment = $payment->load();
		$paymentType = new PaymentType($order->getPaymentTypeId());
		$paymentType = $paymentType->load();

		$terms = OrderTerm::getOrderTermsByOrder($order);
		$numOfTerms = count(array_filter($stringifiedRentHours, function($x) {return ($x > 0);}));
		if($numOfTerms < 1)
			$numOfTerms = 1;
		else if ($numOfTerms > 5)
			$numOfTerms = 5;

		$html .= '<table class="border-around">';

		$html .= '<thead>
            <tr class="padding-around">
                <td class="center bold first">Fotografie</td>
                <td class="center bold first">Název</td>
                <td class="center bold first last">Množství</td>
                <td class="center bold first last">Jednotková cena</td>
                <td class="center bold first last">Sleva</td>
                <td class="center bold first last">Celkem</td>
            </tr>
            <tr class="line-with-spaces"><td colspan="5"></td></tr>
          </thead>';

		foreach ( $products as $index => $product ) {
			// only for long output testing
			/*for ($i = 0; $i < 30; $i++) {
				$product = $products[0];*/

			$sumPrice = 0;
			foreach ( $stringifiedRentHours as $rentHour ) {
				if ( $rentHour > 0 ) {
					$sumPrice += ( ( $rentHour <= 5 ) ?
						( ( $product['price'] ) * $product['quantity'] )
						:
						( ( ( $product['price'] ) + ( ( $product['price'] / 100 ) * ( 5 * ( $rentHour - ( ( $rentHour > 5 ) ? 5 : $rentHour ) ) ) ) ) * $product['quantity'] )
					);
				}
			}
            $productImageMain = ProductImage::getProductImagesByName($product['name'], ProductImage::TYPE_MAIN);

			$html .= '<tr class="padding-around">
            <td class="left bold first"> 

            <img style="height: 70px;border:0;" height="70" src="https://'.$_SERVER["SERVER_NAME"] .'/assets/images/products/' . $productImageMain[0]['name'] .'"/></td>
            
            <td class="right first last">' . $product['name'] . '</td>
            <td class="right first last">' . $product['quantity'] . ' ks</td>
            <td class="right first last">' . CustomCurrency::setDecimals( round( $product['price'] ) ) . '</td>
            <td class="right first last">' . $discount . '%</td>
            <td class="right first last">' . CustomCurrency::setDecimals( round( $sumPrice ) ) . '</td>
          </tr>';

			if ( $numOfTerms > 1 ) {
				$counter = 1;
				foreach ( $stringifiedRentHours as $rentHour ) {
					if ( $rentHour > 0 ) {
						$html .= '<tr class="padding-around"><td class="right bold">' . $counter . '. termín</td><td colspan="3"></td><td class="right first last">' .
						         ( ( $rentHour <= 5 ) ?
							         CustomCurrency::setDecimals( round( ( $product['price'] ) * $product['quantity'] ) )
							         :
							         CustomCurrency::setDecimals( round( ( ( $product['price'] ) + ( ( $product['price'] / 100 ) * ( 5 * ( $rentHour - ( ( $rentHour > 5 ) ? 5 : $rentHour ) ) ) ) ) * $product['quantity'] ) )
						         )
						         . '</td></tr>';
						$counter ++;
					}
				}
				$html .= '<tr class="line-with-spaces"><td colspan="5"></td></tr>';
			}
		}

		$html .= '<tr class="padding-around">
            <td class="left bold first padding-bottom"></td>

            <td class="left bold first padding-bottom">Doprava</td>
            <td class="right first last padding-bottom">' . (($order->getDeliveryPaymentPrice() > 0) ? CustomCurrency::setDecimals($order->getDeliveryPaymentPrice() / 6) : 0) . ' km</td>
            <td class="right first last padding-bottom">' . CustomCurrency::setDecimals(6) . '</td>
            <td class="right first last padding-bottom">' . (($order->getDeliveryPaymentPrice() > 0) ? '0%' : '100%') . '</td>
            <td class="right first last padding-bottom">' . CustomCurrency::setDecimals($order->getDeliveryPaymentPrice()) . '</td>
          </tr>';

		$html .= '</table>';

		// sum price
		$html .= '<table class="border-around">';

		if($discount > 0) {
			$discountAmount = round((floatval($order->getProductPrice()) / floatval(100 - $discount)) * floatval($discount));
			$html .= '<br><tr class="padding-around">
            <td colspan="3" class="right bold first last">Sleva ' . $discount . '%:</td>
            <td class="right fist last">-' . CustomCurrency::setDecimals($discountAmount) . '</td>
          </tr>';
		}

		$html .= '<tr class="padding-around">
        <td colspan="3" class="right bold first last">Celkem bez DPH:</td>
        <td class="right fist last">' . CustomCurrency::setDecimals( $order->getProductPrice() + $order->getDeliveryPaymentPrice() ) . '</td>
      </tr>';

		$html .= '<tr class="padding-around">
        <td colspan="3" class="right bold first last">DPH 21%:</td>
        <td class="right fist last">' . CustomCurrency::setDecimals( $order->getVat() ) . '</td>
      </tr>';

		$html .= '<tr class="padding-around">
        <td colspan="3" class="right bold first last">Celkem včetně DPH:</td>
        <td class="right fist last">' . CustomCurrency::setDecimals( round($order->getSumPrice()) ) . '</td>
      </tr>';

		$html .= '<tr class="padding-around">
        <td colspan="3" class="right bold first last padding-bottom bigger-font">Celkem k platbě včetně DPH:</td>
        <td class="right bold fist last padding-bottom bigger-font">' . CustomCurrency::setCustomCurrencyWithoutDecimals( round( $order->getSumPrice() ) ) . '</td>
      </tr>';

		$html .= '</table>';

		$html .= '<p>Další detaily objednávky najdete <a href="https://www.onlineatrakce.cz/prehled-objednavek/detail/' . $order->getOrderId() . '/">ve svém účtu na OnlineAtrakce.cz</a>.</p>';
		$html .= '<p>Informace k platbě najdete <a href="https://www.onlineatrakce.cz/prehled-objednavek/detail/' . $order->getOrderId() . '/platba/">zde</a>.</p>';

		if($paymentType->getDeposit() > 0)
			$html .= '<p>V příloze tohoto emailu naleznete dokumenty k této objednávce.</p>';

        $html .= '<br>
            <p>Děkujeme za Váš nákup.<br>S pozdravem<br>OnlineAtrakce.cz</p>
        </body>';

        return $html;
	}

    /**
     * @param Reservation $reservation
     * @return string
     */
    private function getNewReservation($reservation) {
	    $reservationProduct = new Product($reservation->getProductId());
	    $reservationProduct = $reservationProduct->load();

	    if(!is_a($reservationProduct, Product::class))
	        return 'Chyba v rezervaci. Prosím kontaktujte zákaznickou podporu.';

        return '<head>
            <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700&subset=latin,latin-ext" rel="stylesheet">
            <meta charset="UTF-8">            
            <style>
                body {
                    font-family: \'Comfortaa\', cursive;
                }
            </style>
        </head>
        <body>
            <p>Na OnlineAtrakce.cz jste vytvořil(a) novou rezervaci na produkt ' . $reservationProduct->getName() . '.</p>
            <br>
            <p><strong>Rezervace vyprší ' . $reservation->getReservationEnd() . '</strong></p>
            <br>
            <br>
            <p>Více informací o své rezervaci najdete v sekci <a href="https://www.onlineatrakce.cz/prehled-rezervaci/">Můj účet na OnlineAtrakce.cz</a>.</p>
            <br>
            <p>Děkujeme<br>OnlineAtrakce.cz</p>
        </body>';
    }

    /**
     * @param Reservation $reservation
     * @return string
     */
    private function getReservationWarning($reservation) {
        $reservationProduct = new Product($reservation->getProductId());
        $reservationProduct = $reservationProduct->load();

        if(!is_a($reservationProduct, Product::class))
            return 'Chyba v rezervaci. Prosím kontaktujte zákaznickou podporu.';

        return '<head>
            <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700&subset=latin,latin-ext" rel="stylesheet">
            <meta charset="UTF-8">            
            <style>
                body {
                    font-family: \'Comfortaa\', cursive;
                }
            </style>
        </head>
        <body>
            <p>Vaše rezervace na OnlineAtrakce.cz pro produkt ' . $reservationProduct->getName() . ' <strong>vyprší již ' . $reservation->getReservationEnd() . '</strong>.</p>
            <br>
            <br>
            <p>Více informací o své rezervaci najdete v sekci <a href="https://www.onlineatrakce.cz/prehled-rezervaci/">Můj účet na OnlineAtrakce.cz</a>.</p>
            <br>
            <p>Děkujeme<br>OnlineAtrakce.cz</p>
        </body>';
    }
}