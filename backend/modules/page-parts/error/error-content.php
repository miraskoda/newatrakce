<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 16.04.2018
 * Time: 16:56
 */
?>

<h1 class="text-center">Omlouváme se! Nastala chyba.</h1>
<h2 class="text-center">
	Prosím kontaktujte zákaznickou podporu!
	<br><a href="/" class="btn btn-primary">Vrátit se na hlavní stránku</a>
</h2>
