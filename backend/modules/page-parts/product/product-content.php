<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 23.07.2017
 * Time: 22:29
 */

use backend\controllers\CartController;
use backend\controllers\CategoryController;
use backend\models\Cart;
use backend\models\CustomCurrency;
use backend\models\Product;
use backend\models\ProductAddition;
use backend\models\ProductCategory;
use backend\models\ProductData;
use backend\models\ProductImage;
use backend\models\Search;
use backend\models\Video;
use backend\models\ProductTarget;
use backend\view\ProductGrid;

$productName = $product->getName();
$productVideo = Video::getProductVideo($product->getProductId());

$productImageMain = ProductImage::getProductImages($product->getProductId(), ProductImage::TYPE_MAIN);
$productImages = ProductImage::getProductImages($product->getProductId(), ProductImage::TYPE_OTHER);


?>


<script>


    </script>



<div class="row product">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-7 parralaxGallery">

                <div class="clearfix"></div>

                <div class="row" >

                    <div class="carousel" data-flickity='{ "bgLazyLoad": true }'>



                    <?php

                     if(count($productImageMain) > 0) {
                         echo '<div class="carousel-cell" data-flickity-bg-lazyload="/assets/images/products/' . $productImageMain[0]['name'] . '"  alt="' . $productName . '"></div>';

                     }
                     ?>

                        <?php

                        if (count($productImages) > 0) {
                            foreach ($productImages as $image) {
                                echo '<div class="carousel-cell" data-flickity-bg-lazyload="/assets/images/products/' . $image['name'] . '"  alt="' . $image['description'] . '"></div>';
                            }
                        }
                        ?>

                    </div>

                </div>

            </div>
            <div class="col-md-5 main-data">
                <h1><?= $productName ?></h1>
                <div class="row">
                    <div class="col-md-12">
                        <p class="price-dph">Cena bez DPH <?= CustomCurrency::setCustomCurrency($product->getPrice()) ?></p>

                        <p class="dph">Cena
                                s
                                DPH <?= CustomCurrency::setCustomCurrency($product->getPriceWithVat()) ?></a></p>
                        <small>Cena je za 5 hodin produkce. Každá další hodina + 5% z ceny bez DPH</small>
                    </div>
                    <div class="col-md-12">
                        <?php
                        $additionsCategory = CategoryController::getCategoryBySlug('doplnky');
                        ?>
                        <div class="row actions" product-id="<?= $product->getProductId() ?>">
                            <?php
                            if (!$product->getVisible()):
                                ?>
                                <div class="col-md-8 not-sold">
                                    <p>Již není v prodeji</p>
                                </div>
                            <?php
                            elseif(ProductCategory::isProductInCategory($additionsCategory->getCategoryId(), $product->getProductId())) :
                            ?>
                                <div class="col-md-8 not-sold">
                                    <p>Není prodejné samostatně. Lze objednat pouze jako doplňek atrakce.</p>
                                </div>
                            <?php
                            else :
                                ?>
                                <div class="col-lg-6 col-md-6 buy">
                                    <a href="#"><span class="ti-shopping-cart"></span>&nbsp;Objednat</a>
                                </div>
                                <div class="col-lg-4 col-md-6 reserve">
                                    <a href="#"><span class="ti-time"></span>&nbsp;Rezervovat</a>
                                </div>
                            <?php
                            endif;
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-10 col-md-12 availability">
                        <p>Chcete zjistit jestli je atrakce volná v potřebném termínu?</p>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="availableTermStart">Datum a čas start akce</label>

                                    </div>

                                    <div class="col-md-5">
                                        <label for="availableTermHours">Počet hodin (trvání akce)</label>

                                    </div>

                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="input-group date reservation-datetimepicker">
                                            <input class="form-control" name="availableTermStart"
                                                   placeholder="Vyberte datum a čas ..." date-part="date"
                                                   id="availableTermStart">
                                            <div class="input-group-addon">
                                                <span class="ti-calendar"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <input type="number" class="form-control" name="availableTermHours" min="1" step="1"
                                               value="5" max="48" id="availableTermHours">
                                    </div>
                                <div class="col-sm-1 visible-lg"></div>
                                    <div class="col-md-2">
                                        <a style="min-width: 70px" class="btn btn-success check-availability">Ověřit</a>
                                    </div>


                                    <div class="col-md-12">
                                            <div class=" center-block alert alert-success" style="display: none; padding-left: 10px; padding-right: 10px">
                                        <div class="availability-result">
                                            Dostupnost produktu
                                        </div>
                                    </div>
                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12 product-info">
        <hr>


        <div id="description" class="col-md-7 info-block">
            <h2 class="title"><?= $product->getName() ?></h2>
            <?= $product->getDescription() ?>



        </div>


        <div id="technical-requirements" class="col-md-5 info-block">
            <h2 class="title">Technické požadavky</h2>
            <ul>
                <?php
                $productTechnical = ProductData::getProductData($product->getProductId(), 'technical');
                foreach ($productTechnical as $data) {
                    echo '<li>' . $data['data'] . '</li>';
                }
                ?>
            </ul>
        </div>

        <div id="description" class="col-md-7 info-block">

            <h2 class="title">Pro koho je atrakce určena?</h2>
            <div class="description-target">

                <?= $product->getTarget() ?>

            </div>

        </div>


        <div id="price-included" class="col-md-5 info-block">
            <h2 class="title">Cena zahrnuje</h2>
            <ul>
                <?php
                $productPriceIncluded = ProductData::getProductData($product->getProductId(), 'price_included');
                foreach ($productPriceIncluded as $data) {
                    echo '<li>' . $data['data'] . '</li>';
                }
                ?>
            </ul>
        </div>


        <?php if(!ProductCategory::isProductInCategory($additionsCategory->getCategoryId(), $product->getProductId())): ?>

            <div id="occasions" class="col-md-7 info-block">
                <h2 class="title">Pro jaké příležitosti</h2>
                <ul>
                    <?php
                    $productCategories = ProductCategory::getProductCategoriesUnderCategory($product->getProductId(), 18);
                    foreach ($productCategories as $category) {
                        echo '<li>' . $category['name'] . '</li>';
                    }
                    ?>
                </ul>
            </div>


        <div id="addons" class="col-md-5 info-block">
            <h2 class="title">Doplňky k dokoupení</h2>
            <div class="additional-product-status" style="display: none;"></div>
            <ul class="product-additions">
                <?php
                $additionalProducts = ProductAddition::getAdditionalProductsByMainProduct($product->getProductId());
                foreach ($additionalProducts as $additionalProduct) {
                    $isInCart = new Cart(0, CartController::getCustomerId(), $additionalProduct->getProductId(), $product->getProductId());
                    $isInCart = $isInCart->load();
                    if(is_a($isInCart, Cart::class))
                        $isInCart = $isInCart->isDuplicate();
                    else
                        $isInCart = false;

                    echo '<li class="checkbox" product-id="' . $additionalProduct->getProductId() . '" addition-to="' . $product->getProductId() . '"><input type="checkbox" ' . (($isInCart) ? 'checked' : '') . '><a href="/produkty/' . $additionalProduct->getNameSlug() . '">' . $additionalProduct->getName() . '</a></li>';
                }
                ?>
            </ul>
        </div>

        <?php endif; ?>


        <div class="col-md-8 col-md-offset-2">
            <div class="row ">



                <?php
                if(is_array($productVideo)) {
                    echo '<h2 class="title">Video k produktu</h2><div style="border-bottom: 2px solid #0a77a9"> </div> <br><br>
                         <div class="videoResponsive">
                         <iframe src="https://www.youtube.com/embed/' . $productVideo['hash'] . '?rel=0" frameborder="0" allowfullscreen width="100%" height="100%" ></iframe>
                         </div>';
                }
                ?>

            </div>

        </div>

    </div>


    <div class="col-lg-10 col-lg-offset-1 col-md-12 row interests-row">
        <div class="col-md-12">
            <p class="interest">Mohlo by vás také zajímat</p>
        </div>

        <?php
        $produc = Product::getProductsWithParams(0, '', 8, 0);
        echo '<div class="clearfix"></div><div class="favourite-carousel">';
        echo ProductGrid::generatePublicProductGridMainProduct($produc);
        echo '</div>';
        ?>

    </div>


    </div>