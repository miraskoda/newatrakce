<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 01.08.2017
 * Time: 20:30
 */

use backend\models\Customer;

if(!isset($customer) || !is_a($customer, Customer::class)) {
    echo '<li><a href="/prihlaseni/"><span class="ti-user tooltipss"><span class="tooltiptextss">Přihlášení</span></span><span class="visible-xs visible-md visible-sm">Přihlášení</span></a></li>';
    echo '<li>
            <a class="dropdown-toggle" data-toggle="dropdown" href="/kosik/"><span class=\'topmenu-text\'><span class="ti-shopping-cart"></span></span><span class="visible-xs visible-md visible-sm">Košík</span><span class="cart-list-counter">0</span></a>' .
        ((!in_array($pageId, [17,18,19,20])) ? '<ul class="dropdown-menu cart-list" role="menu">
                <li><a>V košíku není žádné zboží</a></li>
            </ul>' : '') .
           '</li>';
} else {
    echo '<li><a href="/muj-ucet/"><span class="ti-user tooltipss"><span class="tooltiptextss">Můj účet</span></span><span class="visible-xs visible-md visible-sm">Můj účet</span></a></li>';
    echo '<li>
            <a class="dropdown-toggle" data-toggle="dropdown" href="/kosik/"><span class=\'topmenu-text\'><span class="ti-shopping-cart"></span></span></span><span class="visible-xs visible-md visible-sm">Košík</span><span class="cart-list-counter">0</span></a>' .
        ((!in_array($pageId, [17,18,19,20])) ? '<ul class="dropdown-menu cart-list" role="menu">
                <li><div><a>V košíku není žádné zboží</a></div></li>
            </ul>' : '') .
           '</li>';
    echo '<li><a href="#" class="log-out"><span class="ti-power-off"></span><span class="visible-xs visible-md visible-sm">&nbsp;Odhlásit se</span></a></li>';
}