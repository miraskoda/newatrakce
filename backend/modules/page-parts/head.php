<?php

use backend\models\Category;
use backend\models\Page;
use backend\models\Product;

$page = (new Page($pageId))->load();
if (!is_a($page, Page::class))
    die($page);

//var_dump($page);
//exit;
?>

<!DOCTYPE html>
<html lang="cs">
<head>

    <?php if($pageId == 44) : ?>



        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link href="https://unpkg.com/nanogallery2/dist/css/nanogallery2.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="https://unpkg.com/nanogallery2/dist/jquery.nanogallery2.min.js"></script>


<?php else: ?>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>


    <?php endif; ?>

    <title><?php
        if(isset($product) && is_a($product, Product::class)) {
            echo $product->getPageTitle();
        } else if(isset($category) && is_a($category, Category::class)){
            echo $category->getPageTitle();
        } else {
            echo $page->getTitle();
        }
        ?></title>
    <meta name="description" content="<?php
    if(isset($product) && is_a($product, Product::class)) {
        echo $product->getDescriptionSlug();
    } else if(isset($category) && is_a($category, Category::class)) {
        echo $category->getPageDescription();
    } else {
        echo $page->getDescription();
    }
    ?>"/>
    <meta name="keywords" content="<?= ((isset($product) && is_a($product, Product::class)) ? $product->getKeywords() : $page->getKeywords()) ?>"/>
    <meta name="author" content="ButApps"/>
    <meta charset="UTF-8">
    <?= (!$page->isAllowIndex()) ? '<meta name="robots" content="noindex, nofollow" />' : '<meta name="robots" content="index, follow" />' ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="/styles/flickity.css" media="screen">
    <script src="/js/flickity.pkgd.min.js"></script>

    <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "WebSite",
      "name": "OnlineAtrakce.cz",
      "alternateName": "OnlineAtrakce",
      "url": "https://www.onlineatrakce.cz/",
      "potentialAction": {
        "@type": "SearchAction",
        "target": "https://www.onlineatrakce.cz/vyhledavani/?search={search_term_string}",
        "query-input": "required name=search_term_string"
      }
    }
    </script>






    <!--Bootstrap-->


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="/js/menu-aim.js"></script>

    <!--Bootstrap datetimepicker-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/locale/cs.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="https://unpkg.com/flickity-bg-lazyload@1/bg-lazyload.js"></script>


    <!--Custom styles-->
    <link rel="stylesheet" href="/styles/style.css">
    <link rel="stylesheet" href="/styles/themify-icons.min.css">
    <link rel="stylesheet" href="/styles/online-atrakce.min.css">

    <!--Animations-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

    <!--Fonts-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700&subset=latin,latin-ext" rel="stylesheet">


    <?php if($pageId == 5) : ?>
        <!--Tinymce-->
        <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=1ptm4lyzozd5cqoafjp8h6nd6dnzfntbh61q064xyc7d7uns"></script>
        <script>
            $(function () {
                if(!$("body").is('#public')) {
                    tinyMCE.init({
                        selector: 'textarea',
                        language_url: '/backend/plugins/tinymce_languages/langs/cs_CZ.js',
                        plugins: ["lists", "textcolor", "emoticons", "link"],
                        toolbar: "undo redo | formatselect | bold italic fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | forecolor backcolor | removeformat emoticons | link",
                        height: 170,
                        content_css: "https://fonts.googleapis.com/css?family=Comfortaa",
                        content_style: "body{ font-family: 'Comfortaa', cursive; }"
                    });
                }
            });
        </script>
    <?php elseif(in_array($pageId, [1,33, 34, 35, 36, 37, 40,45,47,44,33])) : ?>
        <!--Tinymce for page content-->
        <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=1ptm4lyzozd5cqoafjp8h6nd6dnzfntbh61q064xyc7d7uns"></script>
        <script>
            $(function () {
                tinyMCE.init({
                    selector: 'textarea',
                    language_url: '/backend/plugins/tinymce_languages/langs/cs_CZ.js',
                    plugins: ["lists", "textcolor", "code", "link"],
                    toolbar: "undo redo | formatselect | bold italic fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | forecolor backcolor | removeformat | code | link",
                    height: 800,
                    content_css: "https://fonts.googleapis.com/css?family=Comfortaa",
                    content_style: "body{ font-family: 'Comfortaa', cursive; }"
                });
            });
        </script>
    <?php endif; ?>

    <!-- Owl Carousel -->
    <link rel="stylesheet" href="/assets/owlcarousel/dist/assets/owl.carousel.css">
    <link rel="stylesheet" href="/assets/owlcarousel/dist/assets/owl.theme.default.css">
    <script src="/assets/owlcarousel/dist/owl.carousel.min.js"></script>

    <?php if(in_array($pageId, [13, 14, 18, 39])) : ?>
        <!--Masonry layout-->
        <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
        <script>
            $(function() {
                $('.grid').masonry({
                    itemSelector: '.grid-item', // use a separate class for itemSelector, other than .col-
                    columnWidth: '.grid-sizer',
                    percentPosition: true
                });
            });
        </script>








    <?php endif; ?>

    <!--Dropzone-->
    <!--<script src="/backend/plugins/dropzone/dropzone.min.js"></script>-->
    <?php if($pageId == 12) : ?>
        <script src="https://www.google.com/recaptcha/api.js?hl=cs&render=explicit&onload=onScriptLoad" async defer></script>
    <?php endif; ?>

    <?php include_once($root . "/modules/app/analyticstracking.php") ?>





</head>
