<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 20.04.2018
 * Time: 16:09
 */
?>


<div class="container-fluid row">

    <div class="row">
        <div class="col-md-8 col-md-offset-2">



            <h1>Tipy na akce</h1>
            <br>

            <p>Inspirace na oslavy a akce, jakou vybrat atrakci?</p>
            <br>

            <p>Chystáte dětský den, firemní párty nebo večírek na rozloučenou?</p>
            <br>
            <p><strong> Inspirujte se u&nbsp;nás</strong>, s&nbsp;jakými atrakcemi jistě zabodujete! Atrakce od nás
                zabaví dospěláky i&nbsp;malé děti. A&nbsp;navíc, vám k&nbsp;nim nabízíme řadu výhod, například vyškolený dozor, pojištění nebo dovoz až na místo a&nbsp;
                složení i&nbsp;rozložení atrakce nebo dopravu od 7&nbsp;000 Kč po celé České republice zdarma. Dočtete se ale u&nbsp;nás i&nbsp;<strong>zajímavé tipy</strong>
                <strong>pro teambuilding, oslavy a&nbsp;jiné akce</strong>. Při organizaci je jistě oceníte!</p>

            <br>


            <div class="tips-carousel">







                <div class="carousel-tips">
                    <div class="imag">
                        <img  class="ima" src="/assets/images/products/201804281820591802.jpg">
                    </div>
                    <div class="detaile">
                        <p class="centrovac pismoVetsi">
                            Neformální firemní oslava                     </p>
                        <div class="text paddingl">
                            <p class="centrovac pismoMensi kraje">
                                Neformální firemní oslava je ideální příležitostí, jak oslavit úspěchy své firmy a poznat své pracovníky.
                            </p>
                        </div>
                    </div>
                </div>


                <div class="carousel-tips">
                    <div class="imag">
                        <img  class="ima" src="/assets/images/products/201803181344317585.jpg">
                    </div>
                    <div class="detaile">
                        <p class="centrovac pismoVetsi">
                            Jak uspořádat dětskou oslavu?                 </p>
                        <div class="text paddingl">
                            <p class="centrovac pismoMensi kraje">
                                Velké životní úspěchy by se měly slavit. A to už od dětství! Povíme vám, jak na tu nejlepší dětskou party.
                            </p>
                        </div>
                    </div>
                </div>


                <div class="carousel-tips">
                    <div class="imag">
                        <img  class="ima" src="/assets/images/products/201804251347299860.jpg">
                    </div>
                    <div class="detaile">
                        <p class="centrovac pismoVetsi">
                            Tipy na firemní teambuilding                     </p>
                        <div class="text paddingl">
                            <p class="centrovac pismoMensi kraje">
                                Vymyslete teambuilding, na který se budou těšit. Máme pro vás několik tipů!                            </p>
                        </div>
                    </div>
                </div>


                <div class="carousel-tips">
                    <div class="imag">
                        <img  class="ima" src="/assets/images/products/201804251418556508.jpg">
                    </div>
                    <div class="detaile">
                        <p class="centrovac pismoVetsi">
                            Jak uspořádat úspěšnou promo akci                    </p>
                        <div class="text paddingl">
                            <p class="centrovac pismoMensi kraje">
                                Nadšení návštěvníci a spokojení pořadatelé, to je jeden z cílů promo akcí. Nepodceňujte přípravu ani samotný průběh.                            </p>
                        </div>
                    </div>
                </div>


                <div class="carousel-tips">
                    <div class="imag">
                        <img  class="ima" src="/assets/images/products/201804251845377310.jpg">
                    </div>
                    <div class="detaile">
                        <p class="centrovac pismoVetsi">
                            Interaktivní atrakce vás budou bavit
                        </p>
                        <div class="text paddingl">
                            <p class="centrovac pismoMensi kraje">
                                Hledáte moderní zábavu na svou party nebo firemní večírek? Zkuste interaktivní atrakce!                            </p>
                        </div>
                    </div>
                </div>


                <div class="carousel-tips">
                    <div class="imag">
                        <img  class="ima" src="/assets/images/products/201805041755449474.jpg">
                    </div>
                    <div class="detaile">
                        <p class="centrovac pismoVetsi">
                            Nová doba VR přichází!                  </p>
                        <div class="text paddingl">
                            <p class="centrovac pismoMensi kraje">
                                Pokud hledáte něco naprosto nového a neokoukaného, tak si buďte jisti, že s virtuální realitou chybu neuděláte. Každý si ji přeci chce zkusit.                            </p>
                        </div>
                    </div>
                </div>









            </div>


            </div>


        </div>
    <br>
</div>
