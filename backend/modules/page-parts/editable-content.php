<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 10.02.2018
 * Time: 16:51
 */

use backend\controllers\UserController;
use backend\models\Page;

?>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
		<?php
		if ( UserController::isLoggedUser() && $page->getType() == Page::PAGE_EDITABLE ) {
			echo '<div class="row">
                    <div class="col-md-6">
                        <a class="btn btn-success save-content">Uložit obsah stránky</a>
                    </div>
                    <div class="alert page-content-status col-md-6" style="display: none"></div>
                  </div>
					<div class="form-group required">
                    <form id="page-content-form">
                        <input name="pageId" value="' . $pageId . '" type="hidden">
                        <textarea name="page-content" class="form-control" id="page-content"
                              placeholder="Obsah stránky ...">' . $page->getContent() . '</textarea></form>
                </div>
                <div><a class="btn btn-success save-content">Uložit obsah stránky</a></div>';
		} else {

		    //ar_dump($page);
			echo $page->getContent();
		}
		?>
    </div>
</div>
