<?
use backend\controllers\OptionController;

?>


<div class="col-md-10 col-md-offset-1 animated slideInUp" style="text-align: center">
    <h2>Nemůžete se rozhodnout? Proč zvolit právě nás?</h2>


</div>


<div class="row promo-line">
    <div class="carousel"
         data-flickity='{
     "freeScroll": false,
     "wrapAround": true,
     "prevNextButtons": true,
     "pageDots": false

     }'>

        <div class="carousel-celll prvni">
            <p class="centrovac pismoVetsi">                       <?=OptionController::getOptionBySlug('mini-carousel-11-text')?>

            </p>
            <p class="centrovac pismoMensi kraje">
                <?=OptionController::getOptionBySlug('mini-carousel-1-text')?>
            </p>


        </div>
        <div class="carousel-celll druha">

            <p class="centrovac pismoVetsi">       <?=OptionController::getOptionBySlug('mini-carousel-21-text')?>

            </p>
            <p class="centrovac pismoMensi kraje">                <?=OptionController::getOptionBySlug('mini-carousel-2-text')?>
            </p>

        </div>
        <div class="carousel-celll treti">

            <p class="centrovac pismoVetsi">       <?=OptionController::getOptionBySlug('mini-carousel-31-text')?>

            </p>

            <p class="centrovac pismoMensi kraje">                <?=OptionController::getOptionBySlug('mini-carousel-3-text')?>
            </p>
        </div>

        <div class="carousel-celll ctvrta">

            <p class="centrovac pismoVetsi">       <?=OptionController::getOptionBySlug('mini-carousel-41-text')?>
            </p>
            <p class="centrovac pismoMensi kraje">                <?=OptionController::getOptionBySlug('mini-carousel-4-text')?>
            </p>

        </div>

        <div class="carousel-celll pata">

            <p class="centrovac pismoVetsi">       <?=OptionController::getOptionBySlug('mini-carousel-51-text')?>
            </p>
            <p class="centrovac pismoMensi kraje">                <?=OptionController::getOptionBySlug('mini-carousel-5-text')?>
            </p>

        </div>

    </div>

</div>
