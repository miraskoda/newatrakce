<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 13.04.2017
 * Time: 20:14
 */

use backend\controllers\OptionController;

?>
<div class="row">
    <div class="col-md-12 owl-carousel owl-theme">
        <?php
        for ($i = 1; $i < 10; $i++) {
            $imageUrl = OptionController::getOptionBySlug($i . '-obrazek-v-carouselu-url-obrazku');
            if(!is_null($imageUrl) && $imageUrl != "") {
                echo '<div class="item item-image"
                     style="background-image: url(' . $imageUrl . ');">
                    <div class="box">
                        <h4 class="' . OptionController::getOptionBySlug( $i . '-obrazek-v-carouselu-pismo-black-white' ) . ' animated">' . OptionController::getOptionBySlug( $i . '-obrazek-v-carouselu-popis' ) . '</h4>
                        </div>
                    </div>';
            }
        }
        ?>
    </div>
</div>
<div class="more">
    <a href="#favourite">Více<br><span class="ti-angle-down"></span></a>
</div>

<script>
    $(document).ready(function () {
        //large carousel
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            loop: true,
            autoplay: true,
            smartSpeed: 4000,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });

        //Events
        owl.on('changed.owl.carousel', function () {
            $('.item .animated').removeClass("fadeInDown").addClass("infinite").animate({}, {
                duration: 1000, start: function () {
                    setTimeout(function () {
                        $('.item .animated').removeClass('infinite').removeClass("zoomIn");
                    }, 1000);
                }
            });
        })
    });
</script>
