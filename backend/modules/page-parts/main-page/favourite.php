<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 15.04.2017
 * Time: 3:16
 */

use backend\models\Product;
use backend\view\ProductGrid;

?>
<div class="col-md-10 col-md-offset-1 animated slideInUp" style="text-align: center">
    <h1>Zábavné atrakce pro děti i dospělé</h1></div>

<div class="row favourite" id="favourite">
    <div class="col-md-10 col-md-offset-1 animated slideInUp">
        <div class="favourite-carousel">
            <?php
            $products = Product::getProductsWithParams(72, '', 12, 0,true);
            //$products = Product::getProductsWithParams(0, '', 8, 0);

            echo ProductGrid::generatePublicProductGridMain($products);
            ?>
        </div>
        <div class="row">
            <div class="col-md-2 col-md-offset-5 centrovac">
                <a href="/nase-produkty/" class="btn btn-lg btn-primary centrovac">Zobrazit všechny</a>
            </div>
        </div>
    </div>
</div>
