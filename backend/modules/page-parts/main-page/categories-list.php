<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 19.04.2017
 * Time: 14:58
 */
use backend\view\CategoryList;

?>

<div class="row categories-line">
    <?= CategoryList::generateCategoryList("public-line") ?>
    <!--<div class="col-md-12 animated slideInDown">
        <h4>Kategorie</h4>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Novinky</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Simulátory a trenažery</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Aktivní centra</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Lezecké stěny</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Lidský stolní fotbal</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Nafukovací skluzavky</h5></a>
                </div>
            </div>-->
            <!--<div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Skákací hrady</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Nafukovací atrakce</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Soutěžní atrakce</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Pro nejmenší</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Pro děti</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Pro dospělé</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Moto atrakce</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Venkovní atrakce</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Atrakce do interiéru</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Kreativní atrakce</h5></a>
                </div>
            </div>-->
        <!--</div>
    </div>

    <div class="col-md-12 animated slideInDown">
        <h4>Příležitosti</h4>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Kontraktační veletrhy</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Akce pro rodiny</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Sportovní akce</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Závodní akce</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Narozeninová párty</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Městské oslavy</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Promo akce</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Akce pro klienty</h5></a>
                </div>
            </div>-->
            <!--<div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Vánoční večírek</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Firemní teambuilding</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Roadshow akce</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Firemní den</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Firemní akce</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Dětský den</h5></a>
                </div>
            </div>
            <div class="col-md-3 scroll-anim animated zoomInUp">
                <div>
                    <a><h5><span class="ti-alarm-clock"></span> Farmářské trhy</h5></a>
                </div>
            </div>-->
        <!--</div>
    </div>-->
</div>

