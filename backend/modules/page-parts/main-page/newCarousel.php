<?php
/**
 * Created by PhpStorm.
 * User: MIra
 * Date: 13.04.2017
 * Time: 20:14
 */

use backend\controllers\OptionController;

?>

<div style="margin-left: -15px;margin-right: -15px" class="carouselBig hidden-xs"
     data-flickity='{

     "wrapAround": true,
     "fullscreen": true,
     "lazyLoad": 1 }'>


    <?php
    for ($i = 1; $i < 10; $i++) {
        $imageUrl = OptionController::getOptionBySlug($i . '-obrazek-v-carouselu-url-obrazku');


        if(!is_null($imageUrl) && $imageUrl != "") {
            echo '  
              <div class="carouselBig-cell">
        <div class="parentBoxBig">
            <div class="centrovacBig boxBig">

                <p class="velkeBig">
                    <b>'.OptionController::getOptionBySlug($i . '-obrazek-v-carouselu-popis-nadpis').'</b>
                </p>
                <p class="maleBig">
                    '.OptionController::getOptionBySlug($i . '-obrazek-v-carouselu-popis').'          </p>
                <a class="centrovac" href="/nase-produkty/"> <button class="btn btn-primary btn-lg" style="margin-top:10px">Zjistit více</button></a>
            </div>
        </div>

        <img data-flickity-lazyload="'.$imageUrl.'" /></div>';
        }
    }
    ?>


</div>

