<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 19.04.2017
 * Time: 22:05
 */

if(is_null($root))
	$root = $_SERVER['DOCUMENT_ROOT'] . "/backend";

//scroll animation file
echo '<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b157570748e32c6"></script>';
echo '<script>';
    require_once $root . "/../js/menu-aim-activator.js";
    require_once $root . "/../js/search-hints.min.js";
    require_once $root . "/../js/admin/page-status.min.js";
    require_once $root . "/../js/datetimepicker.js";
    require_once $root . "/../js/customer/logout.min.js";
    require_once $root . "/../js/overlay-switch.min.js";
    require_once $root . "/../js/text-slider.js";
    require_once $root . "/../js/cart/cart-loader.js";
    require_once $root . "/../js/cart/additionalProductCart.js";
    require_once $root . "/../js/cart/remove-from-cart.js";
    require_once $root . "/../js/reservation/reservation.js";
    require_once $root . "/../js/flickityCarousel.js";
    require_once $root . "/../js/youtubeSwap.js";
    require_once $root . "/../js/scroll-anim.js";
    require_once $root . "/../js/responsiveMenu.js";
    require_once $root . "/../js/cartCounter.js";
    require_once $root . "/../js/cart/add-reservation-to-cart.js";
    require_once $root . "/../js/customer/login-modal.js";
echo '$(document).ready(function(){
        $("[data-toggle=\"tooltip\"]").tooltip();
    })';
echo '</script>';
require_once $root . "/modules/page-parts/foxydesk-chat.html";

