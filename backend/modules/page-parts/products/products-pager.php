<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 14.07.2017
 * Time: 1:49
 */

use backend\models\Product;
use backend\models\Search;

if((isset($_GET['search']) && strlen($_GET['search'])) || ((isset($_GET['date']) && strlen($_GET['date'])))) {
    $search = '';
    if(isset($_GET['search']) && strlen($_GET['search']))
        $search = $_GET['search'];
    $date = '';
    if(isset($_GET['date']) && strlen($_GET['date']))
        $date = $_GET['date'];

    $resultCount = Search::getSearchProductsCount($search, $date);
} else {
    $resultCount = Product::getProductsCount();
}
