<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h1>Cena a způsob platby</h1>
        <ul class="list-margin">
            <li>Všechny platby jsou ZDARMA.</li>
            <li>Na našem eshopu můžete platit pomocí:</li>
        </ul>
    </div>
</div>

<div class="row price-list">
    <div class="price-carousel">
        <div class="carousel-cell credit-card">
            <p class="centrovac pismoVetsi">
                Kreditní karty
            </p>
            <p class="centrovac pismoMensi kraje">
                Plaťte kartou, která má povolené platby na internetu. Přenos je šifrovaný a bezpečný. Kartu si můžete v nastavení účtu uložit i pro další platby. Pro platbu online musí být karta vydaná v EU.
            </p>
        </div>
        <div class="carousel-cell bank-account">
            <p class="centrovac pismoVetsi">
                Bankovní účet
            </p>
            <p class="centrovac pismoMensi kraje">
                Upozorňujeme že dokončení objednávky trvat 1–3 pracovní dny.
                Po odeslání objednávky Vám obratem zašleme údaje pro provedení platby
                O průběhu vyřizování objednávky Vás budeme informovat na Vaši emailovou adresu.
            </p>
        </div>
        <div class="carousel-cell cash">
            <p class="centrovac pismoVetsi">
                Platba hotově
            </p>
            <p class="centrovac pismoMensi kraje">
                Poskytujeme i vybraným zákazníkům možnost platby hotově.
                Platba se provádí vždy v den produkce, ještě před samotným začátkem akce.

            </p>
        </div>
        <div class="carousel-cell pig">
            <p class="centrovac pismoVetsi">
                Pomocí proforma faktury
            </p>
            <p class="centrovac pismoMensi kraje">
                Vybraným zákazníkům, lze taky vystavit fakturu se splatností.
                Tato možnost je dostupná pouze vybraným zákazníkům.
            </p>
        </div>
    </div>
</div>