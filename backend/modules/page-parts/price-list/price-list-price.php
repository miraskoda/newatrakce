<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 16.11.2017
 * Time: 8:19
 */
use backend\models\Category;
use backend\models\CustomCurrency;
use backend\models\Product;
use backend\models\ProductImage;

?>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h1>Ceník</h1>
        <p>
            Ceny uvedené u jednotlivých produktů jsou za 5 hodin produkce.
        </p>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="price-list">
            <ul>
                <?php
                $categories = Category::getCategories(0, false);
                foreach ($categories as $category) {
                    echo '<li>';
                    echo $category->getName();
                    echo ' <span class="ti-angle-down"></span>';
                    $subCategories = Category::getCategories($category->getCategoryId(), false);
                    if (count($subCategories) > 0) {
                        echo '<ul>';
                        foreach ($subCategories as $subCategory) {
                            echo '<li>' . $subCategory->getName();
                            $products = Product::getProductsWithParams($subCategory->getCategoryId(), "", 0);
                            if (count($products) > 0) {
                                echo '<ul style="display: none">';
                                foreach ($products as $product) {

                                    $productImageMain = ProductImage::getProductImages($product->getProductId(), ProductImage::TYPE_MAIN);

                                    echo '<li>' . $product->getName() . ' <a href="/produkty/' . $product->getNameSlug() . '"><i class="ti-search"></i></a>' .
                                        '<p>' . CustomCurrency::setCustomCurrencyWithoutDecimals($product->getPriceWithVat()) . ' s DPH</p></li>';
                                    if(count($productImageMain) > 0) {
                                        echo '<img width="100px" src="/assets/images/products/' . $productImageMain[0]['name'] . '">';
                                    }
                                    echo '<hr style="border-color: grey;">';

                                }
                                echo '</ul>';
                            }
                            echo '</li>';
                        }
                        echo '</ul>';
                    }
                    echo '</li>';
                }
                ?>
            </ul>
        </div>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <span>Po kliknutí na název, dojde k rozbalení nabídky</span>
    </div>
</div>
