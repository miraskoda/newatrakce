<hr class="divider">
<div class="row footer-line">
    <footer>
        <div class="col-md-3 fadeInUp text-center">
            <div class="social">
                <h4 class="con">Sociální sítě</h4>
                <div class="row">
                    <a href="https://www.facebook.com/Sv%C4%9Bt-atrakc%C3%AD-1539389003033531/"><img
                                src="/images/social/facebook.svg" alt="Facebook"></a>
                    <a href="https://www.youtube.com/user/pronajematrakce"><img src="/images/social/youtube.svg"
                                                                                alt="YouTube"></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 text-center">
            <h5 class="con">O nákupu</h5>
            <a href="/cenik/"><p>Ceník</p></a>
            <a href="/informace-o-doprave/"><p>Informace o dopravě</p></a>
            <a href="/zpusoby-platby/"><p>Způsoby platby</p></a>
        </div>
        <div class="col-md-3 text-center">
            <h5 class="con">O společnosti</h5>

            <a href="/kontakt/"><p>Kontakt</p></a>
            <a href="/vseobecne-obchodni-podminky/"><p>Všeobecné obchodní podmínky</p></a>
            <a href="/zpracovani-udaju/"><p>Zásady zpracování osobních údajů</p></a>


        </div>
        <div class="col-md-3 text-center">
            <h5 class="con">Kontaktní osoba</h5>
            <a disabled="true"><p><span class="ti-user"></span> Lenka Kvasničková</p></a>
            <a href="tel:+420774777328"><p><span class="ti-mobile"></span> 774 777 328</p></a>
            <a href="mailto:info@onlineatrakce.cz"><span class="ti-email"></span> info@onlineatrakce.cz</a>
        </div>

    </footer>
    <div class="signature" style=" margin-left: 90px; margin-top: 10px" >
        <small ><p>Web written by: </p> <p><a href="#">Adam Kvasnička</a></p> <p><a href="http://miroslavskoda.cz">Miroslav Škoda</a></p> <p><a href="http://broccoli.games">Ondra Šimeček</a></p> <br> <a href="https://www.onlineatrakce.cz/backend/admin/pages/login.php">-Přejít do administrace-</a></small>
    </div>
</div>


<script>
    $(document).ready(function() {
        var body = document.querySelector('body');
        body.addEventListener('onmousewheel', function(e) {
            console.log(e)
        });
        const topBtn = document.querySelector('#back2Top');
        $("#back2Top").on('click', function(event) {
            console.log(event);
            $("html, body").animate({ scrollTop: 0 }, "slow");
            return false;
        });

    });
</script>
