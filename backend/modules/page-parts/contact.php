

<div class="row">
    <div class="col-md-8 col-md-offset-2">

    <div class="col-xs-12">
        <h2>Kontakty</h2>
    </div>
    <div class="col-xs-12 col-md-12 col-lg-12">
        <div class="row contact-wrapper">
            <div class="col-md-4 col-xs-12 contact-item">
                <div>
                    <h3>Obchodní asistentka</h3>
                    <img class="img-responsive"
                         src="http://www.pronajematrakce.cz/editor/filestore/Image/kontakt/Lenka.jpg"
                         alt="Obchodní asistentka"/>
                    <div>
                        <div class="text-wrapper">
                            <h4>Objednávky vyřizuje</h4>
                            <p>Lenka Kvasničková</p>
                        </div>
                        <div class="text-wrapper">
                            <p>Tel: +420 774 777 328</p>
                            <p>Email: info@pronajematrakce.cz</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12 contact-item">
                <div>
                    <h3>Majitel</h3>
                    <img class="img-responsive"
                         src="http://www.pronajematrakce.cz/editor/filestore/Image/kontakt/Pepa.jpg"
                         alt="Majitel"/>
                    <div>
                        <div class="text-wrapper">
                            <h4>Technické informace</h4>
                            <p>Josef Rybár</p>
                        </div>
                        <div class="text-wrapper">
                            <p>Tel: +420 608 875 101</p>
                            <p>Email: josef@pronajematrakce.cz</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12 contact-item">
                <div>
                    <h3>Majitelka</h3>
                    <img class="img-responsive"
                         src="http://www.pronajematrakce.cz/editor/filestore/Image/kontakt/Ivanka.jpg"
                         alt="Majitelka"/>
                    <div>
                        <div class="text-wrapper">
                            <h4>Marketing</h4>
                            <p>Mgr. Ivana Rybárová</p>
                        </div>
                        <div class="text-wrapper">
                            <p>Tel: +420 774 198 238</p>
                            <p>Email: iva@pronajematrakce.cz</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 margin-top-5 margin-bottom-5">
        <div class="row contact-wrapper">
            <div class="col-xs-12 col-md-4 contact-item">
                <div>
                    <h4>Adresa společnosti:</h4>
                    <div class="text-wrapper">
                        <p>Josef Rybár</p>
                        <p>Syrovátka</p>
                        <p>Lhota pod Libčany - Hradec Králové</p>
                        <p>503 27</p>
                        <p>Czech republic</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 contact-item">
                <div>
<br><br>
                    <div class="text-wrapper">

                    <p>IČ: 627 23 499</p>
                    <p>DIČ: CZ760509311</p>
                    <p>Ž.L. vydal MM v HK</p>
                    <p>Č. j. HK-008579 7-FL</p>
                    </div>

                </div>
            </div>
            <div class="col-xs-12 col-md-4 contact-item">
                <div>
                    <h4>Číslo bankovního účtu:</h4>
                    <div class="text-wrapper">
                            <p>ČSOB HK 184218759/0300</p>
                            <p>IBAN: CZ094 0300 0000 0001 8421 8759</p>
                            <p>BIC: CEKOCZPP</p>
                            <p>ČŠOB Břetislavova 1622</p>
                            <p>500 02 - Hradec Králové</p>
                    </div>
                </div>
            </div>





        </div>
    </div>
    </div>
</div>
