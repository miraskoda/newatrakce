<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 10.06.2017
 * Time: 1:38
 */
use backend\models\Product;
use backend\models\Url;
use backend\view\ProductGrid;

?>

<div class="row category" style="background-image: url('/assets/images/categories/<?= $category->getPageImageName(); ?>')">
    <div class="col-md-12">
        <h1><?= $category->getName(); ?></h1>
    </div>
    <div class="col-md-6 col-md-offset-3 description">
        <p><?= $category->getDescription(); ?></p>
    </div>
    <div class="col-lg-8 col-lg-offset-2 col-md-12 filter">
        <div class="row">
         <!--   <div class="col-md-2 tab <?= ($_SESSION['order'] == 'pc.position' || $_SESSION['order'] == '') ? 'active' : '' ?>"><a href="?order=popular&dir=asc">Populární</a></div>
            <div class="col-md-2 tab <?= ($_SESSION['order'] == 'price' && $_SESSION['dir'] == 'asc') ? 'active' : '' ?>"><a href="?order=price&dir=asc">Nejlevnější</a></div>
            <div class="col-md-2 tab <?= ($_SESSION['order'] == 'price' && $_SESSION['dir'] == 'desc') ? 'active' : '' ?>"><a href="?order=price&dir=desc">Nejdražší</a></div>
           --> <div class="col-md-12 buttons">
                <a href="?page=<?= $pageNum+1 ?>&view=list"><span class="ti-view-list"></span></a>
                <a href="?page=<?= $pageNum+1 ?>&view=table"><span class="ti-view-grid"></span></a>
                <?php
                $products = Product::getProductsWithParams($category->getCategoryId(), (($_SESSION['order'] != "") ? $_SESSION['order'] . ' ' . $_SESSION['dir'] : ''), $pageSize, ($pageSize * $pageNum));
                ?>

             <!--   <p>Počet na stránce <?=sizeof($products)?></p>  -->
                <p>
                    <select name="forma" onchange="location = this.value;">
                        <option value="?order=popular&dir=asc<?= ((!empty($search)) ? '&search=' . $search : '') ?><?= ((!empty($date)) ? '&date=' . $date : '') ?>" <?= ($_SESSION['order'] == 'pc.position' || $_SESSION['order'] == '') ? 'selected' : '' ?> >Populární</option>
                        <option value="?order=price&dir=asc<?= ((!empty($search)) ? '&search=' . $search : '') ?><?= ((!empty($date)) ? '&date=' . $date : '') ?>" <?= ($_SESSION['order'] == 'price' && $_SESSION['dir'] == 'asc') ? 'selected' : '' ?> >Nejlevnější</option>
                        <option value="?order=price&dir=desc<?= ((!empty($search)) ? '&search=' . $search : '') ?> <?= ((!empty($date)) ? '&date=' . $date : '')?>" <?= ($_SESSION['order'] == 'price' && $_SESSION['dir'] == 'desc') ? 'selected' : '' ?>>Nejdražší</option>
                    </select>
                </p>
                <p>Zobrazeno <?= count($products)?></p>

            </div>
        </div>
    </div>
    <div class="col-lg-8 col-lg-offset-2 col-md-12 row content" >
        <?php
            if($_SESSION['view'] == 'list')
                echo ProductGrid::generatePublicProductList($products);
            else
                echo ProductGrid::generatePublicProductGrid($products);
        ?>
        <!--
        <div class="col-md-4">
            <div class="item-box">
                <div class="images">
                    <img src="/images/products/rocking-horse/rh1.jpg">
                    <img src="/images/products/rocking-horse/rh2.jpg" class="image-swap">
                </div>
                <div class="details">
                    <div class="text">
                        <h4>Houpací kůň</h4>
                        <p>25 000 Kč</p>
                        <p><span class="ti-check-box"></span> Aktuálně volné</p>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 reserve">
                            <a href="#"><span class="ti-time"></span> Rezervovat</a>
                        </div>
                        <div class="col-sm-6 buy">
                            <a href="#"><span class="ti-shopping-cart"></span> Objednat</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
    </div>
    <div class="col-lg-8 col-lg-offset-2 col-md-12 ">
        <?php require_once Url::getBackendPathTo("/modules/page-parts/pagination.php"); ?>
    </div>
</div>
