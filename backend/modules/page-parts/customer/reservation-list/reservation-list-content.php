<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 05.12.2017
 * Time: 19:08
 */

use backend\models\Reservation;

$reservations = Reservation::getReservations();
?>
<div class="row" id="conte" style="margin-left: 54px">
	<?php
	if( isset($reservations) && is_array($reservations) && count($reservations) > 0) :
		?>
		<div class="col-md-12 hidden-xs">
			<div class="row box light-blue overflow-auto text-center animated zoomInDown">
				<div class="col-md-1">
					<p>ID</p>
				</div>
				<div class="col-md-3">
					<p>Produkt</p>
				</div>
				<div class="col-md-1">
					<p>Množství</p>
				</div>
				<div class="col-md-2">
					<p>Rezervovaný termín</p>
				</div>
				<div class="col-md-2">
					<p>Počet hodin</p>
				</div>
				<div class="col-md-2">
					<p>Rezervováno do</p>
				</div>
				<div class="col-md-1">
                    <p>Odstranit rezervaci</p>

                </div>
			</div>
		</div>
		<?php
		foreach ($reservations as $reservation):
			?>
			<div id="reservation-<?= $reservation->getReservationId() ?>" class="col-md-12 reservation-row" reservation-id="<?= $reservation->getReservationId() ?>">
				<div class="box text-center animated zoomInDown">
                    <div class="row customer-reservation-list-mobile">
					<div class="col-md-1">
                        <h3 class="visible-xs hidden-md">ID</h3>
						<p><?= $reservation->getReservationId() ?></p>
					</div>
					<div class="col-md-3">
                        <h3 class="visible-xs hidden-md">Produkt</h3>
						<p><?= $reservation->getProductId()  ?></p>
					</div>
					<div class="col-md-1">
                        <h3 class="visible-xs hidden-md">Množství</h3>
						<p><?= $reservation->getQuantity() ?></p>
					</div>
					<div class="col-md-2">
                        <h3 class="visible-xs hidden-md">Rezervovaný termín</h3>
						<p><?= $reservation->getFormatedTermStart() ?></p>
					</div>
					<div class="col-md-2">
                        <h3 class="visible-xs hidden-md">Počet hodin</h3>
						<p><?= $reservation->getTermHours() ?></p>
					</div>
					<div class="col-md-2">
                        <h3 class="visible-xs hidden-md">Rezervováno do</h3>
						<p><?= $reservation->getReservationEnd() ?></p>
					</div>
					<div class="col-md-1">
                        <hr class="visible-xs hidden-md">
                        <a
                                class="btn btn-success customer-add-reservation"
                                data-target="<?= $reservation->getReservationId() ?>"
                                data-name="<?= $reservation->getProductId() ?>"
                                data-quantity="<?= $reservation->getQuantity() ?>"
                                data-term-start="<?= $reservation->getFormatedTermStart() ?>"
                                data-term-hours="<?= $reservation->getTermHours() ?>"
                        ><span>Vložit do košíku</span></a>
						<a class="btn btn-danger delete-reservation"><span>Smazat rezervaci</span></a>
					</div>
                    </div>
				</div>
			</div>
		<?php
		endforeach;
		?>
	<?php
	else :
		?>
		<div class="col-md-12 box">
			<p>Nemáte žádné rezervace. <?= ((isset($reservations) && is_string($reservations)) ? $reservations : '') ?></p>
		</div>
	<?php
	endif;
	?>
</div>