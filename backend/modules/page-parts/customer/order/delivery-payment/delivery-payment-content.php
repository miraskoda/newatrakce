<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 22.08.2017
 * Time: 12:34
 */

use backend\controllers\CartController;
use backend\controllers\CustomerController;
use backend\controllers\CustomerGroupController;
use backend\models\CustomerGroup;
use backend\models\Order;
use backend\models\Payment;
use backend\models\PaymentType;
use backend\models\Url;

?>
<div class="row">
    <div class="col-md-3 text-center">
        <h3>1. Košík</h3>
    </div>
    <div class="col-md-3 text-center">
        <h3>2. Místo konání</h3>
    </div>
    <div class="col-md-3 text-center">
        <h2>3. Údaje</h2>
    </div>
    <div class="col-md-3 text-center">
        <h3>4. Souhrn</h3>
    </div>
</div>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="row title">
            <div class="col-md-12"><h4>Základní údaje</h4></div>
        </div>
        <div class="row">
            <?php
            require_once Url::getBackendPathTo('/modules/page-parts/customer/account-data/account-data-part.php');
            require_once Url::getBackendPathTo('/modules/page-parts/customer/account-data/invoice-data-part.php');
            ?>
        </div>

        <div class="row title">
            <div class="col-md-12"><h4>Platba</h4></div>
        </div>


        <div class="row payment-type">
            <div class="col-md-12">
                <h4>Typ platby:</h4>
                <?php
                $customerGroup = CustomerController::getCustomerGroup();
                if(is_a($customerGroup, CustomerGroup::class))
                    $paymentTypes = PaymentType::getPaymentTypes($customerGroup->getVipPaymentsBool());
                else
	                $paymentTypes = PaymentType::getPaymentTypes();

                if(is_array($paymentTypes) && is_a($order, Order::class)) {
                    foreach ($paymentTypes as $paymentType) {
                        echo '<div class="radio">
                                <label><input type="radio" name="payment-type" value="' . $paymentType->getPaymentTypeId() . '" ' . (($order->getPaymentTypeId() == $paymentType->getPaymentTypeId()) ? 'checked' : null) . '>' . $paymentType->getName() . '</label>
                            </div>';
                    }
                }
                ?>
            </div>
        </div>
        <div class="row payment">
            <div class="col-md-12">
                <h4>Způsob platby:</h4>
                <?php
                if(is_a($customerGroup, CustomerGroup::class))
                    $payments = Payment::getPayments($customerGroup->getVipPaymentsBool());
                else
                    $payments = Payment::getPayments();

                if(is_array($payments) && is_a($order, Order::class)) {
                    foreach ($payments as $payment) {
                        echo '<div class="radio">
                                <label><input type="radio" name="payment" value="' . $payment->getPaymentId() . '" ' . (($order->getPaymentId() == $payment->getPaymentId()) ? 'checked' : null) . '><span class="' . $payment->getIcon() . '"></span> ' . $payment->getName() . '</label>
                            </div>';
                    }
                }
                ?>
            </div>
        </div>

        <div class="row title">
            <div class="col-md-12"><h4>Poznámka</h4></div>
        </div>

        <div class="row customer-note">
            <div class="col-md-12">
			    <div class="form-group">
                    <textarea class="form-control" name="customer-note" placeholder="Další údaje, které chcete k objednávce připojit ..."><?= $order->getCustomerNote() ?></textarea>
                </div>
            </div>
        </div>


        <div class="row buttons">
            <div class="col-md-12">
                <div class="image-status validate-payment-status"><div><p>Nová informace</p></div></div>
            </div>
            <div class="col-md-6"><a class="btn btn-primary" href="/objednavka/misto-konani/"><span class="ti-angle-left"></span> Zpět na místo konání</a></div>
            <div class="col-md-6"><a class="btn btn-success validate-payment">Další krok objednávky <span class="ti-angle-right"></span></a></div>
        </div>
    </div>
</div>