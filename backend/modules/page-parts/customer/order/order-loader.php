<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 11.09.2017
 * Time: 0:52
 */

use backend\controllers\CartController;
use backend\models\Order;

$customerId = CartController::getCustomerId();

$order = new Order();
$order->setCustomerId($customerId);
$order->setType(Order::TYPE_DRAFT);
$order = $order->load();