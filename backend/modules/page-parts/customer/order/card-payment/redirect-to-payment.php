<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 17.04.2018
 * Time: 1:03
 */

use backend\models\CardPayment;
use backend\models\Order;
use backend\models\PaymentType;

$cardPaymentConfig = include $_SERVER['DOCUMENT_ROOT'] . '/backend/config/card-payment.php';

//$orderNo = '124';
$dttm = date('YmdHis');

$paymentType = new PaymentType($order->getPaymentTypeId());
$paymentType = $paymentType->load();
if(!is_a($paymentType, PaymentType::class)) {
    echo 'Nepodařilo se načíst typ platby.';
    return;
}

if($paymentType->getDeposit() > 0) {
    $totalAmount = round((floatval($order->getSumPrice()) / floatval(100)) * floatval($paymentType->getDeposit()));
} else {
    $totalAmount = $order->getSumPrice();
}

// WARNING
// gateway uses price in halire -> 100 Kč is in gateway 10000
$totalAmount *= 100;

$closePayment = 'true';
$returnUrl = 'https://www.onlineatrakce.cz/prehled-objednavek/detail/' . $order->getOrderId() . '/dokonceni-platby/';
$description = 'Objednávka na OnlineAtrakce.cz č. ' . $orderNo;
$merchantData = $orderNo;
$customerId = $order->getCustomerId();

/*echo "sign & verify test ...\n";
$text = "some text to sign";
$private_key = $cardPaymentConfig->privateKey;
$private_key_passwd = null;
$public_key = $cardPaymentConfig->publicKey;
echo "signing text '" . htmlspecialchars($text) . "' using private key " . htmlspecialchars($private_key) . "\n";
$signature = CardPayment::sign($text, $private_key, $private_key_passwd);
echo "signature is '" . htmlspecialchars($signature) . "'\n";
echo "verifying signature using public key " . htmlspecialchars($public_key) . "\n";
$result = CardPayment::verify($text, $signature, $public_key);
echo "verify result: " . ($result == 1 ? "ok" : "failed") . "\n";*/

if($paymentType->getDeposit() > 0){
    $cartData = [
        0 => CardPayment::getCartItemData('Záloha ' . $paymentType->getDeposit() . '%', 1, round($totalAmount), 'Záloha ' . $paymentType->getDeposit() . '%'),
    ];
} else {
    $cartData = [
        0 => CardPayment::getCartItemData('Atrakce', 1, round(($order->getProductPrice() + $order->getVat()) * 100), 'Objednané atrakce'),
        1 => CardPayment::getCartItemData('Doprava', 1, round(($order->getDeliveryPaymentPrice()) * 100), 'Doprava atrakcí na místo konání')
    ];
}

//var_dump($cartData);

try {
    $paymentData = CardPayment::getInitData($cardPaymentConfig->merchantId, $orderNo, $dttm, CardPayment::PAY_OPERATION_PAYMENT, CardPayment::PAY_METHOD_CARD,
        $totalAmount, CardPayment::CURRENCY_CZK, $closePayment, $returnUrl, CardPayment::RETURN_METHOD_POST, $cartData, $description, CardPayment::LANGUAGE_CZ,
        base64_encode($merchantData), $customerId);
} catch (Exception $e) {
    echo $e->getMessage();
    return;
}

/*echo '<br>';
echo '<br>';*/
$paymentDataJSON = json_encode($paymentData);
//var_dump($paymentDataJSON);
/*echo '<br>';
echo '<br>';*/

$ch = curl_init ($cardPaymentConfig->url . CardPayment::API_INIT);
//var_dump($cardPaymentConfig->url . CardPayment::API_INIT);
/*echo '<br>';
echo '<br>';*/
curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
curl_setopt ( $ch, CURLOPT_POSTFIELDS, $paymentDataJSON );
curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
curl_setopt ( $ch, CURLOPT_HTTPHEADER, array (
    'Content-Type: application/json',
    'Accept: application/json;charset=UTF-8'
) );

$result = curl_exec ($ch);
//var_dump($result);

if(curl_errno($ch)) {
    //echo 'payment/init failed, reason: ' . htmlspecialchars(curl_error($ch));
    echo 'Inicializace platby selhala. Důvod: ' . htmlspecialchars(curl_error($ch));
    return;
}

$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

if($httpCode != 200) {
    //echo 'payment/init failed, http response: ' . htmlspecialchars($httpCode);
    echo 'Inicializace platby selhala. Důvod: ' . htmlspecialchars($httpCode);
    return;
}

curl_close($ch);

//echo "payment/init result:\n" . htmlspecialchars($result) . "\n\n";
$result_array = json_decode ( $result, true );

if(is_null($result_array ['resultCode'])) {
    //echo 'payment/init failed, missing resultCode';
    echo 'Inicializace platby selhala. Důvod: Chybějící result code.';
    return;
}

try {
    if (CardPayment::verifyResponse($result_array, $cardPaymentConfig->publicKey) == false) {
        echo 'Inicializace platby selhala. Důvod: Nebylo možné ověřit podpis.';
        //echo 'payment/init failed, unable to verify signature';
        return;
    }
} catch (Exception $e) {
    echo $e->getMessage();
    return;
}

if ($result_array ['resultCode'] != '0') {
    //echo 'payment/init failed, reason: ' . htmlspecialchars($result_array ['resultMessage']);
    echo 'Inicializace platby selhala. Důvod: ' . htmlspecialchars($result_array ['resultMessage']);;
    return;
}
/*echo '<br>';
echo '<br>';*/

$payId = $result_array ['payId'];
/*echo $payId;
echo '<br>';
echo '<br>';*/

if($paymentType->getDeposit() > 0) {
	$order->setDepositPaymentId($payId);
} else {
	$order->setFinishPaymentId($payId);
}
$order = $order->update();
if(!is_a($order, Order::class)){
	echo 'Nepodařilo se uložit identifikátor platby.';
	return;
}

try {
    $params = CardPayment::createGetParams($cardPaymentConfig->merchantId, $payId, $dttm, $cardPaymentConfig->privateKey, $cardPaymentConfig->privateKeyPassword);
} catch (Exception $e) {
    echo $e->getMessage();
    return;
}

if(strlen($cardPaymentConfig->url . CardPayment::API_PROCESS . $params) > 0) {
    header('Location: ' . $cardPaymentConfig->url . CardPayment::API_PROCESS . $params, true);
}

echo '<a href="' . $cardPaymentConfig->url . CardPayment::API_PROCESS . $params . '">Go to payment</a>';