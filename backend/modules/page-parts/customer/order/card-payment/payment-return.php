<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 17.04.2018
 * Time: 17:05
 */

use backend\controllers\OrderController;
use backend\models\Order;

?>

<div class="row title">
    <div class="col-md-8 col-md-offset-2">
        <?php
        //echo 'depositPaymentId ' . $order->getDepositPaymentId() . '<br>';
        //echo 'finishPaymentId ' . $order->getFinishPaymentId() . '<br><br>';
        //var_dump($_GET);
        //var_dump($_POST);

        if (strlen($order->getDepositPaymentId()) < 1 && strlen($order->getFinishPaymentId()) < 1) {
            echo '<h4>Platba neexistuje.</h4>';
            echo '<a class="btn btn-primary" href="/prehled-objednavek/">Jít zpět</a>';
        } else {
            if (isset($_GET['resultCode']))
                $resultCode = $_GET['resultCode'];
            else if (isset($_POST['resultCode']))
                $resultCode = $_POST['resultCode'];

            if (isset($_GET['resultMessage']))
                $resultMessage = $_GET['resultMessage'];
            else if (isset($_POST['resultMessage']))
                $resultMessage = $_POST['resultMessage'];

            if (!isset($resultCode)) {
                echo '<h4>Neznámá návratová hodnota.</h4>';
                echo '<a class="btn btn-primary" href="/prehled-objednavek/">Jít zpět</a>';
                return;
            } else if ($resultCode != 0) {
                echo '<h4>Chyba při návratu z transakce. Prosím kontaktujte zákaznickou podporu. Error: ' . $resultMessage . '</h4>';
                echo '<a class="btn btn-primary" href="/prehled-objednavek/">Jít zpět</a>';
                return;
            }

            if (isset($_GET['paymentStatus']))
                $paymentStatus = $_GET['paymentStatus'];
            else if (isset($_POST['paymentStatus']))
                $paymentStatus = $_POST['paymentStatus'];

            if (isset($paymentStatus)) {
                $success = false;
                switch ($paymentStatus) {
                    case 1:
                        echo '<h4>Platba pouze založena. Zkuste se vrátit zpět do platby.</h4>';
                        //echo '<a class="btn btn-primary" href="/prehled-objednavek/">Jít zpět</a>';
                        break;
                    case 2:
                        echo '<h4>Platba probíhá. Zkuste se vrátit zpět do platby.</h4>';
                        //echo '<a class="btn btn-primary" href="/prehled-objednavek/">Jít zpět</a>';
                        break;
                    case 3:
                        echo '<h4>Platba zrušena.</h4>';
                        echo '<a class="btn btn-primary" href="/prehled-objednavek/detail/' . $order->getOrderId() . '/">Jít zpět do detailu objednávky</a>';
                        break;
                    case 4:
                        echo '<h4>Platba pouze potvrzena. Zkuste se vrátit zpět do platby.</h4>';
                        //echo '<a class="btn btn-primary" href="/prehled-objednavek/">Jít zpět</a>';
                        break;
                    case 5:
                        echo '<h4>Platba byla odvolána.</h4>';
                        echo '<a class="btn btn-primary" href="/prehled-objednavek/detail/' . $order->getOrderId() . '/">Jít zpět do detailu objednávky</a>';
                        break;
                    case 6:
                        echo '<h4>Platba zamítnuta. Důvod: ' . $resultMessage . '</h4>';
                        echo '<a class="btn btn-primary" href="/prehled-objednavek/detail/' . $order->getOrderId() . '/">Jít zpět do detailu objednávky</a>';
                        break;
                    case 7:
                        $success = true;
                        echo '<h4>Čekání na zúčtování. Platba bude brzy dokončena. Děkujeme.</h4>';
                        echo '<a class="btn btn-primary" href="/prehled-objednavek/detail/' . $order->getOrderId() . '/">Jít zpět do detailu objednávky</a>';
                        break;
                    case 8:
                        $success = true;
                        echo '<h4>Platba úspěšně dokončena. Děkujeme.</h4>';
                        echo '<a class="btn btn-primary" href="/prehled-objednavek/detail/' . $order->getOrderId() . '/">Jít zpět do detailu objednávky</a>';
                        break;
                    case 9:
                        echo '<h4>Platba vrácena.</h4>';
                        echo '<a class="btn btn-primary" href="/prehled-objednavek/detail/' . $order->getOrderId() . '/">Jít zpět do detailu objednávky</a>';
                        break;
                    case 10:
                        echo '<h4>Platba vrácena.</h4>';
                        echo '<a class="btn btn-primary" href="/prehled-objednavek/detail/' . $order->getOrderId() . '/">Jít zpět do detailu objednávky</a>';
                        break;

                    default:
                        echo '<h4>Neznámý status platby.</h4>';
                        echo '<a class="btn btn-primary" href="/prehled-objednavek/detail/' . $order->getOrderId() . '/">Jít zpět do detailu objednávky</a>';
                        break;
                }

                if($success) {
                    $order->setIsPaid(true);
                    $order = $order->update();

                    if(!is_a($order, Order::class)) {
                        echo '<h5>Nepodařilo se uložit výsledek platby. Prosím kontaktujte zákaznickou podporu. Důvod: ' . $order . '</a>';
                    }
                }
            } else {
                echo '<h4>Neznámý výsledek transakce.</h4>';
                echo '<a class="btn btn-primary" href="/prehled-objednavek/">Jít zpět</a>';
            }
        }
        ?>

        <?php
        /*		$orderNo = DateTime::createFromFormat('Y-m-d H:i:s', $order->getChangedRaw())->format('y') . $order->getOrderId();

                // send all emails
                $mailer = new MailHelper();
                $orderMail = $mailer->sendOrder($customer->getEmail(), $orderNo, $order);
                if($orderMail !== true) {
                    echo '<p>' . $orderMail . '</p>';
                }
                */ ?>
    </div>
</div>