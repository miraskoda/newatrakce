<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 11.09.2017
 * Time: 0:51
 */

use backend\controllers\CustomerController;
use backend\controllers\OrderController;
use backend\models\Address;
use backend\models\CustomCurrency;
use backend\models\CustomerGroup;
use backend\models\OrderTerm;
use backend\models\Payment;
use backend\models\PaymentType;
use backend\models\Validate;
use backend\view\CartGrid;

?>

<div class="row">
    <div class="col-md-3 text-center">
        <h3>1. Košík</h3>
    </div>
    <div class="col-md-3 text-center">
        <h3>2. Místo konání</h3>
    </div>
    <div class="col-md-3 text-center">
        <h3>3. Údaje</h3>
    </div>
    <div class="col-md-3 text-center">
        <h2>4. Souhrn</h2>
    </div>
</div>

<div class="row title">
    <div class="col-md-8 col-md-offset-2">
        <h4>Výpis produktů</h4>
    </div>
</div>
<div class="row admin-cart-list">
    <?php
    $stringifiedRentHours = OrderTerm::getRentHoursAsStrings();

    echo CartGrid::generateProductsList($stringifiedRentHours, true);
    ?>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="row price-container">
            <div class="col-md-12 price-block">
                <?php
                echo CartGrid::generatePrice($stringifiedRentHours, true);
                ?>
            </div>
        </div>
    </div>
</div>

<div class="row title">
    <div class="col-md-8 col-md-offset-2">
        <h4>Termíny pronájmu</h4>
    </div>
</div>
<div class="row admin-cart-list">
    <?php
    echo CartGrid::generatePublicDates();
    ?>
</div>

<div class="row title">
    <div class="col-md-8 col-md-offset-2">
        <h4>Místo konání a fakturační údaje</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="col-md-4">
            <div class="animated zoomIn box with-top">
                <div class="top top-light-blue">
                    <h4>Kontaktní osoba v den akce</h4>
                </div>
                <div class="content">
                    <form id="contact-data">
                        <div class="form-group">
                            <label class="control-label">Jméno a příjmení</label>
                            <p><?= $order->getContactName() ?></p>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Telefon (např.: 789456123)</label>
                            <p><?= $order->getContactPhone() ?></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="animated zoomIn box with-top">
                <div class="top top-blue">
                    <h4>Adresa místa konání</h4>
                </div>
                <div class="content">
                    <div class="form-group">
                        <label class="control-label">Ulice a číslo popisné</label>
                        <p><?= $order->getVenueStreet() ?></p>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Město</label>
                        <p><?= $order->getVenueCity() ?></p>
                    </div>
                    <div class="form-group">
                        <label class="control-label">PSČ (např. 45612)</label>
                        <p><?= $order->getVenueZip() ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="animated zoomIn box with-top">
                <div class="top top-kaky">
                    <h4>Fakturační údaje</h4>
                </div>
                <div class="content">
                    <?php
                    $invoiceAddress = new Address();
                    $invoiceAddress->setCustomerId($customer->getCustomerId());
                    $invoiceAddress->setAddressType(Address::ADDRESS_INVOICE);
                    $invoiceAddress = $invoiceAddress->load(true);

                    $isAddressLoaded = false;
                    if(is_a($invoiceAddress, Address::class))
                        $isAddressLoaded = true;
                    ?>
                    <div class="form-group">
                        <label class="control-label">Jméno / Název firmy</label>
                        <p><?= ($isAddressLoaded) ? $invoiceAddress->getName() : '' ?></p>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Ulice a číslo popisné</label>
                        <p><?= ($isAddressLoaded) ? $invoiceAddress->getStreet() : '' ?></p>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Město</label>
                        <p><?= ($isAddressLoaded) ? $invoiceAddress->getCity() : '' ?></p>
                    </div>
                    <div class="form-group">
                        <label class="control-label">PSČ (např. 45612)</label>
                        <p><?= ($isAddressLoaded) ? $invoiceAddress->getZip() : '' ?></p>
                    </div>
                    <?php
                    if($customer->isCompany()):
                    ?>
                    <div class="form-group">
                        <label class="control-label">IČO</label>
                        <p><?= $customer->getIco() ?></p>
                    </div>
                        <?php
                        if((new Validate())->validateNotNull([$customer->getDic()])):
                        ?>
                            <div class="form-group">
                                <label class="control-label">DIČ</label>
                                <p><?= $customer->getDic() ?></p>
                            </div>
                        <?php
                        endif;
                        ?>
                    <?php
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row title">
    <div class="col-md-8 col-md-offset-2">
        <h4>Doprava</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="row venue-result">
            <?php
                $venueData = OrderController::getVenueCountedData();
                if(!is_array($venueData))
                    echo '<div class="col-md-12"><p>Chyba. Nepodařilo se načíst data dopravy. Prosím kotaktujte zákaznickou podporu. Chyba: ' . $venueData . '</p></div>';
            ?>
            <div class="col-md-12"><p>Délka cesty z našeho skladu: <span class="travel-length"><?= (is_array($venueData)) ? $venueData['travel-length'] : '' ?></span></p></div>
            <div class="col-md-12"><p>Doba trvání cesty: <span class="travel-time"><?= (is_array($venueData)) ? $venueData['duration-text'] : '' ?></span></p></div>
            <div class="col-md-12">
                <p class="venue-price">Cena dopravy bez DPH:
                    <?php
                    $priceWithoutVat = OrderController::getPriceWithoutVat();

                    $customerGroup = CustomerController::getCustomerGroup();
                    $discount = 0;
                    $discountAmount = 0;
                    if(is_a($customerGroup, CustomerGroup::class)){
                        $discount = $customerGroup->getDiscount();
                    }

                    if($discount > 0){
                        $discountAmount = ($priceWithoutVat / 100) * $discount;
                        $priceWithoutVat = ($priceWithoutVat / 100) * (100 - $discount);
                    }

                    if($priceWithoutVat > 7000) {
                        echo '<span class="green free">ZDARMA (objednávka nad 7 000 Kč bez DPH)</span>';
                    } else {
                        echo '<span class="travel-price">' . CustomCurrency::setCustomCurrencyWithoutDecimals((is_array($venueData)) ? $venueData['travel-price'] : '') . '</span> <span class="red">(Nakupte ještě za ' . CustomCurrency::setCustomCurrencyWithoutDecimals(7000 - $priceWithoutVat) . ' bez DPH a máte dopravu ZDARMA)</span>';
                    }
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="row title">
    <div class="col-md-8 col-md-offset-2">
        <h4>Platba</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="row venue-result">
            <?php
            $payment = new Payment($order->getPaymentId());
            $payment = $payment->load();
            $paymentType = new PaymentType($order->getPaymentTypeId());
            $paymentType = $paymentType->load();
            ?>
            <div class="col-md-12"><p>Způsob platby: <strong><span class="<?= $payment->getIcon() ?>"></span> <?= $payment->getName() ?></strong></p></div>
            <div class="col-md-12"><p>Typ platby: <strong><?= $paymentType->getName() ?></strong></p></div>
        </div>
    </div>
</div>

<?php
if(strlen($order->getCustomerNote()) > 0):
?>
<div class="row title">
    <div class="col-md-8 col-md-offset-2">
        <h4>Poznámka</h4>
    </div>
</div>

<div class="row venue-result">
    <div class="col-md-8 col-md-offset-2">
        <p><?= $order->getCustomerNote() ?></p>
    </div>
</div>
<?php
endif;
?>

<div class="row title">
    <div class="col-md-8 col-md-offset-2">
        <h4>Součet</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="row price-container">
            <div class="col-md-12 price-block">
                <?php
                if($discount > 0)
                    echo '<p>Sleva ' . $discount . '%: -' . CustomCurrency::setCustomCurrencyWithoutDecimals($discountAmount) . '</p>';
                ?>
                <p>Produkty bez DPH: <strong><?= CustomCurrency::setCustomCurrencyWithoutDecimals($priceWithoutVat) ?></strong></p>
                <p>Doprava a platba bez DPH: <strong><?= ($priceWithoutVat > 7000) ? 'ZDARMA' : CustomCurrency::setCustomCurrencyWithoutDecimals((is_array($venueData)) ? $venueData['travel-price'] : '') ?></strong></p>
                <?php
                $sumPriceWithoutVat = round($priceWithoutVat + (($priceWithoutVat > 7000) ? 0 : ((is_array($venueData)) ? $venueData['travel-price'] : '')));
                ?>
                <p>DPH: <strong><?= CustomCurrency::setCustomCurrencyWithoutDecimals(round($sumPriceWithoutVat * 0.21)) ?></strong></p>
                <p class="final-price">Celkem k úhradě: <strong><?= CustomCurrency::setCustomCurrencyWithoutDecimals(round(($sumPriceWithoutVat / 100) * 121)) ?></strong></p>
            </div>
        </div>
    </div>
</div>


<div class="row title">
    <div class="col-md-8 col-md-offset-2">
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-md-12 text-center agreements">
                <!--<div class="checkbox required form-group">
                    <label class="control-label"><input type="checkbox" name="personal-data">Souhlasím se zpracováním osobních údajů (více informací <a>zde</a>)</label>
                </div>
                <div class="checkbox required form-group">
                    <label class="control-label"><input type="checkbox" name="vop">Souhlasím s obchodními podmínkami (najdete je <a>zde</a>)</label>
                </div>-->
                <p>Odesláním objednávky souhlasíte a potvrzujete, že jste se seznámili s <a class="conditions-info">obchodními podmínkami</a> a se <a class="person-info">zpracováním osobních údajů</a>.</p>
                <p>Vyhrazujeme si právo změnit nebo upravit objednávku dle nenadálých okolností.</p>
                <p>Odesláním objednávky také souhlasíte se zpracováním osobních údajů v souladu s GDPR</p>

            </div>
        </div>
    </div>
</div>

<div class="row buttons">
    <div class="col-md-8 col-md-offset-2">
        <div class="image-status finish-order-status"><div><p>Nová informace</p></div></div>
    </div>
    <div class="col-md-6"><a class="btn btn-primary" href="/objednavka/udaje/"><span class="ti-angle-left"></span> Zpět na údaje</a></div>
    <div class="col-md-6"><a class="btn btn-success finish-order"><span class="ti-check"></span> Závazně objednat</a></div>
</div>