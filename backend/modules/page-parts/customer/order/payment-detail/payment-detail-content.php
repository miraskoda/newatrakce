<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 17.04.2018
 * Time: 0:23
 */

use backend\models\CustomCurrency;
use backend\models\Payment;
use backend\models\PaymentType;
use backend\controllers\InvoiceCreditController;


$payment = new Payment($order->getPaymentId());
$payment = $payment->load();
$paymentType = new PaymentType($order->getPaymentTypeId());
$paymentType = $paymentType->load();

?>

<div class="row title">
    <div class="col-md-10 col-md-offset-1">
        <h3>
            Informace k platbě - Objednávka č. <?= $orderNo ?>
        </h3>
        <p>Vytvořena <?= $order->getChanged() ?></p>
    </div>
</div>



<div class="row box">
    <div class="title col-md-12">
        <div class="text-center">
            <h3>
                Vygenerované doklady
            </h3>
        </div>
    </div>
    <div class="col-md-10 col-md-offset-1">
        <div class="panel col-md-12">
            <div class="box text-center">

                <div class="row" style="    background: lightcyan;">
                    <div class="col-md-1 col-md-offset-1">
                        Pořadí
                    </div>
                    <div class="col-md-2">
                        Datum vytvoření
                    </div>
                    <div class="col-md-2">
                        Typ dokumentu
                    </div>
                    <div class="col-md-2">
                        Číslo dokumentu
                    </div>
                    <div class="col-md-2">
                        Typ určení
                    </div>
                    <div class="col-md-2">
                        Celková uvedená cena
                    </div>
                </div>

                <?php
                $countVar = 1;
                echo '<hr>';
                //faktury
                foreach(InvoiceCreditController::getInvoiceFromIdVenue($_GET["orderId"]) as $result => $value){

                    if($value["isAdd"]==1){
                        echo '<a target="_blank" href="/prehled-objednavek/detail/' . $value["id"] . '/doplnkova/faktura/faktura/">';
                    }else{
                        echo '<a target="_blank" href="/prehled-objednavek/detail/' . $value["order_id"] . '/faktura/final/">';
                    }

                    echo '<div class="row" id="invoicea">
                   <div class="col-md-1 col-md-offset-1">
                        '.$countVar.'.
                        </div>
                        <div class="col-md-2">
                        '.date('d.m.Y H:i:s',strtotime($value["date"])).'                    
                        </div>
                    <div class="col-md-2">
                        '."Faktura".'                   
                         </div>
                    <div class="col-md-2">
                        '.$value["id"].'                    
                        </div>
                    <div class="col-md-2">
                        ';
                    if($value["isAdd"]==1){
                        echo "Doplňková";
                    }else{
                        echo "Ostrá";
                    }
                    echo '                    
                        </div>
                     <div class="col-md-2">
                        '.( $value["isAdd"]==0 ? CustomCurrency::setCustomCurrencyWithoutDecimals($order->getSumPrice()) : $value["wVAT"]." Kč" ).'                   
                        </div>
                </div></a><hr>';
                    $countVar = $countVar+1;

                }


                //dobropisy
                foreach(InvoiceCreditController::getInvoiceFromIdVenueCredit($_GET["orderId"]) as $result => $value){

                    if($value["isAdd"]==1){
                        echo '<a target="_blank" href="/prehled-objednavek/detail/' . $value["id"] . '/doplnkova/faktura/dobropis/">';
                    }else{
                        echo '<a target="_blank" href="/prehled-objednavek/detail/' . $value["order_id"] . '/faktura/final/">';
                    }

                    echo '<div class="row" id="invoicea">
                    <div class="col-md-1 col-md-offset-1">
                        '.$countVar.'.
                        </div>
                        <div class="col-md-2">
                        '.date('d.m.Y H:i:s',strtotime($value["date"])).'                    
                        </div>
                    <div class="col-md-2">
                        '."Dobropis".'                   
                         </div>
                    <div class="col-md-2">
                        '.$value["id"].'                    
                        </div>
                    <div class="col-md-2">
                        ';
                    if($value["isAdd"]==1){
                        echo "Doplňková";
                    }else{
                        echo "Ostrá";
                    }
                    echo '                    
                        </div>
                    <div class="col-md-2">
                        -'.( $value["isAdd"]==0 ? CustomCurrency::setCustomCurrencyWithoutDecimals($order->getSumPrice()) : $value["wVAT"]." Kč" ).'                   
                        </div>
                </div></a><hr>';
                    $countVar = $countVar+1;

                }

                //ostatni

                foreach(InvoiceCreditController::getInvoiceFromIdVenueDocument($_GET["orderId"]) as $result => $value){
                    echo '<a target="_blank" href="/prehled-objednavek/detail/' . $order->getOrderId() . '/faktura/deposit/">';
                    echo '<div class="row" id="invoicea">                    
                    <div class="col-md-1 col-md-offset-1">
                        '.$countVar.'.
                        </div>
                        <div class="col-md-2">
                        '.date('d.m.Y H:i:s',strtotime($value["date"])).'                    
                        </div>
                    <div class="col-md-2">
                        '."Zálohová".'                   
                         </div>
                    <div class="col-md-2">
                        '.$value["id"].'                    
                        </div>
                    <div class="col-md-2">
                        Ostrá                   
                        </div>
                    <div class="col-md-2">
                        '.CustomCurrency::setCustomCurrencyWithoutDecimals(round(($order->getSumPrice() / 100) * $paymentType->getDeposit())).'                   
                        </div>
                </div></a><hr>';
                    $countVar = $countVar+1;

                }


                ?>

            </div>
        </div>
    </div>
</div>




<div class="row title" id="payment">
    <div class="col-md-10 col-md-offset-1">
        <h4>Platba</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="row venue-result">
            <?php
            $payment = new Payment($order->getPaymentId());
            $payment = $payment->load();
            $paymentType = new PaymentType($order->getPaymentTypeId());
            $paymentType = $paymentType->load();
            ?>
            <div class="col-md-12"><p>Způsob platby: <strong><span
                                class="<?= $payment->getIcon() ?>"></span> <?= $payment->getName() ?></strong></p></div>
            <div class="col-md-12"><p>Typ platby: <strong><?= $paymentType->getName() ?></strong></p></div>
        </div>
    </div>
</div>

<div class="row title">
    <div class="col-md-10 col-md-offset-1">
        <h4>Součet</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="row price-container">
            <div class="col-md-12 price-block">
                <?php
                $priceWithoutVat = $order->getProductPrice();
                $discount = $order->getDiscount();
                $discountAmount = round((floatval($priceWithoutVat) / floatval(100 - $discount)) * floatval($discount));
                if ($discount > 0)
                    echo '<p>Sleva ' . $discount . '%: -' . CustomCurrency::setCustomCurrencyWithoutDecimals($discountAmount) . '</p>';
                ?>
                <p>Produkty bez DPH:
                    <strong><?= CustomCurrency::setCustomCurrencyWithoutDecimals($order->getProductPrice()) ?></strong>
                </p>
                <p>Doprava a platba bez DPH:
                    <strong><?= ($priceWithoutVat > 7000) ? 'ZDARMA' : CustomCurrency::setCustomCurrencyWithoutDecimals($order->getDeliveryPaymentPrice()) ?></strong>
                </p>
                <p>DPH: <strong><?= CustomCurrency::setCustomCurrencyWithoutDecimals($order->getVat()) ?></strong></p>
                <p class="final-price">Celkem k úhradě:
                    <strong><?= CustomCurrency::setCustomCurrencyWithoutDecimals($order->getSumPrice()) ?></strong></p>
            </div>
        </div>
    </div>
</div>





<div class="row title">
    <div class="col-md-10 col-md-offset-1">
        <h4>Gratulujeme za Vaši objednávku a děkujeme za projevenou důvěru se podílet na realizaci Vámi pořádané akce. </h4>
        <p>Nyní prosíme o uhrazení objednávky.</p>
        <p>Jakmile bude částka připsána na účet, automaticky Vám zašleme doklad k přijaté platbě.</p>
    </div>
</div>


<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <h4>Rychlá QR platba</h4>
        <div class="col-md-6">
            <img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=SPD*1.0*ACC:CZ9403000000000184218759*AM:<?=$order->getSumPrice()?>.00*CC:CZK*MSG:OnlineAtrakce.cz*X-VS:<?=$orderNo?>&choe=UTF-8" title="qrPayment" />
        </div>
        <div class="col-md-3">
            <h4>Číslo bankovního účtu:</h4>
            <div class="text-wrapper">
                <p>ČSOB HK 184218759/0300</p>
                <p>IBAN: CZ094 0300 0000 0001 8421 8759</p>
                <p>BIC: CEKOCZPP</p>
                <p>ČŠOB Břetislavova 1622</p>
                <p>500 02 - Hradec Králové</p>
            </div>
        </div>
        <div class="col-md-3">
            <h4>Variabilní symbol:</h4>
            <div class="text-wrapper">
                <p><?= $orderNo ?></p>
            </div>
        </div>
    </div>
</div>

<div class="row title">
    <div class="col-md-10 col-md-offset-1">
        <h4>Platební brány</h4>
    </div>
</div>

<div class="row">
    <div class="col-md-10 col-md-offset-1">

        <div class="row" style="display: flex;
    align-items: center;">
        <div class="col-sm-2 col-xs-6" ><a href="https://ib24.csob.cz/">
            <img src="/img/csob.png" title="csob" width="100%" style="padding: 20px"/></a>
        </div>
        <div class="col-sm-2 col-xs-6"><a href="https://ib.fio.cz/ib/login">
            <img src="/img/fio.jpg" title="fio" width="100%" style="padding: 20px"/></a>
        </div>
        <div class="col-sm-2 col-xs-6"><a href="https://bezpecnost.csas.cz">
            <img src="/img/ceska.png" title="ceska" width="100%" style="padding: 20px"/></a>
        </div>
        <div class="col-sm-2 col-xs-6"><a href="https://login.kb.cz/login">
                <img src="/img/kb.jpg" title="kb" width="100%" style="padding: 20px"/></a>
        </div>
        <div class="col-sm-2 col-xs-6"><a href="https://cz.unicreditbanking.net/">
            <img src="/img/uni.jpg" title="uni" width="100%" style="padding: 20px"/></a>
        </div>
        <div class="col-sm-2 col-xs-6"><a href="https://www.rb.cz/">
            <img src="/img/raiff.jpg" title="raiff" width="100%" style="padding: 20px"/></a>
        </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="row venue-result">
            <div class="col-md-12">
                <?php
                if($order->getOrderStateId() == 6) {
                    echo '<p>Objednávka je stornována.</p>';
                } else if ($payment->getSlug() == 'credit_card'):
                    if ($order->getIsPaid()):
                        ?>
                        <p>Objednávka již uhrazena.</p>
                    <?php
                    else :
                        ?>
                        <p><a href="/prehled-objednavek/detail/<?= $order->getOrderId() ?>/platebni-brana/"
                              class="btn btn-primary">Kliknutím zde budete přesměrováni na platební bránu</a></p>
                    <?php
                    endif;
                else :
                    if ($paymentType->getDeposit() > 0 && $order->getOrderStateId() < 4):
                        ?>
                        <p>Všechny údaje pro platbu najdete v zálohové faktuře, kterou je možné vygenerovat níže.</p>
                    <?php
                    else :
                        ?>
                        <p>Všechny údaje pro platbu najdete v daňovém dokladu, který je možné vygenerovat níže.</p>
                    <?php
                    endif;
                endif;
                ?>
            </div>
        </div>
    </div>
</div>


<div class="row buttons">
    <div class="col-md-8 col-md-offset-2">
        <div class="image-status finish-order-status">
            <div><p>Nová informace</p></div>
        </div>
    </div>
    <div class="col-md-4"><a class="btn btn-success"
                             href="/prehled-objednavek/detail/<?= $order->getOrderId() ?>/"><span
                    class="ti-angle-left"></span> Zpět na detail objednávky</a></div>
    <?php
    $paymentType = new PaymentType($order->getPaymentTypeId());
    $paymentType = $paymentType->load();

    if ($paymentType->getDeposit() > 0 && $order->getOrderStateId() != 6) {
        echo '<div class="col-md-4"><a class="btn btn-primary" target="_blank" href="/prehled-objednavek/detail/' . $order->getOrderId() . '/faktura/deposit/"><span class="ti-printer"></span> Vygenerovat zálohovou fakturu</a></div>';
    }

    if ($order->getOrderStateId() == 5 || $order->getOrderStateId() == 7) {
        echo '<div class="col-md-4"><a class="btn btn-primary" target="_blank" href="/prehled-objednavek/detail/'. $order->getOrderId().'/faktura/final/"><span class="ti-printer"></span> Vygenerovat daňový doklad</a></div>';
    }
    ?>
    <!--<div class="col-md-6"><a class="btn btn-primary" href="faktura/"><span class="ti-printer"></span> Vygenerovat fakturu</a></div>-->
</div>
