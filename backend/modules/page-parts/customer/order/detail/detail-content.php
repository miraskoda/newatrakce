<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 17.09.2017
 * Time: 20:12
 */

use backend\controllers\OrderController;
use backend\controllers\InvoiceCreditController;
use backend\models\Address;
use backend\models\CustomCurrency;
use backend\models\OrderProduct;
use backend\models\OrderState;
use backend\models\OrderTerm;
use backend\models\Payment;
use backend\models\PaymentType;
use backend\models\Validate;
use backend\view\CartGrid;

?>

<!--<div class="row box animated zoomInDown">
    <div class="col-md-12">
        <a>Přehled objednávek</a> <span class="ti-arrow-right"></span> Objednávka č. <?/*= $orderNo */?>
    </div>
</div>-->
<div class="row box overflow-auto light-blue animated zoomInDown" >
    <div class="col-md-12">
        Aktuální stav objednávky:
        <strong>
        <?php
        // I have no idea what im doing, I just need it working :], call me when you have JS problem
        $stateStateId = $order->getOrderStateId();
        if ($stateStateId == 0) {
            $stateStateId = 1;
        }
        $state = new OrderState($stateStateId);
        $state = $state->load();
        if(is_a($state, OrderState::class))
            echo $state->getName();
        else
            echo 'Neznámý';
        ?>
        </strong>
    </div>
</div>
<?php
if (!$order->getIsPaid() && $order->getOrderStateId() != 7) :
?>
    <div class="row box bright-red animated zoomInDown">
        <div class="col-md-12">
            Objednávka není zaplacena! <a href="/prehled-objednavek/detail/<?= $order->getOrderId() ?>/platba/"><span class="ti-help-alt"></span> Pokyny k platbě</a>
        </div>
    </div>
<?php
else :
?>
    <div class="row box light-green animated zoomInDown">
        <div class="col-md-12">
            Objednávka je uhrazena. Děkujeme
        </div>
    </div>
<?php
endif;
?>
<div class="row title">
    <div class="col-md-10 col-md-offset-1">
        <h3>
            Objednávka č. <?= $orderNo ?>
        </h3>
        <p>Vytvořena <?= $order->getChanged() ?></p>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1" style="display: grid">


        <?php

        if(InvoiceCreditController::getInvoiceBooleanFromOrderId($order->getOrderId())){
            echo ' <div class="col-md-2 animated zoomInDown" style="padding-bottom: 10px;padding-top: 3px"><a class="btn btn-danger" href="/prehled-objednavek/detail/'. $order->getOrderId().'/faktura/final/"><span class="ti-printer"></span> Daňový doklad číslo '.InvoiceCreditController::getInvoiceFromOrderId($order->getOrderId()).'</a></div> ';

        }

        if(InvoiceCreditController::getInvoiceBooleanFromOrderIdDobropis($order->getOrderId())){
            echo ' <div class="col-md-2 animated zoomInDown"><a class="btn btn-danger" href="/prehled-objednavek/detail/'. $order->getOrderId().'/faktura/dobropis/"><span class="ti-printer"></span> Dobropis číslo '.InvoiceCreditController::getInvoiceFromOrderIdDobropis($order->getOrderId()).'</a></div> ';

        }


        ?>


    </div>
</div>

<div class="row title">
    <div class="col-md-10 col-md-offset-1">
        <h4>Výpis produktů</h4>


    </div>
</div>



<div class="row admin-cart-list">
    <?php
    $stringifiedRentHours = OrderTerm::getRentHoursAsStrings($order);
    $products = OrderProduct::getOrderProducts($order, false, false);
    $additionalProducts = OrderProduct::getAdditionalOrderProducts($order);

    echo CartGrid::generateProductsList($stringifiedRentHours, true, $products, 10, 1, true, $order);
    ?>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="row price-container">
            <div class="col-md-12 price-block">
                <?php
                echo CartGrid::generatePrice($stringifiedRentHours, true, $products, $additionalProducts, true, $order->getDiscount());
                ?>
            </div>
        </div>
    </div>
</div>

<div class="row title">
    <div class="col-md-10 col-md-offset-1">
        <h4>Termíny pronájmu</h4>
    </div>
</div>
<div class="row admin-cart-list">
    <?php
    echo CartGrid::generatePublicDates($order, 10, 1);
    ?>
</div>

<div class="row title">
    <div class="col-md-10 col-md-offset-1">
        <h4>Místo konání a fakturační údaje</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="col-md-4">
            <div class="animated zoomIn box with-top">
                <div class="top top-light-blue">
                    <h4>Kontaktní osoba v den akce</h4>
                </div>
                <div class="content">
                    <form id="contact-data">
                        <div class="form-group">
                            <label class="control-label">Jméno a příjmení</label>
                            <p><?= $order->getContactName() ?></p>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Telefon (např.: 789456123)</label>
                            <p><?= $order->getContactPhone() ?></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="animated zoomIn box with-top">
                <div class="top top-blue">
                    <h4>Adresa místa konání</h4>
                </div>
                <div class="content">
                    <div class="form-group">
                        <label class="control-label">Ulice a číslo popisné</label>
                        <p><?= $order->getVenueStreet() ?></p>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Město</label>
                        <p><?= $order->getVenueCity() ?></p>
                    </div>
                    <div class="form-group">
                        <label class="control-label">PSČ (např. 45612)</label>
                        <p><?= $order->getVenueZip() ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="animated zoomIn box with-top">
                <div class="top top-kaky">
                    <h4>Fakturační údaje</h4>
                </div>
                <div class="content">
                    <?php
                    $invoiceAddress = new Address();
                    $invoiceAddress->setOrderId($order->getOrderId());
                    $invoiceAddress->setAddressType(Address::ADDRESS_INVOICE);
                    $invoiceAddress = $invoiceAddress->load();

                    $isAddressLoaded = false;
                    if(is_a($invoiceAddress, Address::class))
                        $isAddressLoaded = true;
                    ?>
                    <div class="form-group">
                        <label class="control-label">Jméno / Název firmy</label>
                        <p><?= ($isAddressLoaded) ? $invoiceAddress->getName() : '' ?></p>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Ulice a číslo popisné</label>
                        <p><?= ($isAddressLoaded) ? $invoiceAddress->getStreet() : '' ?></p>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Město</label>
                        <p><?= ($isAddressLoaded) ? $invoiceAddress->getCity() : '' ?></p>
                    </div>
                    <div class="form-group">
                        <label class="control-label">PSČ (např. 45612)</label>
                        <p><?= ($isAddressLoaded) ? $invoiceAddress->getZip() : '' ?></p>
                    </div>
                    <?php
                    if($order->getInvoiceIco() !== null && is_numeric($order->getInvoiceIco()) && $order->getInvoiceIco() > 0):
                        ?>
                        <div class="form-group">
                            <label class="control-label">IČO</label>
                            <p><?= $order->getInvoiceIco() ?></p>
                        </div>
                        <?php
                        if((new Validate())->validateNotNull([$order->getInvoiceDic()])):
                            ?>
                            <div class="form-group">
                                <label class="control-label">DIČ</label>
                                <p><?= $order->getInvoiceDic() ?></p>
                            </div>
                            <?php
                        endif;
                        ?>
                        <?php
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row title">
    <div class="col-md-10 col-md-offset-1">
        <h4>Doprava</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="row venue-result">
            <?php
            $venueData = OrderController::getVenueCountedDataByOrder($order);
            if(!is_array($venueData))
                echo '<div class="col-md-12"><p>Chyba. Nepodařilo se načíst data dopravy. Prosím kotaktujte zákaznickou podporu. Chyba: ' . $venueData . '</p></div>';
            ?>
            <div class="col-md-12"><p>Délka cesty z našeho skladu: <span class="travel-length"><?= (is_array($venueData)) ? $venueData['travel-length'] : '' ?></span></p></div>
            <div class="col-md-12"><p>Doba trvání cesty: <span class="travel-time"><?= (is_array($venueData)) ? $venueData['duration-text'] : '' ?></span></p></div>
            <div class="col-md-12">
                <p class="venue-price">Cena dopravy bez DPH:
                    <?php
                    $priceWithoutVat = $order->getProductPrice();//Order::getPriceWithoutVat($stringifiedRentHours, $products);
                    if($priceWithoutVat > 7000) {
                        echo '<span class="green free">ZDARMA (objednávka nad 7 000 Kč bez DPH)</span>';
                    } else {
                        echo '<span class="travel-price">' . CustomCurrency::setCustomCurrencyWithoutDecimals((is_array($venueData)) ? $venueData['travel-price'] : '') . '</span>';
                    }
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="row title" id="payment">
    <div class="col-md-10 col-md-offset-1">
        <h4>Platba</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="row venue-result">
            <?php
            $payment = new Payment($order->getPaymentId());
            $payment = $payment->load();
            $paymentType = new PaymentType($order->getPaymentTypeId());
            $paymentType = $paymentType->load();
            ?>
            <div class="col-md-12"><p>Způsob platby: <strong><span class="<?= $payment->getIcon() ?>"></span> <?= $payment->getName() ?></strong></p></div>
            <div class="col-md-12"><p>Typ platby: <strong><?= $paymentType->getName() ?></strong></p></div>
        </div>
    </div>
</div>

<div class="row title">
    <div class="col-md-10 col-md-offset-1">
        <h4>Součet</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="row price-container">
            <div class="col-md-12 price-block">
                <?php
                $discount = $order->getDiscount();
                $discountAmount = round((floatval($priceWithoutVat) / floatval(100 - $discount)) * floatval($discount));
                if($discount > 0)
                    echo '<p>Sleva ' . $discount . '%: -' . CustomCurrency::setCustomCurrencyWithoutDecimals($discountAmount) . '</p>';
                ?>
                <p>Produkty bez DPH: <strong><?= CustomCurrency::setCustomCurrencyWithoutDecimals($order->getProductPrice()) ?></strong></p>
                <p>Doprava a platba bez DPH: <strong><?= ($priceWithoutVat > 7000) ? 'ZDARMA' : CustomCurrency::setCustomCurrencyWithoutDecimals($order->getDeliveryPaymentPrice()) ?></strong></p>
                <p>DPH: <strong><?= CustomCurrency::setCustomCurrencyWithoutDecimals($order->getVat()) ?></strong></p>
                <p class="final-price">Celkem k úhradě: <strong><?= CustomCurrency::setCustomCurrencyWithoutDecimals($order->getSumPrice()) ?></strong></p>
            </div>
        </div>
    </div>
</div>

<?php
if(!$order->getIsPaid() && $order->getOrderStateId() != 7):
    ?>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row venue-result">
                <div class="col-md-12">
                    <p>Objednávka ještě není uhrazena. <a href="/prehled-objednavek/detail/<?= $order->getOrderId() ?>/platba/" class="btn btn-primary">Zobrazit pokyny k platbě</a></p>
                </div>
            </div>
        </div>
    </div>
<?php
endif;
?>

<div class="row buttons">
    <div class="col-md-8 col-md-offset-2">
        <div class="image-status finish-order-status"><div><p>Nová informace</p></div></div>
    </div>
    <div class="col-md-4"><a class="btn btn-success" href="/prehled-objednavek/"><span class="ti-angle-left"></span> Zpět na přehled objednávek</a></div>
    <?php
    if (!$order->getIsCanceled()) {
        echo '<div class="col-md-4"><a class="btn btn-success request-change-order" href="#" data-target="'. $order->getOrderId() .'"><span class="ti-envelope"></span>Žádost o změnu objednávky</a></div>';
    }
    ?>
    <?php
    $paymentType = new PaymentType($order->getPaymentTypeId());
    $paymentType = $paymentType->load();

    if($paymentType->getDeposit() > 0 && $order->getOrderStateId() != 6){
        echo '<div class="col-md-4"><a class="btn btn-primary" target="_blank" href="/prehled-objednavek/detail/' . $order->getOrderId() . '/faktura/deposit/"><span class="ti-printer"></span> Vygenerovat zálohovou fakturu</a></div>';
    }

    if($order->getOrderStateId() == 5 || $order->getOrderStateId() == 7) {
        echo '<div class="col-md-4"><a class="btn btn-primary" target="_blank" href="/prehled-objednavek/detail/'. $order->getOrderId().'/faktura/final/"><span class="ti-printer"></span> Vygenerovat daňový doklad</a></div>';
    }
    ?>
    <!--<div class="col-md-6"><a class="btn btn-primary" href="faktura/"><span class="ti-printer"></span> Vygenerovat fakturu</a></div>-->
</div>
