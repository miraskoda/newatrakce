<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 17.09.2017
 * Time: 20:04
 */

use backend\controllers\UserController;
use backend\models\Order;

$orderId = 0;
if(isset($_GET['orderId']) && is_numeric($_GET['orderId']) && $_GET['orderId'] > 0)
    $orderId = $_GET['orderId'];

$order = new Order($orderId);
$order = $order->load();
if(!is_a($order, Order::class)) {
    // TODO create redirect to 404
    die('Objednávka s daným ID neexistuje. Prosím kontaktujte zákaznickou podporu.');
}

if(!UserController::isLoggedUser() && $order->getCustomerId() != $customer->getCustomerId()) {
    // TODO redirect to not permitted
    die('Přístup odepřen.');
}




//$orderNo = DateTime::createFromFormat('Y-m-d H:i:s', $order->getChangedRaw())->format('y') . $order->getOrderId();

$orderNo = $order->getFormattedOrderId();