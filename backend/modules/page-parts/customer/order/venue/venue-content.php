<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 24.08.2017
 * Time: 10:20
 */

use backend\controllers\OrderController;
use backend\models\Address;
use backend\models\CustomCurrency;
use backend\models\Order;

?>

<div class="row">
    <div class="col-md-3 text-center">
        <h3>1. Košík</h3>
    </div>
    <div class="col-md-3 text-center">
        <h2>2. Místo konání</h2>
    </div>
    <div class="col-md-3 text-center">
        <h3>3. Údaje</h3>
    </div>
    <div class="col-md-3 text-center">
        <h3>4. Souhrn</h3>
    </div>
</div>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="row title">
            <div class="col-md-12"><h4>Údaje</h4></div>
        </div>
        <div class="row">
            <div class="grid-item col-md-6">
                <div class="animated zoomIn box with-top">
                    <div class="top top-blue">
                        <h4>Kontaktní osoba v den akce</h4>
                    </div>
                    <div class="content">
                        <form id="contact-data">
                            <div class="image-status contact-data-status"><div><p>Nová informace</p></div></div>
                            <div class="form-group required">
                                <label class="control-label" for="contact-name">Jméno a příjmení</label>
                                <input name="contact-name" class="form-control" id="contact-name" placeholder="Jméno" value="<?= (($order->getContactName() !== null) ? ($order->getContactName()) : ($customer->getName())) ?>" required>
                            </div>
                            <div class="row no-margin">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="prefix">Předčíslí</label>
                                        <select class="form-control" name="contact-phone-prefix" id="prefix">
                                            <option<?= ($order->getContactPhonePrefix() == '+420') ? ' selected' : '' ?>>+420</option>
                                            <option<?= ($order->getContactPhonePrefix() == '+421') ? ' selected' : '' ?>>+421</option>
                                            <option<?= ($order->getContactPhonePrefix() == '+49') ? ' selected' : '' ?>>+49</option>
                                            <option<?= ($order->getContactPhonePrefix() == '+48') ? ' selected' : '' ?>>+48</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group required">
                                        <label class="control-label" for="contact-phone">Telefon (např.: 789456123)</label>
                                        <input type="tel" name="contact-phone" class="form-control" id="contact-phone" placeholder="789456123" <?php

                                        if($order->getContactPhone() !== null){
                                            if($order->getContactPhone()==0){
                                                echo "";
                                            }else{
                                                echo 'value="'.$order->getContactPhone().'"';
                                            }
                                        }else{

                                            if($customer->getPhone()==0){
                                                echo "";
                                            }else{
                                                echo 'value="'. $customer->getPhone().'"';
                                            }
                                        }
                                        ?> required>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="animated zoomIn box with-top">
                    <div class="top top-light-blue">
                        <h4>Adresa místa konání</h4>
                    </div>
                    <div class="content">
                        <form id="venue-data">
                            <?php
                            $invoiceAddress = new Address();
                            $invoiceAddress->setCustomerId($customer->getCustomerId());
                            $invoiceAddress->setAddressType(Address::ADDRESS_INVOICE);
                            $invoiceAddress = $invoiceAddress->load(true);

                            $isAddressLoaded = false;
                            if(is_a($invoiceAddress, Address::class))
                                $isAddressLoaded = true;
                            ?>
                            <div class="image-status venue-data-status"><div><p>Nová informace</p></div></div>
                            <div class="form-group required">
                                <label class="control-label" for="venue-street">Ulice a číslo popisné</label>
                                <input name="venue-street" class="form-control" id="venue-street" placeholder="Ulice 123" value="<?= (($order->getVenueStreet() !== null) ? ($order->getVenueStreet()) : (($isAddressLoaded) ? $invoiceAddress->getStreet() : '')) ?>" required>
                            </div>
                            <div class="form-group required">
                                <label class="control-label" for="venue-city">Město</label>
                                <input name="venue-city" class="form-control" id="venue-city" placeholder="Město" value="<?= (($order->getVenueCity() !== null) ? ($order->getVenueCity()) : (($isAddressLoaded) ? $invoiceAddress->getCity() : '')) ?>" required>
                            </div>
                            <div class="form-group required">
                                <label class="control-label" for="venue-zip">PSČ (např. 45612)</label>
                                <input name="venue-zip" class="form-control" id="venue-zip" placeholder="45612" value="<?= (($order->getVenueZip() !== null) ? ($order->getVenueZip()) : (($isAddressLoaded) ? $invoiceAddress->getZip() : '')) ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="venue-country">Země</label>
                                <select class="form-control" name="venue-country" id="venue-country">
                                    <option<?= ($order->getVenueCountry() == 'cz') ? ' selected' : '' ?> value="cz">Česká republika</option>
                                    <option<?= ($order->getVenueCountry() == 'sk') ? ' selected' : '' ?> value="sk">Slovensko</option>
                                    <option<?= ($order->getVenueCountry() == 'pl') ? ' selected' : '' ?> value="pl">Polsko</option>
                                    <option<?= ($order->getVenueCountry() == 'de') ? ' selected' : '' ?> value="de">Německo</option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row title">
            <div class="col-md-12"><h4>Výpočet dopravy</h4></div>
        </div>
        <div class="row venue-result">
            <div class="image-status venue-price-status"><div><p>Nová informace</p></div></div>
            <div class="col-md-12"><p>Délka cesty z našeho skladu: <span class="travel-length">Prosím nejdříve vyplňte adresu.</span></p></div>
            <div class="col-md-12"><p>Doba trvání cesty: <span class="travel-time">Prosím nejdříve vyplňte adresu.</span></p></div>
            <div class="col-md-12">
                <p class="venue-price">Cena dopravy bez DPH:
                    <?php
                    $priceWithoutVat = OrderController::getPriceWithoutVat();
                    if($priceWithoutVat > 7000) {
                        echo '<span class="green free">ZDARMA (objednávka nad 7 000 Kč)</span>';
                    } else {
                        echo '<span class="travel-price">Prosím nejdříve vyplňte adresu.</span> <span class="red">(Nakupte ještě za ' . CustomCurrency::setCustomCurrencyWithoutDecimals(7000 - $priceWithoutVat) . ' a máte dopravu ZDARMA)</span>';
                    }
                    ?>
                </p>
            </div>
        </div>
        <div class="alert alert-danger alert-venue">Prosím zadejte validní adresu místa konání, bez validní adresy není možné pokračovat v objednávce</div>
        <div class="row buttons">
            <div class="col-md-12">
                <div class="image-status validate-venue-status"><div><p>Nová informace</p></div></div>
            </div>
            <div class="col-md-6"><a class="btn btn-primary" href="/kosik/"><span class="ti-angle-left"></span> Zpět do košíku</a></div>
            <div class="col-md-6"><a class="btn btn-success validate-venue">Další krok objednávky <span class="ti-angle-right"></span></a></div>
        </div>
    </div>
</div>

