<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 09.08.2017
 * Time: 13:04
 */

use backend\models\Customer;
use backend\models\Password;

$isHashCorrect = false;
$error = '';
if(isset($_GET['email']) && isset($_GET['hash'])) {
    $email = $_GET['email'];
    $hash = $_GET['hash'];

    $customer = new Customer();
    $customer->setEmail($email);
    $customer = $customer->load();
    if(is_a($customer, Customer::class)) {
        if (Password::isPasswordHashEqual($customer->getForgottenPasswordHash(), $hash) && Password::isPasswordHashValid($customer->getForgottenPasswordHash())) {
            $isHashCorrect = true;
        } else {
            $error = '<strong>Hash je nesprávný nebo již expiroval.</strong><br>Zkontrolujte zda je odkaz zkopírován správně. Případně požádejte o nový odkaz.';
        }
    } else {
        $error = '<strong>Pro tento email nebyl nalezen účet.</strong><br>Zkontrolujte zda je odkaz zkopírován správně. Případně požádejte o nový odkaz.';
    }
} else {
    $error = '<strong>Hash nebyl nalezen.</strong><br>Zkontrolujte zda je odkaz zkopírován správně. Případně požádejte o nový odkaz.';
}

?>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="box top-offset text-center animated zoomIn">
            <?php
            if($isHashCorrect):
                ?>
                <form id="forgotten-password-change">
                    <div class="image-status forgotten-password-status"><div><p>Nová informace</p></div></div>
                    <div class="form-group required">
                        <label class="control-label" for="customer-email">Email</label>
                        <input type="email" name="email" class="form-control" id="cutomer-email" placeholder="Email" value="<?= (isset($email)) ? $email : '' ?>" disabled>
                        <input type="hidden" name="hash" value="<?= (isset($hash)) ? $hash : '' ?>">
                    </div>
                    <div><p>Heslo musí mít alespoň 6 znaků. Musí obsahovat číslice a písmena. Doporučujeme použít i speciální znaky a velká písmena.</p></div>
                    <div class="form-group required">
                        <label class="control-label" for="forgotten-password">Heslo</label>
                        <input type="password" name="forgotten-password" class="form-control" id="forgotten-password" placeholder="Heslo" required>
                    </div>
                    <div class="form-group required">
                        <label class="control-label" for="forgotten-password-cofirm">Potvrzení hesla</label>
                        <input type="password" name="forgotten-password-confirm" class="form-control" id="forgotten-password-confirm" placeholder="Heslo podruhé" required>
                    </div>
                    <input type="submit" class="btn btn-info" value="Změnit heslo">
                </form>
                <?php
            else:
                ?>
                <h4 class="red"><?= $error ?></h4>
                <?php
            endif;
            ?>
        </div>
    </div>
</div>