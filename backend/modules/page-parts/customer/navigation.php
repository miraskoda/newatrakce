<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 01.08.2017
 * Time: 15:52
 */

use backend\models\Url;

?>
<nav id="navigation-sidebar" class="sidebar" style="width: 54px">
    <div>
        <ul>
            <li><a href="#" class="hamburger-trigger"><span class="ti-menu"></span> Menu</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/../muj-ucet/'); ?>"><span class="ti-blackboard"></span> Přehled</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/../sprava-udaju/'); ?>"><span class="ti-user"></span> Údaje</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/../prehled-objednavek/'); ?>"><span class="ti-truck"></span> Objednávky</a></li>
            <li><a href="<?= Url::getAbsUrlTo($root . '/../prehled-rezervaci/'); ?>"><span class="ti-timer"></span> Rezervace</a></li>
            <!--<li><a href="#"><span class="ti-calendar"></span> Kalendář</a></li>-->
        </ul>
    </div>
</nav>
