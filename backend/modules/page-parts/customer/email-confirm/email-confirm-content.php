<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 08.08.2017
 * Time: 20:18
 */

use backend\models\Customer;

$isEmailConfirmed = false;
$error = '';
if(isset($_GET['hash'])) {
    $hash = $_GET['hash'];

    $customer = new Customer();
    $customer->setEmailConfirmedHash($hash);
    $customer = $customer->load();

    if(is_a($customer, Customer::class)) {
        if(!$customer->isEmailConfirmed()) {
            $customer->setIsEmailConfirmed(true);
            $customer = $customer->update();
            if (is_a($customer, Customer::class)) {
                $isEmailConfirmed = true;
            } else {
                $error = 'Nepodařilo se uložit data do databáze. Kontaktujte naši technickou podporu. Chyba: ' . $customer;
            }
        } else {
            $isEmailConfirmed = true;
        }
    } else {
        $error = '<strong>Hash je nesprávný.</strong><br>Zkontrolujte zda je odkaz zkopírován správně. Případně kontaktujte naši technickou podporu.';
    }
} else {
    $error = '<strong>Hash nebyl nalezen.</strong><br>Zkontrolujte zda je odkaz zkopírován správně. Případně kontaktujte naši technickou podporu.';
}

?>
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="box top-offset text-center animated zoomIn">
            <?php
            if($isEmailConfirmed):
            ?>
                <h4>Email byl úspěšně ověřen. Nyní můžete pokračovat do administrace.</h4>
                <a class="btn btn-info" href="/muj-ucet/">Do administrace</a>
            <?php
            else:
            ?>
                <h4 class="red"><?= $error ?></h4>
            <?php
            endif;
            ?>
        </div>
    </div>
</div>
