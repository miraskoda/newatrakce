<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 17.08.2017
 * Time: 12:19
 */

use backend\controllers\CartController;

?>

<div class="row">
    <div class="col-md-3 text-center">
        <h2>1. Košík</h2>
    </div>
    <div class="col-md-3 text-center">
        <h3>2. Místo konání</h3>
    </div>
    <div class="col-md-3 text-center">
        <h3>3. Údaje</h3>
    </div>
    <div class="col-md-3 text-center">
        <h3>4. Souhrn</h3>
    </div>
</div>
<div class="row title">
    <div class="col-md-8 col-md-offset-2">
        <h4>Výpis produktů</h4>
    </div>
</div>
<div class="row admin-cart-list">
    <div class="col-md-8 col-md-offset-2 text-center">
        <h4><span class="ti-reload"></span> Načítání ... čekejte prosím</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-md-6">
                <div class="row date-container">
                    <div class="col-md-12"><h4>Termíny pronajmutí (max. 5)</h4></div>
                    <div class="col-md-12 date-blocks">
                        <h4><span class="ti-reload"></span> Načítání ... čekejte prosím</h4>
                    </div>
                    <div class="col-md-12 remove-date"><p><span class="ti-minus"></span> Odebrat poslední termín</p></div>
                    <div class="col-md-12 add-date"><p><span class="ti-plus"></span> Přidat další termín</p></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row price-container">

                    <div class="col-md-12"><h4>Výpočet ceny</h4><a class="green price-info"><span class="ti-help-alt"></span> Jak se počítá cena</a></div>


                    <div class="col-md-12 price-block">
                        <h4><span class="ti-reload"></span> Načítání ... čekejte prosím</h4>
                    </div>
                </div>
                <div class="row buttons">
                    <div class="col-md-12">
                        <div class="image-status validate-cart-status"><div><p>Nová informace</p></div></div>
                    </div>
                    <div class="col-md-6"><a class="btn btn-primary" href="/"><span class="ti-angle-left"></span> Pokračovat v nákupu</a></div>
                    <?php
                    $products = CartController::getCartProducts();
                    if(is_array($products) && count($products) > 0):
                    ?>
                    <div class="col-md-6"><a class="btn btn-success validate-cart">Další krok objednávky <span class="ti-angle-right"></span></a></div>
                    <?php
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
