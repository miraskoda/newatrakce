<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 01.08.2017
 * Time: 16:24
 */
?>

<div class="row" id="conte" style="margin-left: 54px">
    <?php
    if(!$customer->isEmailConfirmed()) :
    ?>
    <div class="col-md-12">
        <div class="box text-center animated zoomIn">
            <h4 class="red">Email není ověřen! <a class="btn btn-info confirm-email-resend">Znovu odeslat ověřovací email</a></h4>
        </div>
    </div>
    <?php
    endif;
    ?>
    <div class="row"  >
        <div class="col-md-12">
            <div class="animated zoomIn box with-top">
                <div class="top top-blue">
                    <h5>Vaše zákaznická administrace</h5>
                </div>
                <div class="content">
                    <h4>Vítejte v administraci vašeho účtu na OnlineAtrakce.cz</h4>
                    <p>Zde můžete upravovat všechny své údaje a spravovat objednávky i rezervace.</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="animated zoomIn box with-top">
                <div class="top top-kaky">
                    <h5>Menu</h5>
                </div>
                <div class="content">
                    <p><span class="ti-arrow-left"></span> V menu po levé straně najdete všechny sekce, které potřebujete pro správu svého účtu.</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="animated zoomIn box with-top">
                <div class="top top-red">
                    <h5>Vybírejte z největšího výběru atrakcí na trhu!</h5>
                </div>
                <div class="content">
                    <p>Z naší široké nabídky atrakcí si určitě vyberete!</p>
                    <a href="/" class="btn btn-success" style="width: 100%">Začněte nakupovat!</a>
                </div>
            </div>
        </div>
    </div>
</div>
