<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 23.08.2017
 * Time: 10:17
 */

use backend\controllers\UserController;
use backend\models\User;

?>

<div class="grid-item <?= ($pageId == 18) ? 'col-md-6' : 'col-md-4' ?>">
    <div class="animated zoomIn box with-top">
        <div class="top top-blue">
            <h4>Ověřovací údaje</h4>
        </div>
        <div class="content">
            <form id="account-data">
                <?= ((is_a(UserController::isLoggedUser(), User::class)) ? '<input type="hidden" name="customer-id" value="' . $customer->getCustomerId() . '">' : '') ?>
                <!--<div class="image-status account-data-status"><div><p>Nová informace</p></div></div>-->
                <div class="form-group required">
                    <label class="control-label" for="company-switch">Jsem firma</label>
                    <input type="checkbox" name="company" id="company-switch" data-toggle="toggle" data-on="Ano" data-off="Ne" <?= ($customer->isCompany()) ? 'checked' : '' ?>>
                </div>
                <div class="person">
                    <div class="form-group required">
                        <label class="control-label" for="birth-number">Rodné číslo (bez lomítka)</label>
                        <input name="birth-number" class="form-control" id="birth-number" placeholder="Rodné číslo" value="<?= $customer->getBirthNumber() ?>">
                    </div>
                </div>
                <div class="company" style="display: none;">
                    <div class="form-group required">
                        <label class="control-label" for="ico">IČO</label>
                        <input name="ico" class="form-control" id="ico" placeholder="IČO" value="<?= $customer->getIco() ?>">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="dic">DIČ</label>
                        <input name="dic" class="form-control" id="dic" placeholder="DIČ" value="<?= $customer->getDic() ?>">
                    </div>
                </div>
                <?= ($pageId != 18) ? '<input type="submit" class="btn btn-info" value="Uložit">' : '' ?>
            </form>
        </div>
    </div>
</div>
