<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 23.08.2017
 * Time: 10:19
 */

use backend\models\Address;
use backend\controllers\UserController;
use backend\models\User;

?>

<div class="grid-item <?= ($pageId == 18) ? 'col-md-6' : 'col-md-4' ?>">
    <div class="animated zoomIn box with-top">
        <div class="top top-light-blue">
            <h4>Fakturační údaje</h4>
        </div>
        <div class="content">
            <form id="invoice-data">
                <?= ((is_a(UserController::isLoggedUser(), User::class)) ? '<input type="hidden" name="customer-id" value="' . $customer->getCustomerId() . '">' : '') ?>
                <?php

                $invoiceAddress = new Address();
                $invoiceAddress->setCustomerId($customer->getCustomerId());
                $invoiceAddress->setAddressType(Address::ADDRESS_INVOICE);
                $invoiceAddress = $invoiceAddress->load(true);

                $isAddressLoaded = false;
                if(is_a($invoiceAddress, Address::class))
                    $isAddressLoaded = true;
                ?>
                <button id="ares-load" class="btn">Načíst data z Aresu</button>
                <div class="form-group required">
                    <label class="control-label" for="invoice-name">Jméno / Název firmy</label>
                    <input name="invoice-name" class="form-control" id="invoice-name" placeholder="Jméno" value="<?= ($isAddressLoaded) ? $invoiceAddress->getName() : '' ?>" required>
                </div>
                <div class="form-group required">
                    <label class="control-label" for="invoice-street">Ulice a číslo popisné</label>
                    <input name="invoice-street" class="form-control" id="invoice-street" placeholder="Ulice 123" value="<?= ($isAddressLoaded) ? $invoiceAddress->getStreet() : '' ?>" required>
                </div>
                <div class="form-group required">
                    <label class="control-label" for="invoice-city">Město</label>
                    <input name="invoice-city" class="form-control" id="invoice-city" placeholder="Město" value="<?= ($isAddressLoaded) ? $invoiceAddress->getCity() : '' ?>" required>
                </div>
                <div class="form-group required">
                    <label class="control-label" for="invoice-zip">PSČ (např. 45612)</label>
                    <input name="invoice-zip" class="form-control" id="invoice-zip" placeholder="45612" value="<?= ($isAddressLoaded) ? $invoiceAddress->getZip() : '' ?>" required>
                </div>
                <?= ($pageId != 18) ? '<input type="submit" class="btn btn-info" value="Uložit">' : '' ?>
            </form>
        </div>
    </div>
</div>
