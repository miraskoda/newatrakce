<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 02.08.2017
 * Time: 19:27
 */

use backend\models\Address;
use backend\controllers\UserController;
use backend\models\User;

if(!isset($customer))
    die('Chyba! Nebylo možné načíst data zákazníka!');

?>

<div class="grid account-data-content-grid" id="conte" style="margin-left: 54px">
    <div class="grid-sizer col-md-4"></div>
    <div class="grid-item col-md-4">
        <div class="animated zoomIn box with-top">
            <div class="top top-blue">
                <h4>Základní údaje</h4>
            </div>
            <div class="content">
                <form id="basic-data">
                    <?= ((is_a(UserController::isLoggedUser(), User::class)) ? '<input type="hidden" name="customer-id" value="' . $customer->getCustomerId() . '">' : '') ?>
                    <div class="image-status basic-data-status"><div><p>Nová informace</p></div></div>
                    <div class="form-group required">
                        <label class="control-label" for="name">Jméno</label>
                        <input name="name" class="form-control" id="name" placeholder="Jméno" value="<?= $customer->getName() ?>" required>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="prefix">Předčíslí</label>
                                <select class="form-control" name="prefix" id="prefix">
                                    <option<?= ($customer->getPhonePrefix() == '+420') ? ' selected' : '' ?>>+420</option>
                                    <option<?= ($customer->getPhonePrefix() == '+421') ? ' selected' : '' ?>>+421</option>
                                    <option<?= ($customer->getPhonePrefix() == '+49') ? ' selected' : '' ?>>+49</option>
                                    <option<?= ($customer->getPhonePrefix() == '+48') ? ' selected' : '' ?>>+48</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group required">
                                <label class="control-label" for="customer-phone">Telefon (např.: 789456123)</label>
                                <input type="tel" name="phone" class="form-control" id="customer-phone" placeholder="789456123" value="<?= $customer->getPhone() ?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="control-label" for="customer-email">Email</label>
                        <input type="email" name="email" class="form-control" id="cutomer-email" placeholder="Email" value="<?= $customer->getEmail() ?>" required>
                    </div>
                    <input type="submit" class="btn btn-info" value="Uložit">
                </form>
            </div>
        </div>
    </div>
    <div class="grid-item col-md-4">
        <div class="animated zoomIn box with-top">
            <div class="top top-blue">
                <h4>Další údaje</h4>
            </div>
            <div class="content">
                <form id="other-data">
                    <?= ((is_a(UserController::isLoggedUser(), User::class)) ? '<input type="hidden" name="customer-id" value="' . $customer->getCustomerId() . '">' : '') ?>
                    <div class="image-status other-data-status"><div><p>Nová informace</p></div></div>
                    <div class="form-group">
                        <label class="control-label" for="bank-account-number">Číslo bankovního účtu</label>
                        <input name="bank-account-number" class="form-control" id="bank-account-number" placeholder="Číslo účtu" value="<?= $customer->getBankAccountNumber() ?>" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="bank-code">Kód banky</label>
                        <input name="bank-code" class="form-control" id="bank-code" placeholder="Kód banky" value="<?= $customer->getBankCode() ?>" required>
                    </div>
                    <input type="submit" class="btn btn-info" value="Uložit">
                </form>
            </div>
        </div>
    </div>
    <?php
    require_once 'account-data-part.php';
    require_once 'invoice-data-part.php';
    ?>
    <div class="grid-item col-md-4">
        <div class="animated zoomIn box with-top">
            <div class="top top-light-blue">
                <h4>Korespondenční adresa</h4>
            </div>
            <div class="content">
                <form id="delivery-data">
                    <?= ((is_a(UserController::isLoggedUser(), User::class)) ? '<input type="hidden" name="customer-id" value="' . $customer->getCustomerId() . '">' : '') ?>
                    <?php
                    $deliveryAddress = new Address();
                    $deliveryAddress->setCustomerId($customer->getCustomerId());
                    $deliveryAddress->setAddressType(Address::ADDRESS_DELIVERY);
                    $deliveryAddress = $deliveryAddress->load(true);

                    $isDeliveryAddressLoaded = false;
                    if(is_a($deliveryAddress, Address::class))
                        $isDeliveryAddressLoaded = true;
                    ?>
                    <div class="image-status delivery-data-status"><div><p>Nová informace</p></div></div>
                    <div class="form-group required">
                        <label class="control-label" for="delivery-address-switch">Korespondenční adresa je stejná jako fakturační</label>
                        <input type="checkbox" name="delivery-address" id="delivery-address-switch" data-toggle="toggle" data-on="Ano" data-off="Ne" <?= (!$isDeliveryAddressLoaded) ? 'checked' : '' ?>>
                    </div>
                    <div class="delivery-address" style="display: none">
                        <div class="form-group required">
                            <label class="control-label" for="delivery-name">Jméno</label>
                            <input name="delivery-name" class="form-control" id="delivery-name" placeholder="Jméno" value="<?= ($isDeliveryAddressLoaded) ? $deliveryAddress->getName() : '' ?>" required>
                        </div>
                        <div class="form-group required">
                            <label class="control-label" for="delivery-street">Ulice a číslo popisné</label>
                            <input name="delivery-street" class="form-control" id="delivery-street" placeholder="Ulice 123" value="<?= ($isDeliveryAddressLoaded) ? $deliveryAddress->getStreet() : '' ?>" required>
                        </div>
                        <div class="form-group required">
                            <label class="control-label" for="delivery-city">Město</label>
                            <input name="delivery-city" class="form-control" id="delivery-city" placeholder="Město" value="<?= ($isDeliveryAddressLoaded) ? $deliveryAddress->getCity() : '' ?>" required>
                        </div>
                        <div class="form-group required">
                            <label class="control-label" for="delivery-zip">PSČ (např. 45612)</label>
                            <input name="delivery-zip" class="form-control" id="delivery-zip" placeholder="45612" value="<?= ($isDeliveryAddressLoaded) ? $deliveryAddress->getZip() : '' ?>" required>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-info" value="Uložit">
                </form>
            </div>
        </div>
    </div>
    <?php
    if(!isset($_GET['customerId'])):
    ?>
    <div class="grid-item col-md-4">
        <div class="animated zoomIn box with-top">
            <div class="top top-red">
                <h4>Změnit heslo</h4>
            </div>
            <div class="content">
                <form id="password-data">
                    <div class="image-status password-data-status"><div><p>Nová informace</p></div></div>
                    <div class="form-group required">
                        <label class="control-label" for="current-password">Původní heslo</label>
                        <input type="password" name="current-password" class="form-control" id="current-password" placeholder="Původní heslo" required>
                    </div>
                    <div class="form-group required">
                        <label class="control-label" for="new-password">Nové heslo</label>
                        <input type="password" name="new-password" class="form-control" id="new-password" placeholder="Nové heslo" required>
                    </div>
                    <div class="form-group required">
                        <label class="control-label" for="new-password-confirm">Potvrzení nového hesla</label>
                        <input type="password" name="new-password-confirm" class="form-control" id="new-password-confirm" placeholder="Potvrzení nového hesla" required>
                    </div>
                    <input type="submit" class="btn btn-info" value="Uložit">
                </form>
            </div>
        </div>
    </div>
    <?php
    endif;
    ?>
</div>