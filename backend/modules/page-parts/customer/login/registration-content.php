<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 30.07.2017
 * Time: 16:15
 */

use backend\models\Url;
//
?>

<div class="col-lg-6 col-md-12 register">
    <div class="animated flipInX">
        <div class="register-container">
            <h2>Ještě nejste zaregistrováni?</h2>
            <div class="register-comment">
                <button id="register-switch">Zaregistrovat se</button>
                <div>
                    <h3>Proč se registrovat?</h3>
                    <ul>
                        <li>Snadný přístup ke všem objednávkám a rezervacím</li>
                        <li>Rezervace jedním kliknutím</li>
                        <li>Automatické upozorňování na vypršení rezervace</li>
                        <li>Vlastní kalendář s přehledem vypůjčených a rezervovaných atrakcí</li>
                        <li>A mnoho dalšího ...</li>
                    </ul>
                </div>
            </div>
            <div class="register-form" style="display: none">
                <form method="post">
                    <div class="action-alert alert alert-danger hidden animated bounceIn"></div>
                    <div class="form-group required">
                        <label class="control-label" for="customer-email">Email</label>
                        <input type="email" name="register-email" class="form-control" id="customer-email" placeholder="Email" required>
                    </div>
                    <div class="form-group required">
                        <label class="control-label" for="customer-password">Heslo</label>
                        <input type="password" name="register-password" class="form-control" id="customer-password" placeholder="Heslo" required>
                    </div>
                    <div class="form-group required">
                        <label class="control-label" for="customer-password-confirm">Potvrzení hesla</label>
                        <input type="password" name="register-password-confirm" class="form-control" id="customer-password-confirm" placeholder="Heslo podruhé" required>
                    </div>
                    <div class="recaptcha">
                        <button class="g-recaptcha" data-sitekey="6LevNCsUAAAAAIW1I0GOBn1WPsYFeEnsQM9NzhND" data-callback='onSubmit' data-badge="inline">
                            Zaregistrovat se</button></div>
                </form>
            </div>
        </div>
    </div>
</div>

