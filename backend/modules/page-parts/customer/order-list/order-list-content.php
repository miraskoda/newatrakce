<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 16.09.2017
 * Time: 17:40
 */

use backend\models\CustomCurrency;
use backend\models\Order;
use backend\models\OrderProduct;
use backend\models\OrderState;

$orders = Order::getOrders();
?>
<div class="row" id="conte" style="margin-left: 54px">


    <?php
    if (isset($orders) && is_array($orders) && count($orders) > 0) :
        ?>
        <div class="col-md-12 hidden-xs">
            <div class="row box overflow-auto light-blue text-center animated zoomInDown">
                <div class="col-md-2">
                    <p>Číslo objednávky</p>
                </div>
                <div class="col-md-2">
                    <p>Datum vytvoření</p>
                </div>
                <div class="col-md-2">
                    <p>Cena s DPH</p>
                </div>
                <div class="col-md-2">
                    <p>Počet produktů v objednávce</p>
                </div>
                <div class="col-md-2">
                    <p>Aktuální stav</p>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
        <?php
        foreach ($orders as $order):
            ?>
            <div class="col-md-12 reservation-row">
                <div class="box text-center animated zoomInDown">
                    <div class="row customer-reservation-list-mobile">
                        <div class="col-md-2">
                            <h3 class="visible-xs hidden-md">Číslo objednávky</h3>
                            <p>
                                <?php
                                echo $order->getFormattedOrderId();
                                ?>
                            </p>
                        </div>
                        <div class="col-md-2">
                            <h3 class="visible-xs hidden-md">Datum vytvoření</h3>
                            <p><?= $order->getChanged() ?></p>
                        </div>
                        <div class="col-md-2">
                            <h3 class="visible-xs hidden-md">Cena s DPH</h3>
                            <p><?= CustomCurrency::setCustomCurrencyWithoutDecimals($order->getSumPrice()) ?></p>
                        </div>
                        <div class="col-md-2">
                            <h3 class="visible-xs hidden-md">Počet produktů v objednávce</h3>
                            <p><?= OrderProduct::getSumOfProductsInOrder($order->getOrderId()) ?></p>
                        </div>
                        <div class="col-md-2">
                            <h3 class="visible-xs hidden-md">Aktuální stav</h3>
                            <p>
                                <?php
                                $stateStateId = $order->getOrderStateId();
                                $isCanceled = $order->getIsCanceled();
                                if ($isCanceled) {
                                    echo '<span class="color-red">Zrušeno</span>';
                                } else {
                                    if ($stateStateId == 0) {
                                        $stateStateId = 1;
                                    }
                                    $state = new OrderState($stateStateId);
                                    $state = $state->load();
                                    if (is_a($state, OrderState::class))
                                        echo '<span class="color-green">' . $state->getName() . '</span>';
                                    else
                                        echo  '<span class="color-orange">Neznámý</span>';
                                }
                                ?>
                            </p>
                        </div>
                        <?php
                        if ($order->getIsCanceled()) {
                            echo '';
                        } else {
                            echo '
                             <div class="col-md-1">
                                 <hr class="visible-xs hidden-md">
                                 <a data-target="' . $order->getOrderId() . '" href="#" class="btn btn-danger cancel-order"><span class="ti-trash"></span>Zrušit</a>
                             </div>
                            ';
                        }
                        ?>
                        <div class="<? $order->getIsCanceled() ? 'col-md-1' : 'col-md-2' ?> ">
                            <hr class="visible-xs hidden-md">
                            <a href="/prehled-objednavek/detail/<?= $order->getOrderId() ?>/"
                               class="btn btn-success"><span
                                        class="ti-search"></span> Detail</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        endforeach;
        ?>
    <?php
    else :
        ?>
        <div class="col-md-12 box">
            <p>Nemáte žádné objednávky. <?= ((isset($orders) && is_string($orders)) ? $orders : '') ?></p>
        </div>
    <?php
    endif;
    ?>
</div>