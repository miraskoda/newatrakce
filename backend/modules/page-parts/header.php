<?php

use backend\view\CategoryList;
use backend\models\Url;
use backend\controllers\OptionController;
use backend\models\Product;
use backend\models\User;
use backend\controllers\UserController;
?>


<header>
    <nav class="navbar navbar-inverse animated fadeInDown">
        <div class="container-fluid ">
            <div class="navbar-header">
                <button id="toggleButton" type="button" class="navbar-toggle" data-target="#navbar"><span
                            class="ti-menu"></span></button>
                <h1 class="navbar-brand"><a class="centrovac" href="<?= Url::getPathToHome() ?>">
                        <span class="modrej">  <img src="/img/logo3.png" style="
    width: 200px; margin-top: 11px;
"></span>
                    </a></h1>
            </div>
            <div class="collapse navbar-collapse" id="navbar">
                <ul class="nav navbar-nav">
                    <?= CategoryList::generateCategoryList("public-list"); ?>

                    <li><a href="/tipy/">Tipy na akce</a></li>
                    <li><a href="/kontakt/">Kontakt</a></li>
                    <li id="aboutUs">
                        <a href="/o-nas/" class="dropdown-toggle" data-target="#aboutUs">O nás<span class="ti-arrow-circle-down"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/fotogalerie/">Fotogalerie</a></li>
                            <li><a href="/reference/">Reference</a></li>
                            <li><a href="/cenik/">Ceník</a></li>
                            <li><a href="/informace-o-doprave/">Informace o dopravě</a></li>

                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right tip">


                    <li class="centrov">
                                    <form action="/vyhledavani/" style="position: relative">
                                        <div class="input-group martop marrig" >

                                    <input type="text" class="menus form-control menus2" name="search" placeholder="Vyhledat" value="<?= ((isset($_GET['search'])) ? $_GET['search'] : '') ?>" autocomplete="off">


                                        </div>
                                        <div id="search-hints" style="z-index: 999999;"></div>

                                    </form>

                    </li>

                    <li class="centrov">
                        <form id="date-form" action="/vyhledavani/">
                            <div class="input-group martop">

                            <input type="text" class="menus form-control search-datetimepicker" autocomplete="off" name="date" placeholder="Datum" style="z-index: 9">

                            </div>

                        </form>

                    </li>


                    <?php require_once Url::getBackendPathTo('/modules/page-parts/header-customer-part.php') ?>
                </ul>

        </div>

    </nav>

    <?php
    $user = UserController::isLoggedUser();
    if(is_a($user, User::class)):
        ?>
        <div class="admin-box">
            <a href="/admin"><p><span class="ti-pencil-alt"></span> Do administrace</p></a>
            <?php
            if(isset($product) && is_a($product, Product::class)){
                echo '<a href=\'/admin/pages/product-management.php?productId=' . $product->getProductId() . '\'><p><span class="ti-package"></span> Upravit produkt</p></a>';
            }
            ?>
        </div>
    <?php
    endif;
    ?>

</header>




<div id="sticky-wrap" style="display: none">
    <a id="toop" href="#public">
        <img class="up" height="50px"  src="/img/back.png"/>
    </a>
</div>
<div class="paddingtop70"></div>
