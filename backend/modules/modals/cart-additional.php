<div class="modal modal-additional" role="dialog" id="addons-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"></h3>
            </div>
            <div class="additional-product-cart-status"></div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-success additional-close">Zpět do košíku</button>
            </div>
        </div>
    </div>
</div>
