<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 23.11.2018
 * Time: 21:36
 */
?>

<div class="modal fade" id="request-order-change" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Popište váš požadavek na změnu.</h3>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <textarea class="form-control" id="request-change-text-area" name="order-request-change" placeholder="Popiště slovně potřebnou změnu..."></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary send-request">Odeslat</button>
                <button type="button" data-dismiss="modal" class="btn btn-success decline-request">Zpět</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="success-order-request-change" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Požadavek na změnu odeslán.</h5>
            </div>
            <div class="modal-body">
                <p class="alert-content">Váš požadavek byl odeslán ke zpracování, v nejbližší době na něj budeme reagovat.</p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-success request-close">Zavřít</button>
            </div>
        </div>

    </div>
</div>
<div class="modal fade" id="change-alert" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Chyba!</h5>
            </div>
            <div class="modal-body">
                <h4 class="alert-content">Info</h4>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-success request-close-error">Zpět</button>
            </div>
        </div>

    </div>
</div>

