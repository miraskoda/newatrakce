<div class="modal fade" id="cart-customer-reservation" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Vložení produktu do košíku</h3>
            </div>
            <div class="reservation-customer-status"></div>
            <div class="modal-body">
                <p>Vložením produktu do košíku dojde ke zrušení vaší rezervace.</p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary reservation-customer-agree">Přejít do košíku</button>
                <button type="button" data-dismiss="modal" class="btn btn-success reservation-admin-close">Přejít zpět</button>
            </div>
        </div>

    </div>
</div>
