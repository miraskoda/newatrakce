<div class="modal fade" id="order-delete" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Zrušení objednávky</h3>
            </div>
            <div class="reservation-customer-status"></div>
            <div class="modal-body">
                <p>Opravdu si přejete zrušit vaši objednávku ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary order-delete-agree">Ano</button>
                <button type="button" data-dismiss="modal" class="btn btn-success order-delete-close">Ne</button>
            </div>
        </div>

    </div>
</div>
