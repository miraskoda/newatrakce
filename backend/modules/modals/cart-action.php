<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 14.08.2017
 * Time: 17:36
 */
?>

<div class="modal fade" id="cart-action" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Produkt úspěšně vložen do košíku</h3>
            </div>
            <div class="modal-body">
                <h4>Jak chcete pokračovat?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary go-shopping">Pokračovat v nákupu</button>
                <button type="button" data-dismiss="modal" class="btn btn-success go-to-cart">Přejít do košíku</button>
            </div>
        </div>

   </div>
</div>
<div class="modal fade" id="cart-alert" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Chyba!</h5>
            </div>
            <div class="modal-body">
                <h4 class="alert-content">Info</h4>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary go-shopping">Pokračovat v nákupu</button>
                <button type="button" data-dismiss="modal" class="btn btn-success go-to-cart">Přejít do košíku</button>
            </div>
        </div>

    </div>
</div>
