<div class="modal modal-additional" role="dialog" id="login-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">
                    Pro pokračování je nutné přihlášení
                </h3>
            </div>
            <div class="login-modal-status"></div>
            <div class="modal-body">
                <div class="col-lg-6 col-md-12 login">
                    <div class="login-form animated zoomIn">
                        <form method="post">
                            <div class="action-alert alert alert-danger hidden animated bounceIn"></div>
                            <div class="form-group required">
                                <label class="control-label" for="customer-email">Email</label>
                                <input type="email" name="email" class="form-control" id="cutomer-email"
                                       placeholder="Email" required>
                            </div>
                            <div class="form-group required">
                                <label class="control-label" for="customer-password">Heslo</label>
                                <input type="password" name="password" class="form-control" id="customer-password"
                                       placeholder="Heslo" required>
                            </div>
                            <input type="submit" class="btn btn-info" value="Přihlásit se">
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary cancel">Zavřít</button>
            </div>
        </div>
    </div>
</div>

