<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 28.11.2017
 * Time: 12:21
 */

namespace backend\controllers;

use backend\controllers\CustomerController;
use backend\models\Customer;
use backend\models\Log;
use backend\models\MailHelper;
use backend\models\Product;
use backend\models\Reservation;
use backend\models\Superproduct;
use backend\models\SuperproductProduct;
use backend\models\User;
use backend\models\Validate;
use DateTime;

class ReservationController {

	/**
	 * @return Reservation|bool|string
	 */
	public static function createReservation() {
		if (isset($_POST['productId']) && isset($_POST['quantity']) && isset($_POST['termStart']) && isset($_POST['termHours']) && isset($_POST['reservationHours']) && is_a(CustomerController::isLoggedCustomer(), Customer::class)) {
			$productId = $_POST['productId'];
			$quantity = $_POST['quantity'];

			$termStart = $_POST['termStart'];
			if(((new Validate())->validateNotEmpty([$termStart]))) {
				$termStart = DateTime::createFromFormat( 'd. m. Y H:i', $_POST['termStart'] );
				$termStart = $termStart->format( 'Y-m-d H:i:s' );
			} else {
				return 'Nejsou správně vyplněna data termínu';
			}

			$termHours = $_POST['termHours'];
			$reservationHours = $_POST['reservationHours'];
			$customerGroup = CustomerController::getCustomerGroup();
			$maxReservationHours = $customerGroup->getMaxReservationHours();
			if($maxReservationHours < $reservationHours)
				return 'Maximální délka rezervace je ' . $maxReservationHours . ' hodin.';

			// used $_POST['termStart'] because d. m. Y H:i is required format for available checking
			$product = new Product($productId);
			$product = $product->load();
			if(is_a($product, Product::class)) {
				$productAvailability = json_decode($product->getProductAvailability($quantity, $_POST['termStart'], $termHours));
				// if problem with availability -> show error
				if($productAvailability->result == 'danger' || $productAvailability->result == 'warning'){
					return $productAvailability->text;
				}
			} else {
				return 'Nepodařilo se načíst produkt.';
			}

			$customer = CustomerController::isLoggedCustomer();

			$reservation = new Reservation();

			$areThereConflicts = count($reservation->areThereConflicts($productId, $termStart, $termHours));

			if (($areThereConflicts + $quantity) > $product->getQuantity()) {
                return 'Omlouváme se, v daném termínu jsou již všechny položky v daném množství zarezervovány.';
            }

			$reservation->setCustomerId($customer->getCustomerId());
			$reservation->setProductId($productId);
			$reservation->setQuantity($quantity);
			$reservation->setTermStart($termStart);
			$reservation->setTermHours($termHours);
			$reservation->setReservationHours($reservationHours);

			$reservation = $reservation->create();

			if(is_a($reservation, Reservation::class)) {
			    // send mail
			    $mailer = new MailHelper();
			    $mailer->sendNewReservation($customer->getEmail(), $reservation);
            }

			Log::checkObjectAndInsert($customer->getCustomerId(), $reservation, Reservation::class, 'Vytvořit rezervaci', Log::LOG_CUSTOMER);

			return $reservation;
		}

		return false;
	}

	/**
	 * @return Reservation|bool|string
	 */
	public static function deleteReservationFromAdmin() {
		if(isset($_POST['reservationId']) && is_a(UserController::isLoggedUser(), User::class)) {
			$reservationId = $_POST['reservationId'];

			$reservation = new Reservation();
			$reservation->setReservationId($reservationId);
			$reservation = $reservation->load();

			if(is_a($reservation, Reservation::class)) {
				$reservation = $reservation->delete();
				Log::checkBoolAndInsert($reservation, true, UserController::getUserId(), 'Smazání rezervace ' . $reservationId, Log::LOG_USER);

				return $reservation;
			} else {
				Log::insert(UserController::getUserId(), 'Smazání rezervace ' . $reservationId, Log::LOG_USER, Log::LOG_STATE_ERROR, $reservation);
				return $reservation;
			}
		}

		return false;
	}

	/**
	 * @return Reservation|bool|string
	 */
	public static function deleteReservationForCustomer() {
		if(isset($_POST['reservationId']) && is_a(CustomerController::isLoggedCustomer(), Customer::class)) {
			$reservationId = $_POST['reservationId'];

			$reservation = new Reservation();
			$reservation->setCustomerId(CustomerController::isLoggedCustomer()->getCustomerId());
			$reservation->setReservationId($reservationId);
			$reservation = $reservation->load(true);

			if(is_a($reservation, Reservation::class)) {
				$reservation = $reservation->delete();
				Log::checkBoolAndInsert($reservation, true, CustomerController::isLoggedCustomer()->getCustomerId(), 'Smazání rezervace ' . $reservationId, Log::LOG_CUSTOMER);

				return $reservation;
			} else {
				Log::insert(CustomerController::isLoggedCustomer()->getCustomerId(), 'Smazání rezervace ' . $reservationId, Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $reservation);
				return $reservation;
			}
		}

		return false;
	}
}