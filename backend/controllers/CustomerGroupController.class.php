<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 08.12.2017
 * Time: 18:32
 */

namespace backend\controllers;


use backend\models\CustomerGroup;
use backend\models\Log;
use backend\models\User;

class CustomerGroupController
{
    /**
     * @return array|CustomerGroup|bool|string
     */
    public static function getCustomerGroup() {
        if (isset($_POST['customerGroupId']) && is_a(UserController::isLoggedUser(), User::class)) {
            $customerGroupId = $_POST['customerGroupId'];

            $customerGroup = new CustomerGroup($customerGroupId);
            $customerGroup = $customerGroup->load();

            if(is_a($customerGroup, CustomerGroup::class))
                return $customerGroup->_toArray();
            else
                return $customerGroup;
        }

        return false;
    }

    /**
     * @return CustomerGroup|bool|string
     */
    public static function addCustomerGroup() {
        if (isset($_POST['name']) && isset($_POST['normalPayments']) && isset($_POST['vipPayments']) && isset($_POST['discount']) && isset($_POST['maxReservationHours']) && is_a(UserController::isLoggedUser(), User::class)) {
            $name = $_POST['name'];
            $normalPayments = $_POST['normalPayments'];
            $vipPayments = $_POST['vipPayments'];
            $discount = $_POST['discount'];
            $maxReservationHours = $_POST['maxReservationHours'];

            $customerGroup = new CustomerGroup(0, $name, $normalPayments, $vipPayments, $discount, $maxReservationHours);
            $customerGroup = $customerGroup->create();

            Log::checkObjectAndInsert(UserController::getUserId(), $customerGroup, CustomerGroup::class, 'Vytvoření zákaznické skupiny', Log::LOG_USER);

            return $customerGroup;
        }

        return false;
    }

    /**
     * @return CustomerGroup|bool|string
     */
    public static function editCustomerGroup() {
        if (isset($_POST['customerGroupId']) && isset($_POST['name']) && isset($_POST['normalPayments']) && isset($_POST['vipPayments']) && isset($_POST['discount']) && isset($_POST['maxReservationHours']) && is_a(UserController::isLoggedUser(), User::class)) {
            $customerGroupId = $_POST['customerGroupId'];
            $name = $_POST['name'];
            $normalPayments = $_POST['normalPayments'];
            $vipPayments = $_POST['vipPayments'];
            $discount = $_POST['discount'];
            $maxReservationHours = $_POST['maxReservationHours'];

            $customerGroup = new CustomerGroup($customerGroupId, $name, $normalPayments, $vipPayments, $discount, $maxReservationHours);
            $customerGroup = $customerGroup->update();

            Log::checkObjectAndInsert(UserController::getUserId(), $customerGroup, CustomerGroup::class, 'Upravení zákaznické skupiny', Log::LOG_USER);

            return $customerGroup;
        }

        return false;
    }

    /**
     * @return CustomerGroup|bool|string
     */
    public static function deleteCustomerGroup() {
        if (isset($_POST['customerGroupId']) && is_a(UserController::isLoggedUser(), User::class)) {
            $customerGroupId = $_POST['customerGroupId'];

            $customerGroup = new CustomerGroup($customerGroupId);
            $customerGroup = $customerGroup->delete();

            Log::checkBoolAndInsert($customerGroup, true, UserController::getUserId(), "Smazání zákaznické skupiny", Log::LOG_USER);

            return $customerGroup;
        }

        return false;
    }
}