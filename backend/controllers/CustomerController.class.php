<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 31.07.2017
 * Time: 0:24
 */

namespace backend\controllers;

use backend\models\Address;
use backend\models\Customer;
use backend\models\CustomerGroup;
use backend\models\Log;
use backend\models\Password;
use backend\models\User;
use backend\models\Validate;

class CustomerController
{
    const SCENARIO_LOGIN = 1;

    /**
     * Check if customer session is valid.
     * If true - return Customer
     * Else return false
     * @return Customer|bool|string
     */
    public static function isLoggedCustomer()
    {
        if (isset($_SESSION['customer'])) {
            $customerId = $_SESSION['customer'];
            if ((new Validate())->validateNumeric(['customerId' => $customerId])) {
                $customer = (new Customer($customerId))->load();
                if (is_a($customer, Customer::class)) {
                    return $customer;
                }
            }
        }

        return false;
    }

    /**
     * @return Customer|bool|mixed|string
     */
    public static function loginCustomer()
    {
        if (isset($_POST['email']) && isset($_POST['password'])) {
            $email = $_POST['email'];
            $plainPassword = $_POST['password'];

            $customer = new Customer();
            $customer->setEmail($email);
            $customer->setPlainPassword($plainPassword);

            $customer = $customer->load();
            if (!is_a($customer, Customer::class)) {
                Log::insert(0, 'Přihlášení zákazníka', Log::LOG_CUSTOMER, Log::LOG_STATE_WARNING, 'Email ' . $email . '. Chyba: ' . $customer);
                return $customer;
            }

            $customerId = $customer->getCustomerId();
            $customer = $customer->login();
            Log::checkBoolAndInsert($customer, true, $customerId, 'Přihlášení zákazníka', Log::LOG_CUSTOMER, 'Email ' . $email);

            return $customer;
        }
        return false;
    }

    /**
     * @return bool
     */
    public static function logoutCustomer()
    {
        if (isset($_SESSION['customer'])) {
            unset($_SESSION['customer']);
        }

        return true;
    }

    /**
     * @return Customer|bool|string
     */
    public static function createCustomer()
    {
        if (isset($_POST['register-email']) && isset($_POST['register-password']) && isset($_POST['register-password-confirm'])) {
            $email = $_POST['register-email'];
            $password = $_POST['register-password'];
            $passwordConfirm = $_POST['register-password-confirm'];

            $customer = new Customer();
            $customer->setEmail($email);
            $customer->setPlainPassword($password);
            $customer->setPlainPasswordConfirm($passwordConfirm);

            $customer = $customer->create();
            Log::checkObjectAndInsert(0, $customer, Customer::class, 'Vytvoření zákazníka', Log::LOG_CUSTOMER, 'Email ' . $email);

            return $customer;
        }

        return false;
    }

    /**
     * @param bool $asArray
     *
     * @param null $customerId
     * @param bool $orderEdit
     * @return array|CustomerGroup|string
     */
	public static function getCustomerGroup($asArray = false, $customerId = null, $orderEdit = false) {
	    if(!$orderEdit) {
            $customer = CustomerController::isLoggedCustomer();
            if (is_a($customer, Customer::class)) {
                $customerGroup = new CustomerGroup($customer->getCustomerGroupId());
                $customerGroup = $customerGroup->load();

                if (!is_a($customerGroup, CustomerGroup::class) || !$asArray)
                    return $customerGroup;

                return $customerGroup->_toArray();
            }

            return [];
        } else {
	        $customer = new Customer();
	        $customer->setCustomerId($customerId);
            $customer = $customer->load();
            if (is_a($customer, Customer::class)) {
                $customerGroup = new CustomerGroup($customer->getCustomerGroupId());
                $customerGroup = $customerGroup->load();

                if (!is_a($customerGroup, CustomerGroup::class) || !$asArray)
                    return $customerGroup;

                return $customerGroup->_toArray();
            }

            return [];
        }
	}

    /**
     * @return Customer|string
     */
    public static function setCustomerGroup() {
        if (isset($_POST['customer-group']) && isset($_POST['customer-id']) && is_a(UserController::isLoggedUser(), User::class)) {
            $customerId = $_POST['customer-id'];
            $customerGroup = $_POST['customer-group'];

            $customer = new Customer($customerId);
            $customer = $customer->load();
            if(is_a($customer, Customer::class)) {
                $customer->setCustomerGroupId($customerGroup);
                $customer = $customer->update();

                Log::checkObjectAndInsert(UserController::getUserId(), $customer, Customer::class, 'Aktualizace zákazníka - nastavení uživatelské skupiny', Log::LOG_USER);

                return $customer;
            } else {
                Log::insert(UserController::getUserId(), 'Aktualizace zákazníka - nastavení uživatelské skupiny', Log::LOG_USER, Log::LOG_STATE_ERROR, $customer);
                return $customer;
            }
        }
    }


    /**
     * @return Customer|bool|string
     */
    public static function sendForgottenPassword() {
        if (isset($_POST['forgotten-password-email']) && ((new Validate())->validateEmail(['email' => $_POST['forgotten-password-email']]))) {
            $email = $_POST['forgotten-password-email'];

            $customer = new Customer();
            $customer->setEmail($email);
            $customer = $customer->load();
            if(is_a($customer, Customer::class)){
                $customerId = $customer->getCustomerId();
                $customer = $customer->sendForgottenPasswordEmail();
                Log::checkBoolAndInsert($customer, true, $customerId, 'Požadavek o zapomenuté heslo', Log::LOG_CUSTOMER, 'Email ' . $email);

                return $customer;
            } else {
                Log::insert(0, 'Požadavek o zapomenuté heslo', Log::LOG_CUSTOMER, 'Email ' . $email . ' Chyba: ' . $customer);
                return $customer;
            }
        }

        return false;
    }

    /**
     * @return Customer|bool|string
     */
    public static function changeForgottenPassword() {
        if (isset($_POST['email']) && isset($_POST['hash']) && isset($_POST['new-password']) && isset($_POST['new-password-confirm']) && ((new Validate())->validateEmail(['email' => $_POST['email']]))) {
            $email = $_POST['email'];
            $hash = $_POST['hash'];
            $newPassword = $_POST['new-password'];
            $newPasswordConfirm = $_POST['new-password-confirm'];

            $customer = new Customer();
            $customer->setEmail($email);
            $customer = $customer->load();
            if(is_a($customer, Customer::class)) {
                $customerId = $customer->getCustomerId();

                if (Password::isPasswordHashEqual($customer->getForgottenPasswordHash(), $hash) && Password::isPasswordHashValid($customer->getForgottenPasswordHash())) {
                    $customer->setPlainPassword($newPassword);
                    $customer->setPlainPasswordConfirm($newPasswordConfirm);

                    $customer = $customer->changePassword();
                    if(is_a($customer, Customer::class)){
                        // change hash - make the first one non valid
                        $customer->setForgottenPasswordHash(Password::generatePasswordHash($customer->getEmail()));

                        $customer = $customer->update();
                        if(is_a($customer, Customer::class)) {
                            Log::insert($customerId, 'Změna zapomenutého hesla', Log::LOG_CUSTOMER, Log::LOG_STATE_SUCCESS);
                            return true;
                        } else {
                            Log::insert($customerId, 'Změna zapomenutého hesla', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, 'Email ' . $email . ' Chyba: ' . $customer);
                            return $customer;
                        }
                    } else {
                        Log::insert($customerId, 'Změna zapomenutého hesla', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, 'Email ' . $email . ' Chyba: ' . $customer);
                        return $customer;
                    }
                } else {
                    Log::insert($customerId, 'Změna zapomenutého hesla', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, 'Email ' . $email . ' Chyba: Hash je nesprávný nebo již expiroval.');
                    return '<p><strong>Hash je nesprávný nebo již expiroval.</strong><br>Zkontrolujte zda je odkaz zkopírován správně. Případně požádejte o nový odkaz.</p>';
                }
            } else {
                Log::insert(0, 'Změna zapomenutého hesla', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, 'Email ' . $email . ' Chyba: ' . $customer);
                return $customer;
            }
        }

        return false;
    }

    /**
     * @return Customer|bool|string
     */
    public static function changePassword() {
        $customer = self::isLoggedCustomer();
        if (isset($_POST['current-password']) && isset($_POST['new-password']) && isset($_POST['new-password-confirm']) && is_a($customer, Customer::class)) {
            $currentPassword = $_POST['current-password'];
            $newPassword = $_POST['new-password'];
            $newPasswordConfirm = $_POST['new-password-confirm'];

            if(Password::verifyPassword($currentPassword, $customer->getHashedPassword())){
                $customer->setPlainPassword($newPassword);
                $customer->setPlainPasswordConfirm($newPasswordConfirm);
                $customer = $customer->changePassword();
                Log::checkObjectAndInsert($customer->getCustomerId(), $customer, Customer::class, 'Změna hesla', Log::LOG_CUSTOMER);

                return $customer;
            } else {
                Log::insert($customer->getCustomerId(), 'Změna hesla', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, 'Nesprávné heslo');
                return 'Nesprávné heslo.';
            }
        }

        return false;
    }

    /**
     * @return Customer|bool|string
     */
    public static function saveBasicData() {
        if(is_a(UserController::isLoggedUser(), User::class) && isset($_POST['customer-id'])) {
            $customer = new Customer($_POST['customer-id']);
            $customer = $customer->load();
            if(!is_a($customer, Customer::class))
                return $customer;
        } else {
            $customer = self::isLoggedCustomer();
        }

        if (isset($_POST['name']) && isset($_POST['prefix']) && isset($_POST['phone']) && isset($_POST['email']) && (is_a($customer, Customer::class) || is_a(UserController::isLoggedUser(), User::class))) {
            $name = $_POST['name'];
            $prefix = $_POST['prefix'];
            $phone = $_POST['phone'];
            $email = $_POST['email'];
            $currentEmail = $customer->getEmail();

            $customer->setName($name);
            $customer->setPhonePrefix($prefix);
            $customer->setPhone($phone);
            $customer->setEmail($email);

            // set email not confirmed if different
            if($currentEmail != $email) {
                $customer->setIsEmailConfirmed(false);
                // regenerate hash and send email
                $customer->sendEmailConfirmEmail();
            }

            $customerId = $customer->getCustomerId();
            $customer = $customer->update();
            Log::checkObjectAndInsert($customerId, $customer, Customer::class, 'Uložení základních dat', Log::LOG_CUSTOMER);

            return $customer;
        }

        return false;
    }

    /**
     * @return Customer|bool|string
     */
    public static function saveOtherData() {
        if(is_a(UserController::isLoggedUser(), User::class) && isset($_POST['customer-id'])) {
            $customer = new Customer($_POST['customer-id']);
            $customer = $customer->load();
            if(!is_a($customer, Customer::class))
                return $customer;
        } else {
            $customer = self::isLoggedCustomer();
        }

        if (isset($_POST['bank-account-number']) && isset($_POST['bank-code']) && (is_a($customer, Customer::class) || is_a(UserController::isLoggedUser(), User::class))) {
            $bankAccountNumber = $_POST['bank-account-number'];
            $bankCode = $_POST['bank-code'];

            $customer->setBankAccountNumber($bankAccountNumber);
            $customer->setBankCode($bankCode);

            $customerId = $customer->getCustomerId();
            $customer = $customer->update();
            Log::checkObjectAndInsert($customerId, $customer, Customer::class, 'Uložení dalších dat', Log::LOG_CUSTOMER);

            return $customer;
        }

        return false;
    }

    /**
     * @return Customer|bool|string
     */
    public static function saveAccountData() {
        if(is_a(UserController::isLoggedUser(), User::class) && isset($_POST['customer-id'])) {
            $customer = new Customer($_POST['customer-id']);
            $customer = $customer->load();
            if(!is_a($customer, Customer::class))
                return $customer;
        } else {
            $customer = self::isLoggedCustomer();
        }

        if (isset($_POST['birth-number']) && isset($_POST['ico']) && isset($_POST['dic']) && (is_a($customer, Customer::class) || is_a(UserController::isLoggedUser(), User::class))) {
            if(isset($_POST['company']))
                $company = 1;
            else
                $company = 0;
            $birthNumber = $_POST['birth-number'];
            $ico = $_POST['ico'];
            $dic = $_POST['dic'];

            $customer->setIsCompany($company);
            $customer->setBirthNumber($birthNumber);
            $customer->setIco($ico);
            $customer->setDic($dic);

            $customerId = $customer->getCustomerId();
            $customer = $customer->update();
            Log::checkObjectAndInsert($customerId, $customer, Customer::class, 'Uložení ověřovacích dat', Log::LOG_CUSTOMER);

            return $customer;
        }

        return false;
    }

    /**
     * @return Address|bool|string
     */
    public static function saveInvoiceData() {
        if(is_a(UserController::isLoggedUser(), User::class) && isset($_POST['customer-id'])) {
            $customer = new Customer($_POST['customer-id']);
            $customer = $customer->load();
            if(!is_a($customer, Customer::class))
                return $customer;
        } else {
            $customer = self::isLoggedCustomer();
        }

        if (isset($_POST['invoice-name']) && isset($_POST['invoice-street']) && isset($_POST['invoice-city']) && isset($_POST['invoice-zip']) && (is_a($customer, Customer::class) || is_a(UserController::isLoggedUser(), User::class))) {
            $name = $_POST['invoice-name'];
            $street = $_POST['invoice-street'];
            $city = $_POST['invoice-city'];
            $zip = $_POST['invoice-zip'];
            $type = Address::ADDRESS_INVOICE;

            $address = new Address();
            $address->setCustomerId($customer->getCustomerId());
            $address->setAddressType($type);
            $address = $address->load(true);
            if(is_a($address, Address::class)) {
                // found - only update
                $address->setAddress($name, $street, $city, $zip, Address::COUNTRY_CZ, $type);
                $address = $address->update();

                Log::checkObjectAndInsert($customer->getCustomerId(), $address, Address::class, 'Uložení fakturačních dat', Log::LOG_CUSTOMER);

                return $address;
            } else {
                // not found - create new
                $address = new Address();
                $address->setCustomerId($customer->getCustomerId());
                $address->setAddress($name, $street, $city, $zip, Address::COUNTRY_CZ, $type);
                $address = $address->create();

                Log::checkObjectAndInsert($customer->getCustomerId(), $address, Address::class, 'Uložení fakturačních dat', Log::LOG_CUSTOMER);

                return $address;
            }
        }

        return false;
    }

    /**
     * @return Address|bool|string
     */
    public static function saveDeliveryData() {
        if(is_a(UserController::isLoggedUser(), User::class) && isset($_POST['customer-id'])) {
            $customer = new Customer($_POST['customer-id']);
            $customer = $customer->load();
            if(!is_a($customer, Customer::class))
                return $customer;
        } else {
            $customer = self::isLoggedCustomer();
        }

        if (isset($_POST['delivery-name']) && isset($_POST['delivery-street']) && isset($_POST['delivery-city']) && isset($_POST['delivery-zip']) && (is_a($customer, Customer::class) || is_a(UserController::isLoggedUser(), User::class))) {
            if(isset($_POST['delivery-address']))
                $isTheSameAsInvoice = 1;
            else
                $isTheSameAsInvoice = 0;

            $name = $_POST['delivery-name'];
            $street = $_POST['delivery-street'];
            $city = $_POST['delivery-city'];
            $zip = $_POST['delivery-zip'];
            $type = Address::ADDRESS_DELIVERY;

            $address = new Address();
            $address->setCustomerId($customer->getCustomerId());
            $address->setAddressType($type);
            $address = $address->load(true);
            if(is_a($address, Address::class)) {
                // found - only update or delete
                if($isTheSameAsInvoice == 1) {
                    return $address->delete();
                } else {
                    $address->setAddress($name, $street, $city, $zip, Address::COUNTRY_CZ, $type);
                    $address = $address->update();

                    Log::checkObjectAndInsert($customer->getCustomerId(), $address, Address::class, 'Uložení doručovacích dat', Log::LOG_CUSTOMER);

                    return $address;
                }
            } else {
                // not found - create new
                if($isTheSameAsInvoice == 0) {
                    $address = new Address();
                    $address->setCustomerId($customer->getCustomerId());
                    $address->setAddress($name, $street, $city, $zip, Address::COUNTRY_CZ, $type);
                    $address = $address->create();

                    Log::checkObjectAndInsert($customer->getCustomerId(), $address, Address::class, 'Uložení doručovacích dat', Log::LOG_CUSTOMER);

                    return $address;
                }
            }
        }

        return false;
    }
}