<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 10.03.2017
 * Time: 16:03
 */

namespace backend\controllers;

use backend\models\Log;
use backend\models\Password;
use backend\view\UserGrid;
use backend\models\User;
use backend\models\Validate;

class UserController
{
    const SCENARIO_LOGIN = 1;

    /**
     * Check if user session is valid.
     * If true - return User
     * Else return false
     * @return User|bool|string
     */
    public static function isLoggedUser()
    {
        if (isset($_SESSION['user'])) {
            $userId = $_SESSION['user'];
            if ((new Validate())->validateNumeric(['userId' => $userId])) {
	            //$user = (new User($userId))->load();
	            $user = new User();
	            if (is_a($user, User::class)) {
                    return $user;
                }
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public static function loginUser()
    {
        if (isset($_POST['email']) && isset($_POST['password'])) {
            $email = $_POST['email'];
            $plainPassword = $_POST['password'];

            $user = new User();
            $user->setEmail($email);
            $user->setPlainPassword($plainPassword);

            $userLoadResult = $user->load();
            if (!is_a($userLoadResult, User::class)) {
                Log::insert(0, 'Přihlášení uživatele', Log::LOG_USER, Log::LOG_STATE_ERROR, 'Email ' . $email . ' Chyba: ' . $userLoadResult);
                return $userLoadResult;
            }

            $userId = $user->getUserId();
            $user = $user->login();

            Log::checkBoolAndInsert($user, true, $userId, 'Přihlášení uživatele', Log::LOG_USER);

            return $user;
        }
        return false;
    }

    /**
     * @return bool
     */
    public static function logoutUser()
    {
        if (isset($_SESSION['user'])) {
            unset($_SESSION['user']);
        }

        return true;
    }

    /**
     * @return User|bool|string
     */
    public static function sendForgottenPassword() {
        if (isset($_POST['forgotten-password-email']) && ((new Validate())->validateEmail(['email' => $_POST['forgotten-password-email']]))) {
            $email = $_POST['forgotten-password-email'];

            $user = new User();
            $user->setEmail($email);
            $user = $user->load();
            if(is_a($user, User::class)){
                $userId = $user->getUserId();
                $user = $user->sendForgottenPasswordEmail();
                Log::checkBoolAndInsert($user, true, $userId, 'Požadavek o zapomenuté heslo', Log::LOG_USER, 'Email ' . $email);

                return $user;
            } else {
                Log::insert(0, 'Požadavek o zapomenuté heslo', Log::LOG_USER, 'Email ' . $email . ' Chyba: ' . $user);
                return $user;
            }
        }

        return false;
    }

    /**
     * @return User|bool|string
     */
    public static function changeForgottenPassword() {
        if (isset($_POST['email']) && isset($_POST['hash']) && isset($_POST['new-password']) && isset($_POST['new-password-confirm']) && ((new Validate())->validateEmail(['email' => $_POST['email']]))) {
            $email = $_POST['email'];
            $hash = $_POST['hash'];
            $newPassword = $_POST['new-password'];
            $newPasswordConfirm = $_POST['new-password-confirm'];

            $user = new User();
            $user->setEmail($email);
            $user = $user->load();
            if(is_a($user, User::class)) {
                $userId = $user->getUserId();

                if (Password::isPasswordHashEqual($user->getForgottenPasswordHash(), $hash) && Password::isPasswordHashValid($user->getForgottenPasswordHash())) {
                    $user->setPlainPassword($newPassword);
                    $user->setPlainPasswordConfirm($newPasswordConfirm);

                    $user = $user->changePassword();
                    if(is_a($user, User::class)){
                        // change hash - make the first one non valid
                        $user->setForgottenPasswordHash(Password::generatePasswordHash($user->getEmail()));

                        $user = $user->update();
                        if(is_a($user, User::class)) {
                            Log::insert($userId, 'Změna zapomenutého hesla', Log::LOG_USER, Log::LOG_STATE_SUCCESS);
                            return true;
                        } else {
                            Log::insert($userId, 'Změna zapomenutého hesla', Log::LOG_USER, Log::LOG_STATE_ERROR, 'Email ' . $email . ' Chyba: ' . $user);
                            return $user;
                        }
                    } else {
                        Log::insert($userId, 'Změna zapomenutého hesla', Log::LOG_USER, Log::LOG_STATE_ERROR, 'Email ' . $email . ' Chyba: ' . $user);
                        return $user;
                    }
                } else {
                    Log::insert($userId, 'Změna zapomenutého hesla', Log::LOG_USER, Log::LOG_STATE_ERROR, 'Email ' . $email . ' Chyba: Hash je nesprávný nebo již expiroval.');
                    return '<p><strong>Hash je nesprávný nebo již expiroval.</strong><br>Zkontrolujte zda je odkaz zkopírován správně. Případně požádejte o nový odkaz.</p>';
                }
            } else {
                Log::insert(0, 'Změna zapomenutého hesla', Log::LOG_USER, Log::LOG_STATE_ERROR, 'Email ' . $email . ' Chyba: ' . $user);
                return $user;
            }
        }

        return false;
    }

    /**
     * For log
     * @return int
     */
    public static function getUserId(){
        if(isset($_SESSION['user']) && is_a(self::isLoggedUser(), User::class))
            return $_SESSION['user'];

        return 0;
    }

    /**
     * @return User|bool|string
     */
    public static function getUser()
    {
        if(isset($_POST['userId']) && is_a(self::isLoggedUser(), User::class)) {
            $userId = $_POST['userId'];
            $user = new User($userId);
            $user->load();
            if (!is_a($user, User::class)) {
                return $user;
            }

            return UserGrid::generateUserGridCell($user);
        }

        return false;
    }

    /**
     * @return User|string
     */
    public static function createUser()
    {
        //if values are set and admin user is logged
        if (isset($_POST['email']) && isset($_POST['password']) && isset($_POST['password-again']) && is_a(self::isLoggedUser(), User::class)) {
            $name = "";
            if (isset($_POST['name']))
                $name = $_POST['name'];

            $email = $_POST['email'];
            $password = $_POST['password'];
            $passwordConfirm = $_POST['password-again'];

            $user = new User();
            $user->setName($name);
            $user->setEmail($email);
            $user->setPlainPassword($password);
            $user->setPlainPasswordConfirm($passwordConfirm);

            $user = $user->create();

            Log::checkObjectAndInsert(0, $user, User::class, 'Vytvoření uživatele', Log::LOG_USER, 'Email ' . $email);

            return $user;
        }

        return false;
    }

    /**
     * @return User|bool|string
     */
    public static function updateUser()
    {
        //if values are set and admin user is logged
        if (isset($_POST['email']) && isset($_POST['userid']) && is_a(self::isLoggedUser(), User::class)) {
            $name = "";
            if (isset($_POST['name']))
                $name = $_POST['name'];

            $email = $_POST['email'];
            $userId = $_POST['userid'];

            $user = new User($userId);
            $user = $user->load();
            if (!is_a($user, User::class)) {
                return $user;
            }

            $user->setName($name);
            $user->setEmail($email);

            $userId = $user->getUserId();
            $user = $user->update();

            Log::checkObjectAndInsert($userId, $user, User::class, 'Aktualizovat uživatele', Log::LOG_USER, 'Email ' . $email);

            return $user;
        }

        return false;
    }

    /**
     * @return User|bool|string
     */
    public static function deleteUser()
    {
        //if userId is sent and admin user is logged
        if (isset($_POST['userId']) && is_a(self::isLoggedUser(), User::class)) {

            $userId = $_POST['userId'];

            $user = new User($userId);
            $user = $user->load();
            if (!is_a($user, User::class)) {
                return $user;
            }

            $user = $user->delete();

            Log::checkBoolAndInsert($user, true, $userId, 'Smazat uživatele', Log::LOG_USER);

            return $user;
        }

        return false;
    }
}