<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 20.06.2017
 * Time: 22:24
 */

namespace backend\controllers;


use backend\models\Log;
use backend\models\ProductCategory;
use backend\models\User;

class ProductCategoryController
{
    /**
     * @return ProductCategory|bool|string
     */
    public static function createProductCategory()
    {
        //if values are set and admin user is logged
        if (isset($_POST['productId']) && isset($_POST['categoryId']) && is_a(UserController::isLoggedUser(), User::class)) {
            $productId = $_POST['productId'];
            $categoryId = $_POST['categoryId'];

            $productCategory = new ProductCategory(0, $productId, $categoryId);

            $productCategory = $productCategory->create();

            Log::checkObjectAndInsert(UserController::getUserId(), $productCategory, ProductCategory::class, 'Vytvoření přiřazení kategorie k produktu', Log::LOG_USER);

            return $productCategory;
        }

        return false;
    }

    /**
     * @return ProductCategory|bool|mixed|string
     */
    public static function updateProductCategoryPositions()
    {
        if (isset($_POST['productIds']) && isset($_POST['productCategoryPositions']) && isset($_POST['categoryId']) && is_a(UserController::isLoggedUser(), User::class)) {

            $productIds = $_POST['productIds'];
            $productCategoryPositions = $_POST['productCategoryPositions'];
            $categoryId = $_POST['categoryId'];

            foreach ($productIds as $index => $productId){
                $productCategory = new ProductCategory(0, $productId, $categoryId);
                $productCategory = $productCategory->load();
                if(is_a($productCategory, ProductCategory::class)) {
                    $productCategory->setPosition($productCategoryPositions[$index]);

                    $productCategory->update();
                } else {
                    Log::insert(UserController::getUserId(), 'Aktualizace umístění produktu v kategorii', Log::LOG_USER, Log::LOG_STATE_ERROR, $productCategory);
                    return $productCategory;
                }
            }

            Log::insert(UserController::getUserId(), 'Aktualizace umístění produktu v kategorii', Log::LOG_USER, Log::LOG_STATE_SUCCESS);
            return true;
        }

        return false;
    }

    /**
     * @return ProductCategory|bool|string
     */
    public static function deleteProductCategory()
    {
        if (isset($_POST['productCategoryId']) && is_a(UserController::isLoggedUser(), User::class)) {
            $productCategoryId = $_POST['productCategoryId'];

            $productCategory = new ProductCategory($productCategoryId);
            $productCategory = $productCategory->load();
            if (!is_a($productCategory, ProductCategory::class)) {
                return $productCategory;
            }

            $productCategory = $productCategory->delete();

            Log::checkBoolAndInsert($productCategory, true, UserController::getUserId(), 'Smazání přiřazení produktu ke kategorii', Log::LOG_USER);

            return $productCategory;
        }

        return false;
    }

    /**
     * @return array|bool
     */
    public static function getProductCategory()
    {
        if (isset($_POST['productCategoryId']) && is_a(UserController::isLoggedUser(), User::class)) {
            $productCategoryId = $_POST['productCategoryId'];

            $productCategory = new ProductCategory($productCategoryId);
            $productCategory = $productCategory->load();

            return $productCategory->_toArray();
        }

        return false;
    }

    /**
     * @param null $productId
     * @param bool $asArray
     * @param bool $toJson
     * @return array|bool|string
     */
    public static function getProductCategories($productId = null, $asArray = true, $toJson = true) {
        if (isset($_POST['productId']) && is_a(UserController::isLoggedUser(), User::class)) {
            if(is_null($productId))
                $productId = $_POST['productId'];

            if($asArray)
                $productCategories = ProductCategory::getProductCategoriesAsArray($productId);
            else
                $productCategories = ProductCategory::getProductCategories($productId);

            if($toJson)
                return json_encode($productCategories);
            else
                return $productCategories;
        }

        return false;
    }
}