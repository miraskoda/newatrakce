<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 07.08.2017
 * Time: 21:40
 */

namespace backend\controllers;

use backend\models\Log;
use backend\models\User;
use backend\models\Video;

class VideoController
{
    /**
     * @return array|bool
     */
    public static function getVideo()
    {
        if (isset($_POST['productId']) && is_a(UserController::isLoggedUser(), User::class)) {
            $productId = $_POST['productId'];

            $video = new Video(0, $productId);
            $video = $video->load();
            if(is_a($video, Video::class))
                return $video->_toArray();
        }

        return false;
    }

    /**
     * @return Video|bool|string
     */
    public static function saveVideo()
    {
        //if values are set and admin user is logged
        if (isset($_POST['productId']) && isset($_POST['hash']) && is_a(UserController::isLoggedUser(), User::class)) {
            $productId = $_POST['productId'];
            $hash = $_POST['hash'];

            $video = new Video(0, $productId);
            $video = $video->load();
            if(is_a($video, Video::class)){
                $video->delete();
            }

            if($hash != '' && $hash != null) {
                $newVideo = new Video(0, $productId, $hash);
                $newVideo = $newVideo->create();

                Log::checkObjectAndInsert(UserController::getUserId(), $newVideo, Video::class, 'Uložit video', Log::LOG_USER);

                return $newVideo;
            }
        }

        return false;
    }
}