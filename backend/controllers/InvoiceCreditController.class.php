<?php
/**
 * Created by PhpStorm.
 * User: miroslavskoda
 * Date: 16/12/2018
 * Time: 15:09
 */

namespace backend\controllers;

use backend\database\MyDB;

class InvoiceCreditController
{

    // atributes for generating document

    const FAKTURA = 3;
    const DOBROPIS = 4;
    const DOCUMENT = 1;
    const CONFIRM = 2;



    public static function ManageConfirmation($id){

        if(!self::getInvoiceFromOrderIdBoolConf($id)){
            self::saveNewEntityConf($id);
        }


    }

    public static function ManageInvoice($id){

    if(!self::getInvoiceFromOrderIdBool($id)){
        self::saveNewEntity($id);
    }


}

    public static function ManageDobropis($id){

        if(!self::getInvoiceFromOrderIdBoolDobropis($id)){
            self::saveNewEntityCredit($id);        }


    }


    public static function ManageDeposit($id){

        if(!self::getInvoiceFromOrderIdBoolDeposit($id)){
            self::saveNewEntityDeposit($id);        }



    }

    private static function getIntValueFrom($val){
        //get integer value from any string data
        return intval($val);

    }

    private static function getParsedYear(){

        //year by first two chars

        return self::parseEntity(DATE("Y"),2,3);

    }



    private static function getLastEntityCredit(){

        // returns last elements of id from db

        $lastCountID = MyDB::getConnection()->queryAll("SELECT id FROM order_credit ORDER BY id DESC LIMIT 1")[0]["id"];


        return $lastCountID;

    }

    private static function getLastEntityDeposit(){

        // returns last elements of id from db

        $lastCountID = MyDB::getConnection()->queryAll("SELECT id FROM order_document ORDER BY id DESC LIMIT 1")[0]["id"];


        return $lastCountID;

    }

    private static function getLastEntity(){

        // returns last elements of id from db

        $lastCountID = MyDB::getConnection()->queryAll("SELECT id FROM order_invoice ORDER BY id DESC LIMIT 1")[0]["id"];


        return $lastCountID;

    }

    private static function getLastEntityConf(){

        // returns last elements of id from db

        $lastCountID = MyDB::getConnection()->queryAll("SELECT id FROM order_conf ORDER BY id DESC LIMIT 1")[0]["id"];


        return $lastCountID;

    }


    private static function parseEntity($str,$min,$max){

        //parser
        return substr($str, $min, $max);

    }

    private static function parseEntityWithout($str,$min){

        //parser without min value
        return substr($str, $min);

    }


    private static function manageCounter($last,$type){
        //adding num function to database
        $afterParse = self::parseEntityWithout($last,3);
        $lastDocumentYear = self::parseEntity($last,1,2);
        $actualYear = self::getParsedYear();

        $intParsed = self::getIntValueFrom($afterParse);
        if($actualYear>$lastDocumentYear){
            $intParsed = 0;
        }
        $intParsedWith = self::getFormattedFive($intParsed+1);
        return $type . $actualYear . $intParsedWith;
    }


    private static function saveNewEntityCredit($order_id){

        $value = self::manageCounter(self::getLastEntityCredit(),self::DOBROPIS);
        MyDB::getConnection()->query("INSERT INTO order_credit (id, order_id,date) VALUES (?,?,now())", [$value,$order_id]);

    }


    private static function saveNewEntityDeposit($order_id){

        $value = self::manageCounter(self::getLastEntityDeposit(),self::DOCUMENT);
        MyDB::getConnection()->query("INSERT INTO order_document (id, order_id,date) VALUES (?,?,now())", [$value,$order_id]);

    }

    private static function saveNewEntity($order_id){

        $value = self::manageCounter(self::getLastEntity(),self::FAKTURA);
        MyDB::getConnection()->query("INSERT INTO order_invoice (id, order_id,date) VALUES (?,?,now())", [$value,$order_id]);

    }
    public static function getFormatted($ret) {
        return str_pad($ret, 6, "0", STR_PAD_LEFT);
    }

    private static function getFormattedFive($ret) {
        return str_pad($ret, 5, "0", STR_PAD_LEFT);
    }


    private static function saveNewEntityConf($order_id){

        $value = self::manageCounter(self::getLastEntityConf(),self::CONFIRM);
        MyDB::getConnection()->query("INSERT INTO order_conf (id, order_id,date) VALUES (?,?,now())", [$value,$order_id]);

    }



    public static function getInvoiceFromOrderId($id){

        $db = MyDB::getConnection();
        $data = $db->queryAll("SELECT * FROM order_invoice WHERE order_id=?", [$id]);

        if (count($data) < 1)
            return [];


        //to proper counting method
        return $data[0]["id"];


    }
    public static function getInvoiceFromOrderIdBool($id){

        $db = MyDB::getConnection();
        $data = $db->queryAll("SELECT * FROM order_invoice WHERE order_id=?", [$id]);

        if (count($data) < 1)
            return false;


        //to proper counting method
        return true;


    }


    public static function getInvoiceFromOrderIdBoolConf($id){

        $db = MyDB::getConnection();
        $data = $db->queryAll("SELECT * FROM order_conf WHERE order_id=?", [$id]);

        if (count($data) < 1)
            return false;


        //to proper counting method
        return true;


    }

    public static function getInvoiceFromOrderIdBoolDobropis($id){

        $db = MyDB::getConnection();
        $data = $db->queryAll("SELECT * FROM order_credit WHERE order_id=?", [$id]);

        if (count($data) < 1)
            return false;


        //to proper counting method
        return true;


    }

    public static function getInvoiceFromOrderIdBoolDeposit($id){

        $db = MyDB::getConnection();
        $data = $db->queryAll("SELECT * FROM order_document WHERE order_id=?", [$id]);

        if (count($data) < 1)
            return false;


        //to proper counting method
        return true;


    }

    public static function getInvoiceFromOrderIdDobropis($id){

        $db = MyDB::getConnection();
        $data = $db->queryAll("SELECT * FROM order_credit WHERE order_id=?", [$id]);

        if (count($data) < 1)
            return [];

        //date from db created
        $date = date_create($data[0]["date"]);

        //to proper counting method
        return $data[0]["id"];


    }

    public static function getInvoiceFromOrderIdDeposit($id){

        $db = MyDB::getConnection();
        $data = $db->queryAll("SELECT * FROM order_document WHERE order_id=?", [$id]);

        if (count($data) < 1)
            return [];

        return $data[0]["id"];


    }


    public static function getInvoiceBooleanFromOrderId($id){

        $db = MyDB::getConnection();
        $data = $db->queryAll("SELECT * FROM order_invoice WHERE order_id=? AND isAdd = 0", [$id]);

        return count($data) < 1 ? false : true;

    }

    public static function getInvoiceBooleanFromOrderIdDobropis($id){

        $db = MyDB::getConnection();
        $data = $db->queryAll("SELECT * FROM order_credit WHERE order_id=? AND isAdd = 0", [$id]);


        return count($data) < 1 ? false : true;

    }


    public static function getInvoiceBooleanFromOrderIdDeposit($id){

        $db = MyDB::getConnection();
        $data = $db->queryAll("SELECT * FROM order_document WHERE order_id=?", [$id]);

        return count($data) < 1 ? false : true;

    }


    public static function createInvoice($orderId){

        $db = MyDB::getConnection();

        $insert = $db->query("INSERT INTO order_invoice (order_id) VALUES (?)", [
            $orderId            ]);

        if ($insert > 0) {

            return $db->queryAll("SELECT * FROM order_invoice WHERE order_id=?", [$orderId])["id"];

        } else {
            return "";
        }

    }



    public static function createInvoiceDobropis($orderId){

        $db = MyDB::getConnection();

        $insert = $db->query("INSERT INTO order_credit (order_id) VALUES (?)", [
            $orderId            ]);

        if ($insert > 0) {

            return $db->queryAll("SELECT * FROM order_credit WHERE order_id=?", [$orderId])["id"];

        } else {
            return "";
        }

    }

    public static function getAllInvoices(){

        $db = MyDB::getConnection();
        return $db->queryAll("SELECT * FROM order_invoice");

    }

    public static function getAllInvoicesDobropis(){

    $db = MyDB::getConnection();
    return $db->queryAll("SELECT * FROM order_credit");

}
    public static function getAllInvoicesDocuments(){

        $db = MyDB::getConnection();
        return $db->queryAll("SELECT * FROM order_document");

    }


    public static function getAllInvoicesFromId(){
        $db = MyDB::getConnection();
        return $db->queryAll("SELECT * FROM order_invoice WHERE isADD=1");
    }

    public static function getInvoiceFromId($id){
        $db = MyDB::getConnection();
        return $db->queryAll("SELECT * FROM order_invoice WHERE isADD=1 AND id=?",[$id]);

    }
    public static function getInvoiceFromIdInvoice($id){
        $db = MyDB::getConnection();
        return $db->queryAll("SELECT * FROM order_credit WHERE isADD=1 AND id=?",[$id]);

    }



    public static function getInvoiceFromIdVenue($id){
        $db = MyDB::getConnection();
        return $db->queryAll("SELECT * FROM order_invoice WHERE order_id=?",[$id]);

    }

    public static function getInvoiceFromIdVenueCredit($id){
        $db = MyDB::getConnection();
        return $db->queryAll("SELECT * FROM order_credit WHERE order_id=?",[$id]);

    }
    public static function getInvoiceFromIdVenueDocument($id){
        $db = MyDB::getConnection();
        return $db->queryAll("SELECT * FROM order_document WHERE order_id=?",[$id]);

    }


    public static function saveAditionalInvoice($id,$nazev,$mnozstvi,$jednotkova,$sleva,$bezdph,$sdph,$desc){

        $value = self::manageCounter(self::getLastEntity(),self::FAKTURA);
        $db = MyDB::getConnection();

        $insert = $db->query("INSERT INTO order_invoice (id, order_id,date,isAdd,descr,content,amount,jednotkova,sleva,woVAT,wVAT) VALUES (?,?,now(),1,?,?,?,?,?,?,?)", [$value,$id,$desc,$nazev,$mnozstvi,$jednotkova,$sleva,$bezdph,$sdph]);


        if ($insert > 0) {

            return self::getLastEntity();

        } else {
            return "";
        }

    }


    public static function saveAditionalCredit($id,$nazev,$mnozstvi,$jednotkova,$sleva,$bezdph,$sdph,$desc){

        $value = self::manageCounter(self::getLastEntityCredit(),self::DOBROPIS);
        $db = MyDB::getConnection();

        $insert = $db->query("INSERT INTO order_credit (id, order_id,date,isAdd,descr,content,amount,jednotkova,sleva,woVAT,wVAT) VALUES (?,?,now(),1,?,?,?,?,?,?,?)", [$value,$id,$desc,$nazev,$mnozstvi,$jednotkova,$sleva,$bezdph,$sdph]);


        if ($insert > 0) {
            return self::getLastEntityCredit();

        } else {
            return "";
        }

    }


    public static function getIDfromConfirmation($Id){

        return MyDB::getConnection()->queryAll("SELECT * FROM order_conf WHERE order_id=?",[$Id])[0]["id"];

    }

    public static function getOrderIDfromId($Id){

        return MyDB::getConnection()->queryAll("SELECT * FROM order_invoice WHERE id=?",[$Id])[0]["order_id"];

    }

    public static function getOrderIDfromIdInvoice($Id){

        return MyDB::getConnection()->queryAll("SELECT * FROM order_credit WHERE id=?",[$Id])[0]["order_id"];

    }




}