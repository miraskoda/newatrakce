<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 13.11.2018
 * Time: 17:22
 */

namespace backend\controllers;

use backend\models\Cart;
use backend\models\Category;
use backend\models\Customer;
use backend\models\Log;
use backend\models\ProductCategory;
use backend\models\Product;

class CartController
{
    private static $cookieName = 'temp-customer';

    /**
     * @return Cart|string
     */
    public static function addToCart() {
        if(isset($_POST['additionToProductId']))
            return self::saveAdditionalCartData();

        $customerId = self::getCustomerId();
        return self::saveCartData($customerId);
    }

    /**
     * @return Cart|string
     */
    public static function saveCartDataFromReservation() {
        if(isset($_POST['productId']) && isset($_POST['quantity'])) {
            $quantity = $_POST['quantity'];
            $productId = $_POST['productId'];

            $product = new Product();
            $product->setName($productId);
            $product = $product->load();

            $cartItem = new Cart();
            $customerId = self::getCustomerId();
            $cartItem->setCustomerId($customerId);
            $cartItem->setProductId($product->getProductId());
            $cartItem->setQuantity($quantity);

            $cartItem = $cartItem->create();
            Log::checkObjectAndInsert($customerId, $cartItem, Cart::class, 'Vložení produktu ' . $productId . ' do košíku', Log::LOG_CUSTOMER);

            return $cartItem;
        }
    }

	/**
	 * @param $customerId
	 * @return Cart|string
	 */
	private static function saveCartData($customerId) {
		if(isset($_POST['productId'])) {
			$additionCategory = CategoryController::getCategoryBySlug('doplnky');
			$productId = $_POST['productId'];

			if(is_a($additionCategory, Category::class)) {
				if(ProductCategory::isProductInCategory( $additionCategory->getCategoryId(), $productId ))
					return 'Doplňky není možné objednat samostatně.';
			}

			$quantity = 1;
			if(isset($_POST['quantity']))
				$quantity = $_POST['quantity'];

			$cartItem = new Cart();
			$cartItem->setCustomerId($customerId);
			$cartItem->setProductId($productId);
			$cartItem->setQuantity($quantity);

			$cartItem = $cartItem->create();
			Log::checkObjectAndInsert($customerId, $cartItem, Cart::class, 'Vložení produktu ' . $productId . ' do košíku', Log::LOG_CUSTOMER);

			return $cartItem;
		}
	}

	/**
     * @return Cart|bool|string
     */
    public static function removeFromCart() {
        $customerId = self::getCustomerId();
        if(isset($_POST['productId'])) {
            $productId = $_POST['productId'];

            $cartItem = new Cart();
            $cartItem->setCustomerId($customerId);
            $cartItem->setProductId($productId);
            $cartItem = $cartItem->load();

            if(is_a($cartItem, Cart::class)) {
                $cartItem = $cartItem->delete();
                Log::checkBoolAndInsert($cartItem, true, $customerId, 'Smazání produktu ' . $productId . ' z košíku', Log::LOG_CUSTOMER);

                return $cartItem;
            } else {
                Log::insert($customerId, 'Smazání produktu ' . $productId . ' z košíku', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $cartItem);
                return $cartItem;
            }
        }
    }

	/**
	 * @return Cart|string
	 */
	public static function saveAdditionalCartData() {
		if(isset($_POST['productId']) && isset($_POST['additionToProductId'])) {
			$customerId = self::getCustomerId();

			$additionCategory = CategoryController::getCategoryBySlug('doplnky');
			$productId = $_POST['productId'];
			$additionToProductId = $_POST['additionToProductId'];

			// first check if its addition
			if(is_a($additionCategory, Category::class)) {
				if(!ProductCategory::isProductInCategory( $additionCategory->getCategoryId(), $productId ))
					return 'Objednávaný produkt není doplněk.';
			}

			// second check if main product is in cart
            $isMainProductInCart = new Cart(0, $customerId, $additionToProductId, 0);
			$isMainProductInCart = $isMainProductInCart->load();
			if(is_a($isMainProductInCart, Cart::class)){
			    $isMainProductInCart = $isMainProductInCart->isDuplicate();
            } else {
			    $isMainProductInCart = false;
            }

            if(!$isMainProductInCart) {
			    return 'Hlavní produkt není v košíku. Doplňky je možné objednávat pouze pokud je hlavní produkt v košíku.';
            }

			$quantity = 1;
			if(isset($_POST['quantity']))
				$quantity = $_POST['quantity'];

			$cartItem = new Cart();
			$cartItem->setCustomerId($customerId);
			$cartItem->setProductId($productId);
			$cartItem->setAdditionToProductId($additionToProductId);
			$cartItem->setQuantity($quantity);

			$cartItem = $cartItem->create();
			Log::checkObjectAndInsert($customerId, $cartItem, Cart::class, 'Vložení produktu ' . $productId . ' do košíku', Log::LOG_CUSTOMER);

			return $cartItem;
		}
	}

	/**
	 * @return Cart|bool|string
	 */
	public static function removeAdditionFromCart() {
		$customerId = self::getCustomerId();
		if(isset($_POST['productId']) && isset($_POST['additionToProductId'])) {
			$productId = $_POST['productId'];
			$additionToProductId = $_POST['additionToProductId'];

			$cartItem = new Cart();
			$cartItem->setCustomerId($customerId);
			$cartItem->setProductId($productId);
			$cartItem->setAdditionToProductId($additionToProductId);
			$cartItem = $cartItem->load();

			if(is_a($cartItem, Cart::class)) {
				$cartItem = $cartItem->delete();
				Log::checkBoolAndInsert($cartItem, true, $customerId, 'Smazání produktu ' . $productId . ' z košíku', Log::LOG_CUSTOMER);

				return $cartItem;
			} else {
				Log::insert($customerId, 'Smazání produktu ' . $productId . ' z košíku', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $cartItem);
				return $cartItem;
			}
		}
	}

	/**
     * @return int|string
     */
    public static function getCustomerId() {
        $customer = CustomerController::isLoggedCustomer();
        // if is customer logged
        if (is_a($customer, Customer::class)) {
            return $customer->getCustomerId();
        } else {
            // create temp customer and save it to cookies
            $tempId = 0;
            if(isset($_COOKIE[self::$cookieName])) {
                $tempId = $_COOKIE[self::$cookieName];
                if(strlen($tempId) == 16 && is_numeric($tempId)) {
                    // prolong cookie
                    self::updateTempCustomer($tempId);
                } else {
                    $tempId = self::createTempCustomer();
                }
            } else {
                $tempId = self::createTempCustomer();
            }

            return $tempId;
        }
    }

    /**
     * @return string
     */
    private static function createTempCustomer() {
        $tempId = Customer::generateTempCustomerId();

        setcookie(self::$cookieName, $tempId, time() + (86400 * 60), '/');

        return $tempId;
    }

    /**
     * @param $tempId
     */
    private static function updateTempCustomer($tempId) {
        if(!headers_sent())
            setcookie(self::$cookieName, $tempId, time() + (86400 * 60), '/');
    }

    /**
     * @param bool $getAdditions
     * @param bool $onlyAdditions
     * @return array|string
     */
    public static function getCartProducts($getAdditions = true, $onlyAdditions = false) {
        $customerId = self::getCustomerId();
        return Cart::getCartProducts($customerId, $getAdditions, $onlyAdditions);
    }

    /**
     * @param $mainProduct
     * @return array|string
     */
    public static function getAdditionalCartProducts($mainProduct) {
        $customerId = self::getCustomerId();
        return Cart::getAdditionalCartProducts($customerId, $mainProduct);
    }
}