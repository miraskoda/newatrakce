<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 09.04.2018
 * Time: 14:30
 */

namespace backend\controllers;

use backend\models\Log;
use backend\models\SuperproductProduct;
use backend\models\User;

class SuperproductProductController {

	/**
	 * @return array|bool|string
	 */
	public static function getProductsBySuperproduct() {
		if (isset($_POST['superproductId']) && is_a(UserController::isLoggedUser(), User::class)) {
			$superproductId = $_POST['superproductId'];

			return SuperproductProduct::getProductsBySuperproduct($superproductId);
		}

		return false;
	}

	/**
	 * @return SuperproductProduct|bool|mixed|string
	 */
	public static function createSuperproductProduct() {
		if (isset($_POST['superproductId']) && isset($_POST['productId']) && isset($_POST['quantity']) && is_a(UserController::isLoggedUser(), User::class)) {
			$superproductId = $_POST['superproductId'];
			$productId = $_POST['productId'];
			$quantity = $_POST['quantity'];

			$superproductProduct = new SuperproductProduct(0, $superproductId, $productId, $quantity);
			$superproductProduct = $superproductProduct->create();

			Log::checkObjectAndInsert(UserController::getUserId(), $superproductProduct, SuperproductProduct::class, 'Vytvoření propojení superproduktu', Log::LOG_USER);

			return $superproductProduct;
		}

		return false;
	}

	/**
	 * @return SuperproductProduct|bool|mixed|string
	 */
	public static function editSuperproductProduct() {
		if (isset($_POST['superproductId']) && isset($_POST['productId']) && isset($_POST['quantity']) && is_a(UserController::isLoggedUser(), User::class)) {
			$superproductId = $_POST['superproductId'];
			$productId = $_POST['productId'];
			$quantity = $_POST['quantity'];

			$superproductProduct = new SuperproductProduct();

			$superproductProduct->setSuperproductId($superproductId);
			$superproductProduct->setProductId($productId);

			$superproductProduct = $superproductProduct->load();

			if(is_a($superproductProduct, SuperproductProduct::class)) {
				$superproductProduct->setQuantity($quantity);

				$superproductProduct = $superproductProduct->update();
			}

			Log::checkObjectAndInsert(UserController::getUserId(), $superproductProduct, SuperproductProduct::class, 'Úprava superproduktu produktu', Log::LOG_USER);

			return $superproductProduct;
		}

		return false;
	}

	/**
	 * @return SuperproductProduct|bool|mixed|string
	 */
	public static function deleteSuperproductProduct() {
		if (isset($_POST['superproductId']) && isset($_POST['productId']) && is_a(UserController::isLoggedUser(), User::class)) {
			$superproductId = $_POST['superproductId'];
			$productId = $_POST['productId'];

			$superproductProduct = new SuperproductProduct();
			$superproductProduct->setSuperproductId($superproductId);
			$superproductProduct->setProductId($productId);
			$superproductProduct = $superproductProduct->load();

			if(is_a($superproductProduct, SuperproductProduct::class)) {
				$superproductProduct = $superproductProduct->delete();

				Log::checkBoolAndInsert($superproductProduct, true, UserController::getUserId(), 'Smazat superprodukt produkt', Log::LOG_USER);

				return $superproductProduct;
			} else {
				return $superproductProduct;
			}
		}
	}
}