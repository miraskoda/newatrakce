<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 23.11.2018
 * Time: 19:20
 */

namespace backend\controllers;


use backend\database\MyDB;
use backend\models\Address;
use backend\models\Customer;
use backend\models\CustomerGroup;
use backend\models\Log;
use backend\models\MailHelper;
use backend\models\Order;
use backend\models\OrderProduct;
use backend\models\OrderState;
use backend\models\OrderTerm;
use backend\models\Payment;
use backend\models\PaymentType;
use backend\models\Product;
use backend\models\Reservation;
use backend\models\Superproduct;
use backend\models\SuperproductProduct;
use backend\models\User;
use backend\models\Validate;
use backend\view\CartGrid;
use DateTime;

class OrderController
{
    /**
     * @return Order|string
     */
    public static function getDraftOrder()
    {
        $customer = CartController::getCustomerId();

        $order = new Order();
        $order->setCustomerId($customer);
        $order->setType(Order::TYPE_DRAFT);

        $order = $order->load();
        if (is_a($order, Order::class)) {
            return $order;
        } else {
            $order = new Order();
            $order->setCustomerId($customer);
            $order->setType(Order::TYPE_DRAFT);

            return $order->create();
        }
    }

    /**
     * @return bool
     */
    public static function existsDraftOrder()
    {
        $customer = CartController::getCustomerId();

        $order = new Order();
        $order->setCustomerId($customer);
        $order->setType(Order::TYPE_DRAFT);

        $order = $order->load();

        if (is_a($order, Order::class))
            return true;
        else
            return false;
    }

    /**
     * @return Order|string
     */
    public static function saveOrderState()
    {
        if (isset($_POST['order-state']) && isset($_POST['order-id']) && is_a(UserController::isLoggedUser(), User::class)) {
            $orderId = $_POST['order-id'];
            $orderState = $_POST['order-state'];

            $order = new Order($orderId);
            $order = $order->load();
            if (is_a($order, Order::class)) {
                $order->setOrderStateId($orderState);
                $order = $order->update();

                Log::checkObjectAndInsert(UserController::getUserId(), $order, Order::class, 'Aktualizace stavu objednávky', Log::LOG_USER);

                return $order;
            } else {
                Log::insert(UserController::getUserId(), 'Aktualizace stavu objednávky', Log::LOG_USER, Log::LOG_STATE_ERROR, $order);
                return $order;
            }
        }
    }

    /**
     * @return Customer|Order|OrderState|string
     */
    public static function sendOrderStatusUpdate()
    {
        if (isset($_POST['order-id']) && is_a(UserController::isLoggedUser(), User::class)) {
            $orderId = $_POST['order-id'];

            $order = new Order($orderId);
            $order = $order->load();

            if (is_a($order, Order::class)) {
                $orderState = new OrderState($order->getOrderStateId());
                $orderState = $orderState->load();

                if (is_a($orderState, OrderState::class)) {
                    $customer = new Customer($order->getCustomerId());
                    $customer = $customer->load();

                    if (is_a($customer, Customer::class)) {
                        $orderNo = DateTime::createFromFormat('Y-m-d H:i:s', $order->getChangedRaw())->format('y') . $order->getOrderId();

                        $mail = new MailHelper();
                        $mail = $mail->sendOrderStatusUpdate($customer->getEmail(), $orderNo, $orderState->getName());

                        Log::checkBoolAndInsert($mail, true, UserController::getUserId(), 'Odeslání emailu o aktualizaci stavu objednávky', Log::LOG_USER);

                        return $mail;
                    } else {
                        Log::insert(UserController::getUserId(), 'Odeslání emailu o aktualizaci stavu objednávky', Log::LOG_USER, Log::LOG_STATE_ERROR, $customer);
                        return $customer;
                    }
                } else {
                    Log::insert(UserController::getUserId(), 'Odeslání emailu o aktualizaci stavu objednávky', Log::LOG_USER, Log::LOG_STATE_ERROR, $orderState);
                    return $orderState;
                }
            } else {
                Log::insert(UserController::getUserId(), 'Odeslání emailu o aktualizaci stavu objednávky', Log::LOG_USER, Log::LOG_STATE_ERROR, $order);
                return $order;
            }
        }
    }

    /**
     * @return Order|bool|string
     */
    public static function saveVenueContact()
    {
        if (isset($_POST['contact-name']) && isset($_POST['contact-phone']) && is_a(CustomerController::isLoggedCustomer(), Customer::class)) {
            $contactName = $_POST['contact-name'];
            $contactPhonePrefix = $_POST['contact-phone-prefix'];
            $contactPhone = $_POST['contact-phone'];

            $customer = CartController::getCustomerId();

            $order = new Order();
            $order->setCustomerId($customer);
            $order->setType(Order::TYPE_DRAFT);

            $order = $order->load();

            if (is_a($order, Order::class)) {
                $order->setContactName($contactName);
                $order->setContactPhonePrefix($contactPhonePrefix);
                $order->setContactPhone($contactPhone);

                $order = $order->update();

                Log::checkObjectAndInsert($customer, $order, Order::class, 'Uložení kontaktu místa konání', Log::LOG_CUSTOMER);

                return $order;
            } else {
                Log::insert($customer, 'Uložení kontaktu místa konání', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $order);
                return $order;
            }
        }

        return false;
    }

    /**
     * @return Order|bool|string
     */
    public static function saveVenue()
    {
        if (isset($_POST['venue-street']) && isset($_POST['venue-city']) && isset($_POST['venue-zip']) && is_a(CustomerController::isLoggedCustomer(), Customer::class)) {
            $venueStreet = $_POST['venue-street'];
            $venueCity = $_POST['venue-city'];
            $venueZip = $_POST['venue-zip'];
            $venueCountry = $_POST['venue-country'];

            $customer = CartController::getCustomerId();

            $order = new Order();
            $order->setCustomerId($customer);
            $order->setType(Order::TYPE_DRAFT);

            $order = $order->load();

            if (is_a($order, Order::class)) {
                $order->setVenueStreet($venueStreet);
                $order->setVenueCity($venueCity);
                $order->setVenueZip($venueZip);
                $order->setVenueCountry($venueCountry);

                $order = $order->update();

                Log::checkObjectAndInsert($customer, $order, Order::class, 'Uložení místa konání', Log::LOG_CUSTOMER);

                return $order;
            } else {
                Log::insert($customer, 'Uložení místa konání', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $order);
                return $order;
            }
        }

        return false;
    }

    /**
     * @return array|Order|bool|string
     */
    public static function getVenue()
    {
        if (is_a(CustomerController::isLoggedCustomer(), Customer::class)) {
            $customer = CartController::getCustomerId();

            $order = new Order();
            $order->setCustomerId($customer);
            $order->setType(Order::TYPE_DRAFT);

            $order = $order->load();

            if (is_a($order, Order::class)) {
                $tempValidate = new Validate();
                if ($tempValidate->validateRequired([
                    $order->getVenueStreet(),
                    $order->getVenueCity(),
                    $order->getVenueZip()
                ])) {
                    return self::getGoogleDistanceApiData($order->getVenueStreet(), $order->getVenueCity(), $order->getVenueZip());
                }
            } else {
                return $order;
            }
        }

        return false;
    }

    /**
     * @param $street
     * @param $city
     * @param $zip
     * @return bool|string
     */
    public static function getGoogleDistanceApiData($street, $city, $zip)
    {
        // protection against calling from another page
        if (is_a(CustomerController::isLoggedCustomer(), Customer::class) || is_a(UserController::isLoggedUser(), User::class)) {

            $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?';

            $data = [
                'origins' => 'Syrovátka 11, Lhota pod Libčany - Hradec Králové, 503 27 Czech Republic',
                'destinations' => $street . ', ' . $city . ', ' . $zip,
                'language' => 'cs',
                'key' => 'AIzaSyDCBDVgGrC5NBm1x-_BVEkxm2GPKo2Bw9w'
            ];

            $url = $url . http_build_query($data);

            $result = file_get_contents($url);

            return $result;
        }

        return false;
    }


    /**
     * @return Order|string|array
     */
    public static function getVenueCountedData()
    {
        $customerId = CartController::getCustomerId();

        $order = new Order();
        $order->setCustomerId($customerId);
        $order->setType(Order::TYPE_DRAFT);

        $order = $order->load();

        return self::getVenueCountedDataByOrder($order);
    }

    /**
     * @param $order
     * @return string | array
     */
    public static function getVenueCountedDataByOrder($order)
    {
        if (is_a($order, Order::class)) {
            $tempValidate = new Validate();
            if ($tempValidate->validateRequired([
                $order->getVenueStreet(),
                $order->getVenueCity(),
                $order->getVenueZip()
            ])) {
                $json = self::getGoogleDistanceApiData($order->getVenueStreet(), $order->getVenueCity(), $order->getVenueZip());
                $data = json_decode($json);

                if ($data) {
                    if ($data->status == 'OK') {
                        if ($data->rows[0]->elements[0]->status == 'OK') {
                            $travelLengthText = $data->rows[0]->elements[0]->distance->text;
                            $travelLengthValue = $data->rows[0]->elements[0]->distance->value;

                            $travelPrice = round(($travelLengthValue / 1000) * 2 * 6);

                            $durationValue = ($data->rows[0]->elements[0]->duration->value * 1.15);
                            $quarters = floor($durationValue / 900);
                            $remainder = $durationValue % 900;
                            if ($remainder > 0.1)
                                $quarters++;

                            $durationText = "";
                            $hours = floor($quarters / 4);
                            if ($hours > 0)
                                $durationText .= $hours . ' ' . CartGrid::czechHourMaker($hours) . ' ';

                            $minutes = round(($quarters % 4) * 15);
                            if ($minutes > 0)
                                $durationText .= $minutes . ' ' . CartGrid::czechMinuteMaker($minutes);

                            return ['travel-length' => $travelLengthText, 'travel-price' => $travelPrice, 'duration-text' => $durationText];
                        }
                    } else if ($data->status == 'OVER_QUERY_LIMIT') {
                        return 'Systém pro výpočet dopravy dosáhl limitu. Prosím kontaktujte zákaznickou podporu.';
                    } else {
                        return 'Nastala chyba. Prosím kontaktujte zákaznickou podporu. Data: ' . $data;
                    }
                } else {
                    return 'Chyba v načítání dat. Prosím kontaktujte zákaznickou podporu. Data: ' . serialize($json);
                }
            }

            return $tempValidate->getValidationSummary();
        } else {
            return $order;
        }
    }

    /**
     * @return float|int
     */
    public static function getPriceWithoutVat()
    {
        $rentHours = OrderTerm::getRentHours();
        $cleanedRentHours = [];
        foreach ($rentHours as $rentHour) {
            $cleanedRentHours[] = intval($rentHour['hours']);
        }

        return Order::getPriceWithoutVat($cleanedRentHours);
    }

    /**
     * @return Order|bool|string
     */
    public static function savePaymentData()
    {
        if (is_a(CustomerController::isLoggedCustomer(), Customer::class)) {
            if (isset($_POST['payment-type']))
                $paymentType = $_POST['payment-type'];
            if (isset($_POST['payment']))
                $payment = $_POST['payment'];

            $customer = CartController::getCustomerId();

            $order = new Order();
            $order->setCustomerId($customer);
            $order->setType(Order::TYPE_DRAFT);

            $order = $order->load();

            if (is_a($order, Order::class)) {
                if (isset($payment))
                    $order->setPaymentId($payment);
                if (isset($paymentType))
                    $order->setPaymentTypeId($paymentType);

                $order = $order->update();

                Log::checkObjectAndInsert($customer, $order, Order::class, 'Uložení platebních údajů', Log::LOG_CUSTOMER);

                return $order;
            } else {
                Log::insert($customer, 'Uložení platebních údajů', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $order);
                return $order;
            }
        }

        return false;
    }

    /**
     * @return Order|bool|string
     */
    public static function saveCustomerNote()
    {
        if (isset($_POST['customerNote']) && is_a(CustomerController::isLoggedCustomer(), Customer::class)) {
            $customerNote = $_POST['customerNote'];

            $customer = CartController::getCustomerId();

            $order = new Order();
            $order->setCustomerId($customer);
            $order->setType(Order::TYPE_DRAFT);

            $order = $order->load();

            if (is_a($order, Order::class)) {
                $order->setCustomerNote($customerNote);

                $order = $order->update();

                Log::checkObjectAndInsert($customer, $order, Order::class, 'Uložení zákaznické poznámky', Log::LOG_CUSTOMER);

                return $order;
            } else {
                Log::insert($customer, 'Uložení zákaznické poznámky', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $order);
                return $order;
            }
        }

        return false;
    }

    /**
     * @return Order|bool|string
     */
    public static function saveAdminNote()
    {
        if (isset($_POST['orderId']) && isset($_POST['adminNote']) && is_a(UserController::isLoggedUser(), User::class)) {
            $orderId = $_POST['orderId'];
            $adminNote = $_POST['adminNote'];

            $order = new Order($orderId);

            $order = $order->load();

            if (is_a($order, Order::class)) {
                $order->setAdminNote($adminNote);

                $order = $order->update();

                Log::checkObjectAndInsert(UserController::getUserId(), $order, Order::class, 'Uložení správcovské poznámky', Log::LOG_USER);

                return $order;
            } else {
                Log::insert(UserController::getUserId(), 'Uložení správcovské poznámky', Log::LOG_USER, Log::LOG_STATE_ERROR, $order);
                return $order;
            }
        }

        return false;
    }

    /**
     * @return array|Order|bool|string
     */
    public static function validateCart()
    {
        $customer = CartController::getCustomerId();

        $order = new Order();
        $order->setCustomerId($customer);
        $order->setType(Order::TYPE_DRAFT);

        $order = $order->load();
        return self::validateCartByOrder($order);
    }

    /**
     * @param $order
     * @return array|Order|bool|string
     */
    public static function validateCartByOrder($order)
    {
        if (is_a($order, Order::class)) {
            $orderTermValidation = OrderTermController::validateOrderTerms();
            if ($orderTermValidation === true) {
                $cartProducts = CartController::getCartProducts();

                $terms = OrderTerm::getOrderTerms();

                if (is_array($cartProducts)) {
                    if (count($cartProducts) > 0) {
                        $error = '';
                        $isDirty = false;
                        foreach ($cartProducts as $cartProduct) {
                            if ($cartProduct['quantity'] <= 0) {
                                $error .= 'Množství zboží v košíku musí být větší než nula. ';
                                $isDirty = true;
                            }

                            if ($cartProduct['visible'] != true) {
                                $error .= 'Nelze nakupovat zboží, které již není v prodeji.';
                                $isDirty = true;
                            }

                            $productObj = new Product($cartProduct['productId']);
                            $productObj = $productObj->load();

                            if (!is_a($productObj, Product::class)) {
                                return 'Chyba při načítání objektů v košíku. Chyba: ' . $productObj;
                            }

                            foreach ($terms as $term) {
                                $isProductAvailable = json_decode($productObj->getProductAvailability($cartProduct['quantity'], $term->getStart(), $term->getHours()));
                                if ($isProductAvailable->result == 'danger' || $isProductAvailable->result == 'warning') {
                                    return 'Chyba produktu ' . $cartProduct['name'] . '. ' . $isProductAvailable->text;
                                }
                            }

                            if ($isDirty)
                                return $error;
                        }

                        // check if product with the same superproduct is in the cart more than once
                        $superproductTable = [];
                        foreach ($cartProducts as $cartProduct) {
                            if (SuperproductProduct::isProductInSuperproduct($cartProduct['productId'])) {
                                // this returns only superproductId
                                $superproduct = SuperproductProduct::getSuperproductByProduct($cartProduct['productId']);
                                $superproductTable[$superproduct][] = $cartProduct;
                            }
                        }

                        // go through superproducts and check if quantity > 1 => check for availability
                        foreach ($superproductTable as $index => $superproduct) {
                            if (count($superproduct) > 1) {
                                $superproductProducts = SuperproductProduct::getProductsBySuperproduct($index);

                                $innerProductSum = 0;
                                foreach ($superproductProducts as $product) {
                                    // get coresponding product from cart
                                    $corespondingCartProduct = null;
                                    foreach ($cartProducts as $cartProduct) {
                                        if ($cartProduct['productId'] == $product['productId']) {
                                            $corespondingCartProduct = $cartProduct;
                                            break;
                                        }
                                    }

                                    if (!is_null($corespondingCartProduct)) {
                                        // in first product is saved superproduct quantity and in the second quantity in cart
                                        // multiply
                                        $innerProductSum += $product['quantity'] * $corespondingCartProduct['quantity'];
                                    }
                                }

                                // if we have num of pieces we want, we can check if superproduct has enough
                                foreach ($terms as $term) {
                                    $availableSuperproductPieces = Superproduct::getAvailableSuperproductPieces($superproduct[1]['productId'], $term->getStart(), $term->getHours(), false);
                                    if (!is_int($availableSuperproductPieces))
                                        return 'Nepodařilo se načíst vícenásobný superprodukt. Prosím kontaktujte zákaznickou podporu.';

                                    if ($availableSuperproductPieces < $innerProductSum) {
                                        return 'Chyba produktu ' . $superproduct[0]['name'] . '. Požadovaný počet kusů není skladem.';
                                    }
                                }
                            }
                        }

                        return true;
                    } else {
                        return 'V košíku není žádné zboží. Pro pokračování v objednávce vložte do košíku zboží.';
                    }
                } else {
                    return $cartProducts;
                }
            } else {
                return $orderTermValidation;
            }
        } else {
            return 'Nejsou vyplněny všechny povinné údaje. Prosím zkontrolujte termíny pronájmu a zboží.';
        }
    }


    /**
     * @return bool|string
     */
    public static function validateVenue()
    {
        $customer = CartController::getCustomerId();

        $order = new Order();
        $order->setCustomerId($customer);
        $order->setType(Order::TYPE_DRAFT);

        $order = $order->load();

        return self::validateVenueByOrder($order);
    }

    /**
     * @param $order
     * @return bool|string
     */
    public static function validateVenueByOrder($order)
    {
        if (is_a($order, Order::class)) {
            $validate = new Validate();

            $validate->validateNotNull([
                'jméno kontaktní osoby' => $order->getContactName(),
                'ulice a číslo popisné místa konání' => $order->getVenueStreet(),
                'město místa konání' => $order->getVenueCity()
            ]);

            if ($validate->validateNotNull([
                'telefon kontaktní osoby' => $order->getContactPhone()
            ])) {
                $validate->validatePhone([
                    'telefon kontaktní osoby' => $order->getContactPhone()
                ]);
            }

            if ($validate->validateNotNull([
                'PSČ místa konání' => $order->getVenueZip()
            ])) {
                $validate->validateZIP([
                    'PSČ místa konání' => $order->getVenueZip()
                ]);
            }

            if ($validate->isValidationResult()) {
                return true;
            } else {
                return $validate->getValidationSummary();
            }
        } else {
            return 'Nejsou vyplněny všechny povinné údaje. Prosím zkontrolujte termíny pronájmu a zboží.';
        }
    }

    /**
     * @return Address|Customer|string
     */
    public static function validatePayment()
    {
        $customerId = CartController::getCustomerId();

        $order = new Order();
        $order->setCustomerId($customerId);
        $order->setType(Order::TYPE_DRAFT);

        $order = $order->load();
        return self::validatePaymentByOrder($order, $customerId);
    }

    /**
     * @param $order
     * @param $customerId
     * @param bool $orderEdit
     * @return Address|Customer|string
     */
    public static function validatePaymentByOrder($order, $customerId = null, $orderEdit = false)
    {
        if (is_a($order, Order::class)) {
            if ($order->getPaymentId() < 1) {
                return 'Není vybrán způsob platby.';
            } else if ($order->getPaymentTypeId() < 1) {
                return 'Není vybrán typ platby.';
            }

            $payment = new Payment($order->getPaymentId());
            $payment = $payment->load();
            $paymentType = new PaymentType($order->getPaymentTypeId());
            $paymentType = $paymentType->load();

            if (!is_a($payment, Payment::class))
                return $payment;
            if (!is_a($paymentType, PaymentType::class))
                return $paymentType;

            $customerGroup = CustomerController::getCustomerGroup(false, $customerId, $orderEdit);

            if(!is_a($customerGroup, CustomerGroup::class)) {
                echo 'cus vole';
                return $customerGroup;
            }

            if (($payment->isVIP() || $paymentType->isVIP()) && !$customerGroup->getVipPaymentsBool())
                return 'Nemáte oprávnění pro využívání VIP plateb.';

            $customer = new Customer($customerId);
            $customer = $customer->load();
            if (is_a($customer, Customer::class)) {
                $confirmDataValid = false;
                $error = '';
                if ($customer->isCompany()) {
                    // validation of ico and dic
                    $companyValidation = new Validate();

                    $companyValidation->validateICO([
                        'IČO' => $customer->getIco()
                    ]);

                    if ((new Validate())->validateNotNull([$customer->getDic()])) {
                        $companyValidation->validateDIC([
                            'DIČ' => $customer->getDic()
                        ]);
                    }

                    if ($companyValidation->isValidationResult()) {
                        $confirmDataValid = true;
                    } else {
                        $error = $companyValidation->getValidationSummary();
                    }
                } else {
                    // validation of birth number
                    $personValidation = new Validate();

                    $personValidation->validateBirthNumber([
                        'rodné číslo' => $customer->getBirthNumber()
                    ]);

                    if ($personValidation->isValidationResult()) {
                        $confirmDataValid = true;
                    } else {
                        $error = $personValidation->getValidationSummary();
                    }
                }

                // if false return validation result
                if (!$confirmDataValid)
                    return $error;

                // validate invoice address
                $invoiceAddress = new Address();
                $invoiceAddress->setCustomerId($customerId);
                $invoiceAddress->setAddressType(Address::ADDRESS_INVOICE);
                $invoiceAddress = $invoiceAddress->load(true);

                if (is_a($invoiceAddress, Address::class)) {
                    $addressValidation = new Validate();

                    $addressValidation->validateNotNull([
                        'jméno / název firmy' => $invoiceAddress->getName(),
                        'ulice a číslo popisné' => $invoiceAddress->getStreet(),
                        'město' => $invoiceAddress->getCity(),
                        'PSČ' => $invoiceAddress->getZip()
                    ]);

                    $addressValidation->validateZIP([
                        'PSČ' => $invoiceAddress->getZip()
                    ]);

                    if ($addressValidation->isValidationResult()) {
                        return true;
                    } else {
                        return $addressValidation->getValidationSummary();
                    }
                } else {
                    return $invoiceAddress;
                }
            } else {
                return $customer;
            }
        } else {
            return 'Nejsou vyplněny všechny povinné údaje. Prosím zkontrolujte termíny pronájmu a zboží.';
        }
    }

    /**
     * @param $terms
     * @param $customerId
     */
    public static function deleteReservations($terms, $customerId)
    {
        foreach ($terms as $term) {
            if (is_a($term, OrderTerm::class)) {
                $reservations = Reservation::getReservationsByDateAndCustomer($term->getStart(), $term->getHours(), $customerId);
                foreach ($reservations as $reservation) {
                    if (is_a($reservation, Reservation::class)) {
                        $reservation = $reservation->load();
                        // deactivate
                        $reservation->setActive(0);
                        $reservation->update();
                    }
                }
            }
        }
    }

    /**
     * @return array|Address|Customer|Order|bool|string
     */
    public static function finishOrder()
    {
        // validate all data
        $validateCart = self::validateCart();
        $validateVenue = self::validateVenue();
        $validatePayment = self::validatePayment();
        if ($validateCart === true && $validateVenue === true && $validatePayment === true) {
            // load order data
            $customerId = CartController::getCustomerId();
            $customer = new Customer($customerId);
            $customer->load();

            if (is_a($customer, Customer::class)) {
                $order = new Order();
                $order->setCustomerId($customer->getCustomerId());
                $order->setType(Order::TYPE_DRAFT);

                $order = $order->load();

                // setup db
                $db = MyDB::getConnection();
                // start transaction
                if ($db->beginTransaction() && is_a($order, Order::class)) {
                    // count price before cart is empty
                    $stringifiedRentHours = OrderTerm::getRentHoursAsStrings();
                    $priceWithoutVat = round(Order::getPriceWithoutVat($stringifiedRentHours));

                    $customerGroup = CustomerController::getCustomerGroup();
                    $discount = 0;
                    $discountAmount = 0;
                    if (is_a($customerGroup, CustomerGroup::class)) {
                        $discount = $customerGroup->getDiscount();
                    }

                    if ($discount > 0) {
                        $discountAmount = ($priceWithoutVat / 100) * $discount;
                        $priceWithoutVat = round(($priceWithoutVat / 100) * (100 - $discount));
                    }

                    $order->setDiscount($discount);

                    // move cart products to order products
                    $copyResult = OrderProduct::copyCartToOrder($customerId, $order->getOrderId(), $db);
                    if ($copyResult === true) {
                        // successfully moved
                        // duplicate invoice address and tie it to order
                        $addressResult = Address::duplicateAndMakeOrder($customerId, $order->getOrderId(), $db);
                        if ($addressResult !== true) {
                            $db->rollBack();
                            Log::insert($customerId, 'Dokončení a uložení objednávky', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, 'Nepodařilo se přiřadit adresu k objednávce. Data: ' . $addressResult);
                            return 'Nepodařilo se přiřadit adresu k objednávce. Prosím kontaktujte zákaznickou podporu. Data: ' . $addressResult;
                        }

                        // update order data
                        $order->setDb($db);
                        $order->setType(Order::TYPE_FINISHED);
                        if ($customer->isCompany()) {
                            $order->setInvoiceIco($customer->getIco());
                            $order->setInvoiceDic($customer->getDic());
                        }

                        $order->setProductPrice($priceWithoutVat);
                        // count delivery price if needed
                        if ($priceWithoutVat <= 7000) {
                            // if order lower than 7000 Kč count delivery price
                            $venueData = self::getVenueCountedData();
                            if (is_array($venueData)) {
                                $order->setDeliveryPaymentPrice($venueData['travel-price']);
                                $order->setVat(round((($priceWithoutVat + $venueData['travel-price']) * 0.21)));
                            } else {
                                $db->rollBack();
                                Log::insert($customerId, 'Dokončení a uložení objednávky', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, 'Nepodařilo se načíst data o dopravě. Data: ' . $venueData);
                                return 'Nepodařilo se načíst data o dopravě. Prosím kontaktujte zákaznickou podporu. Data: ' . $venueData;
                            }
                        } else {
                            $order->setVat(round(($priceWithoutVat * 0.21)));
                        }

                        $orderTerms = OrderTerm::getOrderTerms();
                        // delete all reservations for this customer in term of order
                        self::deleteReservations($orderTerms, $customerId);

                        $order = $order->update();
                        if (!is_a($order, Order::class)) {
                            $db->rollBack();
                            Log::insert($customerId, 'Dokončení a uložení objednávky', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $order);
                            return $order;
                        }

                        Log::insert($customerId, 'Dokončení a uložení objednávky', Log::LOG_CUSTOMER, Log::LOG_STATE_SUCCESS);
                        // everything successful
                        return $db->commit();
                    } else {
                        $db->rollBack();
                        Log::insert($customerId, 'Dokončení a uložení objednávky', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $copyResult);
                        return $copyResult;
                    }
                } else {
                    return 'Nepodařilo se vytvořit transakci. Prosím kontaktujte zákaznickou podporu.';
                }
            } else {
                return $customer;
            }
        } else {
            if ($validateCart !== true)
                return $validateCart;
            else if ($validateVenue !== true)
                return $validateVenue;
            else if ($validatePayment !== true)
                return $validatePayment;
        }
    }

    /**
     * @param null $orderId
     * @return bool
     */
    public static function cancelOrder($orderId = null)
    {
        if ((isset($_POST['orderId']) || $orderId) && (is_a(CustomerController::isLoggedCustomer(), Customer::class) || $orderId)) {
            $order = new Order();
            if($orderId) {
                $order->setOrderId($orderId);
            } else {
                $order->setOrderId($_POST['orderId']);
            }
            $order = $order->load();

            if (is_a($order, Order::class)) {
                $order->setIsCanceled(1);
                // Canceul using using orderStateId too
                $order->setOrderStateId(6);
                $order = $order->update();
                Log::checkObjectAndInsert(UserController::getUserId(), $order, Order::class, 'Uložení zrušení objednávky', Log::LOG_CUSTOMER);
                return true;
            } else {
                Log::insert(UserController::getUserId(), 'Uložení zrušení objednávky', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $order);
                return false;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public static function requestChange()
    {
        if (isset($_POST['orderId']) && isset($_POST['requestChange']) && is_a(CustomerController::isLoggedCustomer(), Customer::class)) {

            $order = new Order();
            $order->setOrderId($_POST['orderId']);
            $order = $order->load();

            if (is_a($order, Order::class)) {
                if ($order->getIsCanceled()) {
                    Log::insert(UserController::getUserId(), 'Uložení požadavku na změnu', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $order);
                    return false;
                } else {
                    $order->setRequestChange($_POST['requestChange']);
                    $order = $order->update();
                    Log::checkObjectAndInsert(UserController::getUserId(), $order, Order::class, 'Uložení požadavku na změnu', Log::LOG_CUSTOMER);
                    return true;
                }
            } else {
                Log::insert(UserController::getUserId(), 'Uložení požadavku na změnu', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $order);
                return false;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public static function requestEdit()
    {
        if (isset($_POST['birth-number']) && isset($_POST['ico']) && isset($_POST['dic']) && isset($_POST['orderId']) && isset($_POST['start-term']) && isset($_POST['venue-street']) && isset($_POST['payment-type']) && isset($_POST['payment']) && isset($_POST['venue-city']) && isset($_POST['venue-zip']) && isset($_POST['contact-name']) && isset($_POST['contact-phone'])) {

            $startTerm = $_POST['start-term'];
            $date = DateTime::createFromFormat('d. m. Y H:i', $startTerm);
            $date = $date->format('Y-m-d H:i:s');
            $orderId = $_POST['orderId'];
            $numberOfHours = $_POST['number-of-hours'];

            $order = new Order();
            $order->setOrderId($orderId);
            $order = $order->load();

            if (is_a($order, Order::class)) {
                $venueStreet = $_POST['venue-street'];
                $venueCity = $_POST['venue-city'];
                $venueZip = $_POST['venue-zip'];
                $venueCountry = $_POST['venue-country'];

                $order->setVenueCity($venueCity);
                $order->setVenueZip($venueZip);
                $order->setVenueCountry($venueCountry);
                $order->setVenueStreet($venueStreet);

                $contactName = $_POST['contact-name'];
                $contactPhonePrefix = $_POST['contact-phone-prefix'];
                $contactPhone = $_POST['contact-phone'];

                $order->setContactName($contactName);
                $order->setContactPhone($contactPhone);
                $order->setContactPhonePrefix($contactPhonePrefix);

                $payment = $_POST['payment'];
                $paymentType = $_POST['payment-type'];

                $order->setPaymentId($payment);
                $order->setPaymentTypeId($paymentType);

                return self::saveEditData($order, $date, $numberOfHours);
            } else {
                Log::insert(UserController::getUserId(), 'Uložení požadavku na editaci objednávky', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $order);

                return false;
            }
        } else {
            return 'Nepodařila se najít všechna data potřebná k editaci. Prosím kontaktujte administrátora.';
        }
    }

    /**
     * @param $order
     * @param $date
     * @param $numberOfHours
     * @return array|Address|Customer|Order|bool|string
     */
    public static function saveEditData($order, $date, $numberOfHours)
    {
        // validate all data
        $customerId = $order->getCustomerId();
        $validateVenue = self::validateVenueByOrder($order);
        $validatePayment = self::validatePaymentByOrder($order, $customerId, true);
        var_dump($validatePayment);
        if (is_a($order, Order::class) && $validateVenue === true && $validatePayment === true) {
            // load order data
            $customer = new Customer($customerId);
            $customer->load();

            if (is_a($customer, Customer::class)) {
                // setup db
                $db = MyDB::getConnection();
                // start transaction
                if ($db->beginTransaction() && is_a($order, Order::class)) {
                    $order->setCustomerId($customer->getCustomerId());
                    $order->setType(Order::TYPE_DRAFT);

                    $oldOrderId = $order->getOrderId();

                    $order = $order->create();

                    $isCanceled = self::cancelOrder($oldOrderId);
                    if(!$isCanceled) {
                        return "Nepodařilo se stornovat stávající objednávku. Prosím kontaktuje administrátora.";
                    }

                    $orderId = $order->getOrderId();

                    $term = new OrderTerm();
                    $term->setTermNo(1);
                    $term->setOrderId($orderId);
                    $term->setStart($date);
                    $term->setHours($numberOfHours);
                    $term->create();

                    $isValid = $term->validate();

                    // move products from order to new order
                    $copyResult = OrderProduct::copyFromOrder($orderId, $oldOrderId, $db);
                    if ($copyResult === true) {
                        // successfully moved
                        // move products from order to new order

                        // count price before cart is empty
                        $stringifiedRentHours = OrderTerm::getRentHoursAsStrings($order);
                        $priceWithoutVat = round(Order::getPriceWithoutVatByOrder($order, $stringifiedRentHours));

                        $customerGroup = CustomerController::getCustomerGroup();
                        $discount = 0;
                        $discountAmount = 0;
                        if (is_a($customerGroup, CustomerGroup::class)) {
                            $discount = $customerGroup->getDiscount();
                        }

                        if ($discount > 0) {
                            $discountAmount = ($priceWithoutVat / 100) * $discount;
                            $priceWithoutVat = round(($priceWithoutVat / 100) * (100 - $discount));
                        }

                        $order->setDiscount($discount);

                        // update order data
                        $order->setDb($db);
                        $order->setType(Order::TYPE_FINISHED);
                        if ($customer->isCompany()) {
                            $order->setInvoiceIco($customer->getIco());
                            $order->setInvoiceDic($customer->getDic());
                        }

                        $order->setProductPrice($priceWithoutVat);
                        // count delivery price if needed
                        if ($priceWithoutVat <= 7000) {
                            // if order lower than 7000 Kč count delivery price
                            $venueData = self::getVenueCountedDataByOrder($order);
                            if (is_array($venueData)) {
                                $order->setDeliveryPaymentPrice($venueData['travel-price']);
                                $order->setVat(round((($priceWithoutVat + $venueData['travel-price']) * 0.21)));
                            } else {
                                $db->rollBack();
                                Log::insert($customerId, 'Dokončení a uložení objednávky', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, 'Nepodařilo se načíst data o dopravě. Data: ' . $venueData);
                                return 'Nepodařilo se načíst data o dopravě. Prosím kontaktujte administrátora. Data: ' . $venueData;
                            }
                        } else {
                            $order->setVat(round(($priceWithoutVat * 0.21)));
                        }

                        // duplicate invoice address and tie it to order
                        $addressResult = Address::duplicateAndMakeOrder($customerId, $order->getOrderId(), $db);
                        if ($addressResult !== true) {
                            $db->rollBack();
                            Log::insert($customerId, 'Dokončení a uložení objednávky', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, 'Nepodařilo se přiřadit adresu k objednávce. Data: ' . $addressResult);
                            return 'Nepodařilo se přiřadit adresu k objednávce. Prosím kontaktujte administrátora. Data: ' . $addressResult;
                        }

                        $order->setChanged(date('Y-m-d H:i:s'));
                        $order->setIsCanceled(0);
                        // Set default state ID
                        $order->setOrderStateId(1);
                        $order = $order->update();

                        if (!$isValid && !is_a($order, Order::class)) {
                            $db->rollBack();
                            Log::insert($customerId, 'Dokončení a uložení objednávky', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $order);
                            return "Nepodařilo se vytvořit novou objednávku. Prosím kontaktuje administrátora.";
                        }

                        Log::insert($customerId, 'Dokončení a uložení objednávky', Log::LOG_CUSTOMER, Log::LOG_STATE_SUCCESS);
                        // everything successful

                        return $db->commit();
                    } else {
                        $db->rollBack();
                        Log::insert($customerId, 'Dokončení a uložení objednávky', Log::LOG_CUSTOMER, Log::LOG_STATE_ERROR, $copyResult);
                        return 'Nepodařilo se zkopírovat požadované produkty. Prosím kontaktuje administrátora.';
                    }
                } else {
                    return 'Nepodařilo se vytvořit transakci. Prosím kontaktuje administrátora.';
                }
            } else {
                return "Zákazník s tímto ID nebyl nalezen. Prosím kontaktuje administrátora.";
            }
        } else {
            if ($validateVenue !== true)
                return "Něco se pokazilo při, editaci kontaktní osoby. Prosím kontaktuje administrátora.";
            else if ($validatePayment !== true)
                return "Něco se pokazilo při, editaci platby. Prosím kontaktuje administrátora.";
            else
                return false;
        }
    }
}


