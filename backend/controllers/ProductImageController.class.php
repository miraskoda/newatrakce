<?php
/**
 * Created by PhpStorm.
 * User: ondra
 * Date: 15.06.2017
 * Time: 0:48
 */

namespace backend\controllers;

use backend\models\Image;
use backend\models\Log;
use backend\models\ProductImage;
use backend\models\Url;
use backend\models\User;

class ProductImageController extends ImageController
{
    public static function createImage()
    {
        //if values are set and admin user is logged
        if (isset($_POST['imageName']) && isset($_POST['imageType']) && isset($_POST['productId']) && is_a(UserController::isLoggedUser(), User::class)) {
            $name = strtolower($_POST['imageName']);
            $type = $_POST['imageType'];
            $productId = $_POST['productId'];

            $image = new ProductImage();
            $image->setProductImage($name, $type, $productId);

            $image = $image->create();

            Log::checkObjectAndInsert(UserController::getUserId(), $image, ProductImage::class, 'Vytvořit obrázek', Log::LOG_USER);

            return $image;
        }

        return false;
    }

    /**
     * @return ProductImage|bool|string
     */
    public static function updateImage()
    {
        if (isset($_POST['imageId']) && is_a(UserController::isLoggedUser(), User::class)) {
            $imageId = $_POST['imageId'];

            $image = new ProductImage($imageId);
            $image = $image->load();

            if(isset($_POST['name'])) {
                $name = $_POST['name'];
                $image->setName($name);
            }

            if(isset($_POST['description'])) {
                $description = $_POST['description'];
                $image->setDescription($description);
            }

            //$type = ProductImage::TYPE_OTHER;
            //$productId = 0;
            if(isset($_POST['type'])) {
                $type = $_POST['type'];
                $image->setType($type);
            }

            if(isset($_POST['productId'])) {
                $productId = $_POST['productId'];
                $image->setProductId($productId);
            }

            $image = $image->update();

            Log::checkObjectAndInsert(UserController::getUserId(), $image, ProductImage::class, 'Aktualizovat obrázek', Log::LOG_USER);

            return $image;
        }

        return false;
    }

    /**
     * @return ProductImage|bool|string
     */
    public static function deleteImage()
    {
        if (isset($_POST['imageId']) && is_a(UserController::isLoggedUser(), User::class)) {

            $imageId = $_POST['imageId'];

            $image = new ProductImage($imageId);
            $image = $image->load();
            if (!is_a($image, ProductImage::class)) {
                Log::insert(UserController::getUserId(), 'Smazat obrázek', Log::LOG_USER, Log::LOG_STATE_ERROR, $image);
                return $image;
            }

            $image = $image->delete();

            Log::checkBoolAndInsert($image, true, UserController::getUserId(), 'Smazat obrázek', Log::LOG_USER);

            return $image;
        }

        return false;
    }

    /**
     * @return bool|string
     */
    public static function deleteImageByName()
    {
        if (isset($_POST['imageName']) && isset($_POST['type']) && is_a(UserController::isLoggedUser(), User::class)) {

            $imageName = $_POST['imageName'];
            $type = $_POST['type'];

            $image = new ProductImage();
            $image->setName($imageName);

            $deleteResult = $image->deleteByName();
            if($deleteResult == true) {
                unlink(Url::getPathTo('/assets/images/products/' . $imageName));
                if($type == 'main')
                    unlink(Url::getPathTo('/assets/images/products/' . pathinfo($imageName, PATHINFO_FILENAME) . 'main.' . pathinfo($imageName, PATHINFO_EXTENSION)));
            }

            Log::checkBoolAndInsert($deleteResult, true, UserController::getUserId(), 'Smazat obrázek podle jména', Log::LOG_USER);

            return $deleteResult;
        }

        return false;
    }

    /**
     * @return array|bool
     */
    public static function getImage()
    {
        if (isset($_POST['imageId']) && is_a(UserController::isLoggedUser(), User::class)) {
            $imageId = $_POST['imageId'];

            $image = new ProductImage($imageId);
            $image = $image->load();

            return $image->_toArray();
        }

        return false;
    }

    public static function getImages() {
        if (isset($_POST['productId']) && isset($_POST['type']) && is_a(UserController::isLoggedUser(), User::class)) {
            $productId = $_POST['productId'];
            $type = $_POST['type'];

            $images = ProductImage::getProductImages($productId, $type);

            return json_encode($images);
        }

        return false;
    }
}