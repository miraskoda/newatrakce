<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 26.03.2017
 * Time: 15:26
 */

namespace backend\controllers;

use backend\models\Log;
use backend\models\ProductAddition;
use backend\models\ProductData;
use backend\models\Search;
use backend\models\Validate;
use backend\view\ProductGrid;
use backend\models\Product;
use backend\models\User;

class ProductController
{
    const PRODUCT_CELL = 0;
    const PRODUCT_DETAIL = 1;
    const PRODUCT_RAW = 2;

    /**
     * @param int $type
     * @return Product|bool|string|array
     */
    public static function getProduct($type = self::PRODUCT_CELL)
    {
        if(isset($_POST['productId']) && is_a(UserController::isLoggedUser(), User::class)) {
            $productId = $_POST['productId'];
            $product = new Product($productId);
            $product->load();
            if (!is_a($product, Product::class)) {
                return $product;
            }

            if($type == self::PRODUCT_DETAIL)
                return ProductGrid::generateProductDetailCell($product);
            else if($type == self::PRODUCT_RAW)
                return $product->_toArray();
            else
                return ProductGrid::generateProductCell($product);
        }

        return false;
    }

    /**
     * @return array|bool
     */
    public static function getProductsByCategory() {
        if(isset($_POST['categoryId']) && is_a(UserController::isLoggedUser(), User::class)) {
            $categoryId = $_POST['categoryId'];

            $products = Product::getProductsWithParams($categoryId, '', 0,0, true);
            $productsArray = [];
            foreach ($products as $product) {
                $product->setDescription('');
                $product->setKeywords('');
                $productsArray[] = $product->_toArray();
            }

            return $productsArray;
        }

        return false;
    }

    /**
     * @return bool|string
     */
    public static function getProductGridBySearch() {
        if(isset($_POST['search']) && is_a(UserController::isLoggedUser(), User::class)) {
            $search = $_POST['search'];

            if(empty($search))
                $products = Product::getProducts();
            else
                $products = Search::searchProducts($search, false, null, '', 0, 0, true);

            return ProductGrid::generateProductGrid($products);
        }

        return false;
    }

    /**
     * @return Product|bool|string
     */
    public static function createProduct()
    {
        //if values are set and admin user is logged
        if (isset($_POST['name']) && isset($_POST['description']) && isset($_POST['target']) && isset($_POST['price']) && isset($_POST['priceWithVat']) && is_a(UserController::isLoggedUser(), User::class)) {
            $keywords = "";
            $visible = false;
            $quantity = 0;
            if (isset($_POST['keywords']))
                $keywords = $_POST['keywords'];
            if (isset($_POST['visible']))
                $visible = boolval($_POST['visible']);
            if (isset($_POST['quantity']))
                $quantity = $_POST['quantity'];

            $name = $_POST['name'];
            $description = $_POST['description'];
            $target = $_POST['target'];

            $price = $_POST['price'];
            $priceWithVat = $_POST['priceWithVat'];

            $product = new Product();
            $product->setName($name);
            $product->setDescription($description);
            $product->setTarget($target);
            $product->setKeywords($keywords);
            $product->setPrice($price);
            $product->setPriceWithVat($priceWithVat);
            $product->setQuantity($quantity);
            $product->setVisible($visible);

            $product = $product->create();

            Log::checkObjectAndInsert(UserController::getUserId(), $product, Product::class, 'Vytvořit produkt', Log::LOG_USER);

            return $product;
        }

        return false;
    }

    /**
     * @return Product|bool|string
     */
    public static function updateProduct()
    {
        if (isset($_POST['productId']) && isset($_POST['name']) && isset($_POST['description']) && isset($_POST['target']) && isset($_POST['price']) && isset($_POST['priceWithVat']) && is_a(UserController::isLoggedUser(), User::class)) {
            $productId = $_POST['productId'];

            $keywords = "";
            $visible = false;
            $quantity = 0;
            if (isset($_POST['keywords']))
                $keywords = $_POST['keywords'];
            if (isset($_POST['visible']))
                $visible = boolval($_POST['visible']);
            if (isset($_POST['quantity']))
                $quantity = $_POST['quantity'];

            $name = $_POST['name'];
            $description = $_POST['description'];
            $target = $_POST['target'];
            $price = $_POST['price'];
            $priceWithVat = $_POST['priceWithVat'];

            $product = new Product($productId);
            $product->setName($name);
            $product->setDescription($description);
            $product->setTarget($target);
            $product->setKeywords($keywords);
            $product->setPrice($price);
            $product->setPriceWithVat($priceWithVat);
            $product->setQuantity($quantity);
            $product->setVisible($visible);

            $product = $product->update();

            Log::checkObjectAndInsert(UserController::getUserId(), $product, Product::class, 'Akutalizace produktu', Log::LOG_USER);

            return $product;
        }

        return false;
    }

    /**
     * @return Product|bool|string
     */
    public static function deleteProduct()
    {
        if (isset($_POST['productId']) && is_a(UserController::isLoggedUser(), User::class)) {

            $productId = $_POST['productId'];

            $product = new Product($productId);
            $product = $product->load();
            if (!is_a($product, Product::class)) {
                Log::insert(UserController::getUserId(), 'Smazat produkt', Log::LOG_USER, Log::LOG_STATE_ERROR, $product);
                return $product;
            }

            $product = $product->delete();

            Log::checkBoolAndInsert($product, true, UserController::getUserId(), 'Smazání produktu', Log::LOG_USER);

            return $product;
        }

        return false;
    }

    /**
     * @return Product|string
     */
    public static function duplicateProduct() {
        if (isset($_POST['productId']) && is_a(UserController::isLoggedUser(), User::class)) {
            $originalProductId = $_POST['productId'];

            // duplicate product
            $product = new Product($originalProductId);
            $product = $product->load();
            $product->setName('(KOPIE) ' . $product->getName());
            $product->setVisible(false);
            $product = $product->create();

            Log::checkObjectAndInsert(UserController::getUserId(), $product, Product::class, 'Duplikování produktu', Log::LOG_USER, $originalProductId);

            // if successfully created product
            if(is_a($product, Product::class)) {
                // duplicate category
                $productCategories = ProductCategoryController::getProductCategories($originalProductId, false, false);
                if(is_array($productCategories)) {
                    foreach ($productCategories as $productCategory) {
                        // set new product id
                        $productCategory->setProductId($product->getProductId());
                        // and try to create
                        $productCategory->create();
                    }
                } else {
                    return 'Produkt byl úspěšně duplikován, ale nepodařilo se duplikovat kategorie.';
                }

                // duplicate product data
                $productDataPriceIncluded = ProductData::getProductData($originalProductId, ProductData::TYPE_PRICE_INCLUDED, false);
                $productDataTechnical = ProductData::getProductData($originalProductId, ProductData::TYPE_TECHNICAL, false);
                if(is_array($productDataPriceIncluded) && is_array($productDataTechnical)) {
                    $productData = array_merge($productDataPriceIncluded, $productDataTechnical);
                    foreach ($productData as $pieceOfProductData) {
                        // set new product id
                        $pieceOfProductData->setProductId($product->getProductId());
                        // and create new
                        $pieceOfProductData->create();
                    }
                } else {
                    return 'Produkt byl úspěšně duplikován, ale nepodařilo se duplikovat data.';
                }

                // duplicate product additions
                $productAdditions = ProductAddition::getProductAdditionsByMainProductAsArray($originalProductId, false);
                if(is_array($productAdditions)) {
                    foreach ($productAdditions as $addition) {
                        // set new main product id
                        $addition->setMainProductId($product->getProductId());
                        // and create duplicates
                        $addition->create();
                    }
                } else {
                    return 'Produkt byl úspěšně duplikován, ale nepodařilo se duplikovat doplňky.';
                }
            }

            return $product;
        }
        return false;
    }

    /**
     * @return Product|string
     */
    public static function loadProduct() {
        if (isset($_GET['nameSlug'])) {
            $nameSlug = $_GET['nameSlug'];

            $product = new Product();
            $product->setNameSlug($nameSlug);
            $product = $product->load();

            return $product;
        }
    }

    /**
     * @return string
     */
    public static function checkProductAvailability() {
        if(isset($_POST['date']) && isset($_POST['hours']) && isset($_POST['productId'])) {
            $productId = $_POST['productId'];
            $date = $_POST['date'];
            $hours = $_POST['hours'];

            $validate = new Validate();
            $validate->validateNotNull([
                'číslo produktu' => $productId,
                'datum' => $date,
                'hodiny' => $hours
            ]);
            $validate->validateNumeric([
                'číslo produktu' => $productId,
                'hodiny' => $hours
            ]);
            if(!$validate->isValidationResult())
                return json_encode($validate->getValidationSummary());

            if($hours > 48)
                $hours = 48;

            $product = new Product($productId);
            $product = $product->load();
            if(is_a($product, Product::class)) {
                // already json encoded
                return $product->getProductAvailability(1, $date, $hours);
            } else {
                json_encode($product);
            }
        }
    }

    /**
     * @return string
     */
    public static function getAvailableProducts() {
        if(isset($_POST['date']) && isset($_POST['hours'])) {
            $date = $_POST['date'];
            $hours = $_POST['hours'];

            $validate = new Validate();
            $validate->validateNotNull([
                'datum' => $date,
                'hodiny' => $hours
            ]);
            $validate->validateNumeric([
                'hodiny' => $hours
            ]);
            if(!$validate->isValidationResult())
                return json_encode($validate->getValidationSummary());

            if($hours > 48)
                $hours = 48;

            $availableProducts = Product::getAvailableProducts($date, $hours, true);

            return json_encode($availableProducts);
        }
    }
}