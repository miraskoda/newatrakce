<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 20.03.2018
 * Time: 1:39
 */

return (object) array(
    'merchantId'      => 'A3227vtaeb',
    'publicKey'  => 'GatewayKeys/mips_iplatebnibrana.csob.cz.pub',
    'privateKey'  => 'rsa_A3227vtaeb.key',
    'privateKeyPassword' => null,
    'url' => 'https://iapi.iplatebnibrana.csob.cz/api/v1.7'
);