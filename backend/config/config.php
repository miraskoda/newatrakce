<?php
/**
 * Created by PhpStorm.
 * User: Ondra
 * Date: 27.02.2017
 * Time: 21:58
 */

// switching between localhost and web database
$local = array(
    'host'      => 'localhost',//'wm145.wedos.net',
    'username'  => 'root',//'w160030_sql',
    'password'  => '',//'TFTFtAU5',
    'database'  => 'd160030_sql',//'d160030_sql',
    'localRoot' => '/Applications/XAMPP/xamppfiles/htdocs',//'/data/web/virtuals/160030/virtual/www',
    'domain'    => 'localhost',//'160030.w30.wedos.ws',
    'protocol'  => 'http',
);


$web = array(
    'host'      => 'wm145.wedos.net',
    'username'  => 'w160030_sql',
    'password'  => 'TFTFtAU5',
    'database'  => 'd160030_sql',
    'localRoot' => '/data/web/virtuals/160030/virtual/www',
    'domain'    => 'onlineatrakce.cz',
    'protocol'  => 'https',
);



$whitelist = array(
    'onlineatrakce.cz',
    'beta.onlineatrakce.cz',
    'www.onlineatrakce.cz',
    'www.beta.onlineatrakce.cz'
);

if(in_array($_SERVER['HTTP_HOST'], $whitelist)){

    return (object) $web;

}else{

    return (object) $local;

}

