<?php
/**
 * Created by PhpStorm.
 * User: adamkvasnicka
 * Date: 25/01/2019
 * Time: 21:09
 */

class Connector
{

    private $refreshToken;
    private $accessToken;
    private $clientSecret;
    private $clientId;
    private $id;




    //OLD test account
    //Api key: l7xxca9bf8cbca944aaab3c9a2ccc95dc80a
    // Api secret key: 763517908da3417781a76d63816dd2ba
    //
    //web app name: onlineatrakce
    //
    //user name: Josef
    //
    //email: josef@pronajematrakce.cz

    public function __construct($clientSecret = "", $clientId = "", $refreshToken = "", $accessToken = "")
    {
        $this->accessToken = $accessToken;
        $this->refreshToken = $refreshToken;
        $this->clientSecret = $clientSecret;
        $this->clientId = $clientId;


    }

    public function registerApp()
    {
        $url = "https://api.csob.cz/api/csob/psd2/oauth2/register";
        $content = json_encode('
        {
            "application_type": "web",
            "client_name": "josefrybar",
            "client_name#en-US": "josefrybar",
            "contact": "[josef@pronajematrakce.cz]",
            "redirect_uris": ["https://www.onlineatrakce.cz"],
            "scopes": [
                "AISP",
                "PISP"
            ]
        }
        ');
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array(
                "APIKEY: l7xxf4764b2b623d433097f02af422a5d984",
                "Cache-Control: no-cache",
                "Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

        $json_response = curl_exec($curl);

        curl_close($curl);

       $response = json_decode($json_response, true);

       $this->setClientSecret($response["client_secret"]);
       $this->setClientId($response["client_id"]);

    }

    public function getTokens()
    {

        $url = "https://api.csob.cz/api/csob/psd2/oauth2/token";
        $content =
       "grant_type=authorization_code
        &code=#
        &client_id=" . $this -> getClientId() . "
        &client_secret= " . $this -> getClientSecret() . "
        &redirect_uri=https%3A%2F%2Fonlineatrakce.cz%2F";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array("APIKEY: l7xxf4764b2b623d433097f02af422a5d984",
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded"
            )
        );
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

        $json_response = curl_exec($curl);

        curl_close($curl);

        $response = json_decode($json_response, true);

        $this->setAccessToken($response["access_token"]);
        $this->setRefreshToken($response["refresh_token"]);
    }

    public function getAccountId()
    {

        $url = "https://api.csob.cz/api/csob/psd2/v1/my/accounts";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array("APIKEY: l7xxf4764b2b623d433097f02af422a5d984",
                "Authorization: Bearer 3/FPLYXU5t3muiv1DiOYWRiAZj9DXM3hYbwh2u4miKG7gg1aUPpWsVP5Nkrv9W8Zdn",
                "Cache-Control: no-cache",
                "Content-type: application/json")
        );


        $json_response = curl_exec($curl);

        curl_close($curl);

        $response = json_decode($json_response, true);

        //TODO only first list of accounts id using
        $this ->setId($response["accounts"][1]["id"]);
    }

    /**
     * @return array
     */
    public function getListTransactions()
    {
        require "Payment.class.php";

        $url = "https://api.csob.cz/api/csob/psd2/v1/my/accounts/" .$this->getId(). "/transactions" ;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array(
                "APIKEY: l7xxf4764b2b623d433097f02af422a5d984",
                "Authorization: Bearer 3/LSNXm1tXISLOSxJ0C7hsAV8hZiS9Rd5DCTY74XyQmnjxJzyIX3kLhX7lwhKaalGo",
                "Cache-Control: no-cache",
                "Content-type: application/json"));


        $json_response = curl_exec($curl);

        curl_close($curl);

        $list = [];
        foreach (json_decode($json_response, true)["transactions"] as $data) {
            $list[] = new Payment(
                $data['amount']["value"],
                $data["entryDetails"]["transactionDetails"]["additionalTransactionInformation"],
                $data['valueDate']["date"],
                $this->getFormatedVS($data["entryDetails"]["transactionDetails"]["remittanceInformation"]["structured"]["creditorReferenceInformation"]["reference"][2]),
                $this->getFormatedVS($data["entryDetails"]["transactionDetails"]["remittanceInformation"]["structured"]["creditorReferenceInformation"]["reference"][0]),
                $this->getFormatedVS($data["entryDetails"]["transactionDetails"]["remittanceInformation"]["structured"]["creditorReferenceInformation"]["reference"][1]),
                $data["entryDetails"]["transactionDetails"]["relatedParties"]["debtorAccount"]["identification"]["other"]["identification"],
                $data["entryDetails"]["transactionDetails"]["relatedAgents"]["debtorAgent"]["financialInstitutionIdentification"]["clearingSystemMemberIdentification"]["memberIdentification"],
                $data["entryDetails"]["transactionDetails"]["relatedParties"]["debtor"]["name"]
            );
        }
        return $list;
    }

    /**
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }



    private function getFormatedVS($string){
        return substr($string,3);
    }

    /**
     * @param string $refreshToken
     */
    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return string
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * @param string $clientSecret
     */
    public function setClientSecret($clientSecret)
    {
        $this->clientSecret = $clientSecret;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

}