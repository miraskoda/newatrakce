<?php

use backend\models\Order;

/**
 * Created by PhpStorm.
 * User: miroslavskoda
 * Date: 23/01/2019
 * Time: 19:00
 */

class CebController
{

    static public function checkPayment()
    {
        // tahle metoda je volana cronem
        // Tady se bude posilat ten request
        // Pokud bude kontrola uspesna

        // Foreach kazdou nezaplacenou platbu a zaslat request na ceb jestli je uz zaplacena
        // Kdyz nebude tak ji zkontrolovat jestli neni dlouze nezaplacena podle nehaky casovy konstanty
        // pokud ne tak zkontrolovat podle checkPayment

        // return list of all finished orders waiting to been paid
        //ceb connection routine
        require "Connector.php";
        $con = new Connector();
        $con -> registerApp();
        $con -> getTokens();
        $con -> getAccountId();

        //foreach of all requested transactions
        $listTrans = $con -> getListTransactions();

        $orders = Order::getOrders(true);

        foreach($orders as $order){
            foreach($listTrans as $tran){

                if(Order::isCorrectAmount($tran -> getAmount(),$order -> getProductPrice())){
                    
                    // mark order as finished and paid, parameter invoice num
                    //send succesfully email of confirmation of getting paid
                    Order::setOrderStateByDocument($tran -> getVariableSymbol());

                }else{
                    return "Částka nesouhlasí";
                }
            }
        }


    }



}