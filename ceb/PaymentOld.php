<?php
/**
 * Created by PhpStorm.
 * User: miroslavskoda
 * Date: 23/01/2019
 * Time: 19:00
 */

class PaymentOld
{
    const SCENARIO_CREATE = 1;
    const SCENARIO_UPDATE = 2;
    const SCENARIO_LOAD = 4;

    private $db;
    private $scenario;

    private $paymentTypeId;

    /**
     * PaymentType.class constructor.
     *
     * @param int $paymentTypeId
     */
    public function __construct($paymentTypeId = 0)
    {
        $this->db = MyDB::getConnection();
        $this->paymentTypeId = $paymentTypeId;
    }

    /**
     * @return PaymentType|string
     */
    public function load()
    {
        $this->scenario = self::SCENARIO_LOAD;

        if ($this->validate()) {
            $paymentTypeData = $this->db->queryOne("SELECT * FROM payment_type WHERE paymentTypeId = ?", [
                $this->paymentTypeId
            ]);

            if (!(new Validate())->validateNotNull([$paymentTypeData]))
                return DatabaseError::getErrorDescription(DatabaseError::QUERY_ONE, 'Nebylo možné načíst typ platby z DB');

            return $this;
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    public function update()
    {
        $this->scenario = self::SCENARIO_UPDATE;

        if ($this->validate()) {
            $update = $this->db->query("UPDATE payment_type SET WHERE paymentTypeId = ?", []);
            if ($update > 0) {
                return $this;
            } else {
                return DatabaseError::getErrorDescription(DatabaseError::UPDATE);
            }
        } else {
            return Validate::getValidationSummaryText();
        }
    }

    /**
     * @return bool
     */
    public function validate()
    {
        $validation = new Validate();

        if ($this->scenario == self::SCENARIO_UPDATE || $this->scenario == self::SCENARIO_LOAD) {
            $validation->validateRequired([
                'paymentTypeId' => $this->paymentTypeId,
            ]);
            $validation->validateNumeric([
                'paymentTypeId' => $this->paymentTypeId,
            ]);
        }

        if ($this->scenario == self::SCENARIO_UPDATE) {
            $validation->validateRequired([]);
        }

        if (!$validation->isValidationResult()) {
            unset($_SESSION['validationSummary']);
            $_SESSION['validationSummary'] = $validation->getValidationSummary();
            return false;
        } else {
            return true;
        }
    }

    public function setAsPaid()
    {
    }

    public function cancelPayment()
    {
    }

    // Jednoduche SQL dotazy na nezaplacene platby
    static public function getUnpaidPayments()
    {
        return 'Pole nezaplacenych instanci plateb';
    }

}