<?php
/**
 * Created by PhpStorm.
 * User: miroslavskoda
 * Date: 12/02/2019
 * Time: 14:09
 */

class Payment
{

    private $amount;
    private $additionalTransactionInformation;
    private $date;
    private $specificSymbol;
    private $variableSymbol;
    private $constantSymbol;
    private $debtorAccount;
    private $debtorAgent;
    private $debtorName;


    /**
     * Payment constructor.
     * @param $amount
     * @param $additionalTransactionInformation
     * @param $date
     * @param $specificSymbol
     * @param $variableSymbol
     * @param $constantSymbol
     * @param $debtorAccount
     * @param $debtorAgent
     * @param $debtorName
     */
    public function __construct($amount, $additionalTransactionInformation, $date, $specificSymbol, $variableSymbol, $constantSymbol, $debtorAccount, $debtorAgent, $debtorName)
    {
        $this->amount = $amount;
        $this->additionalTransactionInformation = $additionalTransactionInformation;
        $this->date = $date;
        $this->specificSymbol = $specificSymbol;
        $this->variableSymbol = $variableSymbol;
        $this->constantSymbol = $constantSymbol;
        $this->debtorAccount = $debtorAccount;
        $this->debtorAgent = $debtorAgent;
        $this->debtorName = $debtorName;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getAdditionalTransactionInformation()
    {
        return $this->additionalTransactionInformation;
    }

    /**
     * @param mixed $additionalTransactionInformation
     */
    public function setAdditionalTransactionInformation($additionalTransactionInformation)
    {
        $this->additionalTransactionInformation = $additionalTransactionInformation;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getSpecificSymbol()
    {
        return $this->specificSymbol;
    }

    /**
     * @param mixed $specificSymbol
     */
    public function setSpecificSymbol($specificSymbol)
    {
        $this->specificSymbol = $specificSymbol;
    }

    /**
     * @return mixed
     */
    public function getVariableSymbol()
    {
        return $this->variableSymbol;
    }

    /**
     * @param mixed $variableSymbol
     */
    public function setVariableSymbol($variableSymbol)
    {
        $this->variableSymbol = $variableSymbol;
    }

    /**
     * @return mixed
     */
    public function getConstantSymbol()
    {
        return $this->constantSymbol;
    }

    /**
     * @param mixed $constantSymbol
     */
    public function setConstantSymbol($constantSymbol)
    {
        $this->constantSymbol = $constantSymbol;
    }

    /**
     * @return mixed
     */
    public function getDebtorAccount()
    {
        return $this->debtorAccount;
    }

    /**
     * @param mixed $debtorAccount
     */
    public function setDebtorAccount($debtorAccount)
    {
        $this->debtorAccount = $debtorAccount;
    }

    /**
     * @return mixed
     */
    public function getDebtorAgent()
    {
        return $this->debtorAgent;
    }

    /**
     * @param mixed $debtorAgent
     */
    public function setDebtorAgent($debtorAgent)
    {
        $this->debtorAgent = $debtorAgent;
    }

    /**
     * @return mixed
     */
    public function getDebtorName()
    {
        return $this->debtorName;
    }

    /**
     * @param mixed $debtorName
     */
    public function setDebtorName($debtorName)
    {
        $this->debtorName = $debtorName;
    }
}