<?php
/**
 * Main page
 */

use backend\controllers\CustomerController;
use backend\models\MailHelper;
use backend\models\Order;
use backend\models\Url;



$pageId = 1;

require_once __DIR__ . "/backend/modules/app/prepare.php";

// customer load
$customer = CustomerController::isLoggedCustomer();

// head

require_once Url::getBackendPathTo("/modules/page-parts/head.php");

?>



<body id="public">




	<?php

	// public header - categories, pages, cart (search)
    require_once Url::getBackendPathTo("/modules/page-parts/header.php");

    echo '<div class="container-fluid">';


    require_once Url::getBackendPathTo("/modules/page-parts/main-page/newCarousel.php");
        require_once Url::getBackendPathTo("/modules/page-parts/main-page/favourite.php");
        echo "<hr class='divider'>";
        require_once Url::getBackendPathTo("/modules/page-parts/main-page/promo-text.php");
        echo "<hr class='divider'>";

    require_once Url::getBackendPathTo("/modules/page-parts/editable-content.php");

echo '<br>';

    require_once Url::getBackendPathTo("/modules/page-parts/footer.php");

    echo '</div>';
    echo '<div id="overlay"></div>';

    echo '<script>';
        require_once Url::getPathTo("/js/cart/add-to-cart.js");
        require_once Url::getPathTo("/js/move-to-more.min.js");
    require_once Url::getPathTo("/js/save-editable-content.min.js");

    echo '</script>';
    require_once Url::getBackendPathTo("/modules/page-parts/js-footer.php");

    // modals
    require_once Url::getBackendPathTo("/modules/modals/cart-action.php");
    require_once Url::getBackendPathTo("/modules/modals/reservation.php");
    require_once Url::getBackendPathTo("/modules/modals/login-modal.php");
    require_once Url::getBackendPathTo("/modules/modals/reservation-success.php");
    require_once Url::getBackendPathTo("/modules/modals/user-not-logged.php");
    //var_dump($time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);



if($_GET["bank"]==1){

    require "ceb/CebController.php";

    CebController::checkPayment();

}



    ?>
    <script>
        $(document).ready(function(){
            $(window).scroll(function(){
                if ($(this).scrollTop() > 100) {
                    $('#scroll').fadeIn();
                } else {
                    $('#scroll').fadeOut();
                }
            });
            $('#scroll').click(function(){
                $("html, body").animate({ scrollTop: 0 }, 600);
                return false;
            });
        });
    </script>
</body>
</html>